// eslint-disable-next-line @typescript-eslint/no-unused-vars
module.exports = function (options, webpack) {
  options.module.rules.forEach((rule) => {
    rule.use?.forEach((use) => {
      if (use.loader === 'ts-loader') {
        use.loader = 'swc-loader';
        delete use.options;
      }
    });
  });

  options.plugins = options.plugins.filter(
    (plugin) =>
      Object.getPrototypeOf(plugin).constructor.name !==
      'ForkTsCheckerWebpackPlugin',
  );

  return {
    ...options,
    devtool: 'source-map',
    mode: 'production',
    optimization: {
      minimizer: [
        {
          apply: (compiler) => {
            // Lazy load the Terser plugin
            // eslint-disable-next-line @typescript-eslint/no-var-requires
            const TerserPlugin = require('terser-webpack-plugin');
            new TerserPlugin({
              terserOptions: {
                compress: {
                  passes: 2,
                },
                keep_classnames: true,
                keep_fnames: true,
              },
            }).apply(compiler);
          },
        },
      ],
    },
  };
};
