-- CreateTable
CREATE TABLE "MessagePayload" (
    "id" TEXT NOT NULL,
    "payload" JSONB NOT NULL,

    CONSTRAINT "MessagePayload_pkey" PRIMARY KEY ("id")
);
