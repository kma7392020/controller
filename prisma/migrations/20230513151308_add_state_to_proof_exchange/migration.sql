-- CreateEnum
CREATE TYPE "ProofExchangeVerifyStatus" AS ENUM ('WAITING', 'VERIFIED', 'REJECTED');

-- AlterEnum
ALTER TYPE "ProofExchangeState" ADD VALUE 'PROOF_SENT';

-- AlterTable
ALTER TABLE "ProofExchange" ADD COLUMN     "verifyStatus" "ProofExchangeVerifyStatus" NOT NULL DEFAULT 'WAITING';
