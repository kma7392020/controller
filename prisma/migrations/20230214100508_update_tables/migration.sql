/*
  Warnings:

  - Added the required column `proverWalletId` to the `ProofExchange` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE "CredentialExchange" DROP CONSTRAINT "CredentialExchange_issuerWalletId_fkey";

-- DropForeignKey
ALTER TABLE "ProofExchange" DROP CONSTRAINT "ProofExchange_verifierWalletId_fkey";

-- AlterTable
ALTER TABLE "ProofExchange" ADD COLUMN     "proverWalletId" TEXT NOT NULL;
