/*
  Warnings:

  - A unique constraint covering the columns `[verifierPresExId]` on the table `ProofExchange` will be added. If there are existing duplicate values, this will fail.

*/
-- AlterTable
ALTER TABLE "ProofExchange" ADD COLUMN     "verifierPresExId" TEXT;

-- CreateIndex
CREATE UNIQUE INDEX "ProofExchange_verifierPresExId_key" ON "ProofExchange"("verifierPresExId");
