/*
  Warnings:

  - The values [VERIFIED,REJECTED] on the enum `ProofExchangeState` will be removed. If these variants are still used in the database, this will fail.

*/
-- AlterEnum
BEGIN;
CREATE TYPE "ProofExchangeState_new" AS ENUM ('WAITING', 'REQUEST_SENT', 'REQUEST_RECEIVED', 'PROOF_SENT');
ALTER TABLE "ProofExchange" ALTER COLUMN "state" DROP DEFAULT;
ALTER TABLE "ProofExchange" ALTER COLUMN "state" TYPE "ProofExchangeState_new" USING ("state"::text::"ProofExchangeState_new");
ALTER TYPE "ProofExchangeState" RENAME TO "ProofExchangeState_old";
ALTER TYPE "ProofExchangeState_new" RENAME TO "ProofExchangeState";
DROP TYPE "ProofExchangeState_old";
ALTER TABLE "ProofExchange" ALTER COLUMN "state" SET DEFAULT 'WAITING';
COMMIT;
