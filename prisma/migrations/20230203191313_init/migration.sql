-- CreateTable
CREATE TABLE "Wallet" (
    "id" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "email" TEXT NOT NULL,

    CONSTRAINT "Wallet_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "IssueCredentialExchange" (
    "id" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "invitationUrl" TEXT NOT NULL,
    "connectionId" TEXT NOT NULL,
    "issuerWalletId" TEXT NOT NULL,
    "credential" JSONB NOT NULL,

    CONSTRAINT "IssueCredentialExchange_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "PresentProofExchange" (
    "id" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "invitationUrl" TEXT NOT NULL,
    "connectionId" TEXT NOT NULL,
    "verifierWalletId" TEXT NOT NULL,
    "request" JSONB NOT NULL,

    CONSTRAINT "PresentProofExchange_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "Wallet_email_key" ON "Wallet"("email");

-- AddForeignKey
ALTER TABLE "IssueCredentialExchange" ADD CONSTRAINT "IssueCredentialExchange_issuerWalletId_fkey" FOREIGN KEY ("issuerWalletId") REFERENCES "Wallet"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "PresentProofExchange" ADD CONSTRAINT "PresentProofExchange_verifierWalletId_fkey" FOREIGN KEY ("verifierWalletId") REFERENCES "Wallet"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
