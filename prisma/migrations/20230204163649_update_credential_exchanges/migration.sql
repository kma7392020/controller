/*
  Warnings:

  - Made the column `issuerConnectionId` on table `CredentialExchange` required. This step will fail if there are existing NULL values in that column.

*/
-- AlterTable
ALTER TABLE "CredentialExchange" ADD COLUMN     "holderConnectionId" TEXT,
ALTER COLUMN "issuerConnectionId" SET NOT NULL;
