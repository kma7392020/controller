-- CreateTable
CREATE TABLE "Device" (
    "token" TEXT NOT NULL,
    "walletId" TEXT NOT NULL,

    CONSTRAINT "Device_pkey" PRIMARY KEY ("token")
);

-- AddForeignKey
ALTER TABLE "Device" ADD CONSTRAINT "Device_walletId_fkey" FOREIGN KEY ("walletId") REFERENCES "Wallet"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
