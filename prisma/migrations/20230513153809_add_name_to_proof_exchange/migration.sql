/*
  Warnings:

  - A unique constraint covering the columns `[name]` on the table `ProofExchange` will be added. If there are existing duplicate values, this will fail.
  - Added the required column `name` to the `ProofExchange` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "ProofExchange" ADD COLUMN     "name" TEXT NOT NULL;

-- CreateIndex
CREATE UNIQUE INDEX "ProofExchange_name_key" ON "ProofExchange"("name");
