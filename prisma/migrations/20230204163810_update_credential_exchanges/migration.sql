/*
  Warnings:

  - You are about to drop the column `holderConnectionId` on the `CredentialExchange` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "CredentialExchange" DROP COLUMN "holderConnectionId",
ALTER COLUMN "issuerConnectionId" DROP NOT NULL;
