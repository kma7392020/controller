/*
  Warnings:

  - A unique constraint covering the columns `[verifierConnectionId]` on the table `ProofExchange` will be added. If there are existing duplicate values, this will fail.

*/
-- CreateIndex
CREATE UNIQUE INDEX "ProofExchange_verifierConnectionId_key" ON "ProofExchange"("verifierConnectionId");
