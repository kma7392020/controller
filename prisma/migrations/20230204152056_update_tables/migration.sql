/*
  Warnings:

  - You are about to drop the `IssueCredentialExchange` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `PresentProofExchange` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE "IssueCredentialExchange" DROP CONSTRAINT "IssueCredentialExchange_issuerWalletId_fkey";

-- DropForeignKey
ALTER TABLE "PresentProofExchange" DROP CONSTRAINT "PresentProofExchange_verifierWalletId_fkey";

-- DropTable
DROP TABLE "IssueCredentialExchange";

-- DropTable
DROP TABLE "PresentProofExchange";

-- CreateTable
CREATE TABLE "CredentialExchange" (
    "id" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "issuerWalletId" TEXT NOT NULL,
    "credential" JSONB NOT NULL,

    CONSTRAINT "CredentialExchange_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "ProofExchange" (
    "id" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "verifierWalletId" TEXT NOT NULL,
    "request" JSONB NOT NULL,

    CONSTRAINT "ProofExchange_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "CredentialExchange" ADD CONSTRAINT "CredentialExchange_issuerWalletId_fkey" FOREIGN KEY ("issuerWalletId") REFERENCES "Wallet"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "ProofExchange" ADD CONSTRAINT "ProofExchange_verifierWalletId_fkey" FOREIGN KEY ("verifierWalletId") REFERENCES "Wallet"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
