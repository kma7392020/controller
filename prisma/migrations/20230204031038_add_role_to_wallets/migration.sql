-- CreateEnum
CREATE TYPE "WalletRole" AS ENUM ('ISSUER', 'VERIFIER', 'HOLDER');

-- AlterTable
ALTER TABLE "Wallet" ADD COLUMN     "role" "WalletRole" NOT NULL DEFAULT 'HOLDER';
