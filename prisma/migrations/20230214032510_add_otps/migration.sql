-- CreateEnum
CREATE TYPE "OtpAlgorithm" AS ENUM ('HOTP', 'TOTP');

-- CreateTable
CREATE TABLE "Otp" (
    "id" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "algorithm" "OtpAlgorithm" NOT NULL,
    "secret" TEXT NOT NULL,
    "digits" INTEGER NOT NULL,
    "issuer" TEXT NOT NULL,
    "walletId" TEXT NOT NULL,

    CONSTRAINT "Otp_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "Otp" ADD CONSTRAINT "Otp_walletId_fkey" FOREIGN KEY ("walletId") REFERENCES "Wallet"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
