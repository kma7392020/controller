/*
  Warnings:

  - You are about to drop the column `presExId` on the `ProofExchange` table. All the data in the column will be lost.
  - A unique constraint covering the columns `[proverPresExId]` on the table `ProofExchange` will be added. If there are existing duplicate values, this will fail.

*/
-- DropIndex
DROP INDEX "ProofExchange_presExId_key";

-- AlterTable
ALTER TABLE "ProofExchange" DROP COLUMN "presExId",
ADD COLUMN     "proverPresExId" TEXT;

-- CreateIndex
CREATE UNIQUE INDEX "ProofExchange_proverPresExId_key" ON "ProofExchange"("proverPresExId");
