/*
  Warnings:

  - You are about to drop the column `invitationUrl` on the `IssueCredentialExchange` table. All the data in the column will be lost.
  - Added the required column `invitation` to the `IssueCredentialExchange` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "IssueCredentialExchange" DROP COLUMN "invitationUrl",
ADD COLUMN     "invitation" JSONB NOT NULL;
