/*
  Warnings:

  - You are about to drop the column `reqAttributes` on the `ProofExchange` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "ProofExchange" DROP COLUMN "reqAttributes",
ADD COLUMN     "presentationAttributes" JSONB NOT NULL DEFAULT '[]';
