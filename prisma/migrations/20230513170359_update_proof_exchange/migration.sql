/*
  Warnings:

  - Added the required column `deviceId` to the `ProofExchange` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "ProofExchange" ADD COLUMN     "deviceId" TEXT NOT NULL;
