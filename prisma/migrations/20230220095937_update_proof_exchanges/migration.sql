/*
  Warnings:

  - You are about to drop the column `name` on the `ProofExchange` table. All the data in the column will be lost.
  - You are about to drop the column `requestedAttributes` on the `ProofExchange` table. All the data in the column will be lost.
  - You are about to drop the column `sent` on the `ProofExchange` table. All the data in the column will be lost.
  - A unique constraint covering the columns `[presExId]` on the table `ProofExchange` will be added. If there are existing duplicate values, this will fail.
  - Added the required column `presRequest` to the `ProofExchange` table without a default value. This is not possible if the table is not empty.
  - Made the column `accessToken` on table `Wallet` required. This step will fail if there are existing NULL values in that column.

*/
-- AlterTable
ALTER TABLE "ProofExchange" DROP COLUMN "name",
DROP COLUMN "requestedAttributes",
DROP COLUMN "sent",
ADD COLUMN     "presExId" TEXT,
ADD COLUMN     "presRequest" JSONB NOT NULL;

-- AlterTable
ALTER TABLE "Wallet" ALTER COLUMN "accessToken" SET NOT NULL;

-- CreateIndex
CREATE UNIQUE INDEX "ProofExchange_presExId_key" ON "ProofExchange"("presExId");
