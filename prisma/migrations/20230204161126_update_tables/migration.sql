/*
  Warnings:

  - You are about to drop the column `credential` on the `CredentialExchange` table. All the data in the column will be lost.
  - Added the required column `attributes` to the `CredentialExchange` table without a default value. This is not possible if the table is not empty.
  - Added the required column `indyFilter` to the `CredentialExchange` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "CredentialExchange" DROP COLUMN "credential",
ADD COLUMN     "attributes" JSONB NOT NULL,
ADD COLUMN     "indyFilter" JSONB NOT NULL;
