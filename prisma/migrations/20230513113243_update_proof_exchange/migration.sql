/*
  Warnings:

  - You are about to drop the column `done` on the `ProofExchange` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "ProofExchange" DROP COLUMN "done";
