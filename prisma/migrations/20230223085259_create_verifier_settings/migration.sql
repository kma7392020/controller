-- CreateTable
CREATE TABLE "VerifierSetting" (
    "id" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "verifierWalletId" TEXT NOT NULL,
    "proofRequestSetting" JSONB NOT NULL,

    CONSTRAINT "VerifierSetting_pkey" PRIMARY KEY ("id")
);
