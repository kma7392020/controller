/*
  Warnings:

  - A unique constraint covering the columns `[proverConnectionId]` on the table `ProofExchange` will be added. If there are existing duplicate values, this will fail.

*/
-- AlterTable
ALTER TABLE "ProofExchange" ADD COLUMN     "proverConnectionId" TEXT;

-- CreateIndex
CREATE UNIQUE INDEX "ProofExchange_proverConnectionId_key" ON "ProofExchange"("proverConnectionId");
