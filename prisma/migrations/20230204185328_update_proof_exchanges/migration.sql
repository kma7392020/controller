/*
  Warnings:

  - You are about to drop the column `request` on the `ProofExchange` table. All the data in the column will be lost.
  - Added the required column `name` to the `ProofExchange` table without a default value. This is not possible if the table is not empty.
  - Added the required column `requestedAttributes` to the `ProofExchange` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "ProofExchange" DROP COLUMN "request",
ADD COLUMN     "name" TEXT NOT NULL,
ADD COLUMN     "requestedAttributes" JSONB NOT NULL,
ADD COLUMN     "verifierConnectionId" TEXT;
