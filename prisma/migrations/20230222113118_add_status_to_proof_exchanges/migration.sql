-- CreateEnum
CREATE TYPE "ProofExchangeState" AS ENUM ('WAITING', 'REQUEST_SENT', 'REQUEST_RECEIVED');

-- AlterTable
ALTER TABLE "ProofExchange" ADD COLUMN     "state" "ProofExchangeState" NOT NULL DEFAULT 'WAITING';
