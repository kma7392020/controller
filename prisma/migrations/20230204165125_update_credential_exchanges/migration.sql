/*
  Warnings:

  - A unique constraint covering the columns `[issuerConnectionId]` on the table `CredentialExchange` will be added. If there are existing duplicate values, this will fail.

*/
-- CreateIndex
CREATE UNIQUE INDEX "CredentialExchange_issuerConnectionId_key" ON "CredentialExchange"("issuerConnectionId");
