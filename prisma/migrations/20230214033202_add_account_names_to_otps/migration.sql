/*
  Warnings:

  - Added the required column `accountName` to the `Otp` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "Otp" ADD COLUMN     "accountName" TEXT NOT NULL;
