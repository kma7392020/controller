// eslint-disable-next-line @typescript-eslint/no-unused-vars
module.exports = function (options, webpack) {
  options.module.rules.forEach((rule) => {
    rule.use?.forEach((use) => {
      if (use.loader === 'ts-loader') {
        use.loader = 'swc-loader';
        delete use.options;
      }
    });
  });

  return {``
    ...options,
    mode: 'development',
  };
};
