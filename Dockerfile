FROM node:lts AS base

FROM base AS dev-deps
WORKDIR /app
RUN npm install -g pnpm
COPY pnpm-lock.yaml  ./
RUN pnpm fetch

COPY package.json ./
RUN pnpm install --frozen-lockfile --offline

COPY prisma prisma
RUN pnpm prisma generate

FROM dev-deps AS builder
WORKDIR /app

COPY .swcrc webpack.build.config.js nest-cli.json tsconfig.build.json tsconfig.json ./
COPY src src
RUN pnpm typecheck && pnpm build

FROM dev-deps AS prod-deps
RUN pnpm prune --prod

FROM base
WORKDIR /app

COPY --from=prod-deps /app/node_modules /app/node_modules
COPY --from=builder /app/dist /app/dist

CMD ["node", "--enable-source-maps", "dist/main.js"]
