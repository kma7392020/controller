/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

export interface AMLRecord {
  aml?: Record<string, string>;
  amlContext?: string;
  version?: string;
}

export interface ActionMenuFetchResult {
  /** Action menu */
  result?: Menu;
}

export type ActionMenuModulesResult = object;

export interface AdminConfig {
  /** Configuration settings */
  config?: object;
}

export interface AdminMediationDeny {
  /** List of mediator rules for recipient */
  mediator_terms?: string[];
  /** List of recipient rules for mediation */
  recipient_terms?: string[];
}

export interface AdminModules {
  /** List of admin modules */
  result?: string[];
}

export type AdminReset = object;

export type AdminShutdown = object;

export interface AdminStatus {
  /** Conductor statistics */
  conductor?: object;
  /** Default label */
  label?: string | null;
  /** Timing results */
  timing?: object;
  /** Version code */
  version?: string;
}

export interface AdminStatusLiveliness {
  /**
   * Liveliness status
   * @example true
   */
  alive?: boolean;
}

export interface AdminStatusReadiness {
  /**
   * Readiness status
   * @example true
   */
  ready?: boolean;
}

export interface AttachDecorator {
  /**
   * Attachment identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  '@id'?: string;
  /**
   * Byte count of data included by reference
   * @format int32
   * @example 1234
   */
  byte_count?: number;
  data: AttachDecoratorData;
  /**
   * Human-readable description of content
   * @example "view from doorway, facing east, with lights off"
   */
  description?: string;
  /**
   * File name
   * @example "IMG1092348.png"
   */
  filename?: string;
  /**
   * Hint regarding last modification datetime, in ISO-8601 format
   * @pattern ^\d{4}-\d\d-\d\d[T ]\d\d:\d\d(?:\:(?:\d\d(?:\.\d{1,6})?))?(?:[+-]\d\d:?\d\d|Z|)$
   * @example "2021-12-31T23:59:59Z"
   */
  lastmod_time?: string;
  /**
   * MIME type
   * @example "image/png"
   */
  'mime-type'?: string;
}

export interface AttachDecoratorData {
  /**
   * Base64-encoded data
   * @pattern ^[a-zA-Z0-9+/]*={0,2}$
   * @example "ey4uLn0="
   */
  base64?: string;
  /**
   * JSON-serialized data
   * @example "{"sample": "content"}"
   */
  json?: any;
  /** Detached Java Web Signature */
  jws?: AttachDecoratorDataJWS;
  /** List of hypertext links to data */
  links?: string[];
  /**
   * SHA256 hash (binhex encoded) of content
   * @pattern ^[a-fA-F0-9+/]{64}$
   * @example "617a48c7c8afe0521efdc03e5bb0ad9e655893e6b4b51f0e794d70fba132aacb"
   */
  sha256?: string;
}

export interface AttachDecoratorData1JWS {
  header: AttachDecoratorDataJWSHeader;
  /**
   * protected JWS header
   * @pattern ^[-_a-zA-Z0-9]*$
   * @example "ey4uLn0"
   */
  protected?: string;
  /**
   * signature
   * @pattern ^[-_a-zA-Z0-9]*$
   * @example "ey4uLn0"
   */
  signature: string;
}

export interface AttachDecoratorDataJWS {
  header?: AttachDecoratorDataJWSHeader;
  /**
   * protected JWS header
   * @pattern ^[-_a-zA-Z0-9]*$
   * @example "ey4uLn0"
   */
  protected?: string;
  /**
   * signature
   * @pattern ^[-_a-zA-Z0-9]*$
   * @example "ey4uLn0"
   */
  signature?: string;
  /** List of signatures */
  signatures?: AttachDecoratorData1JWS[];
}

export interface AttachDecoratorDataJWSHeader {
  /**
   * Key identifier, in W3C did:key or DID URL format
   * @pattern ^did:(?:key:z[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]+|sov:[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}(;.*)?(\?.*)?#.+)$
   * @example "did:sov:LjgpST2rjsoxYegQDRm7EL#keys-4"
   */
  kid: string;
}

export interface AttachmentDef {
  /**
   * Attachment identifier
   * @example "attachment-0"
   */
  id?: string;
  /**
   * Attachment type
   * @example "present-proof"
   */
  type?: 'credential-offer' | 'present-proof';
}

export interface AttributeMimeTypesResult {
  results?: Record<string, string>;
}

export type BasicMessageModuleResponse = object;

export interface ClaimFormat {
  jwt?: object;
  jwt_vc?: object;
  jwt_vp?: object;
  ldp?: object;
  ldp_vc?: object;
  ldp_vp?: object;
}

export interface ClearPendingRevocationsRequest {
  /** Credential revocation ids by revocation registry id: omit for all, specify null or empty list for all pending per revocation registry */
  purge?: Record<string, string[]>;
}

export interface ConnRecord {
  /**
   * Connection acceptance: manual or auto
   * @example "auto"
   */
  accept?: 'manual' | 'auto';
  /**
   * Optional alias to apply to connection for later use
   * @example "Bob, providing quotes"
   */
  alias?: string;
  /**
   * Connection identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  connection_id?: string;
  /**
   * Connection protocol used
   * @example "connections/1.0"
   */
  connection_protocol?: 'connections/1.0' | 'didexchange/1.0';
  /**
   * Time of record creation
   * @pattern ^\d{4}-\d\d-\d\d[T ]\d\d:\d\d(?:\:(?:\d\d(?:\.\d{1,6})?))?(?:[+-]\d\d:?\d\d|Z|)$
   * @example "2021-12-31T23:59:59Z"
   */
  created_at?: string;
  /**
   * Error message
   * @example "No DIDDoc provided; cannot connect to public DID"
   */
  error_msg?: string;
  /**
   * Inbound routing connection id to use
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  inbound_connection_id?: string;
  /**
   * Public key for connection
   * @pattern ^[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{43,44}$
   * @example "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV"
   */
  invitation_key?: string;
  /**
   * Invitation mode
   * @example "once"
   */
  invitation_mode?: 'once' | 'multi' | 'static';
  /**
   * ID of out-of-band invitation message
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  invitation_msg_id?: string;
  /**
   * Our DID for connection
   * @pattern ^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$
   * @example "WgWxqztrNooG92RXvxSTWv"
   */
  my_did?: string;
  /**
   * Connection request identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  request_id?: string;
  /**
   * State per RFC 23
   * @example "invitation-sent"
   */
  readonly rfc23_state?: string;
  /**
   * Routing state of connection
   * @example "active"
   */
  routing_state?: 'none' | 'request' | 'active' | 'error';
  /**
   * Current record state
   * @example "active"
   */
  state?: string;
  /**
   * Their DID for connection
   * @pattern ^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$
   * @example "WgWxqztrNooG92RXvxSTWv"
   */
  their_did?: string;
  /**
   * Their label for connection
   * @example "Bob"
   */
  their_label?: string;
  /**
   * Other agent's public DID for connection
   * @example "2cpBmR3FqGKWi5EyUbpRY8"
   */
  their_public_did?: string;
  /**
   * Their role in the connection protocol
   * @example "requester"
   */
  their_role?: 'invitee' | 'requester' | 'inviter' | 'responder';
  /**
   * Time of last record update
   * @pattern ^\d{4}-\d\d-\d\d[T ]\d\d:\d\d(?:\:(?:\d\d(?:\.\d{1,6})?))?(?:[+-]\d\d:?\d\d|Z|)$
   * @example "2021-12-31T23:59:59Z"
   */
  updated_at?: string;
}

export interface ConnectionInvitation {
  /**
   * Message identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  '@id'?: string;
  /**
   * Message type
   * @example "https://didcomm.org/my-family/1.0/my-message-type"
   */
  readonly '@type'?: string;
  /**
   * DID for connection invitation
   * @pattern ^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$
   * @example "WgWxqztrNooG92RXvxSTWv"
   */
  did?: string;
  /**
   * Optional image URL for connection invitation
   * @format url
   * @example "http://192.168.56.101/img/logo.jpg"
   */
  imageUrl?: string | null;
  /**
   * Optional label for connection invitation
   * @example "Bob"
   */
  label?: string;
  /** List of recipient keys */
  recipientKeys?: string[];
  /** List of routing keys */
  routingKeys?: string[];
  /**
   * Service endpoint at which to reach this agent
   * @example "http://192.168.56.101:8020"
   */
  serviceEndpoint?: string;
}

export interface ConnectionList {
  /** List of connection records */
  results?: ConnRecord[];
}

export interface ConnectionMetadata {
  /** Dictionary of metadata associated with connection. */
  results?: object;
}

export interface ConnectionMetadataSetRequest {
  /** Dictionary of metadata to set for connection. */
  metadata: object;
}

export type ConnectionModuleResponse = object;

export interface ConnectionStaticRequest {
  /** Alias to assign to this connection */
  alias?: string;
  /**
   * Local DID
   * @pattern ^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$
   * @example "WgWxqztrNooG92RXvxSTWv"
   */
  my_did?: string;
  /** Seed to use for the local DID */
  my_seed?: string;
  /**
   * Remote DID
   * @pattern ^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$
   * @example "WgWxqztrNooG92RXvxSTWv"
   */
  their_did?: string;
  /**
   * URL endpoint for other party
   * @pattern ^[A-Za-z0-9\.\-\+]+://([A-Za-z0-9][.A-Za-z0-9-_]+[A-Za-z0-9])+(:[1-9][0-9]*)?(/[^?&#]+)?$
   * @example "https://myhost:8021"
   */
  their_endpoint?: string;
  /** Other party's label for this connection */
  their_label?: string;
  /** Seed to use for the remote DID */
  their_seed?: string;
  /** Remote verification key */
  their_verkey?: string;
}

export interface ConnectionStaticResult {
  /**
   * Local DID
   * @pattern ^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$
   * @example "WgWxqztrNooG92RXvxSTWv"
   */
  my_did: string;
  /**
   * My URL endpoint
   * @pattern ^[A-Za-z0-9\.\-\+]+://([A-Za-z0-9][.A-Za-z0-9-_]+[A-Za-z0-9])+(:[1-9][0-9]*)?(/[^?&#]+)?$
   * @example "https://myhost:8021"
   */
  my_endpoint: string;
  /**
   * My verification key
   * @pattern ^[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{43,44}$
   * @example "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV"
   */
  my_verkey: string;
  record: ConnRecord;
  /**
   * Remote DID
   * @pattern ^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$
   * @example "WgWxqztrNooG92RXvxSTWv"
   */
  their_did: string;
  /**
   * Remote verification key
   * @pattern ^[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{43,44}$
   * @example "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV"
   */
  their_verkey: string;
}

export interface Constraints {
  fields?: DIFField[];
  is_holder?: DIFHolder[];
  /** LimitDisclosure */
  limit_disclosure?: string;
  status_active?: 'required' | 'allowed' | 'disallowed';
  status_revoked?: 'required' | 'allowed' | 'disallowed';
  status_suspended?: 'required' | 'allowed' | 'disallowed';
  /** SubjectIsIssuer */
  subject_is_issuer?: 'required' | 'preferred';
}

export interface CreateInvitationRequest {
  /**
   * Identifier for active mediation record to be used
   * @pattern [a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  mediation_id?: string;
  /** Optional metadata to attach to the connection created with the invitation */
  metadata?: object;
  /**
   * Optional label for connection invitation
   * @example "Bob"
   */
  my_label?: string;
  /** List of recipient keys */
  recipient_keys?: string[];
  /** List of routing keys */
  routing_keys?: string[];
  /**
   * Connection endpoint
   * @example "http://192.168.56.102:8020"
   */
  service_endpoint?: string;
}

export interface CreateWalletRequest {
  /**
   * Image url for this wallet. This image url is publicized            (self-attested) to other agents as part of forming a connection.
   * @example "https://aries.ca/images/sample.png"
   */
  image_url?: string;
  /**
   * Key management method to use for this wallet.
   * @example "managed"
   */
  key_management_mode?: 'managed';
  /**
   * Label for this wallet. This label is publicized            (self-attested) to other agents as part of forming a connection.
   * @example "Alice"
   */
  label?: string;
  /**
   * Webhook target dispatch type for this wallet.             default - Dispatch only to webhooks associated with this wallet.             base - Dispatch only to webhooks associated with the base wallet.             both - Dispatch to both webhook targets.
   * @example "default"
   */
  wallet_dispatch_type?: 'default' | 'both' | 'base';
  /**
   * Master key used for key derivation.
   * @example "MySecretKey123"
   */
  wallet_key?: string;
  /**
   * Key derivation
   * @example "RAW"
   */
  wallet_key_derivation?: 'ARGON2I_MOD' | 'ARGON2I_INT' | 'RAW';
  /**
   * Wallet name
   * @example "MyNewWallet"
   */
  wallet_name?: string;
  /**
   * Type of the wallet to create
   * @example "indy"
   */
  wallet_type?: 'askar' | 'in_memory' | 'indy';
  /** List of Webhook URLs associated with this subwallet */
  wallet_webhook_urls?: string[];
}

export interface CreateWalletResponse {
  /**
   * Time of record creation
   * @pattern ^\d{4}-\d\d-\d\d[T ]\d\d:\d\d(?:\:(?:\d\d(?:\.\d{1,6})?))?(?:[+-]\d\d:?\d\d|Z|)$
   * @example "2021-12-31T23:59:59Z"
   */
  created_at?: string;
  /** Mode regarding management of wallet key */
  key_management_mode: 'managed' | 'unmanaged';
  /** Settings for this wallet. */
  settings?: object;
  /**
   * Current record state
   * @example "active"
   */
  state?: string;
  /**
   * Authorization token to authenticate wallet requests
   * @example "eyJhbGciOiJFZERTQSJ9.eyJhIjogIjAifQ.dBjftJeZ4CVP-mB92K27uhbUJU1p1r_wW1gFWFOEjXk"
   */
  token?: string;
  /**
   * Time of last record update
   * @pattern ^\d{4}-\d\d-\d\d[T ]\d\d:\d\d(?:\:(?:\d\d(?:\.\d{1,6})?))?(?:[+-]\d\d:?\d\d|Z|)$
   * @example "2021-12-31T23:59:59Z"
   */
  updated_at?: string;
  /**
   * Wallet record ID
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  wallet_id: string;
}

export interface CreateWalletTokenRequest {
  /**
   * Master key used for key derivation. Only required for             unamanged wallets.
   * @example "MySecretKey123"
   */
  wallet_key?: string;
}

export interface CreateWalletTokenResponse {
  /**
   * Authorization token to authenticate wallet requests
   * @example "eyJhbGciOiJFZERTQSJ9.eyJhIjogIjAifQ.dBjftJeZ4CVP-mB92K27uhbUJU1p1r_wW1gFWFOEjXk"
   */
  token?: string;
}

export interface CredAttrSpec {
  /**
   * MIME type: omit for (null) default
   * @example "image/jpeg"
   */
  'mime-type'?: string | null;
  /**
   * Attribute name
   * @example "favourite_drink"
   */
  name: string;
  /**
   * Attribute value: base64-encode if MIME type is present
   * @example "martini"
   */
  value: string;
}

export interface CredDefValue {
  /** Primary value for credential definition */
  primary?: CredDefValuePrimary;
  /** Revocation value for credential definition */
  revocation?: CredDefValueRevocation;
}

export interface CredDefValuePrimary {
  /**
   * @pattern ^[0-9]*$
   * @example "0"
   */
  n?: string;
  r?: Generated;
  /**
   * @pattern ^[0-9]*$
   * @example "0"
   */
  rctxt?: string;
  /**
   * @pattern ^[0-9]*$
   * @example "0"
   */
  s?: string;
  /**
   * @pattern ^[0-9]*$
   * @example "0"
   */
  z?: string;
}

export interface CredDefValueRevocation {
  /** @example "1 1F14F&ECB578F 2 095E45DDF417D" */
  g?: string;
  /** @example "1 1D64716fCDC00C 1 0C781960FA66E3D3 2 095E45DDF417D" */
  g_dash?: string;
  /** @example "1 16675DAE54BFAE8 2 095E45DD417D" */
  h?: string;
  /** @example "1 21E5EF9476EAF18 2 095E45DDF417D" */
  h0?: string;
  /** @example "1 236D1D99236090 2 095E45DDF417D" */
  h1?: string;
  /** @example "1 1C3AE8D1F1E277 2 095E45DDF417D" */
  h2?: string;
  /** @example "1 1B2A32CF3167 1 2490FEBF6EE55 1 0000000000000000" */
  h_cap?: string;
  /** @example "1 1D8549E8C0F8 2 095E45DDF417D" */
  htilde?: string;
  /** @example "1 142CD5E5A7DC 1 153885BD903312 2 095E45DDF417D" */
  pk?: string;
  /** @example "1 0C430AAB2B4710 1 1CB3A0932EE7E 1 0000000000000000" */
  u?: string;
  /** @example "1 153558BD903312 2 095E45DDF417D 1 0000000000000000" */
  y?: string;
}

export interface CredInfoList {
  results?: IndyCredInfo[];
}

export interface CredRevIndyRecordsResult {
  /** Indy revocation registry delta */
  rev_reg_delta?: object;
}

export interface CredRevRecordDetailsResult {
  results?: IssuerCredRevRecord[];
}

export interface CredRevRecordResult {
  result?: IssuerCredRevRecord;
}

export interface CredRevokedResult {
  /** Whether credential is revoked on the ledger */
  revoked?: boolean;
}

export interface Credential {
  /**
   * The JSON-LD context of the credential
   * @example ["https://www.w3.org/2018/credentials/v1","https://www.w3.org/2018/credentials/examples/v1"]
   */
  '@context': any[];
  /** @example {"alumniOf":{"id":"did:example:c276e12ec21ebfeb1f712ebc6f1"},"id":"did:example:ebfeb1f712ebc6f1c276e12ec21"} */
  credentialSubject: any;
  /**
   * The expiration date
   * @pattern ^([0-9]{4})-([0-9]{2})-([0-9]{2})([Tt ]([0-9]{2}):([0-9]{2}):([0-9]{2})(\.[0-9]+)?)?(([Zz]|([+-])([0-9]{2}):([0-9]{2})))?$
   * @example "2010-01-01T19:23:24Z"
   */
  expirationDate?: string;
  /**
   * @pattern \w+:(\/?\/?)[^\s]+
   * @example "http://example.edu/credentials/1872"
   */
  id?: string;
  /**
   * The issuance date
   * @pattern ^([0-9]{4})-([0-9]{2})-([0-9]{2})([Tt ]([0-9]{2}):([0-9]{2}):([0-9]{2})(\.[0-9]+)?)?(([Zz]|([+-])([0-9]{2}):([0-9]{2})))?$
   * @example "2010-01-01T19:23:24Z"
   */
  issuanceDate: string;
  /**
   * The JSON-LD Verifiable Credential Issuer. Either string of object with id field.
   * @example "did:key:z6MkpTHR8VNsBxYAAWHut2Geadd9jSwuBV8xRoAnwWsdvktH"
   */
  issuer: any;
  /**
   * The proof of the credential
   * @example {"created":"2019-12-11T03:50:55","jws":"eyJhbGciOiAiRWREU0EiLCAiYjY0IjogZmFsc2UsICJjcml0JiNjQiXX0..lKJU0Df_keblRKhZAS9Qq6zybm-HqUXNVZ8vgEPNTAjQKBhQDxvXNo7nvtUBb_Eq1Ch6YBKY5qBQ","proofPurpose":"assertionMethod","type":"Ed25519Signature2018","verificationMethod":"did:key:z6Mkgg342Ycpuk263R9d8Aq6MUaxPn1DDeHyGo38EefXmgDL#z6Mkgg342Ycpuk263R9d8Aq6MUaxPn1DDeHyGo38EefXmgDL"}
   */
  proof?: LinkedDataProof;
  /**
   * The JSON-LD type of the credential
   * @example ["VerifiableCredential","AlumniCredential"]
   */
  type: string[];
}

export interface CredentialDefinition {
  /**
   * Credential definition identifier
   * @pattern ^([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):3:CL:(([1-9][0-9]*)|([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+)):(.+)?$
   * @example "WgWxqztrNooG92RXvxSTWv:3:CL:20:tag"
   */
  id?: string;
  /**
   * Schema identifier within credential definition identifier
   * @example "20"
   */
  schemaId?: string;
  /**
   * Tag within credential definition identifier
   * @example "tag"
   */
  tag?: string;
  /**
   * Signature type: CL for Camenisch-Lysyanskaya
   * @default "CL"
   * @example "CL"
   */
  type?: any;
  /** Credential definition primary and revocation values */
  value?: CredDefValue;
  /**
   * Node protocol version
   * @pattern ^[0-9.]+$
   * @example "1.0"
   */
  ver?: string;
}

export interface CredentialDefinitionGetResult {
  credential_definition?: CredentialDefinition;
}

export interface CredentialDefinitionSendRequest {
  /**
   * Revocation registry size
   * @format int32
   * @min 4
   * @max 32768
   * @example 1000
   */
  revocation_registry_size?: number;
  /**
   * Schema identifier
   * @pattern ^[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+$
   * @example "WgWxqztrNooG92RXvxSTWv:2:schema_name:1.0"
   */
  schema_id?: string;
  /** Revocation supported flag */
  support_revocation?: boolean;
  /**
   * Credential definition identifier tag
   * @example "default"
   */
  tag?: string;
}

export interface CredentialDefinitionSendResult {
  /**
   * Credential definition identifier
   * @pattern ^([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):3:CL:(([1-9][0-9]*)|([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+)):(.+)?$
   * @example "WgWxqztrNooG92RXvxSTWv:3:CL:20:tag"
   */
  credential_definition_id?: string;
}

export interface CredentialDefinitionsCreatedResult {
  credential_definition_ids?: string[];
}

export interface CredentialOffer {
  /**
   * Message identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  '@id'?: string;
  /**
   * Message type
   * @example "https://didcomm.org/my-family/1.0/my-message-type"
   */
  readonly '@type'?: string;
  /** Human-readable comment */
  comment?: string | null;
  credential_preview?: CredentialPreview;
  'offers~attach': AttachDecorator[];
}

export interface CredentialPreview {
  /**
   * Message type identifier
   * @example "issue-credential/1.0/credential-preview"
   */
  '@type'?: string;
  attributes: CredAttrSpec[];
}

export interface CredentialProposal {
  /**
   * Message identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  '@id'?: string;
  /**
   * Message type
   * @example "https://didcomm.org/my-family/1.0/my-message-type"
   */
  readonly '@type'?: string;
  /** Human-readable comment */
  comment?: string | null;
  /**
   * @pattern ^([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):3:CL:(([1-9][0-9]*)|([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+)):(.+)?$
   * @example "WgWxqztrNooG92RXvxSTWv:3:CL:20:tag"
   */
  cred_def_id?: string;
  credential_proposal?: CredentialPreview;
  /**
   * @pattern ^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$
   * @example "WgWxqztrNooG92RXvxSTWv"
   */
  issuer_did?: string;
  /**
   * @pattern ^[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+$
   * @example "WgWxqztrNooG92RXvxSTWv:2:schema_name:1.0"
   */
  schema_id?: string;
  /**
   * @pattern ^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$
   * @example "WgWxqztrNooG92RXvxSTWv"
   */
  schema_issuer_did?: string;
  schema_name?: string;
  /**
   * @pattern ^[0-9.]+$
   * @example "1.0"
   */
  schema_version?: string;
}

export interface CredentialStatusOptions {
  /**
   * Credential status method type to use for the credential. Should match status method registered in the Verifiable Credential Extension Registry
   * @example "CredentialStatusList2017"
   */
  type: string;
}

export interface DID {
  /**
   * DID of interest
   * @pattern ^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$|^did:([a-zA-Z0-9_]+):([a-zA-Z0-9_.%-]+(:[a-zA-Z0-9_.%-]+)*)((;[a-zA-Z0-9_.:%-]+=[a-zA-Z0-9_.:%-]*)*)(\/[^#?]*)?([?][^#]*)?(\#.*)?$$
   * @example "did:peer:WgWxqztrNooG92RXvxSTWv"
   */
  did?: string;
  /**
   * Key type associated with the DID
   * @example "ed25519"
   */
  key_type?: 'ed25519' | 'bls12381g2';
  /**
   * Did method associated with the DID
   * @example "sov"
   */
  method?: string;
  /**
   * Whether DID is current public DID, posted to ledger but not current public DID, or local to the wallet
   * @example "wallet_only"
   */
  posture?: 'public' | 'posted' | 'wallet_only';
  /**
   * Public verification key
   * @pattern ^[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{43,44}$
   * @example "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV"
   */
  verkey?: string;
}

export interface DIDCreate {
  /**
   * Method for the requested DID.Supported methods are 'key', 'sov', and any other registered method.
   * @example "sov"
   */
  method?: string;
  /** To define a key type and/or a did depending on chosen DID method. */
  options?: DIDCreateOptions;
  /**
   * Optional seed to use for DID, Must beenabled in configuration before use.
   * @example "000000000000000000000000Trustee1"
   */
  seed?: string;
}

export interface DIDCreateOptions {
  /**
   * Specify final value of the did (including did:<method>: prefix)if the method supports or requires so.
   * @pattern ^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$|^did:([a-zA-Z0-9_]+):([a-zA-Z0-9_.%-]+(:[a-zA-Z0-9_.%-]+)*)((;[a-zA-Z0-9_.:%-]+=[a-zA-Z0-9_.:%-]*)*)(\/[^#?]*)?([?][^#]*)?(\#.*)?$$
   * @example "did:peer:WgWxqztrNooG92RXvxSTWv"
   */
  did?: string;
  /**
   * Key type to use for the DID keypair. Validated with the chosen DID method's supported key types.
   * @example "ed25519"
   */
  key_type: 'ed25519' | 'bls12381g2';
}

export interface DIDEndpoint {
  /**
   * DID of interest
   * @pattern ^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$
   * @example "WgWxqztrNooG92RXvxSTWv"
   */
  did: string;
  /**
   * Endpoint to set (omit to delete)
   * @pattern ^[A-Za-z0-9\.\-\+]+://([A-Za-z0-9][.A-Za-z0-9-_]+[A-Za-z0-9])+(:[1-9][0-9]*)?(/[^?&#]+)?$
   * @example "https://myhost:8021"
   */
  endpoint?: string;
}

export interface DIDEndpointWithType {
  /**
   * DID of interest
   * @pattern ^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$
   * @example "WgWxqztrNooG92RXvxSTWv"
   */
  did: string;
  /**
   * Endpoint to set (omit to delete)
   * @pattern ^[A-Za-z0-9\.\-\+]+://([A-Za-z0-9][.A-Za-z0-9-_]+[A-Za-z0-9])+(:[1-9][0-9]*)?(/[^?&#]+)?$
   * @example "https://myhost:8021"
   */
  endpoint?: string;
  /**
   * Endpoint type to set (default 'Endpoint'); affects only public or posted DIDs
   * @example "Endpoint"
   */
  endpoint_type?: 'Endpoint' | 'Profile' | 'LinkedDomains';
}

export interface DIDList {
  /** DID list */
  results?: DID[];
}

export interface DIDResult {
  result?: DID;
}

export interface DIDXRequest {
  /**
   * Message identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  '@id'?: string;
  /**
   * Message type
   * @example "https://didcomm.org/my-family/1.0/my-message-type"
   */
  readonly '@type'?: string;
  /**
   * DID of exchange
   * @pattern ^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$
   * @example "WgWxqztrNooG92RXvxSTWv"
   */
  did?: string;
  /** As signed attachment, DID Doc associated with DID */
  'did_doc~attach'?: AttachDecorator;
  /**
   * Label for DID exchange request
   * @example "Request to connect with Bob"
   */
  label: string;
}

export interface DIFField {
  filter?: Filter;
  /** ID */
  id?: string;
  path?: string[];
  /** Preference */
  predicate?: 'required' | 'preferred';
  /** Purpose */
  purpose?: string;
}

export interface DIFHolder {
  /** Preference */
  directive?: 'required' | 'preferred';
  field_id?: string[];
}

export interface DIFOptions {
  /**
   * Challenge protect against replay attack
   * @pattern [a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  challenge?: string;
  /**
   * Domain protect against replay attack
   * @example "4jt78h47fh47"
   */
  domain?: string;
}

export interface DIFPresSpec {
  /** Issuer identifier to sign the presentation, if different from current public DID */
  issuer_id?: string;
  presentation_definition?: PresentationDefinition;
  /**
   * Mapping of input_descriptor id to list of stored W3C credential record_id
   * @example {"<input descriptor id_1>":["<record id_1>","<record id_2>"],"<input descriptor id_2>":["<record id>"]}
   */
  record_ids?: object;
  /**
   * reveal doc [JSON-LD frame] dict used to derive the credential when selective disclosure is required
   * @example {"@context":["https://www.w3.org/2018/credentials/v1","https://w3id.org/security/bbs/v1"],"@explicit":true,"@requireAll":true,"credentialSubject":{"@explicit":true,"@requireAll":true,"Observation":[{"effectiveDateTime":{},"@explicit":true,"@requireAll":true}]},"issuanceDate":{},"issuer":{},"type":["VerifiableCredential","LabReport"]}
   */
  reveal_doc?: object;
}

export interface DIFProofProposal {
  input_descriptors?: InputDescriptors[];
  options?: DIFOptions;
}

export interface DIFProofRequest {
  options?: DIFOptions;
  presentation_definition: PresentationDefinition;
}

export interface Date {
  /**
   * Expiry Date
   * @format date-time
   * @example "2021-03-29T05:22:19Z"
   */
  expires_time: string;
}

export interface Disclose {
  /**
   * Message identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  '@id'?: string;
  /**
   * Message type
   * @example "https://didcomm.org/my-family/1.0/my-message-type"
   */
  readonly '@type'?: string;
  /** List of protocol descriptors */
  protocols: ProtocolDescriptor[];
}

export interface Disclosures {
  /**
   * Message identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  '@id'?: string;
  /**
   * Message type
   * @example "https://didcomm.org/my-family/1.0/my-message-type"
   */
  readonly '@type'?: string;
  /** List of protocol or goal_code descriptors */
  disclosures: any[];
}

export interface Doc {
  /** Credential to sign */
  credential: object;
  /** Signature options */
  options: SignatureOptions;
}

export interface EndorserInfo {
  /** Endorser DID */
  endorser_did: string;
  /** Endorser Name */
  endorser_name?: string;
}

export interface EndpointsResult {
  /**
   * My endpoint
   * @pattern ^[A-Za-z0-9\.\-\+]+://([A-Za-z0-9][.A-Za-z0-9-_]+[A-Za-z0-9])+(:[1-9][0-9]*)?(/[^?&#]+)?$
   * @example "https://myhost:8021"
   */
  my_endpoint?: string;
  /**
   * Their endpoint
   * @pattern ^[A-Za-z0-9\.\-\+]+://([A-Za-z0-9][.A-Za-z0-9-_]+[A-Za-z0-9])+(:[1-9][0-9]*)?(/[^?&#]+)?$
   * @example "https://myhost:8021"
   */
  their_endpoint?: string;
}

export interface Filter {
  /** Const */
  const?: any;
  enum?: any[];
  /** ExclusiveMaximum */
  exclusiveMaximum?: any;
  /** ExclusiveMinimum */
  exclusiveMinimum?: any;
  /** Format */
  format?: string;
  /**
   * Max Length
   * @format int32
   * @example 1234
   */
  maxLength?: number;
  /** Maximum */
  maximum?: any;
  /**
   * Min Length
   * @format int32
   * @example 1234
   */
  minLength?: number;
  /** Minimum */
  minimum?: any;
  /**
   * Not
   * @example false
   */
  not?: boolean;
  /** Pattern */
  pattern?: string;
  /** Type */
  type?: string;
}

export interface Generated {
  /**
   * @pattern ^[0-9]*$
   * @example "0"
   */
  master_secret?: string;
  /**
   * @pattern ^[0-9]*$
   * @example "0"
   */
  number?: string;
  /**
   * @pattern ^[0-9]*$
   * @example "0"
   */
  remainder?: string;
}

export interface GetDIDEndpointResponse {
  /**
   * Full verification key
   * @pattern ^[A-Za-z0-9\.\-\+]+://([A-Za-z0-9][.A-Za-z0-9-_]+[A-Za-z0-9])+(:[1-9][0-9]*)?(/[^?&#]+)?$
   * @example "https://myhost:8021"
   */
  endpoint?: string | null;
}

export interface GetDIDVerkeyResponse {
  /**
   * Full verification key
   * @pattern ^[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{43,44}$
   * @example "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV"
   */
  verkey?: string | null;
}

export interface GetNymRoleResponse {
  /**
   * Ledger role
   * @example "ENDORSER"
   */
  role?: 'STEWARD' | 'TRUSTEE' | 'ENDORSER' | 'NETWORK_MONITOR' | 'USER' | 'ROLE_REMOVE';
}

export type HolderModuleResponse = object;

export interface IndyAttrValue {
  /**
   * Attribute encoded value
   * @pattern ^-?[0-9]*$
   * @example "-1"
   */
  encoded: string;
  /** Attribute raw value */
  raw: string;
}

export interface IndyCredAbstract {
  /**
   * Credential definition identifier
   * @pattern ^([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):3:CL:(([1-9][0-9]*)|([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+)):(.+)?$
   * @example "WgWxqztrNooG92RXvxSTWv:3:CL:20:tag"
   */
  cred_def_id: string;
  /** Key correctness proof */
  key_correctness_proof: IndyKeyCorrectnessProof;
  /**
   * Nonce in credential abstract
   * @pattern ^[0-9]*$
   * @example "0"
   */
  nonce: string;
  /**
   * Schema identifier
   * @pattern ^[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+$
   * @example "WgWxqztrNooG92RXvxSTWv:2:schema_name:1.0"
   */
  schema_id: string;
}

export interface IndyCredInfo {
  /** Attribute names and value */
  attrs?: Record<string, string>;
  /**
   * Credential definition identifier
   * @pattern ^([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):3:CL:(([1-9][0-9]*)|([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+)):(.+)?$
   * @example "WgWxqztrNooG92RXvxSTWv:3:CL:20:tag"
   */
  cred_def_id?: string;
  /**
   * Credential revocation identifier
   * @pattern ^[1-9][0-9]*$
   * @example "12345"
   */
  cred_rev_id?: string | null;
  /**
   * Wallet referent
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  referent?: string;
  /**
   * Revocation registry identifier
   * @pattern ^([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):4:([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):3:CL:(([1-9][0-9]*)|([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+))(:.+)?:CL_ACCUM:(.+$)
   * @example "WgWxqztrNooG92RXvxSTWv:4:WgWxqztrNooG92RXvxSTWv:3:CL:20:tag:CL_ACCUM:0"
   */
  rev_reg_id?: string | null;
  /**
   * Schema identifier
   * @pattern ^[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+$
   * @example "WgWxqztrNooG92RXvxSTWv:2:schema_name:1.0"
   */
  schema_id?: string;
}

export interface IndyCredPrecis {
  /** Credential info */
  cred_info?: IndyCredInfo;
  /** Non-revocation interval from presentation request */
  interval?: IndyNonRevocationInterval;
  presentation_referents?: string[];
}

export interface IndyCredRequest {
  /** Blinded master secret */
  blinded_ms: object;
  /** Blinded master secret correctness proof */
  blinded_ms_correctness_proof: object;
  /**
   * Credential definition identifier
   * @pattern ^([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):3:CL:(([1-9][0-9]*)|([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+)):(.+)?$
   * @example "WgWxqztrNooG92RXvxSTWv:3:CL:20:tag"
   */
  cred_def_id: string;
  /**
   * Nonce in credential request
   * @pattern ^[0-9]*$
   * @example "0"
   */
  nonce: string;
  /**
   * Prover DID
   * @pattern ^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$
   * @example "WgWxqztrNooG92RXvxSTWv"
   */
  prover_did: string;
}

export interface IndyCredential {
  /**
   * Credential definition identifier
   * @pattern ^([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):3:CL:(([1-9][0-9]*)|([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+)):(.+)?$
   * @example "WgWxqztrNooG92RXvxSTWv:3:CL:20:tag"
   */
  cred_def_id: string;
  /** Revocation registry state */
  rev_reg?: object | null;
  /**
   * Revocation registry identifier
   * @pattern ^([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):4:([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):3:CL:(([1-9][0-9]*)|([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+))(:.+)?:CL_ACCUM:(.+$)
   * @example "WgWxqztrNooG92RXvxSTWv:4:WgWxqztrNooG92RXvxSTWv:3:CL:20:tag:CL_ACCUM:0"
   */
  rev_reg_id?: string | null;
  /**
   * Schema identifier
   * @pattern ^[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+$
   * @example "WgWxqztrNooG92RXvxSTWv:2:schema_name:1.0"
   */
  schema_id: string;
  /** Credential signature */
  signature: object;
  /** Credential signature correctness proof */
  signature_correctness_proof: object;
  /** Credential attributes */
  values: Record<string, IndyAttrValue>;
  /** Witness for revocation proof */
  witness?: object | null;
}

export interface IndyEQProof {
  /**
   * @pattern ^[0-9]*$
   * @example "0"
   */
  a_prime?: string;
  /**
   * @pattern ^[0-9]*$
   * @example "0"
   */
  e?: string;
  m?: Record<string, string>;
  /**
   * @pattern ^[0-9]*$
   * @example "0"
   */
  m2?: string;
  revealed_attrs?: Record<string, string>;
  /**
   * @pattern ^[0-9]*$
   * @example "0"
   */
  v?: string;
}

export interface IndyGEProof {
  /**
   * @pattern ^[0-9]*$
   * @example "0"
   */
  alpha?: string;
  /**
   * @pattern ^[0-9]*$
   * @example "0"
   */
  mj?: string;
  predicate?: IndyGEProofPred;
  r?: Record<string, string>;
  t?: Record<string, string>;
  u?: Record<string, string>;
}

export interface IndyGEProofPred {
  /** Attribute name, indy-canonicalized */
  attr_name?: string;
  /** Predicate type */
  p_type?: 'LT' | 'LE' | 'GE' | 'GT';
  /**
   * Predicate threshold value
   * @format int32
   */
  value?: number;
}

export interface IndyKeyCorrectnessProof {
  /**
   * c in key correctness proof
   * @pattern ^[0-9]*$
   * @example "0"
   */
  c: string;
  /** xr_cap in key correctness proof */
  xr_cap: string[][];
  /**
   * xz_cap in key correctness proof
   * @pattern ^[0-9]*$
   * @example "0"
   */
  xz_cap: string;
}

export interface IndyNonRevocProof {
  c_list?: Record<string, string>;
  x_list?: Record<string, string>;
}

export interface IndyNonRevocationInterval {
  /**
   * Earliest time of interest in non-revocation interval
   * @format int32
   * @min 0
   * @max 18446744073709552000
   * @example 1640995199
   */
  from?: number;
  /**
   * Latest time of interest in non-revocation interval
   * @format int32
   * @min 0
   * @max 18446744073709552000
   * @example 1640995199
   */
  to?: number;
}

export interface IndyPresAttrSpec {
  /**
   * @pattern ^([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):3:CL:(([1-9][0-9]*)|([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+)):(.+)?$
   * @example "WgWxqztrNooG92RXvxSTWv:3:CL:20:tag"
   */
  cred_def_id?: string;
  /**
   * MIME type (default null)
   * @example "image/jpeg"
   */
  'mime-type'?: string;
  /**
   * Attribute name
   * @example "favourite_drink"
   */
  name: string;
  /**
   * Credential referent
   * @example "0"
   */
  referent?: string;
  /**
   * Attribute value
   * @example "martini"
   */
  value?: string;
}

export interface IndyPresPredSpec {
  /**
   * Credential definition identifier
   * @pattern ^([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):3:CL:(([1-9][0-9]*)|([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+)):(.+)?$
   * @example "WgWxqztrNooG92RXvxSTWv:3:CL:20:tag"
   */
  cred_def_id?: string;
  /**
   * Attribute name
   * @example "high_score"
   */
  name: string;
  /**
   * Predicate type ('<', '<=', '>=', or '>')
   * @example ">="
   */
  predicate: '<' | '<=' | '>=' | '>';
  /**
   * Threshold value
   * @format int32
   */
  threshold: number;
}

export interface IndyPresPreview {
  /**
   * Message type identifier
   * @example "did:sov:BzCbsNYhMrjHiqZDTUASHg;spec/present-proof/1.0/presentation-preview"
   */
  '@type'?: string;
  attributes: IndyPresAttrSpec[];
  predicates: IndyPresPredSpec[];
}

export interface IndyPresSpec {
  /** Nested object mapping proof request attribute referents to requested-attribute specifiers */
  requested_attributes: Record<string, IndyRequestedCredsRequestedAttr>;
  /** Nested object mapping proof request predicate referents to requested-predicate specifiers */
  requested_predicates: Record<string, IndyRequestedCredsRequestedPred>;
  /** Self-attested attributes to build into proof */
  self_attested_attributes: Record<string, string>;
  /**
   * Whether to trace event (default false)
   * @example false
   */
  trace?: boolean;
}

export interface IndyPrimaryProof {
  /** Indy equality proof */
  eq_proof?: IndyEQProof | null;
  /** Indy GE proofs */
  ge_proofs?: IndyGEProof[] | null;
}

export interface IndyProof {
  /** Indy proof.identifiers content */
  identifiers?: IndyProofIdentifier[];
  /** Indy proof.proof content */
  proof?: IndyProofProof;
  /** Indy proof.requested_proof content */
  requested_proof?: IndyProofRequestedProof;
}

export interface IndyProofIdentifier {
  /**
   * Credential definition identifier
   * @pattern ^([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):3:CL:(([1-9][0-9]*)|([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+)):(.+)?$
   * @example "WgWxqztrNooG92RXvxSTWv:3:CL:20:tag"
   */
  cred_def_id?: string;
  /**
   * Revocation registry identifier
   * @pattern ^([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):4:([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):3:CL:(([1-9][0-9]*)|([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+))(:.+)?:CL_ACCUM:(.+$)
   * @example "WgWxqztrNooG92RXvxSTWv:4:WgWxqztrNooG92RXvxSTWv:3:CL:20:tag:CL_ACCUM:0"
   */
  rev_reg_id?: string | null;
  /**
   * Schema identifier
   * @pattern ^[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+$
   * @example "WgWxqztrNooG92RXvxSTWv:2:schema_name:1.0"
   */
  schema_id?: string;
  /**
   * Timestamp epoch
   * @format int32
   * @min 0
   * @max 18446744073709552000
   * @example 1640995199
   */
  timestamp?: number | null;
}

export interface IndyProofProof {
  /** Indy proof aggregated proof */
  aggregated_proof?: IndyProofProofAggregatedProof;
  /** Indy proof proofs */
  proofs?: IndyProofProofProofsProof[];
}

export interface IndyProofProofAggregatedProof {
  /** c_hash value */
  c_hash?: string;
  /** c_list value */
  c_list?: number[][];
}

export interface IndyProofProofProofsProof {
  /** Indy non-revocation proof */
  non_revoc_proof?: IndyNonRevocProof | null;
  /** Indy primary proof */
  primary_proof?: IndyPrimaryProof;
}

export interface IndyProofReqAttrSpec {
  /**
   * Attribute name
   * @example "favouriteDrink"
   */
  name?: string;
  /** Attribute name group */
  names?: string[];
  non_revoked?: IndyProofReqAttrSpecNonRevoked | null;
  /** If present, credential must satisfy one of given restrictions: specify schema_id, schema_issuer_did, schema_name, schema_version, issuer_did, cred_def_id, and/or attr::<attribute-name>::value where <attribute-name> represents a credential attribute name */
  restrictions?: Record<string, string>[];
}

export interface IndyProofReqAttrSpecNonRevoked {
  /**
   * Earliest time of interest in non-revocation interval
   * @format int32
   * @min 0
   * @max 18446744073709552000
   * @example 1640995199
   */
  from?: number;
  /**
   * Latest time of interest in non-revocation interval
   * @format int32
   * @min 0
   * @max 18446744073709552000
   * @example 1640995199
   */
  to?: number;
}

export interface IndyProofReqPredSpec {
  /**
   * Attribute name
   * @example "index"
   */
  name: string;
  non_revoked?: IndyProofReqPredSpecNonRevoked | null;
  /**
   * Predicate type ('<', '<=', '>=', or '>')
   * @example ">="
   */
  p_type: '<' | '<=' | '>=' | '>';
  /**
   * Threshold value
   * @format int32
   */
  p_value: number;
  /** If present, credential must satisfy one of given restrictions: specify schema_id, schema_issuer_did, schema_name, schema_version, issuer_did, cred_def_id, and/or attr::<attribute-name>::value where <attribute-name> represents a credential attribute name */
  restrictions?: Record<string, string>[];
}

export interface IndyProofReqPredSpecNonRevoked {
  /**
   * Earliest time of interest in non-revocation interval
   * @format int32
   * @min 0
   * @max 18446744073709552000
   * @example 1640995199
   */
  from?: number;
  /**
   * Latest time of interest in non-revocation interval
   * @format int32
   * @min 0
   * @max 18446744073709552000
   * @example 1640995199
   */
  to?: number;
}

export interface IndyProofRequest {
  /**
   * Proof request name
   * @example "Proof request"
   */
  name?: string;
  non_revoked?: IndyProofRequestNonRevoked | null;
  /**
   * Nonce
   * @pattern ^[1-9][0-9]*$
   * @example "1"
   */
  nonce?: string;
  /** Requested attribute specifications of proof request */
  requested_attributes: Record<string, IndyProofReqAttrSpec>;
  /** Requested predicate specifications of proof request */
  requested_predicates: Record<string, IndyProofReqPredSpec>;
  /**
   * Proof request version
   * @pattern ^[0-9.]+$
   * @example "1.0"
   */
  version?: string;
}

export interface IndyProofRequestNonRevoked {
  /**
   * Earliest time of interest in non-revocation interval
   * @format int32
   * @min 0
   * @max 18446744073709552000
   * @example 1640995199
   */
  from?: number;
  /**
   * Latest time of interest in non-revocation interval
   * @format int32
   * @min 0
   * @max 18446744073709552000
   * @example 1640995199
   */
  to?: number;
}

export interface IndyProofRequestedProof {
  /** Proof requested proof predicates. */
  predicates?: Record<string, IndyProofRequestedProofPredicate>;
  /** Proof requested proof revealed attribute groups */
  revealed_attr_groups?: Record<string, IndyProofRequestedProofRevealedAttrGroup>;
  /** Proof requested proof revealed attributes */
  revealed_attrs?: Record<string, IndyProofRequestedProofRevealedAttr>;
  /** Proof requested proof self-attested attributes */
  self_attested_attrs?: object;
  /** Unrevealed attributes */
  unrevealed_attrs?: object;
}

export interface IndyProofRequestedProofPredicate {
  /**
   * Sub-proof index
   * @format int32
   */
  sub_proof_index?: number;
}

export interface IndyProofRequestedProofRevealedAttr {
  /**
   * Encoded value
   * @pattern ^-?[0-9]*$
   * @example "-1"
   */
  encoded?: string;
  /** Raw value */
  raw?: string;
  /**
   * Sub-proof index
   * @format int32
   */
  sub_proof_index?: number;
}

export interface IndyProofRequestedProofRevealedAttrGroup {
  /**
   * Sub-proof index
   * @format int32
   */
  sub_proof_index?: number;
  /** Indy proof requested proof revealed attr groups group value */
  values?: Record<string, RawEncoded>;
}

export interface IndyRequestedCredsRequestedAttr {
  /**
   * Wallet credential identifier (typically but not necessarily a UUID)
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  cred_id: string;
  /** Whether to reveal attribute in proof (default true) */
  revealed?: boolean;
}

export interface IndyRequestedCredsRequestedPred {
  /**
   * Wallet credential identifier (typically but not necessarily a UUID)
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  cred_id: string;
  /**
   * Epoch timestamp of interest for non-revocation proof
   * @format int32
   * @min 0
   * @max 18446744073709552000
   * @example 1640995199
   */
  timestamp?: number;
}

export interface IndyRevRegDef {
  /**
   * Credential definition identifier
   * @pattern ^([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):3:CL:(([1-9][0-9]*)|([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+)):(.+)?$
   * @example "WgWxqztrNooG92RXvxSTWv:3:CL:20:tag"
   */
  credDefId?: string;
  /**
   * Indy revocation registry identifier
   * @pattern ^([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):4:([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):3:CL:(([1-9][0-9]*)|([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+))(:.+)?:CL_ACCUM:(.+$)
   * @example "WgWxqztrNooG92RXvxSTWv:4:WgWxqztrNooG92RXvxSTWv:3:CL:20:tag:CL_ACCUM:0"
   */
  id?: string;
  /**
   * Revocation registry type (specify CL_ACCUM)
   * @example "CL_ACCUM"
   */
  revocDefType?: 'CL_ACCUM';
  /** Revocation registry tag */
  tag?: string;
  /** Revocation registry definition value */
  value?: IndyRevRegDefValue;
  /**
   * Version of revocation registry definition
   * @pattern ^[0-9.]+$
   * @example "1.0"
   */
  ver?: string;
}

export interface IndyRevRegDefValue {
  /** Issuance type */
  issuanceType?: 'ISSUANCE_ON_DEMAND' | 'ISSUANCE_BY_DEFAULT';
  /**
   * Maximum number of credentials; registry size
   * @format int32
   * @min 1
   * @example 10
   */
  maxCredNum?: number;
  /** Public keys */
  publicKeys?: IndyRevRegDefValuePublicKeys;
  /**
   * Tails hash value
   * @pattern ^[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{43,44}$
   * @example "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV"
   */
  tailsHash?: string;
  /** Tails file location */
  tailsLocation?: string;
}

export interface IndyRevRegDefValuePublicKeys {
  accumKey?: IndyRevRegDefValuePublicKeysAccumKey;
}

export interface IndyRevRegDefValuePublicKeysAccumKey {
  /**
   * Value for z
   * @example "1 120F522F81E6B7 1 09F7A59005C4939854"
   */
  z?: string;
}

export interface IndyRevRegEntry {
  /** Revocation registry entry value */
  value?: IndyRevRegEntryValue;
  /**
   * Version of revocation registry entry
   * @pattern ^[0-9.]+$
   * @example "1.0"
   */
  ver?: string;
}

export interface IndyRevRegEntryValue {
  /**
   * Accumulator value
   * @example "21 11792B036AED0AAA12A4 4 298B2571FFC63A737"
   */
  accum?: string;
  /**
   * Previous accumulator value
   * @example "21 137AC810975E4 6 76F0384B6F23"
   */
  prevAccum?: string;
  /** Revoked credential revocation identifiers */
  revoked?: number[];
}

export interface InputDescriptors {
  constraints?: Constraints;
  group?: string[];
  /** ID */
  id?: string;
  /** Metadata dictionary */
  metadata?: object;
  /** Name */
  name?: string;
  /** Purpose */
  purpose?: string;
  /**
   * Accepts a list of schema or a dict containing filters like oneof_filter.
   * @example {"oneof_filter":[[{"uri":"https://www.w3.org/Test1#Test1"},{"uri":"https://www.w3.org/Test2#Test2"}],{"oneof_filter":[[{"uri":"https://www.w3.org/Test1#Test1"}],[{"uri":"https://www.w3.org/Test2#Test2"}]]}]}
   */
  schema?: SchemasInputDescriptorFilter;
}

export type IntroModuleResponse = object;

export interface InvitationCreateRequest {
  /**
   * List of mime type in order of preference that should be use in responding to the message
   * @example ["didcomm/aip1","didcomm/aip2;env=rfc19"]
   */
  accept?: string[];
  /**
   * Alias for connection
   * @example "Barry"
   */
  alias?: string;
  /** Optional invitation attachments */
  attachments?: AttachmentDef[];
  handshake_protocols?: string[];
  /**
   * Identifier for active mediation record to be used
   * @pattern [a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  mediation_id?: string;
  /** Optional metadata to attach to the connection created with the invitation */
  metadata?: object;
  /**
   * Label for connection invitation
   * @example "Invitation to Barry"
   */
  my_label?: string;
  /**
   * OOB protocol version
   * @example "1.1"
   */
  protocol_version?: string;
  /**
   * Whether to use public DID in invitation
   * @example false
   */
  use_public_did?: boolean;
}

export interface InvitationMessage {
  /**
   * Message identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  '@id'?: string;
  /**
   * Message type
   * @example "https://didcomm.org/my-family/1.0/my-message-type"
   */
  '@type'?: string;
  /**
   * List of mime type in order of preference
   * @example ["didcomm/aip1","didcomm/aip2;env=rfc19"]
   */
  accept?: string[];
  handshake_protocols?: string[];
  /**
   * Optional image URL for out-of-band invitation
   * @format url
   * @example "http://192.168.56.101/img/logo.jpg"
   */
  imageUrl?: string | null;
  /**
   * Optional label
   * @example "Bob"
   */
  label?: string;
  /** Optional request attachment */
  'requests~attach'?: AttachDecorator[];
  /** @example [{"did":"WgWxqztrNooG92RXvxSTWv","id":"string","recipientKeys":["did:key:z6MkpTHR8VNsBxYAAWHut2Geadd9jSwuBV8xRoAnwWsdvktH"],"routingKeys":["did:key:z6MkpTHR8VNsBxYAAWHut2Geadd9jSwuBV8xRoAnwWsdvktH"],"serviceEndpoint":"http://192.168.56.101:8020","type":"string"},"did:sov:WgWxqztrNooG92RXvxSTWv"] */
  services?: any[];
}

export interface InvitationRecord {
  /**
   * Time of record creation
   * @pattern ^\d{4}-\d\d-\d\d[T ]\d\d:\d\d(?:\:(?:\d\d(?:\.\d{1,6})?))?(?:[+-]\d\d:?\d\d|Z|)$
   * @example "2021-12-31T23:59:59Z"
   */
  created_at?: string;
  /**
   * Invitation message identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  invi_msg_id?: string;
  /** Out of band invitation message */
  invitation?: InvitationMessage;
  /**
   * Invitation record identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  invitation_id?: string;
  /**
   * Invitation message URL
   * @example "https://example.com/endpoint?c_i=eyJAdHlwZSI6ICIuLi4iLCAiLi4uIjogIi4uLiJ9XX0="
   */
  invitation_url?: string;
  /**
   * Out of band record identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  oob_id?: string;
  /**
   * Out of band message exchange state
   * @example "await_response"
   */
  state?: string;
  /** Record trace information, based on agent configuration */
  trace?: boolean;
  /**
   * Time of last record update
   * @pattern ^\d{4}-\d\d-\d\d[T ]\d\d:\d\d(?:\:(?:\d\d(?:\.\d{1,6})?))?(?:[+-]\d\d:?\d\d|Z|)$
   * @example "2021-12-31T23:59:59Z"
   */
  updated_at?: string;
}

export interface InvitationResult {
  /**
   * Connection identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  connection_id?: string;
  invitation?: ConnectionInvitation;
  /**
   * Invitation URL
   * @example "http://192.168.56.101:8020/invite?c_i=eyJAdHlwZSI6Li4ufQ=="
   */
  invitation_url?: string;
}

export type IssueCredentialModuleResponse = object;

export interface IssuerCredRevRecord {
  /**
   * Time of record creation
   * @pattern ^\d{4}-\d\d-\d\d[T ]\d\d:\d\d(?:\:(?:\d\d(?:\.\d{1,6})?))?(?:[+-]\d\d:?\d\d|Z|)$
   * @example "2021-12-31T23:59:59Z"
   */
  created_at?: string;
  /**
   * Credential definition identifier
   * @pattern ^([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):3:CL:(([1-9][0-9]*)|([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+)):(.+)?$
   * @example "WgWxqztrNooG92RXvxSTWv:3:CL:20:tag"
   */
  cred_def_id?: string;
  /**
   * Credential exchange record identifier at credential issue
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  cred_ex_id?: string;
  /** Credential exchange version */
  cred_ex_version?: string;
  /**
   * Credential revocation identifier
   * @pattern ^[1-9][0-9]*$
   * @example "12345"
   */
  cred_rev_id?: string;
  /**
   * Issuer credential revocation record identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  record_id?: string;
  /**
   * Revocation registry identifier
   * @pattern ^([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):4:([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):3:CL:(([1-9][0-9]*)|([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+))(:.+)?:CL_ACCUM:(.+$)
   * @example "WgWxqztrNooG92RXvxSTWv:4:WgWxqztrNooG92RXvxSTWv:3:CL:20:tag:CL_ACCUM:0"
   */
  rev_reg_id?: string;
  /**
   * Issue credential revocation record state
   * @example "issued"
   */
  state?: string;
  /**
   * Time of last record update
   * @pattern ^\d{4}-\d\d-\d\d[T ]\d\d:\d\d(?:\:(?:\d\d(?:\.\d{1,6})?))?(?:[+-]\d\d:?\d\d|Z|)$
   * @example "2021-12-31T23:59:59Z"
   */
  updated_at?: string;
}

export interface IssuerRevRegRecord {
  /**
   * Time of record creation
   * @pattern ^\d{4}-\d\d-\d\d[T ]\d\d:\d\d(?:\:(?:\d\d(?:\.\d{1,6})?))?(?:[+-]\d\d:?\d\d|Z|)$
   * @example "2021-12-31T23:59:59Z"
   */
  created_at?: string;
  /**
   * Credential definition identifier
   * @pattern ^([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):3:CL:(([1-9][0-9]*)|([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+)):(.+)?$
   * @example "WgWxqztrNooG92RXvxSTWv:3:CL:20:tag"
   */
  cred_def_id?: string;
  /**
   * Error message
   * @example "Revocation registry undefined"
   */
  error_msg?: string;
  /**
   * Issuer DID
   * @pattern ^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$
   * @example "WgWxqztrNooG92RXvxSTWv"
   */
  issuer_did?: string;
  /**
   * Maximum number of credentials for revocation registry
   * @format int32
   * @example 1000
   */
  max_cred_num?: number;
  /** Credential revocation identifier for credential revoked and pending publication to ledger */
  pending_pub?: string[];
  /**
   * Issuer revocation registry record identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  record_id?: string;
  /**
   * Revocation registry type (specify CL_ACCUM)
   * @example "CL_ACCUM"
   */
  revoc_def_type?: 'CL_ACCUM';
  /** Revocation registry definition */
  revoc_reg_def?: IndyRevRegDef;
  /** Revocation registry entry */
  revoc_reg_entry?: IndyRevRegEntry;
  /**
   * Revocation registry identifier
   * @pattern ^([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):4:([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):3:CL:(([1-9][0-9]*)|([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+))(:.+)?:CL_ACCUM:(.+$)
   * @example "WgWxqztrNooG92RXvxSTWv:4:WgWxqztrNooG92RXvxSTWv:3:CL:20:tag:CL_ACCUM:0"
   */
  revoc_reg_id?: string;
  /**
   * Issue revocation registry record state
   * @example "active"
   */
  state?: string;
  /** Tag within issuer revocation registry identifier */
  tag?: string;
  /**
   * Tails hash
   * @pattern ^[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{43,44}$
   * @example "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV"
   */
  tails_hash?: string;
  /** Local path to tails file */
  tails_local_path?: string;
  /** Public URI for tails file */
  tails_public_uri?: string;
  /**
   * Time of last record update
   * @pattern ^\d{4}-\d\d-\d\d[T ]\d\d:\d\d(?:\:(?:\d\d(?:\.\d{1,6})?))?(?:[+-]\d\d:?\d\d|Z|)$
   * @example "2021-12-31T23:59:59Z"
   */
  updated_at?: string;
}

export interface Keylist {
  /** List of keylist records */
  results?: RouteRecord[];
}

export interface KeylistQuery {
  /**
   * Message identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  '@id'?: string;
  /**
   * Message type
   * @example "https://didcomm.org/my-family/1.0/my-message-type"
   */
  readonly '@type'?: string;
  /**
   * Query dictionary object
   * @example {"filter":{}}
   */
  filter?: object;
  /** Pagination info */
  paginate?: KeylistQueryPaginate;
}

export interface KeylistQueryFilterRequest {
  /** Filter for keylist query */
  filter?: object;
}

export interface KeylistQueryPaginate {
  /**
   * Limit for keylist query
   * @format int32
   * @example 30
   */
  limit?: number;
  /**
   * Offset value for query
   * @format int32
   * @example 0
   */
  offset?: number;
}

export interface KeylistUpdate {
  /**
   * Message identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  '@id'?: string;
  /**
   * Message type
   * @example "https://didcomm.org/my-family/1.0/my-message-type"
   */
  readonly '@type'?: string;
  /** List of update rules */
  updates?: KeylistUpdateRule[];
}

export interface KeylistUpdateRequest {
  updates?: KeylistUpdateRule[];
}

export interface KeylistUpdateRule {
  /**
   * Action for specific key
   * @example "add"
   */
  action: 'add' | 'remove';
  /**
   * Key to remove or add
   * @pattern ^did:key:z[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]+$|^[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{43,44}$
   * @example "did:key:z6MkpTHR8VNsBxYAAWHut2Geadd9jSwuBV8xRoAnwWsdvktH"
   */
  recipient_key: string;
}

export interface LDProofVCDetail {
  /**
   * Detail of the JSON-LD Credential to be issued
   * @example {"@context":["https://www.w3.org/2018/credentials/v1","https://w3id.org/citizenship/v1"],"credentialSubject":{"familyName":"SMITH","gender":"Male","givenName":"JOHN","type":["PermanentResident","Person"]},"description":"Government of Example Permanent Resident Card.","identifier":"83627465","issuanceDate":"2019-12-03T12:19:52Z","issuer":"did:key:z6MkmjY8GnV5i9YTDtPETC2uUAW6ejw3nk5mXF5yci5ab7th","name":"Permanent Resident Card","type":["VerifiableCredential","PermanentResidentCard"]}
   */
  credential: Credential;
  /**
   * Options for specifying how the linked data proof is created.
   * @example {"proofType":"Ed25519Signature2018"}
   */
  options: LDProofVCDetailOptions;
}

export interface LDProofVCDetailOptions {
  /**
   * A challenge to include in the proof. SHOULD be provided by the requesting party of the credential (=holder)
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  challenge?: string;
  /**
   * The date and time of the proof (with a maximum accuracy in seconds). Defaults to current system time
   * @pattern ^\d{4}-\d\d-\d\d[T ]\d\d:\d\d(?:\:(?:\d\d(?:\.\d{1,6})?))?(?:[+-]\d\d:?\d\d|Z|)$
   * @example "2021-12-31T23:59:59Z"
   */
  created?: string;
  /** The credential status mechanism to use for the credential. Omitting the property indicates the issued credential will not include a credential status */
  credentialStatus?: CredentialStatusOptions;
  /**
   * The intended domain of validity for the proof
   * @example "example.com"
   */
  domain?: string;
  /**
   * The proof purpose used for the proof. Should match proof purposes registered in the Linked Data Proofs Specification
   * @example "assertionMethod"
   */
  proofPurpose?: string;
  /**
   * The proof type used for the proof. Should match suites registered in the Linked Data Cryptographic Suite Registry
   * @example "Ed25519Signature2018"
   */
  proofType: string;
}

export interface LedgerConfigInstance {
  /** genesis_file */
  genesis_file?: string;
  /** genesis_transactions */
  genesis_transactions?: string;
  /** genesis_url */
  genesis_url?: string;
  /** ledger_id */
  id?: string;
  /** is_production */
  is_production?: boolean;
}

export interface LedgerConfigList {
  ledger_config_list: LedgerConfigInstance[];
}

export type LedgerModulesResult = object;

export interface LinkedDataProof {
  /**
   * Associates a challenge with a proof, for use with a proofPurpose such as authentication
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  challenge?: string;
  /**
   * The string value of an ISO8601 combined date and time string generated by the Signature Algorithm
   * @pattern ^\d{4}-\d\d-\d\d[T ]\d\d:\d\d(?:\:(?:\d\d(?:\.\d{1,6})?))?(?:[+-]\d\d:?\d\d|Z|)$
   * @example "2021-12-31T23:59:59Z"
   */
  created: string;
  /**
   * A string value specifying the restricted domain of the signature.
   * @pattern \w+:(\/?\/?)[^\s]+
   * @example "example.com"
   */
  domain?: string;
  /**
   * Associates a Detached Json Web Signature with a proof
   * @example "eyJhbGciOiAiRWREUc2UsICJjcml0IjogWyJiNjQiXX0..lKJU0Df_keblRKhZAS9Qq6zybm-HqUXNVZ8vgEPNTAjQ1Ch6YBKY7UBAjg6iBX5qBQ"
   */
  jws?: string;
  /**
   * The nonce
   * @example "CF69iO3nfvqRsRBNElE8b4wO39SyJHPM7Gg1nExltW5vSfQA1lvDCR/zXX1To0/4NLo=="
   */
  nonce?: string;
  /**
   * Proof purpose
   * @example "assertionMethod"
   */
  proofPurpose: string;
  /**
   * The proof value of a proof
   * @example "sy1AahqbzJQ63n9RtekmwzqZeVj494VppdAVJBnMYrTwft6cLJJGeTSSxCCJ6HKnRtwE7jjDh6sB2z2AAiZY9BBnCD8wUVgwqH3qchGRCuC2RugA4eQ9fUrR4Yuycac3caiaaay"
   */
  proofValue?: string;
  /**
   * Identifies the digital signature suite that was used to create the signature
   * @example "Ed25519Signature2018"
   */
  type: string;
  /**
   * Information used for proof verification
   * @pattern \w+:(\/?\/?)[^\s]+
   * @example "did:key:z6Mkgg342Ycpuk263R9d8Aq6MUaxPn1DDeHyGo38EefXmgDL#z6Mkgg342Ycpuk263R9d8Aq6MUaxPn1DDeHyGo38EefXmgDL"
   */
  verificationMethod: string;
}

export interface MediationCreateRequest {
  /** List of mediator rules for recipient */
  mediator_terms?: string[];
  /** List of recipient rules for mediation */
  recipient_terms?: string[];
}

export interface MediationDeny {
  /**
   * Message identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  '@id'?: string;
  /**
   * Message type
   * @example "https://didcomm.org/my-family/1.0/my-message-type"
   */
  readonly '@type'?: string;
  mediator_terms?: string[];
  recipient_terms?: string[];
}

export interface MediationGrant {
  /**
   * Message identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  '@id'?: string;
  /**
   * Message type
   * @example "https://didcomm.org/my-family/1.0/my-message-type"
   */
  readonly '@type'?: string;
  /**
   * endpoint on which messages destined for the recipient are received.
   * @example "http://192.168.56.102:8020/"
   */
  endpoint?: string;
  routing_keys?: string[];
}

export interface MediationIdMatchInfo {
  /**
   * Mediation record identifier
   * @format uuid
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  mediation_id?: string;
}

export interface MediationList {
  /** List of mediation records */
  results?: MediationRecord[];
}

export interface MediationRecord {
  connection_id: string;
  /**
   * Time of record creation
   * @pattern ^\d{4}-\d\d-\d\d[T ]\d\d:\d\d(?:\:(?:\d\d(?:\.\d{1,6})?))?(?:[+-]\d\d:?\d\d|Z|)$
   * @example "2021-12-31T23:59:59Z"
   */
  created_at?: string;
  endpoint?: string;
  mediation_id?: string;
  mediator_terms?: string[];
  recipient_terms?: string[];
  role: string;
  routing_keys?: string[];
  /**
   * Current record state
   * @example "active"
   */
  state?: string;
  /**
   * Time of last record update
   * @pattern ^\d{4}-\d\d-\d\d[T ]\d\d:\d\d(?:\:(?:\d\d(?:\.\d{1,6})?))?(?:[+-]\d\d:?\d\d|Z|)$
   * @example "2021-12-31T23:59:59Z"
   */
  updated_at?: string;
}

export interface Menu {
  /**
   * Message identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  '@id'?: string;
  /**
   * Message type
   * @example "https://didcomm.org/my-family/1.0/my-message-type"
   */
  readonly '@type'?: string;
  /**
   * Introductory text for the menu
   * @example "This menu presents options"
   */
  description?: string;
  /**
   * An optional error message to display in menu header
   * @example "Error: item not found"
   */
  errormsg?: string;
  /** List of menu options */
  options: MenuOption[];
  /**
   * Menu title
   * @example "My Menu"
   */
  title?: string;
}

export interface MenuForm {
  /**
   * Additional descriptive text for menu form
   * @example "Window preference settings"
   */
  description?: string;
  /** List of form parameters */
  params?: MenuFormParam[];
  /**
   * Alternative label for form submit button
   * @example "Send"
   */
  'submit-label'?: string;
  /**
   * Menu form title
   * @example "Preferences"
   */
  title?: string;
}

export interface MenuFormParam {
  /**
   * Default parameter value
   * @example "0"
   */
  default?: string;
  /**
   * Additional descriptive text for menu form parameter
   * @example "Delay in seconds before starting"
   */
  description?: string;
  /**
   * Menu parameter name
   * @example "delay"
   */
  name: string;
  /**
   * Whether parameter is required
   * @example "False"
   */
  required?: boolean;
  /**
   * Menu parameter title
   * @example "Delay in seconds"
   */
  title: string;
  /**
   * Menu form parameter input type
   * @example "int"
   */
  type?: string;
}

export interface MenuJson {
  /**
   * Introductory text for the menu
   * @example "User preferences for window settings"
   */
  description?: string;
  /**
   * Optional error message to display in menu header
   * @example "Error: item not present"
   */
  errormsg?: string;
  /** List of menu options */
  options: MenuOption[];
  /**
   * Menu title
   * @example "My Menu"
   */
  title?: string;
}

export interface MenuOption {
  /**
   * Additional descriptive text for menu option
   * @example "Window display preferences"
   */
  description?: string;
  /**
   * Whether to show option as disabled
   * @example "False"
   */
  disabled?: boolean;
  form?: MenuForm;
  /**
   * Menu option name (unique identifier)
   * @example "window_prefs"
   */
  name: string;
  /**
   * Menu option title
   * @example "Window Preferences"
   */
  title: string;
}

export type MultitenantModuleResponse = object;

export interface OobRecord {
  /**
   * Connection record identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  attach_thread_id?: string;
  /**
   * Connection record identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  connection_id?: string;
  /**
   * Time of record creation
   * @pattern ^\d{4}-\d\d-\d\d[T ]\d\d:\d\d(?:\:(?:\d\d(?:\.\d{1,6})?))?(?:[+-]\d\d:?\d\d|Z|)$
   * @example "2021-12-31T23:59:59Z"
   */
  created_at?: string;
  /**
   * Invitation message identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  invi_msg_id: string;
  /** Out of band invitation message */
  invitation: InvitationMessage;
  /**
   * Oob record identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  oob_id: string;
  /**
   * Recipient key used for oob invitation
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  our_recipient_key?: string;
  /**
   * OOB Role
   * @example "receiver"
   */
  role?: 'sender' | 'receiver';
  /**
   * Out of band message exchange state
   * @example "await-response"
   */
  state:
    | 'initial'
    | 'prepare-response'
    | 'await-response'
    | 'reuse-not-accepted'
    | 'reuse-accepted'
    | 'done'
    | 'deleted';
  their_service?: ServiceDecorator;
  /** Record trace information, based on agent configuration */
  trace?: boolean;
  /**
   * Time of last record update
   * @pattern ^\d{4}-\d\d-\d\d[T ]\d\d:\d\d(?:\:(?:\d\d(?:\.\d{1,6})?))?(?:[+-]\d\d:?\d\d|Z|)$
   * @example "2021-12-31T23:59:59Z"
   */
  updated_at?: string;
}

export interface PerformRequest {
  /**
   * Menu option name
   * @example "Query"
   */
  name?: string;
  /** Input parameter values */
  params?: Record<string, string>;
}

export interface PingRequest {
  /** Comment for the ping message */
  comment?: string | null;
}

export interface PingRequestResponse {
  /** Thread ID of the ping message */
  thread_id?: string;
}

export interface PresentationDefinition {
  format?: ClaimFormat;
  /**
   * Unique Resource Identifier
   * @pattern [a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  id?: string;
  input_descriptors?: InputDescriptors[];
  /** Human-friendly name that describes what the presentation definition pertains to */
  name?: string;
  /** Describes the purpose for which the Presentation Definition's inputs are being requested */
  purpose?: string;
  submission_requirements?: SubmissionRequirements[];
}

export interface PresentationProposal {
  /**
   * Message identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  '@id'?: string;
  /**
   * Message type
   * @example "https://didcomm.org/my-family/1.0/my-message-type"
   */
  readonly '@type'?: string;
  /** Human-readable comment */
  comment?: string | null;
  presentation_proposal: IndyPresPreview;
}

export interface PresentationRequest {
  /**
   * Message identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  '@id'?: string;
  /**
   * Message type
   * @example "https://didcomm.org/my-family/1.0/my-message-type"
   */
  readonly '@type'?: string;
  /** Human-readable comment */
  comment?: string | null;
  'request_presentations~attach': AttachDecorator[];
}

export interface ProtocolDescriptor {
  pid: string;
  /** List of roles */
  roles?: string[] | null;
}

export interface PublishRevocations {
  /** Credential revocation ids by revocation registry id */
  rrid2crid?: Record<string, string[]>;
}

export interface Queries {
  /**
   * Message identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  '@id'?: string;
  /**
   * Message type
   * @example "https://didcomm.org/my-family/1.0/my-message-type"
   */
  readonly '@type'?: string;
  queries?: QueryItem[];
}

export interface Query {
  /**
   * Message identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  '@id'?: string;
  /**
   * Message type
   * @example "https://didcomm.org/my-family/1.0/my-message-type"
   */
  readonly '@type'?: string;
  comment?: string | null;
  query: string;
}

export interface QueryItem {
  /** feature type */
  'feature-type': 'protocol' | 'goal-code';
  /** match */
  match: string;
}

export interface RawEncoded {
  /**
   * Encoded value
   * @pattern ^-?[0-9]*$
   * @example "-1"
   */
  encoded?: string;
  /** Raw value */
  raw?: string;
}

export interface ReceiveInvitationRequest {
  /**
   * Message identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  '@id'?: string;
  /**
   * Message type
   * @example "https://didcomm.org/my-family/1.0/my-message-type"
   */
  readonly '@type'?: string;
  /**
   * DID for connection invitation
   * @pattern ^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$
   * @example "WgWxqztrNooG92RXvxSTWv"
   */
  did?: string;
  /**
   * Optional image URL for connection invitation
   * @format url
   * @example "http://192.168.56.101/img/logo.jpg"
   */
  imageUrl?: string | null;
  /**
   * Optional label for connection invitation
   * @example "Bob"
   */
  label?: string;
  /** List of recipient keys */
  recipientKeys?: string[];
  /** List of routing keys */
  routingKeys?: string[];
  /**
   * Service endpoint at which to reach this agent
   * @example "http://192.168.56.101:8020"
   */
  serviceEndpoint?: string;
}

export interface RemoveWalletRequest {
  /**
   * Master key used for key derivation. Only required for             unmanaged wallets.
   * @example "MySecretKey123"
   */
  wallet_key?: string;
}

export interface ResolutionResult {
  /** DID Document */
  did_doc: object;
  /** Resolution metadata */
  metadata: object;
}

export interface RevRegCreateRequest {
  /**
   * Credential definition identifier
   * @pattern ^([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):3:CL:(([1-9][0-9]*)|([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+)):(.+)?$
   * @example "WgWxqztrNooG92RXvxSTWv:3:CL:20:tag"
   */
  credential_definition_id?: string;
  /**
   * Revocation registry size
   * @format int32
   * @min 4
   * @max 32768
   * @example 1000
   */
  max_cred_num?: number;
}

export interface RevRegIssuedResult {
  /**
   * Number of credentials issued against revocation registry
   * @format int32
   * @min 0
   * @example 0
   */
  result?: number;
}

export interface RevRegResult {
  result?: IssuerRevRegRecord;
}

export interface RevRegUpdateTailsFileUri {
  /**
   * Public URI to the tails file
   * @format url
   * @example "http://192.168.56.133:6543/revocation/registry/WgWxqztrNooG92RXvxSTWv:4:WgWxqztrNooG92RXvxSTWv:3:CL:20:tag:CL_ACCUM:0/tails-file"
   */
  tails_public_uri: string;
}

export interface RevRegWalletUpdatedResult {
  /** Calculated accumulator for phantom revocations */
  accum_calculated?: object;
  /** Applied ledger transaction to fix revocations */
  accum_fixed?: object;
  /** Indy revocation registry delta */
  rev_reg_delta?: object;
}

export interface RevRegsCreated {
  rev_reg_ids?: string[];
}

export type RevocationModuleResponse = object;

export interface RevokeRequest {
  /** Optional comment to include in revocation notification */
  comment?: string;
  /**
   * Connection ID to which the revocation notification will be sent; required if notify is true
   * @pattern [a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  connection_id?: string;
  /**
   * Credential exchange identifier
   * @pattern [a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  cred_ex_id?: string;
  /**
   * Credential revocation identifier
   * @pattern ^[1-9][0-9]*$
   * @example "12345"
   */
  cred_rev_id?: string;
  /** Send a notification to the credential recipient */
  notify?: boolean;
  /** Specify which version of the revocation notification should be sent */
  notify_version?: 'v1_0' | 'v2_0';
  /** (True) publish revocation to ledger immediately, or (default, False) mark it pending */
  publish?: boolean;
  /**
   * Revocation registry identifier
   * @pattern ^([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):4:([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):3:CL:(([1-9][0-9]*)|([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+))(:.+)?:CL_ACCUM:(.+$)
   * @example "WgWxqztrNooG92RXvxSTWv:4:WgWxqztrNooG92RXvxSTWv:3:CL:20:tag:CL_ACCUM:0"
   */
  rev_reg_id?: string;
  /** Thread ID of the credential exchange message thread resulting in the credential now being revoked; required if notify is true */
  thread_id?: string;
}

export interface RouteRecord {
  connection_id?: string;
  /**
   * Time of record creation
   * @pattern ^\d{4}-\d\d-\d\d[T ]\d\d:\d\d(?:\:(?:\d\d(?:\.\d{1,6})?))?(?:[+-]\d\d:?\d\d|Z|)$
   * @example "2021-12-31T23:59:59Z"
   */
  created_at?: string;
  recipient_key: string;
  record_id?: string;
  role?: string;
  /**
   * Current record state
   * @example "active"
   */
  state?: string;
  /**
   * Time of last record update
   * @pattern ^\d{4}-\d\d-\d\d[T ]\d\d:\d\d(?:\:(?:\d\d(?:\.\d{1,6})?))?(?:[+-]\d\d:?\d\d|Z|)$
   * @example "2021-12-31T23:59:59Z"
   */
  updated_at?: string;
  wallet_id?: string;
}

export interface Schema {
  /** Schema attribute names */
  attrNames?: string[];
  /**
   * Schema identifier
   * @pattern ^[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+$
   * @example "WgWxqztrNooG92RXvxSTWv:2:schema_name:1.0"
   */
  id?: string;
  /**
   * Schema name
   * @example "schema_name"
   */
  name?: string;
  /**
   * Schema sequence number
   * @format int32
   * @min 1
   * @example 10
   */
  seqNo?: number;
  /**
   * Node protocol version
   * @pattern ^[0-9.]+$
   * @example "1.0"
   */
  ver?: string;
  /**
   * Schema version
   * @pattern ^[0-9.]+$
   * @example "1.0"
   */
  version?: string;
}

export interface SchemaGetResult {
  schema?: Schema;
}

export interface SchemaInputDescriptor {
  /** Required */
  required?: boolean;
  /** URI */
  uri?: string;
}

export interface SchemaSendRequest {
  /** List of schema attributes */
  attributes: string[];
  /**
   * Schema name
   * @example "prefs"
   */
  schema_name: string;
  /**
   * Schema version
   * @pattern ^[0-9.]+$
   * @example "1.0"
   */
  schema_version: string;
}

export interface SchemaSendResult {
  /** Schema definition */
  schema?: Schema;
  /**
   * Schema identifier
   * @pattern ^[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+$
   * @example "WgWxqztrNooG92RXvxSTWv:2:schema_name:1.0"
   */
  schema_id: string;
}

export interface SchemasCreatedResult {
  schema_ids?: string[];
}

export interface SchemasInputDescriptorFilter {
  /** oneOf */
  oneof_filter?: boolean;
  uri_groups?: SchemaInputDescriptor[][];
}

export interface SendMenu {
  /** Menu to send to connection */
  menu: MenuJson;
}

export interface SendMessage {
  /**
   * Message content
   * @example "Hello"
   */
  content?: string;
}

export interface ServiceDecorator {
  /** List of recipient keys */
  recipientKeys: string[];
  /** List of routing keys */
  routingKeys?: string[];
  /**
   * Service endpoint at which to reach this agent
   * @example "http://192.168.56.101:8020"
   */
  serviceEndpoint: string;
}

export interface SignRequest {
  doc: Doc;
  /** Verkey to use for signing */
  verkey: string;
}

export interface SignResponse {
  /** Error text */
  error?: string;
  /** Signed document */
  signed_doc?: object;
}

export interface SignatureOptions {
  challenge?: string;
  domain?: string;
  proofPurpose: string;
  type?: string;
  verificationMethod: string;
}

export interface SignedDoc {
  /** Linked data proof */
  proof: SignatureOptions;
}

export interface SubmissionRequirements {
  /**
   * Count Value
   * @format int32
   * @example 1234
   */
  count?: number;
  /** From */
  from?: string;
  from_nested?: SubmissionRequirements[];
  /**
   * Max Value
   * @format int32
   * @example 1234
   */
  max?: number;
  /**
   * Min Value
   * @format int32
   * @example 1234
   */
  min?: number;
  /** Name */
  name?: string;
  /** Purpose */
  purpose?: string;
  /** Selection */
  rule?: 'all' | 'pick';
}

export interface TAAAccept {
  mechanism?: string;
  text?: string;
  version?: string;
}

export interface TAAAcceptance {
  mechanism?: string;
  /**
   * @format int32
   * @min 0
   * @max 18446744073709552000
   * @example 1640995199
   */
  time?: number;
}

export interface TAAInfo {
  aml_record?: AMLRecord;
  taa_accepted?: TAAAcceptance;
  taa_record?: TAARecord;
  taa_required?: boolean;
}

export interface TAARecord {
  digest?: string;
  text?: string;
  version?: string;
}

export interface TAAResult {
  result?: TAAInfo;
}

export interface TransactionJobs {
  /** My transaction related job */
  transaction_my_job?: 'TRANSACTION_AUTHOR' | 'TRANSACTION_ENDORSER' | 'reset';
  /** Their transaction related job */
  transaction_their_job?: 'TRANSACTION_AUTHOR' | 'TRANSACTION_ENDORSER' | 'reset';
}

export interface TransactionList {
  /** List of transaction records */
  results?: TransactionRecord[];
}

export interface TransactionRecord {
  /**
   * Transaction type
   * @example "101"
   */
  _type?: string;
  /**
   * The connection identifier for thie particular transaction record
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  connection_id?: string;
  /**
   * Time of record creation
   * @pattern ^\d{4}-\d\d-\d\d[T ]\d\d:\d\d(?:\:(?:\d\d(?:\.\d{1,6})?))?(?:[+-]\d\d:?\d\d|Z|)$
   * @example "2021-12-31T23:59:59Z"
   */
  created_at?: string;
  /**
   * If True, Endorser will write the transaction after endorsing it
   * @example true
   */
  endorser_write_txn?: boolean;
  formats?: Record<string, string>[];
  messages_attach?: object[];
  /** @example {"context":{"param1":"param1_value","param2":"param2_value"},"post_process":[{"topic":"topic_value","other":"other_value"}]} */
  meta_data?: object;
  signature_request?: object[];
  signature_response?: object[];
  /**
   * Current record state
   * @example "active"
   */
  state?: string;
  /**
   * Thread Identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  thread_id?: string;
  /** @example {"expires_time":"2020-12-13T17:29:06+0000"} */
  timing?: object;
  /** Record trace information, based on agent configuration */
  trace?: boolean;
  /**
   * Transaction identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  transaction_id?: string;
  /**
   * Time of last record update
   * @pattern ^\d{4}-\d\d-\d\d[T ]\d\d:\d\d(?:\:(?:\d\d(?:\.\d{1,6})?))?(?:[+-]\d\d:?\d\d|Z|)$
   * @example "2021-12-31T23:59:59Z"
   */
  updated_at?: string;
}

export interface TxnOrCredentialDefinitionSendResult {
  sent?: CredentialDefinitionSendResult;
  /** Credential definition transaction to endorse */
  txn?: TransactionRecord;
}

export interface TxnOrPublishRevocationsResult {
  sent?: PublishRevocations;
  /** Revocation registry revocations transaction to endorse */
  txn?: TransactionRecord;
}

export interface TxnOrRegisterLedgerNymResponse {
  /**
   * Success of nym registration operation
   * @example true
   */
  success?: boolean;
  /** DID transaction to endorse */
  txn?: TransactionRecord;
}

export interface TxnOrRevRegResult {
  sent?: RevRegResult;
  /** Revocation registry definition transaction to endorse */
  txn?: TransactionRecord;
}

export interface TxnOrSchemaSendResult {
  /** Content sent */
  sent?: SchemaSendResult;
  /** Schema transaction to endorse */
  txn?: TransactionRecord;
}

export interface UpdateWalletRequest {
  /**
   * Image url for this wallet. This image url is publicized            (self-attested) to other agents as part of forming a connection.
   * @example "https://aries.ca/images/sample.png"
   */
  image_url?: string;
  /**
   * Label for this wallet. This label is publicized            (self-attested) to other agents as part of forming a connection.
   * @example "Alice"
   */
  label?: string;
  /**
   * Webhook target dispatch type for this wallet.             default - Dispatch only to webhooks associated with this wallet.             base - Dispatch only to webhooks associated with the base wallet.             both - Dispatch to both webhook targets.
   * @example "default"
   */
  wallet_dispatch_type?: 'default' | 'both' | 'base';
  /** List of Webhook URLs associated with this subwallet */
  wallet_webhook_urls?: string[];
}

export interface V10CredentialBoundOfferRequest {
  /** Optional counter-proposal */
  counter_proposal?: CredentialProposal;
}

export interface V10CredentialConnFreeOfferRequest {
  /** Whether to respond automatically to credential requests, creating and issuing requested credentials */
  auto_issue?: boolean;
  /** Whether to remove the credential exchange record on completion (overrides --preserve-exchange-records configuration setting) */
  auto_remove?: boolean;
  /** Human-readable comment */
  comment?: string | null;
  /**
   * Credential definition identifier
   * @pattern ^([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):3:CL:(([1-9][0-9]*)|([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+)):(.+)?$
   * @example "WgWxqztrNooG92RXvxSTWv:3:CL:20:tag"
   */
  cred_def_id: string;
  credential_preview: CredentialPreview;
  /** Record trace information, based on agent configuration */
  trace?: boolean;
}

export interface V10CredentialCreate {
  /** Whether to remove the credential exchange record on completion (overrides --preserve-exchange-records configuration setting) */
  auto_remove?: boolean;
  /** Human-readable comment */
  comment?: string | null;
  /**
   * Credential definition identifier
   * @pattern ^([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):3:CL:(([1-9][0-9]*)|([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+)):(.+)?$
   * @example "WgWxqztrNooG92RXvxSTWv:3:CL:20:tag"
   */
  cred_def_id?: string;
  credential_proposal: CredentialPreview;
  /**
   * Credential issuer DID
   * @pattern ^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$
   * @example "WgWxqztrNooG92RXvxSTWv"
   */
  issuer_did?: string;
  /**
   * Schema identifier
   * @pattern ^[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+$
   * @example "WgWxqztrNooG92RXvxSTWv:2:schema_name:1.0"
   */
  schema_id?: string;
  /**
   * Schema issuer DID
   * @pattern ^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$
   * @example "WgWxqztrNooG92RXvxSTWv"
   */
  schema_issuer_did?: string;
  /**
   * Schema name
   * @example "preferences"
   */
  schema_name?: string;
  /**
   * Schema version
   * @pattern ^[0-9.]+$
   * @example "1.0"
   */
  schema_version?: string;
  /** Record trace information, based on agent configuration */
  trace?: boolean;
}

export interface V10CredentialExchange {
  /**
   * Issuer choice to issue to request in this credential exchange
   * @example false
   */
  auto_issue?: boolean;
  /**
   * Holder choice to accept offer in this credential exchange
   * @example false
   */
  auto_offer?: boolean;
  /**
   * Issuer choice to remove this credential exchange record when complete
   * @example false
   */
  auto_remove?: boolean;
  /**
   * Connection identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  connection_id?: string;
  /**
   * Time of record creation
   * @pattern ^\d{4}-\d\d-\d\d[T ]\d\d:\d\d(?:\:(?:\d\d(?:\.\d{1,6})?))?(?:[+-]\d\d:?\d\d|Z|)$
   * @example "2021-12-31T23:59:59Z"
   */
  created_at?: string;
  /** Credential as stored */
  credential?: IndyCredInfo;
  /**
   * Credential definition identifier
   * @pattern ^([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):3:CL:(([1-9][0-9]*)|([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+)):(.+)?$
   * @example "WgWxqztrNooG92RXvxSTWv:3:CL:20:tag"
   */
  credential_definition_id?: string;
  /**
   * Credential exchange identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  credential_exchange_id?: string;
  /**
   * Credential identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  credential_id?: string;
  /** (Indy) credential offer */
  credential_offer?: IndyCredAbstract;
  /** Credential offer message */
  credential_offer_dict?: CredentialOffer;
  /** Credential proposal message */
  credential_proposal_dict?: CredentialProposal;
  /** (Indy) credential request */
  credential_request?: IndyCredRequest;
  /** (Indy) credential request metadata */
  credential_request_metadata?: object;
  /**
   * Error message
   * @example "Credential definition identifier is not set in proposal"
   */
  error_msg?: string;
  /**
   * Issue-credential exchange initiator: self or external
   * @example "self"
   */
  initiator?: 'self' | 'external';
  /**
   * Parent thread identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  parent_thread_id?: string;
  /** Credential as received, prior to storage in holder wallet */
  raw_credential?: IndyCredential;
  /** Revocation registry identifier */
  revoc_reg_id?: string;
  /** Credential identifier within revocation registry */
  revocation_id?: string;
  /**
   * Issue-credential exchange role: holder or issuer
   * @example "issuer"
   */
  role?: 'holder' | 'issuer';
  /**
   * Schema identifier
   * @pattern ^[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+$
   * @example "WgWxqztrNooG92RXvxSTWv:2:schema_name:1.0"
   */
  schema_id?: string;
  /**
   * Issue-credential exchange state
   * @example "credential_acked"
   */
  state?: string;
  /**
   * Thread identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  thread_id?: string;
  /** Record trace information, based on agent configuration */
  trace?: boolean;
  /**
   * Time of last record update
   * @pattern ^\d{4}-\d\d-\d\d[T ]\d\d:\d\d(?:\:(?:\d\d(?:\.\d{1,6})?))?(?:[+-]\d\d:?\d\d|Z|)$
   * @example "2021-12-31T23:59:59Z"
   */
  updated_at?: string;
}

export interface V10CredentialExchangeListResult {
  /** Aries#0036 v1.0 credential exchange records */
  results?: V10CredentialExchange[];
}

export interface V10CredentialFreeOfferRequest {
  /** Whether to respond automatically to credential requests, creating and issuing requested credentials */
  auto_issue?: boolean;
  /** Whether to remove the credential exchange record on completion (overrides --preserve-exchange-records configuration setting) */
  auto_remove?: boolean;
  /** Human-readable comment */
  comment?: string | null;
  /**
   * Connection identifier
   * @format uuid
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  connection_id: string;
  /**
   * Credential definition identifier
   * @pattern ^([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):3:CL:(([1-9][0-9]*)|([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+)):(.+)?$
   * @example "WgWxqztrNooG92RXvxSTWv:3:CL:20:tag"
   */
  cred_def_id: string;
  credential_preview: CredentialPreview;
  /** Record trace information, based on agent configuration */
  trace?: boolean;
}

export interface V10CredentialIssueRequest {
  /** Human-readable comment */
  comment?: string | null;
}

export interface V10CredentialProblemReportRequest {
  description: string;
}

export interface V10CredentialProposalRequestMand {
  /** Whether to remove the credential exchange record on completion (overrides --preserve-exchange-records configuration setting) */
  auto_remove?: boolean;
  /** Human-readable comment */
  comment?: string | null;
  /**
   * Connection identifier
   * @format uuid
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  connection_id: string;
  /**
   * Credential definition identifier
   * @pattern ^([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):3:CL:(([1-9][0-9]*)|([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+)):(.+)?$
   * @example "WgWxqztrNooG92RXvxSTWv:3:CL:20:tag"
   */
  cred_def_id?: string;
  credential_proposal: CredentialPreview;
  /**
   * Credential issuer DID
   * @pattern ^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$
   * @example "WgWxqztrNooG92RXvxSTWv"
   */
  issuer_did?: string;
  /**
   * Schema identifier
   * @pattern ^[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+$
   * @example "WgWxqztrNooG92RXvxSTWv:2:schema_name:1.0"
   */
  schema_id?: string;
  /**
   * Schema issuer DID
   * @pattern ^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$
   * @example "WgWxqztrNooG92RXvxSTWv"
   */
  schema_issuer_did?: string;
  /**
   * Schema name
   * @example "preferences"
   */
  schema_name?: string;
  /**
   * Schema version
   * @pattern ^[0-9.]+$
   * @example "1.0"
   */
  schema_version?: string;
  /** Record trace information, based on agent configuration */
  trace?: boolean;
}

export interface V10CredentialProposalRequestOpt {
  /** Whether to remove the credential exchange record on completion (overrides --preserve-exchange-records configuration setting) */
  auto_remove?: boolean;
  /** Human-readable comment */
  comment?: string | null;
  /**
   * Connection identifier
   * @format uuid
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  connection_id: string;
  /**
   * Credential definition identifier
   * @pattern ^([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):3:CL:(([1-9][0-9]*)|([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+)):(.+)?$
   * @example "WgWxqztrNooG92RXvxSTWv:3:CL:20:tag"
   */
  cred_def_id?: string;
  credential_proposal?: CredentialPreview;
  /**
   * Credential issuer DID
   * @pattern ^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$
   * @example "WgWxqztrNooG92RXvxSTWv"
   */
  issuer_did?: string;
  /**
   * Schema identifier
   * @pattern ^[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+$
   * @example "WgWxqztrNooG92RXvxSTWv:2:schema_name:1.0"
   */
  schema_id?: string;
  /**
   * Schema issuer DID
   * @pattern ^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$
   * @example "WgWxqztrNooG92RXvxSTWv"
   */
  schema_issuer_did?: string;
  /**
   * Schema name
   * @example "preferences"
   */
  schema_name?: string;
  /**
   * Schema version
   * @pattern ^[0-9.]+$
   * @example "1.0"
   */
  schema_version?: string;
  /** Record trace information, based on agent configuration */
  trace?: boolean;
}

export interface V10CredentialStoreRequest {
  credential_id?: string;
}

export interface V10DiscoveryExchangeListResult {
  results?: V10DiscoveryRecord[];
}

export interface V10DiscoveryRecord {
  /**
   * Connection identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  connection_id?: string;
  /**
   * Time of record creation
   * @pattern ^\d{4}-\d\d-\d\d[T ]\d\d:\d\d(?:\:(?:\d\d(?:\.\d{1,6})?))?(?:[+-]\d\d:?\d\d|Z|)$
   * @example "2021-12-31T23:59:59Z"
   */
  created_at?: string;
  /** Disclose message */
  disclose?: Disclose;
  /**
   * Credential exchange identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  discovery_exchange_id?: string;
  /** Query message */
  query_msg?: Query;
  /**
   * Current record state
   * @example "active"
   */
  state?: string;
  /**
   * Thread identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  thread_id?: string;
  /** Record trace information, based on agent configuration */
  trace?: boolean;
  /**
   * Time of last record update
   * @pattern ^\d{4}-\d\d-\d\d[T ]\d\d:\d\d(?:\:(?:\d\d(?:\.\d{1,6})?))?(?:[+-]\d\d:?\d\d|Z|)$
   * @example "2021-12-31T23:59:59Z"
   */
  updated_at?: string;
}

export type V10PresentProofModuleResponse = object;

export interface V10PresentationCreateRequestRequest {
  /**
   * Verifier choice to auto-verify proof presentation
   * @example false
   */
  auto_verify?: boolean;
  comment?: string | null;
  proof_request: IndyProofRequest;
  /**
   * Whether to trace event (default false)
   * @example false
   */
  trace?: boolean;
}

export interface V10PresentationExchange {
  /**
   * Prover choice to auto-present proof as verifier requests
   * @example false
   */
  auto_present?: boolean;
  /** Verifier choice to auto-verify proof presentation */
  auto_verify?: boolean;
  /**
   * Connection identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  connection_id?: string;
  /**
   * Time of record creation
   * @pattern ^\d{4}-\d\d-\d\d[T ]\d\d:\d\d(?:\:(?:\d\d(?:\.\d{1,6})?))?(?:[+-]\d\d:?\d\d|Z|)$
   * @example "2021-12-31T23:59:59Z"
   */
  created_at?: string;
  /**
   * Error message
   * @example "Invalid structure"
   */
  error_msg?: string;
  /**
   * Present-proof exchange initiator: self or external
   * @example "self"
   */
  initiator?: 'self' | 'external';
  /** (Indy) presentation (also known as proof) */
  presentation?: IndyProof;
  /**
   * Presentation exchange identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  presentation_exchange_id?: string;
  /** Presentation proposal message */
  presentation_proposal_dict?: PresentationProposal;
  /** (Indy) presentation request (also known as proof request) */
  presentation_request?: IndyProofRequest;
  /** Presentation request message */
  presentation_request_dict?: PresentationRequest;
  /**
   * Present-proof exchange role: prover or verifier
   * @example "prover"
   */
  role?: 'prover' | 'verifier';
  /**
   * Present-proof exchange state
   * @example "verified"
   */
  state?: string;
  /**
   * Thread identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  thread_id?: string;
  /** Record trace information, based on agent configuration */
  trace?: boolean;
  /**
   * Time of last record update
   * @pattern ^\d{4}-\d\d-\d\d[T ]\d\d:\d\d(?:\:(?:\d\d(?:\.\d{1,6})?))?(?:[+-]\d\d:?\d\d|Z|)$
   * @example "2021-12-31T23:59:59Z"
   */
  updated_at?: string;
  /**
   * Whether presentation is verified: true or false
   * @example "true"
   */
  verified?: 'true' | 'false';
  verified_msgs?: string[];
}

export interface V10PresentationExchangeList {
  /** Aries RFC 37 v1.0 presentation exchange records */
  results?: V10PresentationExchange[];
}

export interface V10PresentationProblemReportRequest {
  description: string;
}

export interface V10PresentationProposalRequest {
  /** Whether to respond automatically to presentation requests, building and presenting requested proof */
  auto_present?: boolean;
  /** Human-readable comment */
  comment?: string | null;
  /**
   * Connection identifier
   * @format uuid
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  connection_id: string;
  presentation_proposal: IndyPresPreview;
  /**
   * Whether to trace event (default false)
   * @example false
   */
  trace?: boolean;
}

export interface V10PresentationSendRequestRequest {
  /**
   * Verifier choice to auto-verify proof presentation
   * @example false
   */
  auto_verify?: boolean;
  comment?: string | null;
  /**
   * Connection identifier
   * @format uuid
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  connection_id: string;
  proof_request: IndyProofRequest;
  /**
   * Whether to trace event (default false)
   * @example false
   */
  trace?: boolean;
}

export interface V10PresentationSendRequestToProposal {
  /**
   * Verifier choice to auto-verify proof presentation
   * @example false
   */
  auto_verify?: boolean;
  /**
   * Whether to trace event (default false)
   * @example false
   */
  trace?: boolean;
}

export interface V20CredAttrSpec {
  /**
   * MIME type: omit for (null) default
   * @example "image/jpeg"
   */
  'mime-type'?: string | null;
  /**
   * Attribute name
   * @example "favourite_drink"
   */
  name: string;
  /**
   * Attribute value: base64-encode if MIME type is present
   * @example "martini"
   */
  value: string;
}

export interface V20CredBoundOfferRequest {
  /** Optional content for counter-proposal */
  counter_preview?: V20CredPreview;
  /** Credential specification criteria by format */
  filter?: V20CredFilter;
}

export interface V20CredExFree {
  /** Whether to remove the credential exchange record on completion (overrides --preserve-exchange-records configuration setting) */
  auto_remove?: boolean;
  /** Human-readable comment */
  comment?: string | null;
  /**
   * Connection identifier
   * @format uuid
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  connection_id: string;
  credential_preview?: V20CredPreview;
  /** Credential specification criteria by format */
  filter: V20CredFilter;
  /** Record trace information, based on agent configuration */
  trace?: boolean;
}

export interface V20CredExRecord {
  /**
   * Issuer choice to issue to request in this credential exchange
   * @example false
   */
  auto_issue?: boolean;
  /**
   * Holder choice to accept offer in this credential exchange
   * @example false
   */
  auto_offer?: boolean;
  /**
   * Issuer choice to remove this credential exchange record when complete
   * @example false
   */
  auto_remove?: boolean;
  /** Attachment content by format for proposal, offer, request, and issue */
  readonly by_format?: V20CredExRecordByFormat;
  /**
   * Connection identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  connection_id?: string;
  /**
   * Time of record creation
   * @pattern ^\d{4}-\d\d-\d\d[T ]\d\d:\d\d(?:\:(?:\d\d(?:\.\d{1,6})?))?(?:[+-]\d\d:?\d\d|Z|)$
   * @example "2021-12-31T23:59:59Z"
   */
  created_at?: string;
  /**
   * Credential exchange identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  cred_ex_id?: string;
  /** Serialized credential issue message */
  cred_issue?: V20CredIssue;
  /** Credential offer message */
  cred_offer?: V20CredOffer;
  /** Credential preview from credential proposal */
  readonly cred_preview?: V20CredPreview;
  /** Credential proposal message */
  cred_proposal?: V20CredProposal;
  /** Serialized credential request message */
  cred_request?: V20CredRequest;
  /**
   * Error message
   * @example "The front fell off"
   */
  error_msg?: string;
  /**
   * Issue-credential exchange initiator: self or external
   * @example "self"
   */
  initiator?: 'self' | 'external';
  /**
   * Parent thread identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  parent_thread_id?: string;
  /**
   * Issue-credential exchange role: holder or issuer
   * @example "issuer"
   */
  role?: 'issuer' | 'holder';
  /**
   * Issue-credential exchange state
   * @example "done"
   */
  state?:
    | 'proposal-sent'
    | 'proposal-received'
    | 'offer-sent'
    | 'offer-received'
    | 'request-sent'
    | 'request-received'
    | 'credential-issued'
    | 'credential-received'
    | 'done'
    | 'credential-revoked'
    | 'abandoned'
    | 'deleted';
  /**
   * Thread identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  thread_id?: string;
  /** Record trace information, based on agent configuration */
  trace?: boolean;
  /**
   * Time of last record update
   * @pattern ^\d{4}-\d\d-\d\d[T ]\d\d:\d\d(?:\:(?:\d\d(?:\.\d{1,6})?))?(?:[+-]\d\d:?\d\d|Z|)$
   * @example "2021-12-31T23:59:59Z"
   */
  updated_at?: string;
}

export interface V20CredExRecordByFormat {
  cred_issue?: object;
  cred_offer?: object;
  cred_proposal?: object;
  cred_request?: object;
}

export interface V20CredExRecordDetail {
  /** Credential exchange record */
  cred_ex_record?: V20CredExRecord;
  indy?: V20CredExRecordIndy;
  ld_proof?: V20CredExRecordLDProof;
}

export interface V20CredExRecordIndy {
  /**
   * Time of record creation
   * @pattern ^\d{4}-\d\d-\d\d[T ]\d\d:\d\d(?:\:(?:\d\d(?:\.\d{1,6})?))?(?:[+-]\d\d:?\d\d|Z|)$
   * @example "2021-12-31T23:59:59Z"
   */
  created_at?: string;
  /**
   * Corresponding v2.0 credential exchange record identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  cred_ex_id?: string;
  /**
   * Record identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  cred_ex_indy_id?: string;
  /**
   * Credential identifier stored in wallet
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  cred_id_stored?: string;
  /** Credential request metadata for indy holder */
  cred_request_metadata?: object;
  /**
   * Credential revocation identifier within revocation registry
   * @pattern ^[1-9][0-9]*$
   * @example "12345"
   */
  cred_rev_id?: string;
  /**
   * Revocation registry identifier
   * @pattern ^([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):4:([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):3:CL:(([1-9][0-9]*)|([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+))(:.+)?:CL_ACCUM:(.+$)
   * @example "WgWxqztrNooG92RXvxSTWv:4:WgWxqztrNooG92RXvxSTWv:3:CL:20:tag:CL_ACCUM:0"
   */
  rev_reg_id?: string;
  /**
   * Current record state
   * @example "active"
   */
  state?: string;
  /**
   * Time of last record update
   * @pattern ^\d{4}-\d\d-\d\d[T ]\d\d:\d\d(?:\:(?:\d\d(?:\.\d{1,6})?))?(?:[+-]\d\d:?\d\d|Z|)$
   * @example "2021-12-31T23:59:59Z"
   */
  updated_at?: string;
}

export interface V20CredExRecordLDProof {
  /**
   * Time of record creation
   * @pattern ^\d{4}-\d\d-\d\d[T ]\d\d:\d\d(?:\:(?:\d\d(?:\.\d{1,6})?))?(?:[+-]\d\d:?\d\d|Z|)$
   * @example "2021-12-31T23:59:59Z"
   */
  created_at?: string;
  /**
   * Corresponding v2.0 credential exchange record identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  cred_ex_id?: string;
  /**
   * Record identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  cred_ex_ld_proof_id?: string;
  /**
   * Credential identifier stored in wallet
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  cred_id_stored?: string;
  /**
   * Current record state
   * @example "active"
   */
  state?: string;
  /**
   * Time of last record update
   * @pattern ^\d{4}-\d\d-\d\d[T ]\d\d:\d\d(?:\:(?:\d\d(?:\.\d{1,6})?))?(?:[+-]\d\d:?\d\d|Z|)$
   * @example "2021-12-31T23:59:59Z"
   */
  updated_at?: string;
}

export interface V20CredExRecordListResult {
  /** Credential exchange records and corresponding detail records */
  results?: V20CredExRecordDetail[];
}

export interface V20CredFilter {
  /** Credential filter for indy */
  indy?: V20CredFilterIndy;
  /** Credential filter for linked data proof */
  ld_proof?: LDProofVCDetail;
}

export interface V20CredFilterIndy {
  /**
   * Credential definition identifier
   * @pattern ^([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):3:CL:(([1-9][0-9]*)|([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+)):(.+)?$
   * @example "WgWxqztrNooG92RXvxSTWv:3:CL:20:tag"
   */
  cred_def_id?: string;
  /**
   * Credential issuer DID
   * @pattern ^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$
   * @example "WgWxqztrNooG92RXvxSTWv"
   */
  issuer_did?: string;
  /**
   * Schema identifier
   * @pattern ^[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+$
   * @example "WgWxqztrNooG92RXvxSTWv:2:schema_name:1.0"
   */
  schema_id?: string;
  /**
   * Schema issuer DID
   * @pattern ^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$
   * @example "WgWxqztrNooG92RXvxSTWv"
   */
  schema_issuer_did?: string;
  /**
   * Schema name
   * @example "preferences"
   */
  schema_name?: string;
  /**
   * Schema version
   * @pattern ^[0-9.]+$
   * @example "1.0"
   */
  schema_version?: string;
}

export interface V20CredFilterLDProof {
  /** Credential filter for linked data proof */
  ld_proof: LDProofVCDetail;
}

export interface V20CredFormat {
  /**
   * Attachment identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  attach_id: string;
  /**
   * Attachment format specifier
   * @example "aries/ld-proof-vc-detail@v1.0"
   */
  format: string;
}

export interface V20CredIssue {
  /**
   * Message identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  '@id'?: string;
  /**
   * Message type
   * @example "https://didcomm.org/my-family/1.0/my-message-type"
   */
  readonly '@type'?: string;
  /** Human-readable comment */
  comment?: string | null;
  /** Credential attachments */
  'credentials~attach': AttachDecorator[];
  /** Acceptable attachment formats */
  formats: V20CredFormat[];
  /**
   * Issuer-unique identifier to coordinate credential replacement
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  replacement_id?: string;
}

export interface V20CredIssueProblemReportRequest {
  description: string;
}

export interface V20CredIssueRequest {
  /** Human-readable comment */
  comment?: string | null;
}

export interface V20CredOffer {
  /**
   * Message identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  '@id'?: string;
  /**
   * Message type
   * @example "https://didcomm.org/my-family/1.0/my-message-type"
   */
  readonly '@type'?: string;
  /** Human-readable comment */
  comment?: string | null;
  credential_preview?: V20CredPreview;
  /** Acceptable credential formats */
  formats: V20CredFormat[];
  /** Offer attachments */
  'offers~attach': AttachDecorator[];
  /**
   * Issuer-unique identifier to coordinate credential replacement
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  replacement_id?: string;
}

export interface V20CredOfferConnFreeRequest {
  /** Whether to respond automatically to credential requests, creating and issuing requested credentials */
  auto_issue?: boolean;
  /** Whether to remove the credential exchange record on completion (overrides --preserve-exchange-records configuration setting) */
  auto_remove?: boolean;
  /** Human-readable comment */
  comment?: string | null;
  credential_preview?: V20CredPreview;
  /** Credential specification criteria by format */
  filter: V20CredFilter;
  /** Record trace information, based on agent configuration */
  trace?: boolean;
}

export interface V20CredOfferRequest {
  /** Whether to respond automatically to credential requests, creating and issuing requested credentials */
  auto_issue?: boolean;
  /** Whether to remove the credential exchange record on completion (overrides --preserve-exchange-records configuration setting) */
  auto_remove?: boolean;
  /** Human-readable comment */
  comment?: string | null;
  /**
   * Connection identifier
   * @format uuid
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  connection_id: string;
  credential_preview?: V20CredPreview;
  /** Credential specification criteria by format */
  filter: V20CredFilter;
  /** Record trace information, based on agent configuration */
  trace?: boolean;
}

export interface V20CredPreview {
  /**
   * Message type identifier
   * @example "issue-credential/2.0/credential-preview"
   */
  '@type'?: string;
  attributes: V20CredAttrSpec[];
}

export interface V20CredProposal {
  /**
   * Message identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  '@id'?: string;
  /**
   * Message type
   * @example "https://didcomm.org/my-family/1.0/my-message-type"
   */
  readonly '@type'?: string;
  /** Human-readable comment */
  comment?: string | null;
  /** Credential preview */
  credential_preview?: V20CredPreview;
  /** Credential filter per acceptable format on corresponding identifier */
  'filters~attach': AttachDecorator[];
  /** Attachment formats */
  formats: V20CredFormat[];
}

export interface V20CredRequest {
  /**
   * Message identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  '@id'?: string;
  /**
   * Message type
   * @example "https://didcomm.org/my-family/1.0/my-message-type"
   */
  readonly '@type'?: string;
  /** Human-readable comment */
  comment?: string | null;
  /** Acceptable attachment formats */
  formats: V20CredFormat[];
  /** Request attachments */
  'requests~attach': AttachDecorator[];
}

export interface V20CredRequestFree {
  /** Whether to remove the credential exchange record on completion (overrides --preserve-exchange-records configuration setting) */
  auto_remove?: boolean;
  /** Human-readable comment */
  comment?: string | null;
  /**
   * Connection identifier
   * @format uuid
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  connection_id: string;
  /** Credential specification criteria by format */
  filter: V20CredFilterLDProof;
  /**
   * Holder DID to substitute for the credentialSubject.id
   * @example "did:key:ahsdkjahsdkjhaskjdhakjshdkajhsdkjahs"
   */
  holder_did?: string | null;
  /**
   * Whether to trace event (default false)
   * @example false
   */
  trace?: boolean;
}

export interface V20CredRequestRequest {
  /**
   * Holder DID to substitute for the credentialSubject.id
   * @example "did:key:ahsdkjahsdkjhaskjdhakjshdkajhsdkjahs"
   */
  holder_did?: string | null;
}

export interface V20CredStoreRequest {
  credential_id?: string;
}

export interface V20DiscoveryExchangeListResult {
  results?: V20DiscoveryRecord[];
}

export interface V20DiscoveryExchangeResult {
  /** Discover Features v2.0 exchange record */
  results?: V20DiscoveryRecord;
}

export interface V20DiscoveryRecord {
  /**
   * Connection identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  connection_id?: string;
  /**
   * Time of record creation
   * @pattern ^\d{4}-\d\d-\d\d[T ]\d\d:\d\d(?:\:(?:\d\d(?:\.\d{1,6})?))?(?:[+-]\d\d:?\d\d|Z|)$
   * @example "2021-12-31T23:59:59Z"
   */
  created_at?: string;
  /** Disclosures message */
  disclosures?: Disclosures;
  /**
   * Credential exchange identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  discovery_exchange_id?: string;
  /** Queries message */
  queries_msg?: Queries;
  /**
   * Current record state
   * @example "active"
   */
  state?: string;
  /**
   * Thread identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  thread_id?: string;
  /** Record trace information, based on agent configuration */
  trace?: boolean;
  /**
   * Time of last record update
   * @pattern ^\d{4}-\d\d-\d\d[T ]\d\d:\d\d(?:\:(?:\d\d(?:\.\d{1,6})?))?(?:[+-]\d\d:?\d\d|Z|)$
   * @example "2021-12-31T23:59:59Z"
   */
  updated_at?: string;
}

export interface V20IssueCredSchemaCore {
  /** Whether to remove the credential exchange record on completion (overrides --preserve-exchange-records configuration setting) */
  auto_remove?: boolean;
  /** Human-readable comment */
  comment?: string | null;
  credential_preview?: V20CredPreview;
  /** Credential specification criteria by format */
  filter: V20CredFilter;
  /** Record trace information, based on agent configuration */
  trace?: boolean;
}

export type V20IssueCredentialModuleResponse = object;

export interface V20Pres {
  /**
   * Message identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  '@id'?: string;
  /**
   * Message type
   * @example "https://didcomm.org/my-family/1.0/my-message-type"
   */
  readonly '@type'?: string;
  /** Human-readable comment */
  comment?: string | null;
  /** Acceptable attachment formats */
  formats: V20PresFormat[];
  'presentations~attach': AttachDecorator[];
}

export interface V20PresCreateRequestRequest {
  /**
   * Verifier choice to auto-verify proof presentation
   * @example false
   */
  auto_verify?: boolean;
  comment?: string | null;
  presentation_request: V20PresRequestByFormat;
  /**
   * Whether to trace event (default false)
   * @example false
   */
  trace?: boolean;
}

export interface V20PresExRecord {
  /**
   * Prover choice to auto-present proof as verifier requests
   * @example false
   */
  auto_present?: boolean;
  /** Verifier choice to auto-verify proof presentation */
  auto_verify?: boolean;
  /** Attachment content by format for proposal, request, and presentation */
  readonly by_format?: V20PresExRecordByFormat;
  /**
   * Connection identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  connection_id?: string;
  /**
   * Time of record creation
   * @pattern ^\d{4}-\d\d-\d\d[T ]\d\d:\d\d(?:\:(?:\d\d(?:\.\d{1,6})?))?(?:[+-]\d\d:?\d\d|Z|)$
   * @example "2021-12-31T23:59:59Z"
   */
  created_at?: string;
  /**
   * Error message
   * @example "Invalid structure"
   */
  error_msg?: string;
  /**
   * Present-proof exchange initiator: self or external
   * @example "self"
   */
  initiator?: 'self' | 'external';
  /** Presentation message */
  pres?: V20Pres;
  /**
   * Presentation exchange identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  pres_ex_id?: string;
  /** Presentation proposal message */
  pres_proposal?: V20PresProposal;
  /** Presentation request message */
  pres_request?: V20PresRequest;
  /**
   * Present-proof exchange role: prover or verifier
   * @example "prover"
   */
  role?: 'prover' | 'verifier';
  /** Present-proof exchange state */
  state?:
    | 'proposal-sent'
    | 'proposal-received'
    | 'request-sent'
    | 'request-received'
    | 'presentation-sent'
    | 'presentation-received'
    | 'done'
    | 'abandoned'
    | 'deleted';
  /**
   * Thread identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  thread_id?: string;
  /** Record trace information, based on agent configuration */
  trace?: boolean;
  /**
   * Time of last record update
   * @pattern ^\d{4}-\d\d-\d\d[T ]\d\d:\d\d(?:\:(?:\d\d(?:\.\d{1,6})?))?(?:[+-]\d\d:?\d\d|Z|)$
   * @example "2021-12-31T23:59:59Z"
   */
  updated_at?: string;
  /**
   * Whether presentation is verified: 'true' or 'false'
   * @example "true"
   */
  verified?: 'true' | 'false';
  verified_msgs?: string[];
}

export interface V20PresExRecordByFormat {
  pres?: object;
  pres_proposal?: object;
  pres_request?: object;
}

export interface V20PresExRecordList {
  /** Presentation exchange records */
  results?: V20PresExRecord[];
}

export interface V20PresFormat {
  /**
   * Attachment identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  attach_id: string;
  /**
   * Attachment format specifier
   * @example "dif/presentation-exchange/submission@v1.0"
   */
  format: string;
}

export interface V20PresProblemReportRequest {
  description: string;
}

export interface V20PresProposal {
  /**
   * Message identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  '@id'?: string;
  /**
   * Message type
   * @example "https://didcomm.org/my-family/1.0/my-message-type"
   */
  readonly '@type'?: string;
  /** Human-readable comment */
  comment?: string;
  formats: V20PresFormat[];
  /** Attachment per acceptable format on corresponding identifier */
  'proposals~attach': AttachDecorator[];
}

export interface V20PresProposalByFormat {
  /** Presentation proposal for DIF */
  dif?: DIFProofProposal;
  /** Presentation proposal for indy */
  indy?: IndyProofRequest;
}

export interface V20PresProposalRequest {
  /** Whether to respond automatically to presentation requests, building and presenting requested proof */
  auto_present?: boolean;
  /** Human-readable comment */
  comment?: string | null;
  /**
   * Connection identifier
   * @format uuid
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  connection_id: string;
  presentation_proposal: V20PresProposalByFormat;
  /**
   * Whether to trace event (default false)
   * @example false
   */
  trace?: boolean;
}

export interface V20PresRequest {
  /**
   * Message identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  '@id'?: string;
  /**
   * Message type
   * @example "https://didcomm.org/my-family/1.0/my-message-type"
   */
  readonly '@type'?: string;
  /** Human-readable comment */
  comment?: string;
  formats: V20PresFormat[];
  /** Attachment per acceptable format on corresponding identifier */
  'request_presentations~attach': AttachDecorator[];
  /** Whether verifier will send confirmation ack */
  will_confirm?: boolean;
}

export interface V20PresRequestByFormat {
  /** Presentation request for DIF */
  dif?: DIFProofRequest;
  /** Presentation request for indy */
  indy?: IndyProofRequest;
}

export interface V20PresSendRequestRequest {
  /**
   * Verifier choice to auto-verify proof presentation
   * @example false
   */
  auto_verify?: boolean;
  comment?: string | null;
  /**
   * Connection identifier
   * @format uuid
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  connection_id: string;
  presentation_request: V20PresRequestByFormat;
  /**
   * Whether to trace event (default false)
   * @example false
   */
  trace?: boolean;
}

export interface V20PresSpecByFormatRequest {
  /** Optional Presentation specification for DIF, overrides the PresentionExchange record's PresRequest */
  dif?: DIFPresSpec;
  /** Presentation specification for indy */
  indy?: IndyPresSpec;
  /** Record trace information, based on agent configuration */
  trace?: boolean;
}

export type V20PresentProofModuleResponse = object;

export interface V20PresentationSendRequestToProposal {
  /**
   * Verifier choice to auto-verify proof presentation
   * @example false
   */
  auto_verify?: boolean;
  /**
   * Whether to trace event (default false)
   * @example false
   */
  trace?: boolean;
}

export interface VCRecord {
  contexts?: string[];
  cred_tags?: Record<string, string>;
  /** (JSON-serializable) credential value */
  cred_value?: object;
  expanded_types?: string[];
  /**
   * Credential identifier
   * @example "http://example.edu/credentials/3732"
   */
  given_id?: string;
  /**
   * Issuer identifier
   * @example "https://example.edu/issuers/14"
   */
  issuer_id?: string;
  proof_types?: string[];
  /**
   * Record identifier
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  record_id?: string;
  schema_ids?: string[];
  subject_ids?: string[];
}

export interface VCRecordList {
  results?: VCRecord[];
}

export interface VerifyRequest {
  /** Signed document */
  doc: SignedDoc;
  /** Verkey to use for doc verification */
  verkey?: string;
}

export interface VerifyResponse {
  /** Error text */
  error?: string;
  valid: boolean;
}

export interface W3CCredentialsListRequest {
  contexts?: string[];
  /** Given credential id to match */
  given_id?: string;
  /** Credential issuer identifier to match */
  issuer_id?: string;
  /**
   * Maximum number of results to return
   * @format int32
   */
  max_results?: number;
  proof_types?: string[];
  /** Schema identifiers, all of which to match */
  schema_ids?: string[];
  /** Subject identifiers, all of which to match */
  subject_ids?: string[];
  /** Tag filter */
  tag_query?: Record<string, string>;
  types?: string[];
}

export interface WalletList {
  /** List of wallet records */
  results?: WalletRecord[];
}

export type WalletModuleResponse = object;

export interface WalletRecord {
  /**
   * Time of record creation
   * @pattern ^\d{4}-\d\d-\d\d[T ]\d\d:\d\d(?:\:(?:\d\d(?:\.\d{1,6})?))?(?:[+-]\d\d:?\d\d|Z|)$
   * @example "2021-12-31T23:59:59Z"
   */
  created_at?: string;
  /** Mode regarding management of wallet key */
  key_management_mode: 'managed' | 'unmanaged';
  /** Settings for this wallet. */
  settings?: object;
  /**
   * Current record state
   * @example "active"
   */
  state?: string;
  /**
   * Time of last record update
   * @pattern ^\d{4}-\d\d-\d\d[T ]\d\d:\d\d(?:\:(?:\d\d(?:\.\d{1,6})?))?(?:[+-]\d\d:?\d\d|Z|)$
   * @example "2021-12-31T23:59:59Z"
   */
  updated_at?: string;
  /**
   * Wallet record ID
   * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
   */
  wallet_id: string;
}

export interface WriteLedgerRequest {
  ledger_id?: string;
}

export namespace ActionMenu {
  /**
   * No description
   * @tags action-menu
   * @name CloseCreate
   * @summary Close the active menu associated with a connection
   * @request POST:/action-menu/{conn_id}/close
   * @secure
   * @response `200` `ActionMenuModulesResult`
   */
  export namespace CloseCreate {
    export type RequestParams = {
      /**
       * Connection identifier
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      connId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = ActionMenuModulesResult;
  }
  /**
   * No description
   * @tags action-menu
   * @name FetchCreate
   * @summary Fetch the active menu
   * @request POST:/action-menu/{conn_id}/fetch
   * @secure
   * @response `200` `ActionMenuFetchResult`
   */
  export namespace FetchCreate {
    export type RequestParams = {
      /**
       * Connection identifier
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      connId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = ActionMenuFetchResult;
  }
  /**
   * No description
   * @tags action-menu
   * @name PerformCreate
   * @summary Perform an action associated with the active menu
   * @request POST:/action-menu/{conn_id}/perform
   * @secure
   * @response `200` `ActionMenuModulesResult`
   */
  export namespace PerformCreate {
    export type RequestParams = {
      /**
       * Connection identifier
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      connId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = PerformRequest;
    export type RequestHeaders = {};
    export type ResponseBody = ActionMenuModulesResult;
  }
  /**
   * No description
   * @tags action-menu
   * @name RequestCreate
   * @summary Request the active menu
   * @request POST:/action-menu/{conn_id}/request
   * @secure
   * @response `200` `ActionMenuModulesResult`
   */
  export namespace RequestCreate {
    export type RequestParams = {
      /**
       * Connection identifier
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      connId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = ActionMenuModulesResult;
  }
  /**
   * No description
   * @tags action-menu
   * @name SendMenuCreate
   * @summary Send an action menu to a connection
   * @request POST:/action-menu/{conn_id}/send-menu
   * @secure
   * @response `200` `ActionMenuModulesResult`
   */
  export namespace SendMenuCreate {
    export type RequestParams = {
      /**
       * Connection identifier
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      connId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = SendMenu;
    export type RequestHeaders = {};
    export type ResponseBody = ActionMenuModulesResult;
  }
}

export namespace Connections {
  /**
   * No description
   * @tags connection
   * @name ConnectionsList
   * @summary Query agent-to-agent connections
   * @request GET:/connections
   * @secure
   * @response `200` `ConnectionList`
   */
  export namespace ConnectionsList {
    export type RequestParams = {};
    export type RequestQuery = {
      /**
       * Alias
       * @example "Barry"
       */
      alias?: string;
      /**
       * Connection protocol used
       * @example "connections/1.0"
       */
      connection_protocol?: 'connections/1.0' | 'didexchange/1.0';
      /**
       * invitation key
       * @pattern ^[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{43,44}$
       * @example "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV"
       */
      invitation_key?: string;
      /**
       * Identifier of the associated Invitation Mesage
       * @format uuid
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      invitation_msg_id?: string;
      /**
       * My DID
       * @pattern ^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$
       * @example "WgWxqztrNooG92RXvxSTWv"
       */
      my_did?: string;
      /** Connection state */
      state?: 'init' | 'active' | 'completed' | 'start' | 'response' | 'invitation' | 'abandoned' | 'request' | 'error';
      /**
       * Their DID
       * @pattern ^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$
       * @example "WgWxqztrNooG92RXvxSTWv"
       */
      their_did?: string;
      /**
       * Their Public DID
       * @pattern ^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$
       * @example "WgWxqztrNooG92RXvxSTWv"
       */
      their_public_did?: string;
      /**
       * Their role in the connection protocol
       * @example "invitee"
       */
      their_role?: 'invitee' | 'requester' | 'inviter' | 'responder';
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = ConnectionList;
  }
  /**
   * No description
   * @tags connection
   * @name CreateInvitationCreate
   * @summary Create a new connection invitation
   * @request POST:/connections/create-invitation
   * @secure
   * @response `200` `InvitationResult`
   */
  export namespace CreateInvitationCreate {
    export type RequestParams = {};
    export type RequestQuery = {
      /**
       * Alias
       * @example "Barry"
       */
      alias?: string;
      /** Auto-accept connection (defaults to configuration) */
      auto_accept?: boolean;
      /** Create invitation for multiple use (default false) */
      multi_use?: boolean;
      /** Create invitation from public DID (default false) */
      public?: boolean;
    };
    export type RequestBody = CreateInvitationRequest;
    export type RequestHeaders = {};
    export type ResponseBody = InvitationResult;
  }
  /**
   * No description
   * @tags connection
   * @name CreateStaticCreate
   * @summary Create a new static connection
   * @request POST:/connections/create-static
   * @secure
   * @response `200` `ConnectionStaticResult`
   */
  export namespace CreateStaticCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = ConnectionStaticRequest;
    export type RequestHeaders = {};
    export type ResponseBody = ConnectionStaticResult;
  }
  /**
   * No description
   * @tags connection
   * @name ReceiveInvitationCreate
   * @summary Receive a new connection invitation
   * @request POST:/connections/receive-invitation
   * @secure
   * @response `200` `ConnRecord`
   */
  export namespace ReceiveInvitationCreate {
    export type RequestParams = {};
    export type RequestQuery = {
      /**
       * Alias
       * @example "Barry"
       */
      alias?: string;
      /** Auto-accept connection (defaults to configuration) */
      auto_accept?: boolean;
      /**
       * Identifier for active mediation record to be used
       * @pattern [a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      mediation_id?: string;
    };
    export type RequestBody = ReceiveInvitationRequest;
    export type RequestHeaders = {};
    export type ResponseBody = ConnRecord;
  }
  /**
   * No description
   * @tags connection
   * @name ConnectionsDetail
   * @summary Fetch a single connection record
   * @request GET:/connections/{conn_id}
   * @secure
   * @response `200` `ConnRecord`
   */
  export namespace ConnectionsDetail {
    export type RequestParams = {
      /**
       * Connection identifier
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      connId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = ConnRecord;
  }
  /**
   * No description
   * @tags connection
   * @name ConnectionsDelete
   * @summary Remove an existing connection record
   * @request DELETE:/connections/{conn_id}
   * @secure
   * @response `200` `ConnectionModuleResponse`
   */
  export namespace ConnectionsDelete {
    export type RequestParams = {
      /**
       * Connection identifier
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      connId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = ConnectionModuleResponse;
  }
  /**
   * No description
   * @tags connection
   * @name AcceptInvitationCreate
   * @summary Accept a stored connection invitation
   * @request POST:/connections/{conn_id}/accept-invitation
   * @secure
   * @response `200` `ConnRecord`
   */
  export namespace AcceptInvitationCreate {
    export type RequestParams = {
      /**
       * Connection identifier
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      connId: string;
    };
    export type RequestQuery = {
      /**
       * Identifier for active mediation record to be used
       * @pattern [a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      mediation_id?: string;
      /**
       * My URL endpoint
       * @pattern ^[A-Za-z0-9\.\-\+]+://([A-Za-z0-9][.A-Za-z0-9-_]+[A-Za-z0-9])+(:[1-9][0-9]*)?(/[^?&#]+)?$
       * @example "https://myhost:8021"
       */
      my_endpoint?: string;
      /**
       * Label for connection
       * @example "Broker"
       */
      my_label?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = ConnRecord;
  }
  /**
   * No description
   * @tags connection
   * @name AcceptRequestCreate
   * @summary Accept a stored connection request
   * @request POST:/connections/{conn_id}/accept-request
   * @secure
   * @response `200` `ConnRecord`
   */
  export namespace AcceptRequestCreate {
    export type RequestParams = {
      /**
       * Connection identifier
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      connId: string;
    };
    export type RequestQuery = {
      /**
       * My URL endpoint
       * @pattern ^[A-Za-z0-9\.\-\+]+://([A-Za-z0-9][.A-Za-z0-9-_]+[A-Za-z0-9])+(:[1-9][0-9]*)?(/[^?&#]+)?$
       * @example "https://myhost:8021"
       */
      my_endpoint?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = ConnRecord;
  }
  /**
   * No description
   * @tags connection
   * @name EndpointsDetail
   * @summary Fetch connection remote endpoint
   * @request GET:/connections/{conn_id}/endpoints
   * @secure
   * @response `200` `EndpointsResult`
   */
  export namespace EndpointsDetail {
    export type RequestParams = {
      /**
       * Connection identifier
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      connId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = EndpointsResult;
  }
  /**
   * No description
   * @tags connection
   * @name EstablishInboundCreate
   * @summary Assign another connection as the inbound connection
   * @request POST:/connections/{conn_id}/establish-inbound/{ref_id}
   * @secure
   * @response `200` `ConnectionModuleResponse`
   */
  export namespace EstablishInboundCreate {
    export type RequestParams = {
      /**
       * Connection identifier
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      connId: string;
      /**
       * Inbound connection identifier
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      refId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = ConnectionModuleResponse;
  }
  /**
   * No description
   * @tags connection
   * @name MetadataDetail
   * @summary Fetch connection metadata
   * @request GET:/connections/{conn_id}/metadata
   * @secure
   * @response `200` `ConnectionMetadata`
   */
  export namespace MetadataDetail {
    export type RequestParams = {
      /**
       * Connection identifier
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      connId: string;
    };
    export type RequestQuery = {
      /** Key to retrieve. */
      key?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = ConnectionMetadata;
  }
  /**
   * No description
   * @tags connection
   * @name MetadataCreate
   * @summary Set connection metadata
   * @request POST:/connections/{conn_id}/metadata
   * @secure
   * @response `200` `ConnectionMetadata`
   */
  export namespace MetadataCreate {
    export type RequestParams = {
      /**
       * Connection identifier
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      connId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = ConnectionMetadataSetRequest;
    export type RequestHeaders = {};
    export type ResponseBody = ConnectionMetadata;
  }
  /**
   * No description
   * @tags basicmessage
   * @name SendMessageCreate
   * @summary Send a basic message to a connection
   * @request POST:/connections/{conn_id}/send-message
   * @secure
   * @response `200` `BasicMessageModuleResponse`
   */
  export namespace SendMessageCreate {
    export type RequestParams = {
      /**
       * Connection identifier
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      connId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = SendMessage;
    export type RequestHeaders = {};
    export type ResponseBody = BasicMessageModuleResponse;
  }
  /**
   * No description
   * @tags trustping
   * @name SendPingCreate
   * @summary Send a trust ping to a connection
   * @request POST:/connections/{conn_id}/send-ping
   * @secure
   * @response `200` `PingRequestResponse`
   */
  export namespace SendPingCreate {
    export type RequestParams = {
      /**
       * Connection identifier
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      connId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = PingRequest;
    export type RequestHeaders = {};
    export type ResponseBody = PingRequestResponse;
  }
  /**
   * No description
   * @tags introduction
   * @name StartIntroductionCreate
   * @summary Start an introduction between two connections
   * @request POST:/connections/{conn_id}/start-introduction
   * @secure
   * @response `200` `IntroModuleResponse`
   */
  export namespace StartIntroductionCreate {
    export type RequestParams = {
      /**
       * Connection identifier
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      connId: string;
    };
    export type RequestQuery = {
      /**
       * Target connection identifier
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      target_connection_id: string;
      /**
       * Message
       * @example "Allow me to introduce ..."
       */
      message?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = IntroModuleResponse;
  }
}

export namespace CredentialDefinitions {
  /**
   * No description
   * @tags credential-definition
   * @name CredentialDefinitionsCreate
   * @summary Sends a credential definition to the ledger
   * @request POST:/credential-definitions
   * @secure
   * @response `200` `TxnOrCredentialDefinitionSendResult`
   */
  export namespace CredentialDefinitionsCreate {
    export type RequestParams = {};
    export type RequestQuery = {
      /**
       * Connection identifier
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      conn_id?: string;
      /** Create Transaction For Endorser's signature */
      create_transaction_for_endorser?: boolean;
    };
    export type RequestBody = CredentialDefinitionSendRequest;
    export type RequestHeaders = {};
    export type ResponseBody = TxnOrCredentialDefinitionSendResult;
  }
  /**
   * No description
   * @tags credential-definition
   * @name CreatedList
   * @summary Search for matching credential definitions that agent originated
   * @request GET:/credential-definitions/created
   * @secure
   * @response `200` `CredentialDefinitionsCreatedResult`
   */
  export namespace CreatedList {
    export type RequestParams = {};
    export type RequestQuery = {
      /**
       * Credential definition id
       * @pattern ^([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):3:CL:(([1-9][0-9]*)|([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+)):(.+)?$
       * @example "WgWxqztrNooG92RXvxSTWv:3:CL:20:tag"
       */
      cred_def_id?: string;
      /**
       * Issuer DID
       * @pattern ^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$
       * @example "WgWxqztrNooG92RXvxSTWv"
       */
      issuer_did?: string;
      /**
       * Schema identifier
       * @pattern ^[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+$
       * @example "WgWxqztrNooG92RXvxSTWv:2:schema_name:1.0"
       */
      schema_id?: string;
      /**
       * Schema issuer DID
       * @pattern ^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$
       * @example "WgWxqztrNooG92RXvxSTWv"
       */
      schema_issuer_did?: string;
      /**
       * Schema name
       * @example "membership"
       */
      schema_name?: string;
      /**
       * Schema version
       * @pattern ^[0-9.]+$
       * @example "1.0"
       */
      schema_version?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = CredentialDefinitionsCreatedResult;
  }
  /**
   * No description
   * @tags credential-definition
   * @name CredentialDefinitionsDetail
   * @summary Gets a credential definition from the ledger
   * @request GET:/credential-definitions/{cred_def_id}
   * @secure
   * @response `200` `CredentialDefinitionGetResult`
   */
  export namespace CredentialDefinitionsDetail {
    export type RequestParams = {
      /**
       * Credential definition identifier
       * @pattern ^([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):3:CL:(([1-9][0-9]*)|([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+)):(.+)?$
       * @example "WgWxqztrNooG92RXvxSTWv:3:CL:20:tag"
       */
      credDefId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = CredentialDefinitionGetResult;
  }
  /**
   * No description
   * @tags credential-definition
   * @name WriteRecordCreate
   * @summary Writes a credential definition non-secret record to the wallet
   * @request POST:/credential-definitions/{cred_def_id}/write_record
   * @secure
   * @response `200` `CredentialDefinitionGetResult`
   */
  export namespace WriteRecordCreate {
    export type RequestParams = {
      /**
       * Credential definition identifier
       * @pattern ^([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):3:CL:(([1-9][0-9]*)|([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+)):(.+)?$
       * @example "WgWxqztrNooG92RXvxSTWv:3:CL:20:tag"
       */
      credDefId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = CredentialDefinitionGetResult;
  }
}

export namespace Credential {
  /**
   * No description
   * @tags credentials
   * @name MimeTypesDetail
   * @summary Get attribute MIME types from wallet
   * @request GET:/credential/mime-types/{credential_id}
   * @secure
   * @response `200` `AttributeMimeTypesResult`
   */
  export namespace MimeTypesDetail {
    export type RequestParams = {
      /**
       * Credential identifier
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      credentialId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = AttributeMimeTypesResult;
  }
  /**
   * No description
   * @tags credentials
   * @name RevokedDetail
   * @summary Query credential revocation status by id
   * @request GET:/credential/revoked/{credential_id}
   * @secure
   * @response `200` `CredRevokedResult`
   */
  export namespace RevokedDetail {
    export type RequestParams = {
      /**
       * Credential identifier
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      credentialId: string;
    };
    export type RequestQuery = {
      /**
       * Earliest epoch of revocation status interval of interest
       * @pattern ^[0-9]*$
       * @example "0"
       */
      from?: string;
      /**
       * Latest epoch of revocation status interval of interest
       * @pattern ^[0-9]*$
       * @example "0"
       */
      to?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = CredRevokedResult;
  }
  /**
   * No description
   * @tags credentials
   * @name GetCredential
   * @summary Fetch W3C credential from wallet by id
   * @request GET:/credential/w3c/{credential_id}
   * @secure
   * @response `200` `VCRecord`
   */
  export namespace GetCredential {
    export type RequestParams = {
      /**
       * Credential identifier
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      credentialId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = VCRecord;
  }
  /**
   * No description
   * @tags credentials
   * @name DeleteCredential
   * @summary Remove W3C credential from wallet by id
   * @request DELETE:/credential/w3c/{credential_id}
   * @secure
   * @response `200` `HolderModuleResponse`
   */
  export namespace DeleteCredential {
    export type RequestParams = {
      /**
       * Credential identifier
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      credentialId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = HolderModuleResponse;
  }
  /**
   * No description
   * @tags credentials
   * @name CredentialDetail
   * @summary Fetch credential from wallet by id
   * @request GET:/credential/{credential_id}
   * @secure
   * @response `200` `IndyCredInfo`
   */
  export namespace CredentialDetail {
    export type RequestParams = {
      /**
       * Credential identifier
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      credentialId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = IndyCredInfo;
  }
  /**
   * No description
   * @tags credentials
   * @name CredentialDelete
   * @summary Remove credential from wallet by id
   * @request DELETE:/credential/{credential_id}
   * @secure
   * @response `200` `HolderModuleResponse`
   */
  export namespace CredentialDelete {
    export type RequestParams = {
      /**
       * Credential identifier
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      credentialId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = HolderModuleResponse;
  }
}

export namespace Credentials {
  /**
   * No description
   * @tags credentials
   * @name CredentialsList
   * @summary Fetch credentials from wallet
   * @request GET:/credentials
   * @secure
   * @response `200` `CredInfoList`
   */
  export namespace CredentialsList {
    export type RequestParams = {};
    export type RequestQuery = {
      /**
       * Maximum number to retrieve
       * @pattern ^[1-9][0-9]*$
       * @example "1"
       */
      count?: string;
      /**
       * Start index
       * @pattern ^[0-9]*$
       * @example "0"
       */
      start?: string;
      /**
       * (JSON) WQL query
       * @pattern ^{.*}$
       * @example "{"attr::name::value": "Alex"}"
       */
      wql?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = CredInfoList;
  }
  /**
   * No description
   * @tags credentials
   * @name PostCredentials
   * @summary Fetch W3C credentials from wallet
   * @request POST:/credentials/w3c
   * @secure
   * @response `200` `VCRecordList`
   */
  export namespace PostCredentials {
    export type RequestParams = {};
    export type RequestQuery = {
      /**
       * Maximum number to retrieve
       * @pattern ^[1-9][0-9]*$
       * @example "1"
       */
      count?: string;
      /**
       * Start index
       * @pattern ^[0-9]*$
       * @example "0"
       */
      start?: string;
      /**
       * (JSON) WQL query
       * @pattern ^{.*}$
       * @example "{"attr::name::value": "Alex"}"
       */
      wql?: string;
    };
    export type RequestBody = W3CCredentialsListRequest;
    export type RequestHeaders = {};
    export type ResponseBody = VCRecordList;
  }
}

export namespace Didexchange {
  /**
   * No description
   * @tags did-exchange
   * @name CreateRequestCreate
   * @summary Create and send a request against public DID's implicit invitation
   * @request POST:/didexchange/create-request
   * @secure
   * @response `200` `ConnRecord`
   */
  export namespace CreateRequestCreate {
    export type RequestParams = {};
    export type RequestQuery = {
      /**
       * Qualified public DID to which to request connection
       * @pattern ^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$|^did:([a-zA-Z0-9_]+):([a-zA-Z0-9_.%-]+(:[a-zA-Z0-9_.%-]+)*)((;[a-zA-Z0-9_.:%-]+=[a-zA-Z0-9_.:%-]*)*)(\/[^#?]*)?([?][^#]*)?(\#.*)?$$
       * @example "did:peer:WgWxqztrNooG92RXvxSTWv"
       */
      their_public_did: string;
      /**
       * Alias for connection
       * @example "Barry"
       */
      alias?: string;
      /**
       * Identifier for active mediation record to be used
       * @pattern [a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      mediation_id?: string;
      /**
       * My URL endpoint
       * @pattern ^[A-Za-z0-9\.\-\+]+://([A-Za-z0-9][.A-Za-z0-9-_]+[A-Za-z0-9])+(:[1-9][0-9]*)?(/[^?&#]+)?$
       * @example "https://myhost:8021"
       */
      my_endpoint?: string;
      /**
       * Label for connection request
       * @example "Broker"
       */
      my_label?: string;
      /** Use public DID for this connection */
      use_public_did?: boolean;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = ConnRecord;
  }
  /**
   * No description
   * @tags did-exchange
   * @name ReceiveRequestCreate
   * @summary Receive request against public DID's implicit invitation
   * @request POST:/didexchange/receive-request
   * @secure
   * @response `200` `ConnRecord`
   */
  export namespace ReceiveRequestCreate {
    export type RequestParams = {};
    export type RequestQuery = {
      /**
       * Alias for connection
       * @example "Barry"
       */
      alias?: string;
      /** Auto-accept connection (defaults to configuration) */
      auto_accept?: boolean;
      /**
       * Identifier for active mediation record to be used
       * @pattern [a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      mediation_id?: string;
      /**
       * My URL endpoint
       * @pattern ^[A-Za-z0-9\.\-\+]+://([A-Za-z0-9][.A-Za-z0-9-_]+[A-Za-z0-9])+(:[1-9][0-9]*)?(/[^?&#]+)?$
       * @example "https://myhost:8021"
       */
      my_endpoint?: string;
    };
    export type RequestBody = DIDXRequest;
    export type RequestHeaders = {};
    export type ResponseBody = ConnRecord;
  }
  /**
   * No description
   * @tags did-exchange
   * @name AcceptInvitationCreate
   * @summary Accept a stored connection invitation
   * @request POST:/didexchange/{conn_id}/accept-invitation
   * @secure
   * @response `200` `ConnRecord`
   */
  export namespace AcceptInvitationCreate {
    export type RequestParams = {
      /**
       * Connection identifier
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      connId: string;
    };
    export type RequestQuery = {
      /**
       * My URL endpoint
       * @pattern ^[A-Za-z0-9\.\-\+]+://([A-Za-z0-9][.A-Za-z0-9-_]+[A-Za-z0-9])+(:[1-9][0-9]*)?(/[^?&#]+)?$
       * @example "https://myhost:8021"
       */
      my_endpoint?: string;
      /**
       * Label for connection request
       * @example "Broker"
       */
      my_label?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = ConnRecord;
  }
  /**
   * No description
   * @tags did-exchange
   * @name AcceptRequestCreate
   * @summary Accept a stored connection request
   * @request POST:/didexchange/{conn_id}/accept-request
   * @secure
   * @response `200` `ConnRecord`
   */
  export namespace AcceptRequestCreate {
    export type RequestParams = {
      /**
       * Connection identifier
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      connId: string;
    };
    export type RequestQuery = {
      /**
       * Identifier for active mediation record to be used
       * @pattern [a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      mediation_id?: string;
      /**
       * My URL endpoint
       * @pattern ^[A-Za-z0-9\.\-\+]+://([A-Za-z0-9][.A-Za-z0-9-_]+[A-Za-z0-9])+(:[1-9][0-9]*)?(/[^?&#]+)?$
       * @example "https://myhost:8021"
       */
      my_endpoint?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = ConnRecord;
  }
}

export namespace DiscoverFeatures20 {
  /**
   * No description
   * @tags discover-features v2.0
   * @name QueriesList
   * @summary Query supported features
   * @request GET:/discover-features-2.0/queries
   * @secure
   * @response `200` `V20DiscoveryExchangeResult`
   */
  export namespace QueriesList {
    export type RequestParams = {};
    export type RequestQuery = {
      /**
       * Connection identifier, if none specified, then the query will provide features for this agent.
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      connection_id?: string;
      /**
       * Goal-code feature-type query
       * @example "*"
       */
      query_goal_code?: string;
      /**
       * Protocol feature-type query
       * @example "*"
       */
      query_protocol?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = V20DiscoveryExchangeResult;
  }
  /**
   * No description
   * @tags discover-features v2.0
   * @name RecordsList
   * @summary Discover Features v2.0 records
   * @request GET:/discover-features-2.0/records
   * @secure
   * @response `200` `V20DiscoveryExchangeListResult`
   */
  export namespace RecordsList {
    export type RequestParams = {};
    export type RequestQuery = {
      /**
       * Connection identifier
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      connection_id?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = V20DiscoveryExchangeListResult;
  }
}

export namespace DiscoverFeatures {
  /**
   * No description
   * @tags discover-features
   * @name QueryList
   * @summary Query supported features
   * @request GET:/discover-features/query
   * @secure
   * @response `200` `V10DiscoveryRecord`
   */
  export namespace QueryList {
    export type RequestParams = {};
    export type RequestQuery = {
      /**
       * Comment
       * @example "test"
       */
      comment?: string;
      /**
       * Connection identifier, if none specified, then the query will provide features for this agent.
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      connection_id?: string;
      /**
       * Protocol feature query
       * @example "*"
       */
      query?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = V10DiscoveryRecord;
  }
  /**
   * No description
   * @tags discover-features
   * @name RecordsList
   * @summary Discover Features records
   * @request GET:/discover-features/records
   * @secure
   * @response `200` `V10DiscoveryExchangeListResult`
   */
  export namespace RecordsList {
    export type RequestParams = {};
    export type RequestQuery = {
      /**
       * Connection identifier
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      connection_id?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = V10DiscoveryExchangeListResult;
  }
}

export namespace IssueCredential20 {
  /**
   * No description
   * @tags issue-credential v2.0
   * @name CreateCreate
   * @summary Create a credential record without sending (generally for use with Out-Of-Band)
   * @request POST:/issue-credential-2.0/create
   * @secure
   * @response `200` `V20CredExRecord`
   */
  export namespace CreateCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = V20IssueCredSchemaCore;
    export type RequestHeaders = {};
    export type ResponseBody = V20CredExRecord;
  }
  /**
   * No description
   * @tags issue-credential v2.0
   * @name CreateOfferCreate
   * @summary Create a credential offer, independent of any proposal or connection
   * @request POST:/issue-credential-2.0/create-offer
   * @secure
   * @response `200` `V20CredExRecord`
   */
  export namespace CreateOfferCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = V20CredOfferConnFreeRequest;
    export type RequestHeaders = {};
    export type ResponseBody = V20CredExRecord;
  }
  /**
   * No description
   * @tags issue-credential v2.0
   * @name RecordsList
   * @summary Fetch all credential exchange records
   * @request GET:/issue-credential-2.0/records
   * @secure
   * @response `200` `V20CredExRecordListResult`
   */
  export namespace RecordsList {
    export type RequestParams = {};
    export type RequestQuery = {
      /**
       * Connection identifier
       * @format uuid
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      connection_id?: string;
      /** Role assigned in credential exchange */
      role?: 'issuer' | 'holder';
      /** Credential exchange state */
      state?:
        | 'proposal-sent'
        | 'proposal-received'
        | 'offer-sent'
        | 'offer-received'
        | 'request-sent'
        | 'request-received'
        | 'credential-issued'
        | 'credential-received'
        | 'done'
        | 'credential-revoked'
        | 'abandoned';
      /**
       * Thread identifier
       * @format uuid
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      thread_id?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = V20CredExRecordListResult;
  }
  /**
   * No description
   * @tags issue-credential v2.0
   * @name RecordsDetail
   * @summary Fetch a single credential exchange record
   * @request GET:/issue-credential-2.0/records/{cred_ex_id}
   * @secure
   * @response `200` `V20CredExRecordDetail`
   */
  export namespace RecordsDetail {
    export type RequestParams = {
      /**
       * Credential exchange identifier
       * @pattern [a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      credExId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = V20CredExRecordDetail;
  }
  /**
   * No description
   * @tags issue-credential v2.0
   * @name RecordsDelete
   * @summary Remove an existing credential exchange record
   * @request DELETE:/issue-credential-2.0/records/{cred_ex_id}
   * @secure
   * @response `200` `V20IssueCredentialModuleResponse`
   */
  export namespace RecordsDelete {
    export type RequestParams = {
      /**
       * Credential exchange identifier
       * @pattern [a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      credExId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = V20IssueCredentialModuleResponse;
  }
  /**
   * No description
   * @tags issue-credential v2.0
   * @name RecordsIssueCreate
   * @summary Send holder a credential
   * @request POST:/issue-credential-2.0/records/{cred_ex_id}/issue
   * @secure
   * @response `200` `V20CredExRecordDetail`
   */
  export namespace RecordsIssueCreate {
    export type RequestParams = {
      /**
       * Credential exchange identifier
       * @pattern [a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      credExId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = V20CredIssueRequest;
    export type RequestHeaders = {};
    export type ResponseBody = V20CredExRecordDetail;
  }
  /**
   * No description
   * @tags issue-credential v2.0
   * @name RecordsProblemReportCreate
   * @summary Send a problem report for credential exchange
   * @request POST:/issue-credential-2.0/records/{cred_ex_id}/problem-report
   * @secure
   * @response `200` `V20IssueCredentialModuleResponse`
   */
  export namespace RecordsProblemReportCreate {
    export type RequestParams = {
      /**
       * Credential exchange identifier
       * @pattern [a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      credExId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = V20CredIssueProblemReportRequest;
    export type RequestHeaders = {};
    export type ResponseBody = V20IssueCredentialModuleResponse;
  }
  /**
   * No description
   * @tags issue-credential v2.0
   * @name RecordsSendOfferCreate
   * @summary Send holder a credential offer in reference to a proposal with preview
   * @request POST:/issue-credential-2.0/records/{cred_ex_id}/send-offer
   * @secure
   * @response `200` `V20CredExRecord`
   */
  export namespace RecordsSendOfferCreate {
    export type RequestParams = {
      /**
       * Credential exchange identifier
       * @pattern [a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      credExId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = V20CredBoundOfferRequest;
    export type RequestHeaders = {};
    export type ResponseBody = V20CredExRecord;
  }
  /**
   * No description
   * @tags issue-credential v2.0
   * @name RecordsSendRequestCreate
   * @summary Send issuer a credential request
   * @request POST:/issue-credential-2.0/records/{cred_ex_id}/send-request
   * @secure
   * @response `200` `V20CredExRecord`
   */
  export namespace RecordsSendRequestCreate {
    export type RequestParams = {
      /**
       * Credential exchange identifier
       * @pattern [a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      credExId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = V20CredRequestRequest;
    export type RequestHeaders = {};
    export type ResponseBody = V20CredExRecord;
  }
  /**
   * No description
   * @tags issue-credential v2.0
   * @name RecordsStoreCreate
   * @summary Store a received credential
   * @request POST:/issue-credential-2.0/records/{cred_ex_id}/store
   * @secure
   * @response `200` `V20CredExRecordDetail`
   */
  export namespace RecordsStoreCreate {
    export type RequestParams = {
      /**
       * Credential exchange identifier
       * @pattern [a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      credExId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = V20CredStoreRequest;
    export type RequestHeaders = {};
    export type ResponseBody = V20CredExRecordDetail;
  }
  /**
   * No description
   * @tags issue-credential v2.0
   * @name SendCreate
   * @summary Send holder a credential, automating entire flow
   * @request POST:/issue-credential-2.0/send
   * @secure
   * @response `200` `V20CredExRecord`
   */
  export namespace SendCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = V20CredExFree;
    export type RequestHeaders = {};
    export type ResponseBody = V20CredExRecord;
  }
  /**
   * No description
   * @tags issue-credential v2.0
   * @name SendOfferCreate
   * @summary Send holder a credential offer, independent of any proposal
   * @request POST:/issue-credential-2.0/send-offer
   * @secure
   * @response `200` `V20CredExRecord`
   */
  export namespace SendOfferCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = V20CredOfferRequest;
    export type RequestHeaders = {};
    export type ResponseBody = V20CredExRecord;
  }
  /**
   * No description
   * @tags issue-credential v2.0
   * @name SendProposalCreate
   * @summary Send issuer a credential proposal
   * @request POST:/issue-credential-2.0/send-proposal
   * @secure
   * @response `200` `V20CredExRecord`
   */
  export namespace SendProposalCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = V20CredExFree;
    export type RequestHeaders = {};
    export type ResponseBody = V20CredExRecord;
  }
  /**
   * No description
   * @tags issue-credential v2.0
   * @name SendRequestCreate
   * @summary Send issuer a credential request not bound to an existing thread. Indy credentials cannot start at a request
   * @request POST:/issue-credential-2.0/send-request
   * @secure
   * @response `200` `V20CredExRecord`
   */
  export namespace SendRequestCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = V20CredRequestFree;
    export type RequestHeaders = {};
    export type ResponseBody = V20CredExRecord;
  }
}

export namespace IssueCredential {
  /**
   * No description
   * @tags issue-credential v1.0
   * @name CreateCreate
   * @summary Create a credential record without sending (generally for use with Out-Of-Band)
   * @request POST:/issue-credential/create
   * @secure
   * @response `200` `V10CredentialExchange`
   */
  export namespace CreateCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = V10CredentialCreate;
    export type RequestHeaders = {};
    export type ResponseBody = V10CredentialExchange;
  }
  /**
   * No description
   * @tags issue-credential v1.0
   * @name CreateOfferCreate
   * @summary Create a credential offer, independent of any proposal or connection
   * @request POST:/issue-credential/create-offer
   * @secure
   * @response `200` `V10CredentialExchange`
   */
  export namespace CreateOfferCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = V10CredentialConnFreeOfferRequest;
    export type RequestHeaders = {};
    export type ResponseBody = V10CredentialExchange;
  }
  /**
   * No description
   * @tags issue-credential v1.0
   * @name RecordsList
   * @summary Fetch all credential exchange records
   * @request GET:/issue-credential/records
   * @secure
   * @response `200` `V10CredentialExchangeListResult`
   */
  export namespace RecordsList {
    export type RequestParams = {};
    export type RequestQuery = {
      /**
       * Connection identifier
       * @format uuid
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      connection_id?: string;
      /** Role assigned in credential exchange */
      role?: 'issuer' | 'holder';
      /** Credential exchange state */
      state?:
        | 'proposal_sent'
        | 'proposal_received'
        | 'offer_sent'
        | 'offer_received'
        | 'request_sent'
        | 'request_received'
        | 'credential_issued'
        | 'credential_received'
        | 'credential_acked'
        | 'credential_revoked'
        | 'abandoned';
      /**
       * Thread identifier
       * @format uuid
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      thread_id?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = V10CredentialExchangeListResult;
  }
  /**
   * No description
   * @tags issue-credential v1.0
   * @name RecordsDetail
   * @summary Fetch a single credential exchange record
   * @request GET:/issue-credential/records/{cred_ex_id}
   * @secure
   * @response `200` `V10CredentialExchange`
   */
  export namespace RecordsDetail {
    export type RequestParams = {
      /**
       * Credential exchange identifier
       * @pattern [a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      credExId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = V10CredentialExchange;
  }
  /**
   * No description
   * @tags issue-credential v1.0
   * @name RecordsDelete
   * @summary Remove an existing credential exchange record
   * @request DELETE:/issue-credential/records/{cred_ex_id}
   * @secure
   * @response `200` `IssueCredentialModuleResponse`
   */
  export namespace RecordsDelete {
    export type RequestParams = {
      /**
       * Credential exchange identifier
       * @pattern [a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      credExId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = IssueCredentialModuleResponse;
  }
  /**
   * No description
   * @tags issue-credential v1.0
   * @name RecordsIssueCreate
   * @summary Send holder a credential
   * @request POST:/issue-credential/records/{cred_ex_id}/issue
   * @secure
   * @response `200` `V10CredentialExchange`
   */
  export namespace RecordsIssueCreate {
    export type RequestParams = {
      /**
       * Credential exchange identifier
       * @pattern [a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      credExId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = V10CredentialIssueRequest;
    export type RequestHeaders = {};
    export type ResponseBody = V10CredentialExchange;
  }
  /**
   * No description
   * @tags issue-credential v1.0
   * @name RecordsProblemReportCreate
   * @summary Send a problem report for credential exchange
   * @request POST:/issue-credential/records/{cred_ex_id}/problem-report
   * @secure
   * @response `200` `IssueCredentialModuleResponse`
   */
  export namespace RecordsProblemReportCreate {
    export type RequestParams = {
      /**
       * Credential exchange identifier
       * @pattern [a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      credExId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = V10CredentialProblemReportRequest;
    export type RequestHeaders = {};
    export type ResponseBody = IssueCredentialModuleResponse;
  }
  /**
   * No description
   * @tags issue-credential v1.0
   * @name RecordsSendOfferCreate
   * @summary Send holder a credential offer in reference to a proposal with preview
   * @request POST:/issue-credential/records/{cred_ex_id}/send-offer
   * @secure
   * @response `200` `V10CredentialExchange`
   */
  export namespace RecordsSendOfferCreate {
    export type RequestParams = {
      /**
       * Credential exchange identifier
       * @pattern [a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      credExId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = V10CredentialBoundOfferRequest;
    export type RequestHeaders = {};
    export type ResponseBody = V10CredentialExchange;
  }
  /**
   * No description
   * @tags issue-credential v1.0
   * @name RecordsSendRequestCreate
   * @summary Send issuer a credential request
   * @request POST:/issue-credential/records/{cred_ex_id}/send-request
   * @secure
   * @response `200` `V10CredentialExchange`
   */
  export namespace RecordsSendRequestCreate {
    export type RequestParams = {
      /**
       * Credential exchange identifier
       * @pattern [a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      credExId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = V10CredentialExchange;
  }
  /**
   * No description
   * @tags issue-credential v1.0
   * @name RecordsStoreCreate
   * @summary Store a received credential
   * @request POST:/issue-credential/records/{cred_ex_id}/store
   * @secure
   * @response `200` `V10CredentialExchange`
   */
  export namespace RecordsStoreCreate {
    export type RequestParams = {
      /**
       * Credential exchange identifier
       * @pattern [a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      credExId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = V10CredentialStoreRequest;
    export type RequestHeaders = {};
    export type ResponseBody = V10CredentialExchange;
  }
  /**
   * No description
   * @tags issue-credential v1.0
   * @name SendCreate
   * @summary Send holder a credential, automating entire flow
   * @request POST:/issue-credential/send
   * @secure
   * @response `200` `V10CredentialExchange`
   */
  export namespace SendCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = V10CredentialProposalRequestMand;
    export type RequestHeaders = {};
    export type ResponseBody = V10CredentialExchange;
  }
  /**
   * No description
   * @tags issue-credential v1.0
   * @name SendOfferCreate
   * @summary Send holder a credential offer, independent of any proposal
   * @request POST:/issue-credential/send-offer
   * @secure
   * @response `200` `V10CredentialExchange`
   */
  export namespace SendOfferCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = V10CredentialFreeOfferRequest;
    export type RequestHeaders = {};
    export type ResponseBody = V10CredentialExchange;
  }
  /**
   * No description
   * @tags issue-credential v1.0
   * @name SendProposalCreate
   * @summary Send issuer a credential proposal
   * @request POST:/issue-credential/send-proposal
   * @secure
   * @response `200` `V10CredentialExchange`
   */
  export namespace SendProposalCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = V10CredentialProposalRequestOpt;
    export type RequestHeaders = {};
    export type ResponseBody = V10CredentialExchange;
  }
}

export namespace Jsonld {
  /**
   * No description
   * @tags jsonld
   * @name SignCreate
   * @summary Sign a JSON-LD structure and return it
   * @request POST:/jsonld/sign
   * @secure
   * @response `200` `SignResponse`
   */
  export namespace SignCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = SignRequest;
    export type RequestHeaders = {};
    export type ResponseBody = SignResponse;
  }
  /**
   * No description
   * @tags jsonld
   * @name VerifyCreate
   * @summary Verify a JSON-LD structure.
   * @request POST:/jsonld/verify
   * @secure
   * @response `200` `VerifyResponse`
   */
  export namespace VerifyCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = VerifyRequest;
    export type RequestHeaders = {};
    export type ResponseBody = VerifyResponse;
  }
}

export namespace Ledger {
  /**
   * No description
   * @tags ledger
   * @name DidEndpointList
   * @summary Get the endpoint for a DID from the ledger.
   * @request GET:/ledger/did-endpoint
   * @secure
   * @response `200` `GetDIDEndpointResponse`
   */
  export namespace DidEndpointList {
    export type RequestParams = {};
    export type RequestQuery = {
      /**
       * DID of interest
       * @pattern ^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$
       * @example "WgWxqztrNooG92RXvxSTWv"
       */
      did: string;
      /**
       * Endpoint type of interest (default 'Endpoint')
       * @example "Endpoint"
       */
      endpoint_type?: 'Endpoint' | 'Profile' | 'LinkedDomains';
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = GetDIDEndpointResponse;
  }
  /**
   * No description
   * @tags ledger
   * @name DidVerkeyList
   * @summary Get the verkey for a DID from the ledger.
   * @request GET:/ledger/did-verkey
   * @secure
   * @response `200` `GetDIDVerkeyResponse`
   */
  export namespace DidVerkeyList {
    export type RequestParams = {};
    export type RequestQuery = {
      /**
       * DID of interest
       * @pattern ^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$
       * @example "WgWxqztrNooG92RXvxSTWv"
       */
      did: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = GetDIDVerkeyResponse;
  }
  /**
   * No description
   * @tags ledger
   * @name GetNymRoleList
   * @summary Get the role from the NYM registration of a public DID.
   * @request GET:/ledger/get-nym-role
   * @secure
   * @response `200` `GetNymRoleResponse`
   */
  export namespace GetNymRoleList {
    export type RequestParams = {};
    export type RequestQuery = {
      /**
       * DID of interest
       * @pattern ^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$
       * @example "WgWxqztrNooG92RXvxSTWv"
       */
      did: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = GetNymRoleResponse;
  }
  /**
   * No description
   * @tags ledger
   * @name MultipleConfigList
   * @summary Fetch the multiple ledger configuration currently in use
   * @request GET:/ledger/multiple/config
   * @secure
   * @response `200` `LedgerConfigList`
   */
  export namespace MultipleConfigList {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = LedgerConfigList;
  }
  /**
   * No description
   * @tags ledger
   * @name MultipleGetWriteLedgerList
   * @summary Fetch the current write ledger
   * @request GET:/ledger/multiple/get-write-ledger
   * @secure
   * @response `200` `WriteLedgerRequest`
   */
  export namespace MultipleGetWriteLedgerList {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = WriteLedgerRequest;
  }
  /**
   * No description
   * @tags ledger
   * @name RegisterNymCreate
   * @summary Send a NYM registration to the ledger.
   * @request POST:/ledger/register-nym
   * @secure
   * @response `200` `TxnOrRegisterLedgerNymResponse`
   */
  export namespace RegisterNymCreate {
    export type RequestParams = {};
    export type RequestQuery = {
      /**
       * DID to register
       * @pattern ^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$
       * @example "WgWxqztrNooG92RXvxSTWv"
       */
      did: string;
      /**
       * Verification key
       * @pattern ^[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{43,44}$
       * @example "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV"
       */
      verkey: string;
      /**
       * Alias
       * @example "Barry"
       */
      alias?: string;
      /**
       * Connection identifier
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      conn_id?: string;
      /** Create Transaction For Endorser's signature */
      create_transaction_for_endorser?: boolean;
      /** Role */
      role?: 'STEWARD' | 'TRUSTEE' | 'ENDORSER' | 'NETWORK_MONITOR' | 'reset';
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = TxnOrRegisterLedgerNymResponse;
  }
  /**
   * No description
   * @tags ledger
   * @name RotatePublicDidKeypairPartialUpdate
   * @summary Rotate key pair for public DID.
   * @request PATCH:/ledger/rotate-public-did-keypair
   * @secure
   * @response `200` `LedgerModulesResult`
   */
  export namespace RotatePublicDidKeypairPartialUpdate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = LedgerModulesResult;
  }
  /**
   * No description
   * @tags ledger
   * @name GetLedger
   * @summary Fetch the current transaction author agreement, if any
   * @request GET:/ledger/taa
   * @secure
   * @response `200` `TAAResult`
   */
  export namespace GetLedger {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = TAAResult;
  }
  /**
   * No description
   * @tags ledger
   * @name TaaAcceptCreate
   * @summary Accept the transaction author agreement
   * @request POST:/ledger/taa/accept
   * @secure
   * @response `200` `LedgerModulesResult`
   */
  export namespace TaaAcceptCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = TAAAccept;
    export type RequestHeaders = {};
    export type ResponseBody = LedgerModulesResult;
  }
}

export namespace Mediation {
  /**
   * No description
   * @tags mediation
   * @name DefaultMediatorList
   * @summary Get default mediator
   * @request GET:/mediation/default-mediator
   * @secure
   * @response `200` `MediationRecord`
   */
  export namespace DefaultMediatorList {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = MediationRecord;
  }
  /**
   * No description
   * @tags mediation
   * @name DefaultMediatorDelete
   * @summary Clear default mediator
   * @request DELETE:/mediation/default-mediator
   * @secure
   * @response `201` `MediationRecord`
   */
  export namespace DefaultMediatorDelete {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = MediationRecord;
  }
  /**
   * No description
   * @tags mediation
   * @name KeylistsList
   * @summary Retrieve keylists by connection or role
   * @request GET:/mediation/keylists
   * @secure
   * @response `200` `Keylist`
   */
  export namespace KeylistsList {
    export type RequestParams = {};
    export type RequestQuery = {
      /**
       * Connection identifier (optional)
       * @format uuid
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      conn_id?: string;
      /**
       * Filer on role, 'client' for keys         mediated by other agents, 'server' for keys         mediated by this agent
       * @default "server"
       */
      role?: 'client' | 'server';
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = Keylist;
  }
  /**
   * No description
   * @tags mediation
   * @name KeylistsSendKeylistQueryCreate
   * @summary Send keylist query to mediator
   * @request POST:/mediation/keylists/{mediation_id}/send-keylist-query
   * @secure
   * @response `201` `KeylistQuery`
   */
  export namespace KeylistsSendKeylistQueryCreate {
    export type RequestParams = {
      /**
       * Mediation record identifier
       * @format uuid
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      mediationId: string;
    };
    export type RequestQuery = {
      /**
       * limit number of results
       * @format int32
       * @default -1
       */
      paginate_limit?: number;
      /**
       * offset to use in pagination
       * @format int32
       * @default 0
       */
      paginate_offset?: number;
    };
    export type RequestBody = KeylistQueryFilterRequest;
    export type RequestHeaders = {};
    export type ResponseBody = KeylistQuery;
  }
  /**
   * No description
   * @tags mediation
   * @name KeylistsSendKeylistUpdateCreate
   * @summary Send keylist update to mediator
   * @request POST:/mediation/keylists/{mediation_id}/send-keylist-update
   * @secure
   * @response `201` `KeylistUpdate`
   */
  export namespace KeylistsSendKeylistUpdateCreate {
    export type RequestParams = {
      /**
       * Mediation record identifier
       * @format uuid
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      mediationId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = KeylistUpdateRequest;
    export type RequestHeaders = {};
    export type ResponseBody = KeylistUpdate;
  }
  /**
   * No description
   * @tags mediation
   * @name RequestCreate
   * @summary Request mediation from connection
   * @request POST:/mediation/request/{conn_id}
   * @secure
   * @response `201` `MediationRecord`
   */
  export namespace RequestCreate {
    export type RequestParams = {
      /**
       * Connection identifier
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      connId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = MediationCreateRequest;
    export type RequestHeaders = {};
    export type ResponseBody = MediationRecord;
  }
  /**
   * No description
   * @tags mediation
   * @name RequestsList
   * @summary Query mediation requests, returns list of all mediation records
   * @request GET:/mediation/requests
   * @secure
   * @response `200` `MediationList`
   */
  export namespace RequestsList {
    export type RequestParams = {};
    export type RequestQuery = {
      /**
       * Connection identifier (optional)
       * @format uuid
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      conn_id?: string;
      /** List of mediator rules for recipient */
      mediator_terms?: string[];
      /** List of recipient rules for mediation */
      recipient_terms?: string[];
      /**
       * Mediation state (optional)
       * @example "granted"
       */
      state?: 'request' | 'granted' | 'denied';
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = MediationList;
  }
  /**
   * No description
   * @tags mediation
   * @name RequestsDetail
   * @summary Retrieve mediation request record
   * @request GET:/mediation/requests/{mediation_id}
   * @secure
   * @response `200` `MediationRecord`
   */
  export namespace RequestsDetail {
    export type RequestParams = {
      /**
       * Mediation record identifier
       * @format uuid
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      mediationId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = MediationRecord;
  }
  /**
   * No description
   * @tags mediation
   * @name RequestsDelete
   * @summary Delete mediation request by ID
   * @request DELETE:/mediation/requests/{mediation_id}
   * @secure
   * @response `200` `MediationRecord`
   */
  export namespace RequestsDelete {
    export type RequestParams = {
      /**
       * Mediation record identifier
       * @format uuid
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      mediationId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = MediationRecord;
  }
  /**
   * No description
   * @tags mediation
   * @name RequestsDenyCreate
   * @summary Deny a stored mediation request
   * @request POST:/mediation/requests/{mediation_id}/deny
   * @secure
   * @response `201` `MediationDeny`
   */
  export namespace RequestsDenyCreate {
    export type RequestParams = {
      /**
       * Mediation record identifier
       * @format uuid
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      mediationId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = AdminMediationDeny;
    export type RequestHeaders = {};
    export type ResponseBody = MediationDeny;
  }
  /**
   * No description
   * @tags mediation
   * @name RequestsGrantCreate
   * @summary Grant received mediation
   * @request POST:/mediation/requests/{mediation_id}/grant
   * @secure
   * @response `201` `MediationGrant`
   */
  export namespace RequestsGrantCreate {
    export type RequestParams = {
      /**
       * Mediation record identifier
       * @format uuid
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      mediationId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = MediationGrant;
  }
  /**
   * No description
   * @tags mediation
   * @name UpdateKeylistCreate
   * @summary Update keylist for a connection
   * @request POST:/mediation/update-keylist/{conn_id}
   * @secure
   * @response `200` `KeylistUpdate`
   */
  export namespace UpdateKeylistCreate {
    export type RequestParams = {
      /**
       * Connection identifier
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      connId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = MediationIdMatchInfo;
    export type RequestHeaders = {};
    export type ResponseBody = KeylistUpdate;
  }
  /**
   * No description
   * @tags mediation
   * @name DefaultMediatorUpdate
   * @summary Set default mediator
   * @request PUT:/mediation/{mediation_id}/default-mediator
   * @secure
   * @response `201` `MediationRecord`
   */
  export namespace DefaultMediatorUpdate {
    export type RequestParams = {
      /**
       * Mediation record identifier
       * @format uuid
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      mediationId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = MediationRecord;
  }
}

export namespace Multitenancy {
  /**
   * No description
   * @tags multitenancy
   * @name WalletCreate
   * @summary Create a subwallet
   * @request POST:/multitenancy/wallet
   * @secure
   * @response `200` `CreateWalletResponse`
   */
  export namespace WalletCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = CreateWalletRequest;
    export type RequestHeaders = {};
    export type ResponseBody = CreateWalletResponse;
  }
  /**
   * No description
   * @tags multitenancy
   * @name WalletDetail
   * @summary Get a single subwallet
   * @request GET:/multitenancy/wallet/{wallet_id}
   * @secure
   * @response `200` `WalletRecord`
   */
  export namespace WalletDetail {
    export type RequestParams = {
      /**
       * Subwallet identifier
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      walletId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = WalletRecord;
  }
  /**
   * No description
   * @tags multitenancy
   * @name WalletUpdate
   * @summary Update a subwallet
   * @request PUT:/multitenancy/wallet/{wallet_id}
   * @secure
   * @response `200` `WalletRecord`
   */
  export namespace WalletUpdate {
    export type RequestParams = {
      /**
       * Subwallet identifier
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      walletId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = UpdateWalletRequest;
    export type RequestHeaders = {};
    export type ResponseBody = WalletRecord;
  }
  /**
   * No description
   * @tags multitenancy
   * @name WalletRemoveCreate
   * @summary Remove a subwallet
   * @request POST:/multitenancy/wallet/{wallet_id}/remove
   * @secure
   * @response `200` `MultitenantModuleResponse`
   */
  export namespace WalletRemoveCreate {
    export type RequestParams = {
      /**
       * Subwallet identifier
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      walletId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = RemoveWalletRequest;
    export type RequestHeaders = {};
    export type ResponseBody = MultitenantModuleResponse;
  }
  /**
   * No description
   * @tags multitenancy
   * @name WalletTokenCreate
   * @summary Get auth token for a subwallet
   * @request POST:/multitenancy/wallet/{wallet_id}/token
   * @secure
   * @response `200` `CreateWalletTokenResponse`
   */
  export namespace WalletTokenCreate {
    export type RequestParams = {
      walletId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = CreateWalletTokenRequest;
    export type RequestHeaders = {};
    export type ResponseBody = CreateWalletTokenResponse;
  }
  /**
   * No description
   * @tags multitenancy
   * @name WalletsList
   * @summary Query subwallets
   * @request GET:/multitenancy/wallets
   * @secure
   * @response `200` `WalletList`
   */
  export namespace WalletsList {
    export type RequestParams = {};
    export type RequestQuery = {
      /**
       * Wallet name
       * @example "MyNewWallet"
       */
      wallet_name?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = WalletList;
  }
}

export namespace OutOfBand {
  /**
   * No description
   * @tags out-of-band
   * @name CreateInvitationCreate
   * @summary Create a new connection invitation
   * @request POST:/out-of-band/create-invitation
   * @secure
   * @response `200` `InvitationRecord`
   */
  export namespace CreateInvitationCreate {
    export type RequestParams = {};
    export type RequestQuery = {
      /** Auto-accept connection (defaults to configuration) */
      auto_accept?: boolean;
      /** Create invitation for multiple use (default false) */
      multi_use?: boolean;
    };
    export type RequestBody = InvitationCreateRequest;
    export type RequestHeaders = {};
    export type ResponseBody = InvitationRecord;
  }
  /**
   * No description
   * @tags out-of-band
   * @name ReceiveInvitationCreate
   * @summary Receive a new connection invitation
   * @request POST:/out-of-band/receive-invitation
   * @secure
   * @response `200` `OobRecord`
   */
  export namespace ReceiveInvitationCreate {
    export type RequestParams = {};
    export type RequestQuery = {
      /**
       * Alias for connection
       * @example "Barry"
       */
      alias?: string;
      /** Auto-accept connection (defaults to configuration) */
      auto_accept?: boolean;
      /**
       * Identifier for active mediation record to be used
       * @pattern [a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      mediation_id?: string;
      /** Use an existing connection, if possible */
      use_existing_connection?: boolean;
    };
    export type RequestBody = InvitationMessage;
    export type RequestHeaders = {};
    export type ResponseBody = OobRecord;
  }
}

export namespace Plugins {
  /**
   * No description
   * @tags server
   * @name PluginsList
   * @summary Fetch the list of loaded plugins
   * @request GET:/plugins
   * @secure
   * @response `200` `AdminModules`
   */
  export namespace PluginsList {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = AdminModules;
  }
}

export namespace PresentProof20 {
  /**
   * No description
   * @tags present-proof v2.0
   * @name CreateRequestCreate
   * @summary Creates a presentation request not bound to any proposal or connection
   * @request POST:/present-proof-2.0/create-request
   * @secure
   * @response `200` `V20PresExRecord`
   */
  export namespace CreateRequestCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = V20PresCreateRequestRequest;
    export type RequestHeaders = {};
    export type ResponseBody = V20PresExRecord;
  }
  /**
   * No description
   * @tags present-proof v2.0
   * @name RecordsList
   * @summary Fetch all present-proof exchange records
   * @request GET:/present-proof-2.0/records
   * @secure
   * @response `200` `V20PresExRecordList`
   */
  export namespace RecordsList {
    export type RequestParams = {};
    export type RequestQuery = {
      /**
       * Connection identifier
       * @format uuid
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      connection_id?: string;
      /** Role assigned in presentation exchange */
      role?: 'prover' | 'verifier';
      /** Presentation exchange state */
      state?:
        | 'proposal-sent'
        | 'proposal-received'
        | 'request-sent'
        | 'request-received'
        | 'presentation-sent'
        | 'presentation-received'
        | 'done'
        | 'abandoned';
      /**
       * Thread identifier
       * @format uuid
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      thread_id?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = V20PresExRecordList;
  }
  /**
   * No description
   * @tags present-proof v2.0
   * @name RecordsDetail
   * @summary Fetch a single presentation exchange record
   * @request GET:/present-proof-2.0/records/{pres_ex_id}
   * @secure
   * @response `200` `V20PresExRecord`
   */
  export namespace RecordsDetail {
    export type RequestParams = {
      /**
       * Presentation exchange identifier
       * @pattern [a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      presExId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = V20PresExRecord;
  }
  /**
   * No description
   * @tags present-proof v2.0
   * @name RecordsDelete
   * @summary Remove an existing presentation exchange record
   * @request DELETE:/present-proof-2.0/records/{pres_ex_id}
   * @secure
   * @response `200` `V20PresentProofModuleResponse`
   */
  export namespace RecordsDelete {
    export type RequestParams = {
      /**
       * Presentation exchange identifier
       * @pattern [a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      presExId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = V20PresentProofModuleResponse;
  }
  /**
   * No description
   * @tags present-proof v2.0
   * @name RecordsCredentialsDetail
   * @summary Fetch credentials from wallet for presentation request
   * @request GET:/present-proof-2.0/records/{pres_ex_id}/credentials
   * @secure
   * @response `200` `(IndyCredPrecis)[]`
   */
  export namespace RecordsCredentialsDetail {
    export type RequestParams = {
      /**
       * Presentation exchange identifier
       * @pattern [a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      presExId: string;
    };
    export type RequestQuery = {
      /**
       * Maximum number to retrieve
       * @pattern ^[1-9][0-9]*$
       * @example "1"
       */
      count?: string;
      /**
       * (JSON) object mapping referents to extra WQL queries
       * @pattern ^{\s*".*?"\s*:\s*{.*?}\s*(,\s*".*?"\s*:\s*{.*?}\s*)*\s*}$
       * @example "{"0_drink_uuid": {"attr::drink::value": "martini"}}"
       */
      extra_query?: string;
      /**
       * Proof request referents of interest, comma-separated
       * @example "1_name_uuid,2_score_uuid"
       */
      referent?: string;
      /**
       * Start index
       * @pattern ^[0-9]*$
       * @example "0"
       */
      start?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = IndyCredPrecis[];
  }
  /**
   * No description
   * @tags present-proof v2.0
   * @name RecordsProblemReportCreate
   * @summary Send a problem report for presentation exchange
   * @request POST:/present-proof-2.0/records/{pres_ex_id}/problem-report
   * @secure
   * @response `200` `V20PresentProofModuleResponse`
   */
  export namespace RecordsProblemReportCreate {
    export type RequestParams = {
      /**
       * Presentation exchange identifier
       * @pattern [a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      presExId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = V20PresProblemReportRequest;
    export type RequestHeaders = {};
    export type ResponseBody = V20PresentProofModuleResponse;
  }
  /**
   * No description
   * @tags present-proof v2.0
   * @name RecordsSendPresentationCreate
   * @summary Sends a proof presentation
   * @request POST:/present-proof-2.0/records/{pres_ex_id}/send-presentation
   * @secure
   * @response `200` `V20PresExRecord`
   */
  export namespace RecordsSendPresentationCreate {
    export type RequestParams = {
      /**
       * Presentation exchange identifier
       * @pattern [a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      presExId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = V20PresSpecByFormatRequest;
    export type RequestHeaders = {};
    export type ResponseBody = V20PresExRecord;
  }
  /**
   * No description
   * @tags present-proof v2.0
   * @name RecordsSendRequestCreate
   * @summary Sends a presentation request in reference to a proposal
   * @request POST:/present-proof-2.0/records/{pres_ex_id}/send-request
   * @secure
   * @response `200` `V20PresExRecord`
   */
  export namespace RecordsSendRequestCreate {
    export type RequestParams = {
      /**
       * Presentation exchange identifier
       * @pattern [a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      presExId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = V20PresentationSendRequestToProposal;
    export type RequestHeaders = {};
    export type ResponseBody = V20PresExRecord;
  }
  /**
   * No description
   * @tags present-proof v2.0
   * @name RecordsVerifyPresentationCreate
   * @summary Verify a received presentation
   * @request POST:/present-proof-2.0/records/{pres_ex_id}/verify-presentation
   * @secure
   * @response `200` `V20PresExRecord`
   */
  export namespace RecordsVerifyPresentationCreate {
    export type RequestParams = {
      /**
       * Presentation exchange identifier
       * @pattern [a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      presExId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = V20PresExRecord;
  }
  /**
   * No description
   * @tags present-proof v2.0
   * @name SendProposalCreate
   * @summary Sends a presentation proposal
   * @request POST:/present-proof-2.0/send-proposal
   * @secure
   * @response `200` `V20PresExRecord`
   */
  export namespace SendProposalCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = V20PresProposalRequest;
    export type RequestHeaders = {};
    export type ResponseBody = V20PresExRecord;
  }
  /**
   * No description
   * @tags present-proof v2.0
   * @name SendRequestCreate
   * @summary Sends a free presentation request not bound to any proposal
   * @request POST:/present-proof-2.0/send-request
   * @secure
   * @response `200` `V20PresExRecord`
   */
  export namespace SendRequestCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = V20PresSendRequestRequest;
    export type RequestHeaders = {};
    export type ResponseBody = V20PresExRecord;
  }
}

export namespace PresentProof {
  /**
   * No description
   * @tags present-proof v1.0
   * @name CreateRequestCreate
   * @summary Creates a presentation request not bound to any proposal or connection
   * @request POST:/present-proof/create-request
   * @secure
   * @response `200` `V10PresentationExchange`
   */
  export namespace CreateRequestCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = V10PresentationCreateRequestRequest;
    export type RequestHeaders = {};
    export type ResponseBody = V10PresentationExchange;
  }
  /**
   * No description
   * @tags present-proof v1.0
   * @name RecordsList
   * @summary Fetch all present-proof exchange records
   * @request GET:/present-proof/records
   * @secure
   * @response `200` `V10PresentationExchangeList`
   */
  export namespace RecordsList {
    export type RequestParams = {};
    export type RequestQuery = {
      /**
       * Connection identifier
       * @format uuid
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      connection_id?: string;
      /** Role assigned in presentation exchange */
      role?: 'prover' | 'verifier';
      /** Presentation exchange state */
      state?:
        | 'proposal_sent'
        | 'proposal_received'
        | 'request_sent'
        | 'request_received'
        | 'presentation_sent'
        | 'presentation_received'
        | 'verified'
        | 'presentation_acked'
        | 'abandoned';
      /**
       * Thread identifier
       * @format uuid
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      thread_id?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = V10PresentationExchangeList;
  }
  /**
   * No description
   * @tags present-proof v1.0
   * @name RecordsDetail
   * @summary Fetch a single presentation exchange record
   * @request GET:/present-proof/records/{pres_ex_id}
   * @secure
   * @response `200` `V10PresentationExchange`
   */
  export namespace RecordsDetail {
    export type RequestParams = {
      /**
       * Presentation exchange identifier
       * @pattern [a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      presExId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = V10PresentationExchange;
  }
  /**
   * No description
   * @tags present-proof v1.0
   * @name RecordsDelete
   * @summary Remove an existing presentation exchange record
   * @request DELETE:/present-proof/records/{pres_ex_id}
   * @secure
   * @response `200` `V10PresentProofModuleResponse`
   */
  export namespace RecordsDelete {
    export type RequestParams = {
      /**
       * Presentation exchange identifier
       * @pattern [a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      presExId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = V10PresentProofModuleResponse;
  }
  /**
   * No description
   * @tags present-proof v1.0
   * @name RecordsCredentialsDetail
   * @summary Fetch credentials for a presentation request from wallet
   * @request GET:/present-proof/records/{pres_ex_id}/credentials
   * @secure
   * @response `200` `(IndyCredPrecis)[]`
   */
  export namespace RecordsCredentialsDetail {
    export type RequestParams = {
      /**
       * Presentation exchange identifier
       * @pattern [a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      presExId: string;
    };
    export type RequestQuery = {
      /**
       * Maximum number to retrieve
       * @pattern ^[1-9][0-9]*$
       * @example "1"
       */
      count?: string;
      /**
       * (JSON) object mapping referents to extra WQL queries
       * @pattern ^{\s*".*?"\s*:\s*{.*?}\s*(,\s*".*?"\s*:\s*{.*?}\s*)*\s*}$
       * @example "{"0_drink_uuid": {"attr::drink::value": "martini"}}"
       */
      extra_query?: string;
      /**
       * Proof request referents of interest, comma-separated
       * @example "1_name_uuid,2_score_uuid"
       */
      referent?: string;
      /**
       * Start index
       * @pattern ^[0-9]*$
       * @example "0"
       */
      start?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = IndyCredPrecis[];
  }
  /**
   * No description
   * @tags present-proof v1.0
   * @name RecordsProblemReportCreate
   * @summary Send a problem report for presentation exchange
   * @request POST:/present-proof/records/{pres_ex_id}/problem-report
   * @secure
   * @response `200` `V10PresentProofModuleResponse`
   */
  export namespace RecordsProblemReportCreate {
    export type RequestParams = {
      /**
       * Presentation exchange identifier
       * @pattern [a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      presExId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = V10PresentationProblemReportRequest;
    export type RequestHeaders = {};
    export type ResponseBody = V10PresentProofModuleResponse;
  }
  /**
   * No description
   * @tags present-proof v1.0
   * @name RecordsSendPresentationCreate
   * @summary Sends a proof presentation
   * @request POST:/present-proof/records/{pres_ex_id}/send-presentation
   * @secure
   * @response `200` `V10PresentationExchange`
   */
  export namespace RecordsSendPresentationCreate {
    export type RequestParams = {
      /**
       * Presentation exchange identifier
       * @pattern [a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      presExId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = IndyPresSpec;
    export type RequestHeaders = {};
    export type ResponseBody = V10PresentationExchange;
  }
  /**
   * No description
   * @tags present-proof v1.0
   * @name RecordsSendRequestCreate
   * @summary Sends a presentation request in reference to a proposal
   * @request POST:/present-proof/records/{pres_ex_id}/send-request
   * @secure
   * @response `200` `V10PresentationExchange`
   */
  export namespace RecordsSendRequestCreate {
    export type RequestParams = {
      /**
       * Presentation exchange identifier
       * @pattern [a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      presExId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = V10PresentationSendRequestToProposal;
    export type RequestHeaders = {};
    export type ResponseBody = V10PresentationExchange;
  }
  /**
   * No description
   * @tags present-proof v1.0
   * @name RecordsVerifyPresentationCreate
   * @summary Verify a received presentation
   * @request POST:/present-proof/records/{pres_ex_id}/verify-presentation
   * @secure
   * @response `200` `V10PresentationExchange`
   */
  export namespace RecordsVerifyPresentationCreate {
    export type RequestParams = {
      /**
       * Presentation exchange identifier
       * @pattern [a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      presExId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = V10PresentationExchange;
  }
  /**
   * No description
   * @tags present-proof v1.0
   * @name SendProposalCreate
   * @summary Sends a presentation proposal
   * @request POST:/present-proof/send-proposal
   * @secure
   * @response `200` `V10PresentationExchange`
   */
  export namespace SendProposalCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = V10PresentationProposalRequest;
    export type RequestHeaders = {};
    export type ResponseBody = V10PresentationExchange;
  }
  /**
   * No description
   * @tags present-proof v1.0
   * @name SendRequestCreate
   * @summary Sends a free presentation request not bound to any proposal
   * @request POST:/present-proof/send-request
   * @secure
   * @response `200` `V10PresentationExchange`
   */
  export namespace SendRequestCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = V10PresentationSendRequestRequest;
    export type RequestHeaders = {};
    export type ResponseBody = V10PresentationExchange;
  }
}

export namespace Resolver {
  /**
   * No description
   * @tags resolver
   * @name ResolveDetail
   * @summary Retrieve doc for requested did
   * @request GET:/resolver/resolve/{did}
   * @secure
   * @response `200` `ResolutionResult`
   */
  export namespace ResolveDetail {
    export type RequestParams = {
      /**
       * DID
       * @pattern ^did:([a-z0-9]+):((?:[a-zA-Z0-9._%-]*:)*[a-zA-Z0-9._%-]+)$
       * @example "did:ted:WgWxqztrNooG92RXvxSTWv"
       */
      did: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = ResolutionResult;
  }
}

export namespace Revocation {
  /**
   * No description
   * @tags revocation
   * @name ActiveRegistryDetail
   * @summary Get current active revocation registry by credential definition id
   * @request GET:/revocation/active-registry/{cred_def_id}
   * @secure
   * @response `200` `RevRegResult`
   */
  export namespace ActiveRegistryDetail {
    export type RequestParams = {
      /**
       * Credential definition identifier
       * @pattern ^([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):3:CL:(([1-9][0-9]*)|([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+)):(.+)?$
       * @example "WgWxqztrNooG92RXvxSTWv:3:CL:20:tag"
       */
      credDefId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = RevRegResult;
  }
  /**
   * No description
   * @tags revocation
   * @name ClearPendingRevocationsCreate
   * @summary Clear pending revocations
   * @request POST:/revocation/clear-pending-revocations
   * @secure
   * @response `200` `PublishRevocations`
   */
  export namespace ClearPendingRevocationsCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = ClearPendingRevocationsRequest;
    export type RequestHeaders = {};
    export type ResponseBody = PublishRevocations;
  }
  /**
   * No description
   * @tags revocation
   * @name CreateRegistryCreate
   * @summary Creates a new revocation registry
   * @request POST:/revocation/create-registry
   * @secure
   * @response `200` `RevRegResult`
   */
  export namespace CreateRegistryCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = RevRegCreateRequest;
    export type RequestHeaders = {};
    export type ResponseBody = RevRegResult;
  }
  /**
   * No description
   * @tags revocation
   * @name CredentialRecordList
   * @summary Get credential revocation status
   * @request GET:/revocation/credential-record
   * @secure
   * @response `200` `CredRevRecordResult`
   */
  export namespace CredentialRecordList {
    export type RequestParams = {};
    export type RequestQuery = {
      /**
       * Credential exchange identifier
       * @pattern [a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      cred_ex_id?: string;
      /**
       * Credential revocation identifier
       * @pattern ^[1-9][0-9]*$
       * @example "12345"
       */
      cred_rev_id?: string;
      /**
       * Revocation registry identifier
       * @pattern ^([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):4:([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):3:CL:(([1-9][0-9]*)|([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+))(:.+)?:CL_ACCUM:(.+$)
       * @example "WgWxqztrNooG92RXvxSTWv:4:WgWxqztrNooG92RXvxSTWv:3:CL:20:tag:CL_ACCUM:0"
       */
      rev_reg_id?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = CredRevRecordResult;
  }
  /**
   * No description
   * @tags revocation
   * @name PublishRevocationsCreate
   * @summary Publish pending revocations to ledger
   * @request POST:/revocation/publish-revocations
   * @secure
   * @response `200` `TxnOrPublishRevocationsResult`
   */
  export namespace PublishRevocationsCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = PublishRevocations;
    export type RequestHeaders = {};
    export type ResponseBody = TxnOrPublishRevocationsResult;
  }
  /**
   * No description
   * @tags revocation
   * @name RegistriesCreatedList
   * @summary Search for matching revocation registries that current agent created
   * @request GET:/revocation/registries/created
   * @secure
   * @response `200` `RevRegsCreated`
   */
  export namespace RegistriesCreatedList {
    export type RequestParams = {};
    export type RequestQuery = {
      /**
       * Credential definition identifier
       * @pattern ^([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):3:CL:(([1-9][0-9]*)|([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+)):(.+)?$
       * @example "WgWxqztrNooG92RXvxSTWv:3:CL:20:tag"
       */
      cred_def_id?: string;
      /** Revocation registry state */
      state?: 'init' | 'generated' | 'posted' | 'active' | 'full';
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = RevRegsCreated;
  }
  /**
   * No description
   * @tags revocation
   * @name RegistryDetail
   * @summary Get revocation registry by revocation registry id
   * @request GET:/revocation/registry/{rev_reg_id}
   * @secure
   * @response `200` `RevRegResult`
   */
  export namespace RegistryDetail {
    export type RequestParams = {
      /**
       * Revocation Registry identifier
       * @pattern ^([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):4:([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):3:CL:(([1-9][0-9]*)|([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+))(:.+)?:CL_ACCUM:(.+$)
       * @example "WgWxqztrNooG92RXvxSTWv:4:WgWxqztrNooG92RXvxSTWv:3:CL:20:tag:CL_ACCUM:0"
       */
      revRegId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = RevRegResult;
  }
  /**
   * No description
   * @tags revocation
   * @name RegistryPartialUpdate
   * @summary Update revocation registry with new public URI to its tails file
   * @request PATCH:/revocation/registry/{rev_reg_id}
   * @secure
   * @response `200` `RevRegResult`
   */
  export namespace RegistryPartialUpdate {
    export type RequestParams = {
      /**
       * Revocation Registry identifier
       * @pattern ^([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):4:([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):3:CL:(([1-9][0-9]*)|([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+))(:.+)?:CL_ACCUM:(.+$)
       * @example "WgWxqztrNooG92RXvxSTWv:4:WgWxqztrNooG92RXvxSTWv:3:CL:20:tag:CL_ACCUM:0"
       */
      revRegId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = RevRegUpdateTailsFileUri;
    export type RequestHeaders = {};
    export type ResponseBody = RevRegResult;
  }
  /**
   * No description
   * @tags revocation
   * @name RegistryDefinitionCreate
   * @summary Send revocation registry definition to ledger
   * @request POST:/revocation/registry/{rev_reg_id}/definition
   * @secure
   * @response `200` `TxnOrRevRegResult`
   */
  export namespace RegistryDefinitionCreate {
    export type RequestParams = {
      /**
       * Revocation Registry identifier
       * @pattern ^([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):4:([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):3:CL:(([1-9][0-9]*)|([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+))(:.+)?:CL_ACCUM:(.+$)
       * @example "WgWxqztrNooG92RXvxSTWv:4:WgWxqztrNooG92RXvxSTWv:3:CL:20:tag:CL_ACCUM:0"
       */
      revRegId: string;
    };
    export type RequestQuery = {
      /**
       * Connection identifier
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      conn_id?: string;
      /** Create Transaction For Endorser's signature */
      create_transaction_for_endorser?: boolean;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = TxnOrRevRegResult;
  }
  /**
   * No description
   * @tags revocation
   * @name RegistryEntryCreate
   * @summary Send revocation registry entry to ledger
   * @request POST:/revocation/registry/{rev_reg_id}/entry
   * @secure
   * @response `200` `RevRegResult`
   */
  export namespace RegistryEntryCreate {
    export type RequestParams = {
      /**
       * Revocation Registry identifier
       * @pattern ^([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):4:([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):3:CL:(([1-9][0-9]*)|([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+))(:.+)?:CL_ACCUM:(.+$)
       * @example "WgWxqztrNooG92RXvxSTWv:4:WgWxqztrNooG92RXvxSTWv:3:CL:20:tag:CL_ACCUM:0"
       */
      revRegId: string;
    };
    export type RequestQuery = {
      /**
       * Connection identifier
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      conn_id?: string;
      /** Create Transaction For Endorser's signature */
      create_transaction_for_endorser?: boolean;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = RevRegResult;
  }
  /**
   * No description
   * @tags revocation
   * @name RegistryFixRevocationEntryStateUpdate
   * @summary Fix revocation state in wallet and return number of updated entries
   * @request PUT:/revocation/registry/{rev_reg_id}/fix-revocation-entry-state
   * @secure
   * @response `200` `RevRegWalletUpdatedResult`
   */
  export namespace RegistryFixRevocationEntryStateUpdate {
    export type RequestParams = {
      /**
       * Revocation Registry identifier
       * @pattern ^([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):4:([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):3:CL:(([1-9][0-9]*)|([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+))(:.+)?:CL_ACCUM:(.+$)
       * @example "WgWxqztrNooG92RXvxSTWv:4:WgWxqztrNooG92RXvxSTWv:3:CL:20:tag:CL_ACCUM:0"
       */
      revRegId: string;
    };
    export type RequestQuery = {
      /** Apply updated accumulator transaction to ledger */
      apply_ledger_update: boolean;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = RevRegWalletUpdatedResult;
  }
  /**
   * No description
   * @tags revocation
   * @name RegistryIssuedDetail
   * @summary Get number of credentials issued against revocation registry
   * @request GET:/revocation/registry/{rev_reg_id}/issued
   * @secure
   * @response `200` `RevRegIssuedResult`
   */
  export namespace RegistryIssuedDetail {
    export type RequestParams = {
      /**
       * Revocation Registry identifier
       * @pattern ^([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):4:([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):3:CL:(([1-9][0-9]*)|([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+))(:.+)?:CL_ACCUM:(.+$)
       * @example "WgWxqztrNooG92RXvxSTWv:4:WgWxqztrNooG92RXvxSTWv:3:CL:20:tag:CL_ACCUM:0"
       */
      revRegId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = RevRegIssuedResult;
  }
  /**
   * No description
   * @tags revocation
   * @name RegistryIssuedDetailsDetail
   * @summary Get details of credentials issued against revocation registry
   * @request GET:/revocation/registry/{rev_reg_id}/issued/details
   * @secure
   * @response `200` `CredRevRecordDetailsResult`
   */
  export namespace RegistryIssuedDetailsDetail {
    export type RequestParams = {
      /**
       * Revocation Registry identifier
       * @pattern ^([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):4:([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):3:CL:(([1-9][0-9]*)|([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+))(:.+)?:CL_ACCUM:(.+$)
       * @example "WgWxqztrNooG92RXvxSTWv:4:WgWxqztrNooG92RXvxSTWv:3:CL:20:tag:CL_ACCUM:0"
       */
      revRegId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = CredRevRecordDetailsResult;
  }
  /**
   * No description
   * @tags revocation
   * @name RegistryIssuedIndyRecsDetail
   * @summary Get details of revoked credentials from ledger
   * @request GET:/revocation/registry/{rev_reg_id}/issued/indy_recs
   * @secure
   * @response `200` `CredRevIndyRecordsResult`
   */
  export namespace RegistryIssuedIndyRecsDetail {
    export type RequestParams = {
      /**
       * Revocation Registry identifier
       * @pattern ^([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):4:([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):3:CL:(([1-9][0-9]*)|([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+))(:.+)?:CL_ACCUM:(.+$)
       * @example "WgWxqztrNooG92RXvxSTWv:4:WgWxqztrNooG92RXvxSTWv:3:CL:20:tag:CL_ACCUM:0"
       */
      revRegId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = CredRevIndyRecordsResult;
  }
  /**
   * No description
   * @tags revocation
   * @name RegistrySetStatePartialUpdate
   * @summary Set revocation registry state manually
   * @request PATCH:/revocation/registry/{rev_reg_id}/set-state
   * @secure
   * @response `200` `RevRegResult`
   */
  export namespace RegistrySetStatePartialUpdate {
    export type RequestParams = {
      /**
       * Revocation Registry identifier
       * @pattern ^([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):4:([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):3:CL:(([1-9][0-9]*)|([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+))(:.+)?:CL_ACCUM:(.+$)
       * @example "WgWxqztrNooG92RXvxSTWv:4:WgWxqztrNooG92RXvxSTWv:3:CL:20:tag:CL_ACCUM:0"
       */
      revRegId: string;
    };
    export type RequestQuery = {
      /** Revocation registry state to set */
      state: 'init' | 'generated' | 'posted' | 'active' | 'full';
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = RevRegResult;
  }
  /**
   * No description
   * @tags revocation
   * @name RegistryTailsFileUpdate
   * @summary Upload local tails file to server
   * @request PUT:/revocation/registry/{rev_reg_id}/tails-file
   * @secure
   * @response `200` `RevocationModuleResponse`
   */
  export namespace RegistryTailsFileUpdate {
    export type RequestParams = {
      /**
       * Revocation Registry identifier
       * @pattern ^([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):4:([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):3:CL:(([1-9][0-9]*)|([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+))(:.+)?:CL_ACCUM:(.+$)
       * @example "WgWxqztrNooG92RXvxSTWv:4:WgWxqztrNooG92RXvxSTWv:3:CL:20:tag:CL_ACCUM:0"
       */
      revRegId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = RevocationModuleResponse;
  }
  /**
   * No description
   * @tags revocation
   * @name RegistryTailsFileDetail
   * @summary Download tails file
   * @request GET:/revocation/registry/{rev_reg_id}/tails-file
   * @secure
   * @response `200` `File` tails file
   */
  export namespace RegistryTailsFileDetail {
    export type RequestParams = {
      /**
       * Revocation Registry identifier
       * @pattern ^([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):4:([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):3:CL:(([1-9][0-9]*)|([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+))(:.+)?:CL_ACCUM:(.+$)
       * @example "WgWxqztrNooG92RXvxSTWv:4:WgWxqztrNooG92RXvxSTWv:3:CL:20:tag:CL_ACCUM:0"
       */
      revRegId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = File;
  }
  /**
   * No description
   * @tags revocation
   * @name RevokeCreate
   * @summary Revoke an issued credential
   * @request POST:/revocation/revoke
   * @secure
   * @response `200` `RevocationModuleResponse`
   */
  export namespace RevokeCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = RevokeRequest;
    export type RequestHeaders = {};
    export type ResponseBody = RevocationModuleResponse;
  }
}

export namespace Schemas {
  /**
   * No description
   * @tags schema
   * @name SchemasCreate
   * @summary Sends a schema to the ledger
   * @request POST:/schemas
   * @secure
   * @response `200` `TxnOrSchemaSendResult`
   */
  export namespace SchemasCreate {
    export type RequestParams = {};
    export type RequestQuery = {
      /**
       * Connection identifier
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      conn_id?: string;
      /** Create Transaction For Endorser's signature */
      create_transaction_for_endorser?: boolean;
    };
    export type RequestBody = SchemaSendRequest;
    export type RequestHeaders = {};
    export type ResponseBody = TxnOrSchemaSendResult;
  }
  /**
   * No description
   * @tags schema
   * @name CreatedList
   * @summary Search for matching schema that agent originated
   * @request GET:/schemas/created
   * @secure
   * @response `200` `SchemasCreatedResult`
   */
  export namespace CreatedList {
    export type RequestParams = {};
    export type RequestQuery = {
      /**
       * Schema identifier
       * @pattern ^[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+$
       * @example "WgWxqztrNooG92RXvxSTWv:2:schema_name:1.0"
       */
      schema_id?: string;
      /**
       * Schema issuer DID
       * @pattern ^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$
       * @example "WgWxqztrNooG92RXvxSTWv"
       */
      schema_issuer_did?: string;
      /**
       * Schema name
       * @example "membership"
       */
      schema_name?: string;
      /**
       * Schema version
       * @pattern ^[0-9.]+$
       * @example "1.0"
       */
      schema_version?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = SchemasCreatedResult;
  }
  /**
   * No description
   * @tags schema
   * @name SchemasDetail
   * @summary Gets a schema from the ledger
   * @request GET:/schemas/{schema_id}
   * @secure
   * @response `200` `SchemaGetResult`
   */
  export namespace SchemasDetail {
    export type RequestParams = {
      /**
       * Schema identifier
       * @pattern ^[1-9][0-9]*|[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+$
       * @example "WgWxqztrNooG92RXvxSTWv:2:schema_name:1.0"
       */
      schemaId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = SchemaGetResult;
  }
  /**
   * No description
   * @tags schema
   * @name WriteRecordCreate
   * @summary Writes a schema non-secret record to the wallet
   * @request POST:/schemas/{schema_id}/write_record
   * @secure
   * @response `200` `SchemaGetResult`
   */
  export namespace WriteRecordCreate {
    export type RequestParams = {
      /**
       * Schema identifier
       * @pattern ^[1-9][0-9]*|[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+$
       * @example "WgWxqztrNooG92RXvxSTWv:2:schema_name:1.0"
       */
      schemaId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = SchemaGetResult;
  }
}

export namespace Shutdown {
  /**
   * No description
   * @tags server
   * @name ShutdownList
   * @summary Shut down server
   * @request GET:/shutdown
   * @secure
   * @response `200` `AdminShutdown`
   */
  export namespace ShutdownList {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = AdminShutdown;
  }
}

export namespace Status {
  /**
   * No description
   * @tags server
   * @name StatusList
   * @summary Fetch the server status
   * @request GET:/status
   * @secure
   * @response `200` `AdminStatus`
   */
  export namespace StatusList {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = AdminStatus;
  }
  /**
   * No description
   * @tags server
   * @name ConfigList
   * @summary Fetch the server configuration
   * @request GET:/status/config
   * @secure
   * @response `200` `AdminConfig`
   */
  export namespace ConfigList {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = AdminConfig;
  }
  /**
   * No description
   * @tags server
   * @name LiveList
   * @summary Liveliness check
   * @request GET:/status/live
   * @secure
   * @response `200` `AdminStatusLiveliness`
   */
  export namespace LiveList {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = AdminStatusLiveliness;
  }
  /**
   * No description
   * @tags server
   * @name ReadyList
   * @summary Readiness check
   * @request GET:/status/ready
   * @secure
   * @response `200` `AdminStatusReadiness`
   */
  export namespace ReadyList {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = AdminStatusReadiness;
  }
  /**
   * No description
   * @tags server
   * @name ResetCreate
   * @summary Reset statistics
   * @request POST:/status/reset
   * @secure
   * @response `200` `AdminReset`
   */
  export namespace ResetCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = AdminReset;
  }
}

export namespace Transaction {
  /**
   * No description
   * @tags endorse-transaction
   * @name ResendCreate
   * @summary For Author to resend a particular transaction request
   * @request POST:/transaction/{tran_id}/resend
   * @secure
   * @response `200` `TransactionRecord`
   */
  export namespace ResendCreate {
    export type RequestParams = {
      /**
       * Transaction identifier
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      tranId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = TransactionRecord;
  }
}

export namespace Transactions {
  /**
   * No description
   * @tags endorse-transaction
   * @name TransactionsList
   * @summary Query transactions
   * @request GET:/transactions
   * @secure
   * @response `200` `TransactionList`
   */
  export namespace TransactionsList {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = TransactionList;
  }
  /**
   * No description
   * @tags endorse-transaction
   * @name CreateRequestCreate
   * @summary For author to send a transaction request
   * @request POST:/transactions/create-request
   * @secure
   * @response `200` `TransactionRecord`
   */
  export namespace CreateRequestCreate {
    export type RequestParams = {};
    export type RequestQuery = {
      /**
       * Transaction identifier
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      tran_id: string;
      /** Endorser will write the transaction after endorsing it */
      endorser_write_txn?: boolean;
    };
    export type RequestBody = Date;
    export type RequestHeaders = {};
    export type ResponseBody = TransactionRecord;
  }
  /**
   * No description
   * @tags endorse-transaction
   * @name SetEndorserInfoCreate
   * @summary Set Endorser Info
   * @request POST:/transactions/{conn_id}/set-endorser-info
   * @secure
   * @response `200` `EndorserInfo`
   */
  export namespace SetEndorserInfoCreate {
    export type RequestParams = {
      /**
       * Connection identifier
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      connId: string;
    };
    export type RequestQuery = {
      /** Endorser DID */
      endorser_did: string;
      /** Endorser Name */
      endorser_name?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = EndorserInfo;
  }
  /**
   * No description
   * @tags endorse-transaction
   * @name SetEndorserRoleCreate
   * @summary Set transaction jobs
   * @request POST:/transactions/{conn_id}/set-endorser-role
   * @secure
   * @response `200` `TransactionJobs`
   */
  export namespace SetEndorserRoleCreate {
    export type RequestParams = {
      /**
       * Connection identifier
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      connId: string;
    };
    export type RequestQuery = {
      /** Transaction related jobs */
      transaction_my_job?: 'TRANSACTION_AUTHOR' | 'TRANSACTION_ENDORSER' | 'reset';
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = TransactionJobs;
  }
  /**
   * No description
   * @tags endorse-transaction
   * @name TransactionsDetail
   * @summary Fetch a single transaction record
   * @request GET:/transactions/{tran_id}
   * @secure
   * @response `200` `TransactionRecord`
   */
  export namespace TransactionsDetail {
    export type RequestParams = {
      /**
       * Transaction identifier
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      tranId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = TransactionRecord;
  }
  /**
   * No description
   * @tags endorse-transaction
   * @name CancelCreate
   * @summary For Author to cancel a particular transaction request
   * @request POST:/transactions/{tran_id}/cancel
   * @secure
   * @response `200` `TransactionRecord`
   */
  export namespace CancelCreate {
    export type RequestParams = {
      /**
       * Transaction identifier
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      tranId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = TransactionRecord;
  }
  /**
   * No description
   * @tags endorse-transaction
   * @name EndorseCreate
   * @summary For Endorser to endorse a particular transaction record
   * @request POST:/transactions/{tran_id}/endorse
   * @secure
   * @response `200` `TransactionRecord`
   */
  export namespace EndorseCreate {
    export type RequestParams = {
      /**
       * Transaction identifier
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      tranId: string;
    };
    export type RequestQuery = {
      /** Endorser DID */
      endorser_did?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = TransactionRecord;
  }
  /**
   * No description
   * @tags endorse-transaction
   * @name RefuseCreate
   * @summary For Endorser to refuse a particular transaction record
   * @request POST:/transactions/{tran_id}/refuse
   * @secure
   * @response `200` `TransactionRecord`
   */
  export namespace RefuseCreate {
    export type RequestParams = {
      /**
       * Transaction identifier
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      tranId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = TransactionRecord;
  }
  /**
   * No description
   * @tags endorse-transaction
   * @name WriteCreate
   * @summary For Author / Endorser to write an endorsed transaction to the ledger
   * @request POST:/transactions/{tran_id}/write
   * @secure
   * @response `200` `TransactionRecord`
   */
  export namespace WriteCreate {
    export type RequestParams = {
      /**
       * Transaction identifier
       * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
       */
      tranId: string;
    };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = TransactionRecord;
  }
}

export namespace Wallet {
  /**
   * No description
   * @tags wallet
   * @name GetWallet
   * @summary List wallet DIDs
   * @request GET:/wallet/did
   * @secure
   * @response `200` `DIDList`
   */
  export namespace GetWallet {
    export type RequestParams = {};
    export type RequestQuery = {
      /**
       * DID of interest
       * @pattern ^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$|^did:([a-zA-Z0-9_]+):([a-zA-Z0-9_.%-]+(:[a-zA-Z0-9_.%-]+)*)((;[a-zA-Z0-9_.:%-]+=[a-zA-Z0-9_.:%-]*)*)(\/[^#?]*)?([?][^#]*)?(\#.*)?$$
       * @example "did:peer:WgWxqztrNooG92RXvxSTWv"
       */
      did?: string;
      /**
       * Key type to query for.
       * @example "ed25519"
       */
      key_type?: 'ed25519' | 'bls12381g2';
      /**
       * DID method to query for. e.g. sov to only fetch indy/sov DIDs
       * @example "key"
       */
      method?: 'key' | 'sov';
      /**
       * Whether DID is current public DID, posted to ledger but current public DID, or local to the wallet
       * @example "wallet_only"
       */
      posture?: 'public' | 'posted' | 'wallet_only';
      /**
       * Verification key of interest
       * @pattern ^[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{43,44}$
       * @example "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV"
       */
      verkey?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = DIDList;
  }
  /**
   * No description
   * @tags wallet
   * @name DidCreateCreate
   * @summary Create a local DID
   * @request POST:/wallet/did/create
   * @secure
   * @response `200` `DIDResult`
   */
  export namespace DidCreateCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = DIDCreate;
    export type RequestHeaders = {};
    export type ResponseBody = DIDResult;
  }
  /**
   * No description
   * @tags wallet
   * @name DidLocalRotateKeypairPartialUpdate
   * @summary Rotate keypair for a DID not posted to the ledger
   * @request PATCH:/wallet/did/local/rotate-keypair
   * @secure
   * @response `200` `WalletModuleResponse`
   */
  export namespace DidLocalRotateKeypairPartialUpdate {
    export type RequestParams = {};
    export type RequestQuery = {
      /**
       * DID of interest
       * @pattern ^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$
       * @example "WgWxqztrNooG92RXvxSTWv"
       */
      did: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = WalletModuleResponse;
  }
  /**
   * No description
   * @tags wallet
   * @name DidPublicList
   * @summary Fetch the current public DID
   * @request GET:/wallet/did/public
   * @secure
   * @response `200` `DIDResult`
   */
  export namespace DidPublicList {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = DIDResult;
  }
  /**
   * No description
   * @tags wallet
   * @name DidPublicCreate
   * @summary Assign the current public DID
   * @request POST:/wallet/did/public
   * @secure
   * @response `200` `DIDResult`
   */
  export namespace DidPublicCreate {
    export type RequestParams = {};
    export type RequestQuery = {
      /**
       * DID of interest
       * @pattern ^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$
       * @example "WgWxqztrNooG92RXvxSTWv"
       */
      did: string;
      /** Connection identifier */
      conn_id?: string;
      /** Create Transaction For Endorser's signature */
      create_transaction_for_endorser?: boolean;
      /** Mediation identifier */
      mediation_id?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = DIDResult;
  }
  /**
   * No description
   * @tags wallet
   * @name GetDidEndpointList
   * @summary Query DID endpoint in wallet
   * @request GET:/wallet/get-did-endpoint
   * @secure
   * @response `200` `DIDEndpoint`
   */
  export namespace GetDidEndpointList {
    export type RequestParams = {};
    export type RequestQuery = {
      /**
       * DID of interest
       * @pattern ^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$
       * @example "WgWxqztrNooG92RXvxSTWv"
       */
      did: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = DIDEndpoint;
  }
  /**
   * No description
   * @tags wallet
   * @name SetDidEndpointCreate
   * @summary Update endpoint in wallet and on ledger if posted to it
   * @request POST:/wallet/set-did-endpoint
   * @secure
   * @response `200` `WalletModuleResponse`
   */
  export namespace SetDidEndpointCreate {
    export type RequestParams = {};
    export type RequestQuery = {
      /** Connection identifier */
      conn_id?: string;
      /** Create Transaction For Endorser's signature */
      create_transaction_for_endorser?: boolean;
    };
    export type RequestBody = DIDEndpointWithType;
    export type RequestHeaders = {};
    export type ResponseBody = WalletModuleResponse;
  }
}

export type QueryParamsType = Record<string | number, any>;
export type ResponseFormat = keyof Omit<Body, 'body' | 'bodyUsed'>;

export interface FullRequestParams extends Omit<RequestInit, 'body'> {
  /** set parameter to `true` for call `securityWorker` for this request */
  secure?: boolean;
  /** request path */
  path: string;
  /** content type of request body */
  type?: ContentType;
  /** query params */
  query?: QueryParamsType;
  /** format of response (i.e. response.json() -> format: "json") */
  format?: ResponseFormat;
  /** request body */
  body?: unknown;
  /** base url */
  baseUrl?: string;
  /** request cancellation token */
  cancelToken?: CancelToken;
}

export type RequestParams = Omit<FullRequestParams, 'body' | 'method' | 'query' | 'path'>;

export interface ApiConfig<SecurityDataType = unknown> {
  baseUrl?: string;
  baseApiParams?: Omit<RequestParams, 'baseUrl' | 'cancelToken' | 'signal'>;
  securityWorker?: (securityData: SecurityDataType | null) => Promise<RequestParams | void> | RequestParams | void;
  customFetch?: typeof fetch;
}

export interface HttpResponse<D extends unknown, E extends unknown = unknown> extends Response {
  data: D;
  error: E;
}

type CancelToken = Symbol | string | number;

export enum ContentType {
  Json = 'application/json',
  FormData = 'multipart/form-data',
  UrlEncoded = 'application/x-www-form-urlencoded',
  Text = 'text/plain',
}

export class HttpClient<SecurityDataType = unknown> {
  public baseUrl: string = '';
  private securityData: SecurityDataType | null = null;
  private securityWorker?: ApiConfig<SecurityDataType>['securityWorker'];
  private abortControllers = new Map<CancelToken, AbortController>();
  private customFetch = (...fetchParams: Parameters<typeof fetch>) => fetch(...fetchParams);

  private baseApiParams: RequestParams = {
    credentials: 'same-origin',
    headers: {},
    redirect: 'follow',
    referrerPolicy: 'no-referrer',
  };

  constructor(apiConfig: ApiConfig<SecurityDataType> = {}) {
    Object.assign(this, apiConfig);
  }

  public setSecurityData = (data: SecurityDataType | null) => {
    this.securityData = data;
  };

  protected encodeQueryParam(key: string, value: any) {
    const encodedKey = encodeURIComponent(key);
    return `${encodedKey}=${encodeURIComponent(typeof value === 'number' ? value : `${value}`)}`;
  }

  protected addQueryParam(query: QueryParamsType, key: string) {
    return this.encodeQueryParam(key, query[key]);
  }

  protected addArrayQueryParam(query: QueryParamsType, key: string) {
    const value = query[key];
    return value.map((v: any) => this.encodeQueryParam(key, v)).join('&');
  }

  protected toQueryString(rawQuery?: QueryParamsType): string {
    const query = rawQuery || {};
    const keys = Object.keys(query).filter((key) => 'undefined' !== typeof query[key]);
    return keys
      .map((key) => (Array.isArray(query[key]) ? this.addArrayQueryParam(query, key) : this.addQueryParam(query, key)))
      .join('&');
  }

  protected addQueryParams(rawQuery?: QueryParamsType): string {
    const queryString = this.toQueryString(rawQuery);
    return queryString ? `?${queryString}` : '';
  }

  private contentFormatters: Record<ContentType, (input: any) => any> = {
    [ContentType.Json]: (input: any) =>
      input !== null && (typeof input === 'object' || typeof input === 'string') ? JSON.stringify(input) : input,
    [ContentType.Text]: (input: any) => (input !== null && typeof input !== 'string' ? JSON.stringify(input) : input),
    [ContentType.FormData]: (input: any) =>
      Object.keys(input || {}).reduce((formData, key) => {
        const property = input[key];
        formData.append(
          key,
          property instanceof Blob
            ? property
            : typeof property === 'object' && property !== null
            ? JSON.stringify(property)
            : `${property}`,
        );
        return formData;
      }, new FormData()),
    [ContentType.UrlEncoded]: (input: any) => this.toQueryString(input),
  };

  protected mergeRequestParams(params1: RequestParams, params2?: RequestParams): RequestParams {
    return {
      ...this.baseApiParams,
      ...params1,
      ...(params2 || {}),
      headers: {
        ...(this.baseApiParams.headers || {}),
        ...(params1.headers || {}),
        ...((params2 && params2.headers) || {}),
      },
    };
  }

  protected createAbortSignal = (cancelToken: CancelToken): AbortSignal | undefined => {
    if (this.abortControllers.has(cancelToken)) {
      const abortController = this.abortControllers.get(cancelToken);
      if (abortController) {
        return abortController.signal;
      }
      return void 0;
    }

    const abortController = new AbortController();
    this.abortControllers.set(cancelToken, abortController);
    return abortController.signal;
  };

  public abortRequest = (cancelToken: CancelToken) => {
    const abortController = this.abortControllers.get(cancelToken);

    if (abortController) {
      abortController.abort();
      this.abortControllers.delete(cancelToken);
    }
  };

  public request = async <T = any, E = any>({
    body,
    secure,
    path,
    type,
    query,
    format,
    baseUrl,
    cancelToken,
    ...params
  }: FullRequestParams): Promise<HttpResponse<T, E>> => {
    const secureParams =
      ((typeof secure === 'boolean' ? secure : this.baseApiParams.secure) &&
        this.securityWorker &&
        (await this.securityWorker(this.securityData))) ||
      {};
    const requestParams = this.mergeRequestParams(params, secureParams);
    const queryString = query && this.toQueryString(query);
    const payloadFormatter = this.contentFormatters[type || ContentType.Json];
    const responseFormat = format || requestParams.format;

    return this.customFetch(`${baseUrl || this.baseUrl || ''}${path}${queryString ? `?${queryString}` : ''}`, {
      ...requestParams,
      headers: {
        ...(requestParams.headers || {}),
        ...(type && type !== ContentType.FormData ? { 'Content-Type': type } : {}),
      },
      signal: cancelToken ? this.createAbortSignal(cancelToken) : requestParams.signal,
      body: typeof body === 'undefined' || body === null ? null : payloadFormatter(body),
    }).then(async (response) => {
      const r = response as HttpResponse<T, E>;
      r.data = null as unknown as T;
      r.error = null as unknown as E;

      const data = !responseFormat
        ? r
        : await response[responseFormat]()
            .then((data) => {
              if (r.ok) {
                r.data = data;
              } else {
                r.error = data;
              }
              return r;
            })
            .catch((e) => {
              r.error = e;
              return r;
            });

      if (cancelToken) {
        this.abortControllers.delete(cancelToken);
      }

      if (!response.ok) throw data;
      return data;
    });
  };
}

/**
 * @title agent
 * @version v1.0.0-rc1
 */
export class AgentAdminApi<SecurityDataType extends unknown> extends HttpClient<SecurityDataType> {
  actionMenu = {
    /**
     * No description
     *
     * @tags action-menu
     * @name CloseCreate
     * @summary Close the active menu associated with a connection
     * @request POST:/action-menu/{conn_id}/close
     * @secure
     * @response `200` `ActionMenuModulesResult`
     */
    closeCreate: (connId: string, params: RequestParams = {}) =>
      this.request<ActionMenuModulesResult, any>({
        path: `/action-menu/${connId}/close`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags action-menu
     * @name FetchCreate
     * @summary Fetch the active menu
     * @request POST:/action-menu/{conn_id}/fetch
     * @secure
     * @response `200` `ActionMenuFetchResult`
     */
    fetchCreate: (connId: string, params: RequestParams = {}) =>
      this.request<ActionMenuFetchResult, any>({
        path: `/action-menu/${connId}/fetch`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags action-menu
     * @name PerformCreate
     * @summary Perform an action associated with the active menu
     * @request POST:/action-menu/{conn_id}/perform
     * @secure
     * @response `200` `ActionMenuModulesResult`
     */
    performCreate: (connId: string, body: PerformRequest, params: RequestParams = {}) =>
      this.request<ActionMenuModulesResult, any>({
        path: `/action-menu/${connId}/perform`,
        method: 'POST',
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags action-menu
     * @name RequestCreate
     * @summary Request the active menu
     * @request POST:/action-menu/{conn_id}/request
     * @secure
     * @response `200` `ActionMenuModulesResult`
     */
    requestCreate: (connId: string, params: RequestParams = {}) =>
      this.request<ActionMenuModulesResult, any>({
        path: `/action-menu/${connId}/request`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags action-menu
     * @name SendMenuCreate
     * @summary Send an action menu to a connection
     * @request POST:/action-menu/{conn_id}/send-menu
     * @secure
     * @response `200` `ActionMenuModulesResult`
     */
    sendMenuCreate: (connId: string, body: SendMenu, params: RequestParams = {}) =>
      this.request<ActionMenuModulesResult, any>({
        path: `/action-menu/${connId}/send-menu`,
        method: 'POST',
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),
  };
  connections = {
    /**
     * No description
     *
     * @tags connection
     * @name ConnectionsList
     * @summary Query agent-to-agent connections
     * @request GET:/connections
     * @secure
     * @response `200` `ConnectionList`
     */
    connectionsList: (
      query?: {
        /**
         * Alias
         * @example "Barry"
         */
        alias?: string;
        /**
         * Connection protocol used
         * @example "connections/1.0"
         */
        connection_protocol?: 'connections/1.0' | 'didexchange/1.0';
        /**
         * invitation key
         * @pattern ^[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{43,44}$
         * @example "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV"
         */
        invitation_key?: string;
        /**
         * Identifier of the associated Invitation Mesage
         * @format uuid
         * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
         */
        invitation_msg_id?: string;
        /**
         * My DID
         * @pattern ^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$
         * @example "WgWxqztrNooG92RXvxSTWv"
         */
        my_did?: string;
        /** Connection state */
        state?:
          | 'init'
          | 'active'
          | 'completed'
          | 'start'
          | 'response'
          | 'invitation'
          | 'abandoned'
          | 'request'
          | 'error';
        /**
         * Their DID
         * @pattern ^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$
         * @example "WgWxqztrNooG92RXvxSTWv"
         */
        their_did?: string;
        /**
         * Their Public DID
         * @pattern ^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$
         * @example "WgWxqztrNooG92RXvxSTWv"
         */
        their_public_did?: string;
        /**
         * Their role in the connection protocol
         * @example "invitee"
         */
        their_role?: 'invitee' | 'requester' | 'inviter' | 'responder';
      },
      params: RequestParams = {},
    ) =>
      this.request<ConnectionList, any>({
        path: `/connections`,
        method: 'GET',
        query: query,
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags connection
     * @name CreateInvitationCreate
     * @summary Create a new connection invitation
     * @request POST:/connections/create-invitation
     * @secure
     * @response `200` `InvitationResult`
     */
    createInvitationCreate: (
      body: CreateInvitationRequest,
      query?: {
        /**
         * Alias
         * @example "Barry"
         */
        alias?: string;
        /** Auto-accept connection (defaults to configuration) */
        auto_accept?: boolean;
        /** Create invitation for multiple use (default false) */
        multi_use?: boolean;
        /** Create invitation from public DID (default false) */
        public?: boolean;
      },
      params: RequestParams = {},
    ) =>
      this.request<InvitationResult, any>({
        path: `/connections/create-invitation`,
        method: 'POST',
        query: query,
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags connection
     * @name CreateStaticCreate
     * @summary Create a new static connection
     * @request POST:/connections/create-static
     * @secure
     * @response `200` `ConnectionStaticResult`
     */
    createStaticCreate: (body: ConnectionStaticRequest, params: RequestParams = {}) =>
      this.request<ConnectionStaticResult, any>({
        path: `/connections/create-static`,
        method: 'POST',
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags connection
     * @name ReceiveInvitationCreate
     * @summary Receive a new connection invitation
     * @request POST:/connections/receive-invitation
     * @secure
     * @response `200` `ConnRecord`
     */
    receiveInvitationCreate: (
      body: ReceiveInvitationRequest,
      query?: {
        /**
         * Alias
         * @example "Barry"
         */
        alias?: string;
        /** Auto-accept connection (defaults to configuration) */
        auto_accept?: boolean;
        /**
         * Identifier for active mediation record to be used
         * @pattern [a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}
         * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
         */
        mediation_id?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ConnRecord, any>({
        path: `/connections/receive-invitation`,
        method: 'POST',
        query: query,
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags connection
     * @name ConnectionsDetail
     * @summary Fetch a single connection record
     * @request GET:/connections/{conn_id}
     * @secure
     * @response `200` `ConnRecord`
     */
    connectionsDetail: (connId: string, params: RequestParams = {}) =>
      this.request<ConnRecord, any>({
        path: `/connections/${connId}`,
        method: 'GET',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags connection
     * @name ConnectionsDelete
     * @summary Remove an existing connection record
     * @request DELETE:/connections/{conn_id}
     * @secure
     * @response `200` `ConnectionModuleResponse`
     */
    connectionsDelete: (connId: string, params: RequestParams = {}) =>
      this.request<ConnectionModuleResponse, any>({
        path: `/connections/${connId}`,
        method: 'DELETE',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags connection
     * @name AcceptInvitationCreate
     * @summary Accept a stored connection invitation
     * @request POST:/connections/{conn_id}/accept-invitation
     * @secure
     * @response `200` `ConnRecord`
     */
    acceptInvitationCreate: (
      connId: string,
      query?: {
        /**
         * Identifier for active mediation record to be used
         * @pattern [a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}
         * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
         */
        mediation_id?: string;
        /**
         * My URL endpoint
         * @pattern ^[A-Za-z0-9\.\-\+]+://([A-Za-z0-9][.A-Za-z0-9-_]+[A-Za-z0-9])+(:[1-9][0-9]*)?(/[^?&#]+)?$
         * @example "https://myhost:8021"
         */
        my_endpoint?: string;
        /**
         * Label for connection
         * @example "Broker"
         */
        my_label?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ConnRecord, any>({
        path: `/connections/${connId}/accept-invitation`,
        method: 'POST',
        query: query,
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags connection
     * @name AcceptRequestCreate
     * @summary Accept a stored connection request
     * @request POST:/connections/{conn_id}/accept-request
     * @secure
     * @response `200` `ConnRecord`
     */
    acceptRequestCreate: (
      connId: string,
      query?: {
        /**
         * My URL endpoint
         * @pattern ^[A-Za-z0-9\.\-\+]+://([A-Za-z0-9][.A-Za-z0-9-_]+[A-Za-z0-9])+(:[1-9][0-9]*)?(/[^?&#]+)?$
         * @example "https://myhost:8021"
         */
        my_endpoint?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ConnRecord, any>({
        path: `/connections/${connId}/accept-request`,
        method: 'POST',
        query: query,
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags connection
     * @name EndpointsDetail
     * @summary Fetch connection remote endpoint
     * @request GET:/connections/{conn_id}/endpoints
     * @secure
     * @response `200` `EndpointsResult`
     */
    endpointsDetail: (connId: string, params: RequestParams = {}) =>
      this.request<EndpointsResult, any>({
        path: `/connections/${connId}/endpoints`,
        method: 'GET',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags connection
     * @name EstablishInboundCreate
     * @summary Assign another connection as the inbound connection
     * @request POST:/connections/{conn_id}/establish-inbound/{ref_id}
     * @secure
     * @response `200` `ConnectionModuleResponse`
     */
    establishInboundCreate: (connId: string, refId: string, params: RequestParams = {}) =>
      this.request<ConnectionModuleResponse, any>({
        path: `/connections/${connId}/establish-inbound/${refId}`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags connection
     * @name MetadataDetail
     * @summary Fetch connection metadata
     * @request GET:/connections/{conn_id}/metadata
     * @secure
     * @response `200` `ConnectionMetadata`
     */
    metadataDetail: (
      connId: string,
      query?: {
        /** Key to retrieve. */
        key?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ConnectionMetadata, any>({
        path: `/connections/${connId}/metadata`,
        method: 'GET',
        query: query,
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags connection
     * @name MetadataCreate
     * @summary Set connection metadata
     * @request POST:/connections/{conn_id}/metadata
     * @secure
     * @response `200` `ConnectionMetadata`
     */
    metadataCreate: (connId: string, body: ConnectionMetadataSetRequest, params: RequestParams = {}) =>
      this.request<ConnectionMetadata, any>({
        path: `/connections/${connId}/metadata`,
        method: 'POST',
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags basicmessage
     * @name SendMessageCreate
     * @summary Send a basic message to a connection
     * @request POST:/connections/{conn_id}/send-message
     * @secure
     * @response `200` `BasicMessageModuleResponse`
     */
    sendMessageCreate: (connId: string, body: SendMessage, params: RequestParams = {}) =>
      this.request<BasicMessageModuleResponse, any>({
        path: `/connections/${connId}/send-message`,
        method: 'POST',
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags trustping
     * @name SendPingCreate
     * @summary Send a trust ping to a connection
     * @request POST:/connections/{conn_id}/send-ping
     * @secure
     * @response `200` `PingRequestResponse`
     */
    sendPingCreate: (connId: string, body: PingRequest, params: RequestParams = {}) =>
      this.request<PingRequestResponse, any>({
        path: `/connections/${connId}/send-ping`,
        method: 'POST',
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags introduction
     * @name StartIntroductionCreate
     * @summary Start an introduction between two connections
     * @request POST:/connections/{conn_id}/start-introduction
     * @secure
     * @response `200` `IntroModuleResponse`
     */
    startIntroductionCreate: (
      connId: string,
      query: {
        /**
         * Target connection identifier
         * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
         */
        target_connection_id: string;
        /**
         * Message
         * @example "Allow me to introduce ..."
         */
        message?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<IntroModuleResponse, any>({
        path: `/connections/${connId}/start-introduction`,
        method: 'POST',
        query: query,
        secure: true,
        format: 'json',
        ...params,
      }),
  };
  credentialDefinitions = {
    /**
     * No description
     *
     * @tags credential-definition
     * @name CredentialDefinitionsCreate
     * @summary Sends a credential definition to the ledger
     * @request POST:/credential-definitions
     * @secure
     * @response `200` `TxnOrCredentialDefinitionSendResult`
     */
    credentialDefinitionsCreate: (
      body: CredentialDefinitionSendRequest,
      query?: {
        /**
         * Connection identifier
         * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
         */
        conn_id?: string;
        /** Create Transaction For Endorser's signature */
        create_transaction_for_endorser?: boolean;
      },
      params: RequestParams = {},
    ) =>
      this.request<TxnOrCredentialDefinitionSendResult, any>({
        path: `/credential-definitions`,
        method: 'POST',
        query: query,
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags credential-definition
     * @name CreatedList
     * @summary Search for matching credential definitions that agent originated
     * @request GET:/credential-definitions/created
     * @secure
     * @response `200` `CredentialDefinitionsCreatedResult`
     */
    createdList: (
      query?: {
        /**
         * Credential definition id
         * @pattern ^([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):3:CL:(([1-9][0-9]*)|([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+)):(.+)?$
         * @example "WgWxqztrNooG92RXvxSTWv:3:CL:20:tag"
         */
        cred_def_id?: string;
        /**
         * Issuer DID
         * @pattern ^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$
         * @example "WgWxqztrNooG92RXvxSTWv"
         */
        issuer_did?: string;
        /**
         * Schema identifier
         * @pattern ^[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+$
         * @example "WgWxqztrNooG92RXvxSTWv:2:schema_name:1.0"
         */
        schema_id?: string;
        /**
         * Schema issuer DID
         * @pattern ^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$
         * @example "WgWxqztrNooG92RXvxSTWv"
         */
        schema_issuer_did?: string;
        /**
         * Schema name
         * @example "membership"
         */
        schema_name?: string;
        /**
         * Schema version
         * @pattern ^[0-9.]+$
         * @example "1.0"
         */
        schema_version?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<CredentialDefinitionsCreatedResult, any>({
        path: `/credential-definitions/created`,
        method: 'GET',
        query: query,
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags credential-definition
     * @name CredentialDefinitionsDetail
     * @summary Gets a credential definition from the ledger
     * @request GET:/credential-definitions/{cred_def_id}
     * @secure
     * @response `200` `CredentialDefinitionGetResult`
     */
    credentialDefinitionsDetail: (credDefId: string, params: RequestParams = {}) =>
      this.request<CredentialDefinitionGetResult, any>({
        path: `/credential-definitions/${credDefId}`,
        method: 'GET',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags credential-definition
     * @name WriteRecordCreate
     * @summary Writes a credential definition non-secret record to the wallet
     * @request POST:/credential-definitions/{cred_def_id}/write_record
     * @secure
     * @response `200` `CredentialDefinitionGetResult`
     */
    writeRecordCreate: (credDefId: string, params: RequestParams = {}) =>
      this.request<CredentialDefinitionGetResult, any>({
        path: `/credential-definitions/${credDefId}/write_record`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),
  };
  credential = {
    /**
     * No description
     *
     * @tags credentials
     * @name MimeTypesDetail
     * @summary Get attribute MIME types from wallet
     * @request GET:/credential/mime-types/{credential_id}
     * @secure
     * @response `200` `AttributeMimeTypesResult`
     */
    mimeTypesDetail: (credentialId: string, params: RequestParams = {}) =>
      this.request<AttributeMimeTypesResult, any>({
        path: `/credential/mime-types/${credentialId}`,
        method: 'GET',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags credentials
     * @name RevokedDetail
     * @summary Query credential revocation status by id
     * @request GET:/credential/revoked/{credential_id}
     * @secure
     * @response `200` `CredRevokedResult`
     */
    revokedDetail: (
      credentialId: string,
      query?: {
        /**
         * Earliest epoch of revocation status interval of interest
         * @pattern ^[0-9]*$
         * @example "0"
         */
        from?: string;
        /**
         * Latest epoch of revocation status interval of interest
         * @pattern ^[0-9]*$
         * @example "0"
         */
        to?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<CredRevokedResult, any>({
        path: `/credential/revoked/${credentialId}`,
        method: 'GET',
        query: query,
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags credentials
     * @name GetCredential
     * @summary Fetch W3C credential from wallet by id
     * @request GET:/credential/w3c/{credential_id}
     * @secure
     * @response `200` `VCRecord`
     */
    getCredential: (credentialId: string, params: RequestParams = {}) =>
      this.request<VCRecord, any>({
        path: `/credential/w3c/${credentialId}`,
        method: 'GET',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags credentials
     * @name DeleteCredential
     * @summary Remove W3C credential from wallet by id
     * @request DELETE:/credential/w3c/{credential_id}
     * @secure
     * @response `200` `HolderModuleResponse`
     */
    deleteCredential: (credentialId: string, params: RequestParams = {}) =>
      this.request<HolderModuleResponse, any>({
        path: `/credential/w3c/${credentialId}`,
        method: 'DELETE',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags credentials
     * @name CredentialDetail
     * @summary Fetch credential from wallet by id
     * @request GET:/credential/{credential_id}
     * @secure
     * @response `200` `IndyCredInfo`
     */
    credentialDetail: (credentialId: string, params: RequestParams = {}) =>
      this.request<IndyCredInfo, any>({
        path: `/credential/${credentialId}`,
        method: 'GET',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags credentials
     * @name CredentialDelete
     * @summary Remove credential from wallet by id
     * @request DELETE:/credential/{credential_id}
     * @secure
     * @response `200` `HolderModuleResponse`
     */
    credentialDelete: (credentialId: string, params: RequestParams = {}) =>
      this.request<HolderModuleResponse, any>({
        path: `/credential/${credentialId}`,
        method: 'DELETE',
        secure: true,
        format: 'json',
        ...params,
      }),
  };
  credentials = {
    /**
     * No description
     *
     * @tags credentials
     * @name CredentialsList
     * @summary Fetch credentials from wallet
     * @request GET:/credentials
     * @secure
     * @response `200` `CredInfoList`
     */
    credentialsList: (
      query?: {
        /**
         * Maximum number to retrieve
         * @pattern ^[1-9][0-9]*$
         * @example "1"
         */
        count?: string;
        /**
         * Start index
         * @pattern ^[0-9]*$
         * @example "0"
         */
        start?: string;
        /**
         * (JSON) WQL query
         * @pattern ^{.*}$
         * @example "{"attr::name::value": "Alex"}"
         */
        wql?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<CredInfoList, any>({
        path: `/credentials`,
        method: 'GET',
        query: query,
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags credentials
     * @name PostCredentials
     * @summary Fetch W3C credentials from wallet
     * @request POST:/credentials/w3c
     * @secure
     * @response `200` `VCRecordList`
     */
    postCredentials: (
      body: W3CCredentialsListRequest,
      query?: {
        /**
         * Maximum number to retrieve
         * @pattern ^[1-9][0-9]*$
         * @example "1"
         */
        count?: string;
        /**
         * Start index
         * @pattern ^[0-9]*$
         * @example "0"
         */
        start?: string;
        /**
         * (JSON) WQL query
         * @pattern ^{.*}$
         * @example "{"attr::name::value": "Alex"}"
         */
        wql?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<VCRecordList, any>({
        path: `/credentials/w3c`,
        method: 'POST',
        query: query,
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),
  };
  didexchange = {
    /**
     * No description
     *
     * @tags did-exchange
     * @name CreateRequestCreate
     * @summary Create and send a request against public DID's implicit invitation
     * @request POST:/didexchange/create-request
     * @secure
     * @response `200` `ConnRecord`
     */
    createRequestCreate: (
      query: {
        /**
         * Qualified public DID to which to request connection
         * @pattern ^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$|^did:([a-zA-Z0-9_]+):([a-zA-Z0-9_.%-]+(:[a-zA-Z0-9_.%-]+)*)((;[a-zA-Z0-9_.:%-]+=[a-zA-Z0-9_.:%-]*)*)(\/[^#?]*)?([?][^#]*)?(\#.*)?$$
         * @example "did:peer:WgWxqztrNooG92RXvxSTWv"
         */
        their_public_did: string;
        /**
         * Alias for connection
         * @example "Barry"
         */
        alias?: string;
        /**
         * Identifier for active mediation record to be used
         * @pattern [a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}
         * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
         */
        mediation_id?: string;
        /**
         * My URL endpoint
         * @pattern ^[A-Za-z0-9\.\-\+]+://([A-Za-z0-9][.A-Za-z0-9-_]+[A-Za-z0-9])+(:[1-9][0-9]*)?(/[^?&#]+)?$
         * @example "https://myhost:8021"
         */
        my_endpoint?: string;
        /**
         * Label for connection request
         * @example "Broker"
         */
        my_label?: string;
        /** Use public DID for this connection */
        use_public_did?: boolean;
      },
      params: RequestParams = {},
    ) =>
      this.request<ConnRecord, any>({
        path: `/didexchange/create-request`,
        method: 'POST',
        query: query,
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags did-exchange
     * @name ReceiveRequestCreate
     * @summary Receive request against public DID's implicit invitation
     * @request POST:/didexchange/receive-request
     * @secure
     * @response `200` `ConnRecord`
     */
    receiveRequestCreate: (
      body: DIDXRequest,
      query?: {
        /**
         * Alias for connection
         * @example "Barry"
         */
        alias?: string;
        /** Auto-accept connection (defaults to configuration) */
        auto_accept?: boolean;
        /**
         * Identifier for active mediation record to be used
         * @pattern [a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}
         * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
         */
        mediation_id?: string;
        /**
         * My URL endpoint
         * @pattern ^[A-Za-z0-9\.\-\+]+://([A-Za-z0-9][.A-Za-z0-9-_]+[A-Za-z0-9])+(:[1-9][0-9]*)?(/[^?&#]+)?$
         * @example "https://myhost:8021"
         */
        my_endpoint?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ConnRecord, any>({
        path: `/didexchange/receive-request`,
        method: 'POST',
        query: query,
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags did-exchange
     * @name AcceptInvitationCreate
     * @summary Accept a stored connection invitation
     * @request POST:/didexchange/{conn_id}/accept-invitation
     * @secure
     * @response `200` `ConnRecord`
     */
    acceptInvitationCreate: (
      connId: string,
      query?: {
        /**
         * My URL endpoint
         * @pattern ^[A-Za-z0-9\.\-\+]+://([A-Za-z0-9][.A-Za-z0-9-_]+[A-Za-z0-9])+(:[1-9][0-9]*)?(/[^?&#]+)?$
         * @example "https://myhost:8021"
         */
        my_endpoint?: string;
        /**
         * Label for connection request
         * @example "Broker"
         */
        my_label?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ConnRecord, any>({
        path: `/didexchange/${connId}/accept-invitation`,
        method: 'POST',
        query: query,
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags did-exchange
     * @name AcceptRequestCreate
     * @summary Accept a stored connection request
     * @request POST:/didexchange/{conn_id}/accept-request
     * @secure
     * @response `200` `ConnRecord`
     */
    acceptRequestCreate: (
      connId: string,
      query?: {
        /**
         * Identifier for active mediation record to be used
         * @pattern [a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}
         * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
         */
        mediation_id?: string;
        /**
         * My URL endpoint
         * @pattern ^[A-Za-z0-9\.\-\+]+://([A-Za-z0-9][.A-Za-z0-9-_]+[A-Za-z0-9])+(:[1-9][0-9]*)?(/[^?&#]+)?$
         * @example "https://myhost:8021"
         */
        my_endpoint?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ConnRecord, any>({
        path: `/didexchange/${connId}/accept-request`,
        method: 'POST',
        query: query,
        secure: true,
        format: 'json',
        ...params,
      }),
  };
  discoverFeatures20 = {
    /**
     * No description
     *
     * @tags discover-features v2.0
     * @name QueriesList
     * @summary Query supported features
     * @request GET:/discover-features-2.0/queries
     * @secure
     * @response `200` `V20DiscoveryExchangeResult`
     */
    queriesList: (
      query?: {
        /**
         * Connection identifier, if none specified, then the query will provide features for this agent.
         * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
         */
        connection_id?: string;
        /**
         * Goal-code feature-type query
         * @example "*"
         */
        query_goal_code?: string;
        /**
         * Protocol feature-type query
         * @example "*"
         */
        query_protocol?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<V20DiscoveryExchangeResult, any>({
        path: `/discover-features-2.0/queries`,
        method: 'GET',
        query: query,
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags discover-features v2.0
     * @name RecordsList
     * @summary Discover Features v2.0 records
     * @request GET:/discover-features-2.0/records
     * @secure
     * @response `200` `V20DiscoveryExchangeListResult`
     */
    recordsList: (
      query?: {
        /**
         * Connection identifier
         * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
         */
        connection_id?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<V20DiscoveryExchangeListResult, any>({
        path: `/discover-features-2.0/records`,
        method: 'GET',
        query: query,
        secure: true,
        format: 'json',
        ...params,
      }),
  };
  discoverFeatures = {
    /**
     * No description
     *
     * @tags discover-features
     * @name QueryList
     * @summary Query supported features
     * @request GET:/discover-features/query
     * @secure
     * @response `200` `V10DiscoveryRecord`
     */
    queryList: (
      query?: {
        /**
         * Comment
         * @example "test"
         */
        comment?: string;
        /**
         * Connection identifier, if none specified, then the query will provide features for this agent.
         * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
         */
        connection_id?: string;
        /**
         * Protocol feature query
         * @example "*"
         */
        query?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<V10DiscoveryRecord, any>({
        path: `/discover-features/query`,
        method: 'GET',
        query: query,
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags discover-features
     * @name RecordsList
     * @summary Discover Features records
     * @request GET:/discover-features/records
     * @secure
     * @response `200` `V10DiscoveryExchangeListResult`
     */
    recordsList: (
      query?: {
        /**
         * Connection identifier
         * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
         */
        connection_id?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<V10DiscoveryExchangeListResult, any>({
        path: `/discover-features/records`,
        method: 'GET',
        query: query,
        secure: true,
        format: 'json',
        ...params,
      }),
  };
  issueCredential20 = {
    /**
     * No description
     *
     * @tags issue-credential v2.0
     * @name CreateCreate
     * @summary Create a credential record without sending (generally for use with Out-Of-Band)
     * @request POST:/issue-credential-2.0/create
     * @secure
     * @response `200` `V20CredExRecord`
     */
    createCreate: (body: V20IssueCredSchemaCore, params: RequestParams = {}) =>
      this.request<V20CredExRecord, any>({
        path: `/issue-credential-2.0/create`,
        method: 'POST',
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags issue-credential v2.0
     * @name CreateOfferCreate
     * @summary Create a credential offer, independent of any proposal or connection
     * @request POST:/issue-credential-2.0/create-offer
     * @secure
     * @response `200` `V20CredExRecord`
     */
    createOfferCreate: (body: V20CredOfferConnFreeRequest, params: RequestParams = {}) =>
      this.request<V20CredExRecord, any>({
        path: `/issue-credential-2.0/create-offer`,
        method: 'POST',
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags issue-credential v2.0
     * @name RecordsList
     * @summary Fetch all credential exchange records
     * @request GET:/issue-credential-2.0/records
     * @secure
     * @response `200` `V20CredExRecordListResult`
     */
    recordsList: (
      query?: {
        /**
         * Connection identifier
         * @format uuid
         * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
         */
        connection_id?: string;
        /** Role assigned in credential exchange */
        role?: 'issuer' | 'holder';
        /** Credential exchange state */
        state?:
          | 'proposal-sent'
          | 'proposal-received'
          | 'offer-sent'
          | 'offer-received'
          | 'request-sent'
          | 'request-received'
          | 'credential-issued'
          | 'credential-received'
          | 'done'
          | 'credential-revoked'
          | 'abandoned';
        /**
         * Thread identifier
         * @format uuid
         * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
         */
        thread_id?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<V20CredExRecordListResult, any>({
        path: `/issue-credential-2.0/records`,
        method: 'GET',
        query: query,
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags issue-credential v2.0
     * @name RecordsDetail
     * @summary Fetch a single credential exchange record
     * @request GET:/issue-credential-2.0/records/{cred_ex_id}
     * @secure
     * @response `200` `V20CredExRecordDetail`
     */
    recordsDetail: (credExId: string, params: RequestParams = {}) =>
      this.request<V20CredExRecordDetail, any>({
        path: `/issue-credential-2.0/records/${credExId}`,
        method: 'GET',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags issue-credential v2.0
     * @name RecordsDelete
     * @summary Remove an existing credential exchange record
     * @request DELETE:/issue-credential-2.0/records/{cred_ex_id}
     * @secure
     * @response `200` `V20IssueCredentialModuleResponse`
     */
    recordsDelete: (credExId: string, params: RequestParams = {}) =>
      this.request<V20IssueCredentialModuleResponse, any>({
        path: `/issue-credential-2.0/records/${credExId}`,
        method: 'DELETE',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags issue-credential v2.0
     * @name RecordsIssueCreate
     * @summary Send holder a credential
     * @request POST:/issue-credential-2.0/records/{cred_ex_id}/issue
     * @secure
     * @response `200` `V20CredExRecordDetail`
     */
    recordsIssueCreate: (credExId: string, body: V20CredIssueRequest, params: RequestParams = {}) =>
      this.request<V20CredExRecordDetail, any>({
        path: `/issue-credential-2.0/records/${credExId}/issue`,
        method: 'POST',
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags issue-credential v2.0
     * @name RecordsProblemReportCreate
     * @summary Send a problem report for credential exchange
     * @request POST:/issue-credential-2.0/records/{cred_ex_id}/problem-report
     * @secure
     * @response `200` `V20IssueCredentialModuleResponse`
     */
    recordsProblemReportCreate: (
      credExId: string,
      body: V20CredIssueProblemReportRequest,
      params: RequestParams = {},
    ) =>
      this.request<V20IssueCredentialModuleResponse, any>({
        path: `/issue-credential-2.0/records/${credExId}/problem-report`,
        method: 'POST',
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags issue-credential v2.0
     * @name RecordsSendOfferCreate
     * @summary Send holder a credential offer in reference to a proposal with preview
     * @request POST:/issue-credential-2.0/records/{cred_ex_id}/send-offer
     * @secure
     * @response `200` `V20CredExRecord`
     */
    recordsSendOfferCreate: (credExId: string, body: V20CredBoundOfferRequest, params: RequestParams = {}) =>
      this.request<V20CredExRecord, any>({
        path: `/issue-credential-2.0/records/${credExId}/send-offer`,
        method: 'POST',
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags issue-credential v2.0
     * @name RecordsSendRequestCreate
     * @summary Send issuer a credential request
     * @request POST:/issue-credential-2.0/records/{cred_ex_id}/send-request
     * @secure
     * @response `200` `V20CredExRecord`
     */
    recordsSendRequestCreate: (credExId: string, body: V20CredRequestRequest, params: RequestParams = {}) =>
      this.request<V20CredExRecord, any>({
        path: `/issue-credential-2.0/records/${credExId}/send-request`,
        method: 'POST',
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags issue-credential v2.0
     * @name RecordsStoreCreate
     * @summary Store a received credential
     * @request POST:/issue-credential-2.0/records/{cred_ex_id}/store
     * @secure
     * @response `200` `V20CredExRecordDetail`
     */
    recordsStoreCreate: (credExId: string, body: V20CredStoreRequest, params: RequestParams = {}) =>
      this.request<V20CredExRecordDetail, any>({
        path: `/issue-credential-2.0/records/${credExId}/store`,
        method: 'POST',
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags issue-credential v2.0
     * @name SendCreate
     * @summary Send holder a credential, automating entire flow
     * @request POST:/issue-credential-2.0/send
     * @secure
     * @response `200` `V20CredExRecord`
     */
    sendCreate: (body: V20CredExFree, params: RequestParams = {}) =>
      this.request<V20CredExRecord, any>({
        path: `/issue-credential-2.0/send`,
        method: 'POST',
        body: body,
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags issue-credential v2.0
     * @name SendOfferCreate
     * @summary Send holder a credential offer, independent of any proposal
     * @request POST:/issue-credential-2.0/send-offer
     * @secure
     * @response `200` `V20CredExRecord`
     */
    sendOfferCreate: (body: V20CredOfferRequest, params: RequestParams = {}) =>
      this.request<V20CredExRecord, any>({
        path: `/issue-credential-2.0/send-offer`,
        method: 'POST',
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags issue-credential v2.0
     * @name SendProposalCreate
     * @summary Send issuer a credential proposal
     * @request POST:/issue-credential-2.0/send-proposal
     * @secure
     * @response `200` `V20CredExRecord`
     */
    sendProposalCreate: (body: V20CredExFree, params: RequestParams = {}) =>
      this.request<V20CredExRecord, any>({
        path: `/issue-credential-2.0/send-proposal`,
        method: 'POST',
        body: body,
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags issue-credential v2.0
     * @name SendRequestCreate
     * @summary Send issuer a credential request not bound to an existing thread. Indy credentials cannot start at a request
     * @request POST:/issue-credential-2.0/send-request
     * @secure
     * @response `200` `V20CredExRecord`
     */
    sendRequestCreate: (body: V20CredRequestFree, params: RequestParams = {}) =>
      this.request<V20CredExRecord, any>({
        path: `/issue-credential-2.0/send-request`,
        method: 'POST',
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),
  };
  issueCredential = {
    /**
     * No description
     *
     * @tags issue-credential v1.0
     * @name CreateCreate
     * @summary Create a credential record without sending (generally for use with Out-Of-Band)
     * @request POST:/issue-credential/create
     * @secure
     * @response `200` `V10CredentialExchange`
     */
    createCreate: (body: V10CredentialCreate, params: RequestParams = {}) =>
      this.request<V10CredentialExchange, any>({
        path: `/issue-credential/create`,
        method: 'POST',
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags issue-credential v1.0
     * @name CreateOfferCreate
     * @summary Create a credential offer, independent of any proposal or connection
     * @request POST:/issue-credential/create-offer
     * @secure
     * @response `200` `V10CredentialExchange`
     */
    createOfferCreate: (body: V10CredentialConnFreeOfferRequest, params: RequestParams = {}) =>
      this.request<V10CredentialExchange, any>({
        path: `/issue-credential/create-offer`,
        method: 'POST',
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags issue-credential v1.0
     * @name RecordsList
     * @summary Fetch all credential exchange records
     * @request GET:/issue-credential/records
     * @secure
     * @response `200` `V10CredentialExchangeListResult`
     */
    recordsList: (
      query?: {
        /**
         * Connection identifier
         * @format uuid
         * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
         */
        connection_id?: string;
        /** Role assigned in credential exchange */
        role?: 'issuer' | 'holder';
        /** Credential exchange state */
        state?:
          | 'proposal_sent'
          | 'proposal_received'
          | 'offer_sent'
          | 'offer_received'
          | 'request_sent'
          | 'request_received'
          | 'credential_issued'
          | 'credential_received'
          | 'credential_acked'
          | 'credential_revoked'
          | 'abandoned';
        /**
         * Thread identifier
         * @format uuid
         * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
         */
        thread_id?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<V10CredentialExchangeListResult, any>({
        path: `/issue-credential/records`,
        method: 'GET',
        query: query,
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags issue-credential v1.0
     * @name RecordsDetail
     * @summary Fetch a single credential exchange record
     * @request GET:/issue-credential/records/{cred_ex_id}
     * @secure
     * @response `200` `V10CredentialExchange`
     */
    recordsDetail: (credExId: string, params: RequestParams = {}) =>
      this.request<V10CredentialExchange, any>({
        path: `/issue-credential/records/${credExId}`,
        method: 'GET',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags issue-credential v1.0
     * @name RecordsDelete
     * @summary Remove an existing credential exchange record
     * @request DELETE:/issue-credential/records/{cred_ex_id}
     * @secure
     * @response `200` `IssueCredentialModuleResponse`
     */
    recordsDelete: (credExId: string, params: RequestParams = {}) =>
      this.request<IssueCredentialModuleResponse, any>({
        path: `/issue-credential/records/${credExId}`,
        method: 'DELETE',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags issue-credential v1.0
     * @name RecordsIssueCreate
     * @summary Send holder a credential
     * @request POST:/issue-credential/records/{cred_ex_id}/issue
     * @secure
     * @response `200` `V10CredentialExchange`
     */
    recordsIssueCreate: (credExId: string, body: V10CredentialIssueRequest, params: RequestParams = {}) =>
      this.request<V10CredentialExchange, any>({
        path: `/issue-credential/records/${credExId}/issue`,
        method: 'POST',
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags issue-credential v1.0
     * @name RecordsProblemReportCreate
     * @summary Send a problem report for credential exchange
     * @request POST:/issue-credential/records/{cred_ex_id}/problem-report
     * @secure
     * @response `200` `IssueCredentialModuleResponse`
     */
    recordsProblemReportCreate: (
      credExId: string,
      body: V10CredentialProblemReportRequest,
      params: RequestParams = {},
    ) =>
      this.request<IssueCredentialModuleResponse, any>({
        path: `/issue-credential/records/${credExId}/problem-report`,
        method: 'POST',
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags issue-credential v1.0
     * @name RecordsSendOfferCreate
     * @summary Send holder a credential offer in reference to a proposal with preview
     * @request POST:/issue-credential/records/{cred_ex_id}/send-offer
     * @secure
     * @response `200` `V10CredentialExchange`
     */
    recordsSendOfferCreate: (credExId: string, body: V10CredentialBoundOfferRequest, params: RequestParams = {}) =>
      this.request<V10CredentialExchange, any>({
        path: `/issue-credential/records/${credExId}/send-offer`,
        method: 'POST',
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags issue-credential v1.0
     * @name RecordsSendRequestCreate
     * @summary Send issuer a credential request
     * @request POST:/issue-credential/records/{cred_ex_id}/send-request
     * @secure
     * @response `200` `V10CredentialExchange`
     */
    recordsSendRequestCreate: (credExId: string, params: RequestParams = {}) =>
      this.request<V10CredentialExchange, any>({
        path: `/issue-credential/records/${credExId}/send-request`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags issue-credential v1.0
     * @name RecordsStoreCreate
     * @summary Store a received credential
     * @request POST:/issue-credential/records/{cred_ex_id}/store
     * @secure
     * @response `200` `V10CredentialExchange`
     */
    recordsStoreCreate: (credExId: string, body: V10CredentialStoreRequest, params: RequestParams = {}) =>
      this.request<V10CredentialExchange, any>({
        path: `/issue-credential/records/${credExId}/store`,
        method: 'POST',
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags issue-credential v1.0
     * @name SendCreate
     * @summary Send holder a credential, automating entire flow
     * @request POST:/issue-credential/send
     * @secure
     * @response `200` `V10CredentialExchange`
     */
    sendCreate: (body: V10CredentialProposalRequestMand, params: RequestParams = {}) =>
      this.request<V10CredentialExchange, any>({
        path: `/issue-credential/send`,
        method: 'POST',
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags issue-credential v1.0
     * @name SendOfferCreate
     * @summary Send holder a credential offer, independent of any proposal
     * @request POST:/issue-credential/send-offer
     * @secure
     * @response `200` `V10CredentialExchange`
     */
    sendOfferCreate: (body: V10CredentialFreeOfferRequest, params: RequestParams = {}) =>
      this.request<V10CredentialExchange, any>({
        path: `/issue-credential/send-offer`,
        method: 'POST',
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags issue-credential v1.0
     * @name SendProposalCreate
     * @summary Send issuer a credential proposal
     * @request POST:/issue-credential/send-proposal
     * @secure
     * @response `200` `V10CredentialExchange`
     */
    sendProposalCreate: (body: V10CredentialProposalRequestOpt, params: RequestParams = {}) =>
      this.request<V10CredentialExchange, any>({
        path: `/issue-credential/send-proposal`,
        method: 'POST',
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),
  };
  jsonld = {
    /**
     * No description
     *
     * @tags jsonld
     * @name SignCreate
     * @summary Sign a JSON-LD structure and return it
     * @request POST:/jsonld/sign
     * @secure
     * @response `200` `SignResponse`
     */
    signCreate: (body: SignRequest, params: RequestParams = {}) =>
      this.request<SignResponse, any>({
        path: `/jsonld/sign`,
        method: 'POST',
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags jsonld
     * @name VerifyCreate
     * @summary Verify a JSON-LD structure.
     * @request POST:/jsonld/verify
     * @secure
     * @response `200` `VerifyResponse`
     */
    verifyCreate: (body: VerifyRequest, params: RequestParams = {}) =>
      this.request<VerifyResponse, any>({
        path: `/jsonld/verify`,
        method: 'POST',
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),
  };
  ledger = {
    /**
     * No description
     *
     * @tags ledger
     * @name DidEndpointList
     * @summary Get the endpoint for a DID from the ledger.
     * @request GET:/ledger/did-endpoint
     * @secure
     * @response `200` `GetDIDEndpointResponse`
     */
    didEndpointList: (
      query: {
        /**
         * DID of interest
         * @pattern ^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$
         * @example "WgWxqztrNooG92RXvxSTWv"
         */
        did: string;
        /**
         * Endpoint type of interest (default 'Endpoint')
         * @example "Endpoint"
         */
        endpoint_type?: 'Endpoint' | 'Profile' | 'LinkedDomains';
      },
      params: RequestParams = {},
    ) =>
      this.request<GetDIDEndpointResponse, any>({
        path: `/ledger/did-endpoint`,
        method: 'GET',
        query: query,
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags ledger
     * @name DidVerkeyList
     * @summary Get the verkey for a DID from the ledger.
     * @request GET:/ledger/did-verkey
     * @secure
     * @response `200` `GetDIDVerkeyResponse`
     */
    didVerkeyList: (
      query: {
        /**
         * DID of interest
         * @pattern ^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$
         * @example "WgWxqztrNooG92RXvxSTWv"
         */
        did: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<GetDIDVerkeyResponse, any>({
        path: `/ledger/did-verkey`,
        method: 'GET',
        query: query,
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags ledger
     * @name GetNymRoleList
     * @summary Get the role from the NYM registration of a public DID.
     * @request GET:/ledger/get-nym-role
     * @secure
     * @response `200` `GetNymRoleResponse`
     */
    getNymRoleList: (
      query: {
        /**
         * DID of interest
         * @pattern ^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$
         * @example "WgWxqztrNooG92RXvxSTWv"
         */
        did: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<GetNymRoleResponse, any>({
        path: `/ledger/get-nym-role`,
        method: 'GET',
        query: query,
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags ledger
     * @name MultipleConfigList
     * @summary Fetch the multiple ledger configuration currently in use
     * @request GET:/ledger/multiple/config
     * @secure
     * @response `200` `LedgerConfigList`
     */
    multipleConfigList: (params: RequestParams = {}) =>
      this.request<LedgerConfigList, any>({
        path: `/ledger/multiple/config`,
        method: 'GET',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags ledger
     * @name MultipleGetWriteLedgerList
     * @summary Fetch the current write ledger
     * @request GET:/ledger/multiple/get-write-ledger
     * @secure
     * @response `200` `WriteLedgerRequest`
     */
    multipleGetWriteLedgerList: (params: RequestParams = {}) =>
      this.request<WriteLedgerRequest, any>({
        path: `/ledger/multiple/get-write-ledger`,
        method: 'GET',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags ledger
     * @name RegisterNymCreate
     * @summary Send a NYM registration to the ledger.
     * @request POST:/ledger/register-nym
     * @secure
     * @response `200` `TxnOrRegisterLedgerNymResponse`
     */
    registerNymCreate: (
      query: {
        /**
         * DID to register
         * @pattern ^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$
         * @example "WgWxqztrNooG92RXvxSTWv"
         */
        did: string;
        /**
         * Verification key
         * @pattern ^[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{43,44}$
         * @example "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV"
         */
        verkey: string;
        /**
         * Alias
         * @example "Barry"
         */
        alias?: string;
        /**
         * Connection identifier
         * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
         */
        conn_id?: string;
        /** Create Transaction For Endorser's signature */
        create_transaction_for_endorser?: boolean;
        /** Role */
        role?: 'STEWARD' | 'TRUSTEE' | 'ENDORSER' | 'NETWORK_MONITOR' | 'reset';
      },
      params: RequestParams = {},
    ) =>
      this.request<TxnOrRegisterLedgerNymResponse, any>({
        path: `/ledger/register-nym`,
        method: 'POST',
        query: query,
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags ledger
     * @name RotatePublicDidKeypairPartialUpdate
     * @summary Rotate key pair for public DID.
     * @request PATCH:/ledger/rotate-public-did-keypair
     * @secure
     * @response `200` `LedgerModulesResult`
     */
    rotatePublicDidKeypairPartialUpdate: (params: RequestParams = {}) =>
      this.request<LedgerModulesResult, any>({
        path: `/ledger/rotate-public-did-keypair`,
        method: 'PATCH',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags ledger
     * @name GetLedger
     * @summary Fetch the current transaction author agreement, if any
     * @request GET:/ledger/taa
     * @secure
     * @response `200` `TAAResult`
     */
    getLedger: (params: RequestParams = {}) =>
      this.request<TAAResult, any>({
        path: `/ledger/taa`,
        method: 'GET',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags ledger
     * @name TaaAcceptCreate
     * @summary Accept the transaction author agreement
     * @request POST:/ledger/taa/accept
     * @secure
     * @response `200` `LedgerModulesResult`
     */
    taaAcceptCreate: (body: TAAAccept, params: RequestParams = {}) =>
      this.request<LedgerModulesResult, any>({
        path: `/ledger/taa/accept`,
        method: 'POST',
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),
  };
  mediation = {
    /**
     * No description
     *
     * @tags mediation
     * @name DefaultMediatorList
     * @summary Get default mediator
     * @request GET:/mediation/default-mediator
     * @secure
     * @response `200` `MediationRecord`
     */
    defaultMediatorList: (params: RequestParams = {}) =>
      this.request<MediationRecord, any>({
        path: `/mediation/default-mediator`,
        method: 'GET',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags mediation
     * @name DefaultMediatorDelete
     * @summary Clear default mediator
     * @request DELETE:/mediation/default-mediator
     * @secure
     * @response `201` `MediationRecord`
     */
    defaultMediatorDelete: (params: RequestParams = {}) =>
      this.request<MediationRecord, any>({
        path: `/mediation/default-mediator`,
        method: 'DELETE',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags mediation
     * @name KeylistsList
     * @summary Retrieve keylists by connection or role
     * @request GET:/mediation/keylists
     * @secure
     * @response `200` `Keylist`
     */
    keylistsList: (
      query?: {
        /**
         * Connection identifier (optional)
         * @format uuid
         * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
         */
        conn_id?: string;
        /**
         * Filer on role, 'client' for keys         mediated by other agents, 'server' for keys         mediated by this agent
         * @default "server"
         */
        role?: 'client' | 'server';
      },
      params: RequestParams = {},
    ) =>
      this.request<Keylist, any>({
        path: `/mediation/keylists`,
        method: 'GET',
        query: query,
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags mediation
     * @name KeylistsSendKeylistQueryCreate
     * @summary Send keylist query to mediator
     * @request POST:/mediation/keylists/{mediation_id}/send-keylist-query
     * @secure
     * @response `201` `KeylistQuery`
     */
    keylistsSendKeylistQueryCreate: (
      mediationId: string,
      body: KeylistQueryFilterRequest,
      query?: {
        /**
         * limit number of results
         * @format int32
         * @default -1
         */
        paginate_limit?: number;
        /**
         * offset to use in pagination
         * @format int32
         * @default 0
         */
        paginate_offset?: number;
      },
      params: RequestParams = {},
    ) =>
      this.request<KeylistQuery, any>({
        path: `/mediation/keylists/${mediationId}/send-keylist-query`,
        method: 'POST',
        query: query,
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags mediation
     * @name KeylistsSendKeylistUpdateCreate
     * @summary Send keylist update to mediator
     * @request POST:/mediation/keylists/{mediation_id}/send-keylist-update
     * @secure
     * @response `201` `KeylistUpdate`
     */
    keylistsSendKeylistUpdateCreate: (mediationId: string, body: KeylistUpdateRequest, params: RequestParams = {}) =>
      this.request<KeylistUpdate, any>({
        path: `/mediation/keylists/${mediationId}/send-keylist-update`,
        method: 'POST',
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags mediation
     * @name RequestCreate
     * @summary Request mediation from connection
     * @request POST:/mediation/request/{conn_id}
     * @secure
     * @response `201` `MediationRecord`
     */
    requestCreate: (connId: string, body: MediationCreateRequest, params: RequestParams = {}) =>
      this.request<MediationRecord, any>({
        path: `/mediation/request/${connId}`,
        method: 'POST',
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags mediation
     * @name RequestsList
     * @summary Query mediation requests, returns list of all mediation records
     * @request GET:/mediation/requests
     * @secure
     * @response `200` `MediationList`
     */
    requestsList: (
      query?: {
        /**
         * Connection identifier (optional)
         * @format uuid
         * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
         */
        conn_id?: string;
        /** List of mediator rules for recipient */
        mediator_terms?: string[];
        /** List of recipient rules for mediation */
        recipient_terms?: string[];
        /**
         * Mediation state (optional)
         * @example "granted"
         */
        state?: 'request' | 'granted' | 'denied';
      },
      params: RequestParams = {},
    ) =>
      this.request<MediationList, any>({
        path: `/mediation/requests`,
        method: 'GET',
        query: query,
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags mediation
     * @name RequestsDetail
     * @summary Retrieve mediation request record
     * @request GET:/mediation/requests/{mediation_id}
     * @secure
     * @response `200` `MediationRecord`
     */
    requestsDetail: (mediationId: string, params: RequestParams = {}) =>
      this.request<MediationRecord, any>({
        path: `/mediation/requests/${mediationId}`,
        method: 'GET',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags mediation
     * @name RequestsDelete
     * @summary Delete mediation request by ID
     * @request DELETE:/mediation/requests/{mediation_id}
     * @secure
     * @response `200` `MediationRecord`
     */
    requestsDelete: (mediationId: string, params: RequestParams = {}) =>
      this.request<MediationRecord, any>({
        path: `/mediation/requests/${mediationId}`,
        method: 'DELETE',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags mediation
     * @name RequestsDenyCreate
     * @summary Deny a stored mediation request
     * @request POST:/mediation/requests/{mediation_id}/deny
     * @secure
     * @response `201` `MediationDeny`
     */
    requestsDenyCreate: (mediationId: string, body: AdminMediationDeny, params: RequestParams = {}) =>
      this.request<MediationDeny, any>({
        path: `/mediation/requests/${mediationId}/deny`,
        method: 'POST',
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags mediation
     * @name RequestsGrantCreate
     * @summary Grant received mediation
     * @request POST:/mediation/requests/{mediation_id}/grant
     * @secure
     * @response `201` `MediationGrant`
     */
    requestsGrantCreate: (mediationId: string, params: RequestParams = {}) =>
      this.request<MediationGrant, any>({
        path: `/mediation/requests/${mediationId}/grant`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags mediation
     * @name UpdateKeylistCreate
     * @summary Update keylist for a connection
     * @request POST:/mediation/update-keylist/{conn_id}
     * @secure
     * @response `200` `KeylistUpdate`
     */
    updateKeylistCreate: (connId: string, body: MediationIdMatchInfo, params: RequestParams = {}) =>
      this.request<KeylistUpdate, any>({
        path: `/mediation/update-keylist/${connId}`,
        method: 'POST',
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags mediation
     * @name DefaultMediatorUpdate
     * @summary Set default mediator
     * @request PUT:/mediation/{mediation_id}/default-mediator
     * @secure
     * @response `201` `MediationRecord`
     */
    defaultMediatorUpdate: (mediationId: string, params: RequestParams = {}) =>
      this.request<MediationRecord, any>({
        path: `/mediation/${mediationId}/default-mediator`,
        method: 'PUT',
        secure: true,
        format: 'json',
        ...params,
      }),
  };
  multitenancy = {
    /**
     * No description
     *
     * @tags multitenancy
     * @name WalletCreate
     * @summary Create a subwallet
     * @request POST:/multitenancy/wallet
     * @secure
     * @response `200` `CreateWalletResponse`
     */
    walletCreate: (body: CreateWalletRequest, params: RequestParams = {}) =>
      this.request<CreateWalletResponse, any>({
        path: `/multitenancy/wallet`,
        method: 'POST',
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags multitenancy
     * @name WalletDetail
     * @summary Get a single subwallet
     * @request GET:/multitenancy/wallet/{wallet_id}
     * @secure
     * @response `200` `WalletRecord`
     */
    walletDetail: (walletId: string, params: RequestParams = {}) =>
      this.request<WalletRecord, any>({
        path: `/multitenancy/wallet/${walletId}`,
        method: 'GET',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags multitenancy
     * @name WalletUpdate
     * @summary Update a subwallet
     * @request PUT:/multitenancy/wallet/{wallet_id}
     * @secure
     * @response `200` `WalletRecord`
     */
    walletUpdate: (walletId: string, body: UpdateWalletRequest, params: RequestParams = {}) =>
      this.request<WalletRecord, any>({
        path: `/multitenancy/wallet/${walletId}`,
        method: 'PUT',
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags multitenancy
     * @name WalletRemoveCreate
     * @summary Remove a subwallet
     * @request POST:/multitenancy/wallet/{wallet_id}/remove
     * @secure
     * @response `200` `MultitenantModuleResponse`
     */
    walletRemoveCreate: (walletId: string, body: RemoveWalletRequest, params: RequestParams = {}) =>
      this.request<MultitenantModuleResponse, any>({
        path: `/multitenancy/wallet/${walletId}/remove`,
        method: 'POST',
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags multitenancy
     * @name WalletTokenCreate
     * @summary Get auth token for a subwallet
     * @request POST:/multitenancy/wallet/{wallet_id}/token
     * @secure
     * @response `200` `CreateWalletTokenResponse`
     */
    walletTokenCreate: (walletId: string, body: CreateWalletTokenRequest, params: RequestParams = {}) =>
      this.request<CreateWalletTokenResponse, any>({
        path: `/multitenancy/wallet/${walletId}/token`,
        method: 'POST',
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags multitenancy
     * @name WalletsList
     * @summary Query subwallets
     * @request GET:/multitenancy/wallets
     * @secure
     * @response `200` `WalletList`
     */
    walletsList: (
      query?: {
        /**
         * Wallet name
         * @example "MyNewWallet"
         */
        wallet_name?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<WalletList, any>({
        path: `/multitenancy/wallets`,
        method: 'GET',
        query: query,
        secure: true,
        format: 'json',
        ...params,
      }),
  };
  outOfBand = {
    /**
     * No description
     *
     * @tags out-of-band
     * @name CreateInvitationCreate
     * @summary Create a new connection invitation
     * @request POST:/out-of-band/create-invitation
     * @secure
     * @response `200` `InvitationRecord`
     */
    createInvitationCreate: (
      body: InvitationCreateRequest,
      query?: {
        /** Auto-accept connection (defaults to configuration) */
        auto_accept?: boolean;
        /** Create invitation for multiple use (default false) */
        multi_use?: boolean;
      },
      params: RequestParams = {},
    ) =>
      this.request<InvitationRecord, any>({
        path: `/out-of-band/create-invitation`,
        method: 'POST',
        query: query,
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags out-of-band
     * @name ReceiveInvitationCreate
     * @summary Receive a new connection invitation
     * @request POST:/out-of-band/receive-invitation
     * @secure
     * @response `200` `OobRecord`
     */
    receiveInvitationCreate: (
      body: InvitationMessage,
      query?: {
        /**
         * Alias for connection
         * @example "Barry"
         */
        alias?: string;
        /** Auto-accept connection (defaults to configuration) */
        auto_accept?: boolean;
        /**
         * Identifier for active mediation record to be used
         * @pattern [a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}
         * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
         */
        mediation_id?: string;
        /** Use an existing connection, if possible */
        use_existing_connection?: boolean;
      },
      params: RequestParams = {},
    ) =>
      this.request<OobRecord, any>({
        path: `/out-of-band/receive-invitation`,
        method: 'POST',
        query: query,
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),
  };
  plugins = {
    /**
     * No description
     *
     * @tags server
     * @name PluginsList
     * @summary Fetch the list of loaded plugins
     * @request GET:/plugins
     * @secure
     * @response `200` `AdminModules`
     */
    pluginsList: (params: RequestParams = {}) =>
      this.request<AdminModules, any>({
        path: `/plugins`,
        method: 'GET',
        secure: true,
        format: 'json',
        ...params,
      }),
  };
  presentProof20 = {
    /**
     * No description
     *
     * @tags present-proof v2.0
     * @name CreateRequestCreate
     * @summary Creates a presentation request not bound to any proposal or connection
     * @request POST:/present-proof-2.0/create-request
     * @secure
     * @response `200` `V20PresExRecord`
     */
    createRequestCreate: (body: V20PresCreateRequestRequest, params: RequestParams = {}) =>
      this.request<V20PresExRecord, any>({
        path: `/present-proof-2.0/create-request`,
        method: 'POST',
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags present-proof v2.0
     * @name RecordsList
     * @summary Fetch all present-proof exchange records
     * @request GET:/present-proof-2.0/records
     * @secure
     * @response `200` `V20PresExRecordList`
     */
    recordsList: (
      query?: {
        /**
         * Connection identifier
         * @format uuid
         * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
         */
        connection_id?: string;
        /** Role assigned in presentation exchange */
        role?: 'prover' | 'verifier';
        /** Presentation exchange state */
        state?:
          | 'proposal-sent'
          | 'proposal-received'
          | 'request-sent'
          | 'request-received'
          | 'presentation-sent'
          | 'presentation-received'
          | 'done'
          | 'abandoned';
        /**
         * Thread identifier
         * @format uuid
         * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
         */
        thread_id?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<V20PresExRecordList, any>({
        path: `/present-proof-2.0/records`,
        method: 'GET',
        query: query,
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags present-proof v2.0
     * @name RecordsDetail
     * @summary Fetch a single presentation exchange record
     * @request GET:/present-proof-2.0/records/{pres_ex_id}
     * @secure
     * @response `200` `V20PresExRecord`
     */
    recordsDetail: (presExId: string, params: RequestParams = {}) =>
      this.request<V20PresExRecord, any>({
        path: `/present-proof-2.0/records/${presExId}`,
        method: 'GET',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags present-proof v2.0
     * @name RecordsDelete
     * @summary Remove an existing presentation exchange record
     * @request DELETE:/present-proof-2.0/records/{pres_ex_id}
     * @secure
     * @response `200` `V20PresentProofModuleResponse`
     */
    recordsDelete: (presExId: string, params: RequestParams = {}) =>
      this.request<V20PresentProofModuleResponse, any>({
        path: `/present-proof-2.0/records/${presExId}`,
        method: 'DELETE',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags present-proof v2.0
     * @name RecordsCredentialsDetail
     * @summary Fetch credentials from wallet for presentation request
     * @request GET:/present-proof-2.0/records/{pres_ex_id}/credentials
     * @secure
     * @response `200` `(IndyCredPrecis)[]`
     */
    recordsCredentialsDetail: (
      presExId: string,
      query?: {
        /**
         * Maximum number to retrieve
         * @pattern ^[1-9][0-9]*$
         * @example "1"
         */
        count?: string;
        /**
         * (JSON) object mapping referents to extra WQL queries
         * @pattern ^{\s*".*?"\s*:\s*{.*?}\s*(,\s*".*?"\s*:\s*{.*?}\s*)*\s*}$
         * @example "{"0_drink_uuid": {"attr::drink::value": "martini"}}"
         */
        extra_query?: string;
        /**
         * Proof request referents of interest, comma-separated
         * @example "1_name_uuid,2_score_uuid"
         */
        referent?: string;
        /**
         * Start index
         * @pattern ^[0-9]*$
         * @example "0"
         */
        start?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<IndyCredPrecis[], any>({
        path: `/present-proof-2.0/records/${presExId}/credentials`,
        method: 'GET',
        query: query,
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags present-proof v2.0
     * @name RecordsProblemReportCreate
     * @summary Send a problem report for presentation exchange
     * @request POST:/present-proof-2.0/records/{pres_ex_id}/problem-report
     * @secure
     * @response `200` `V20PresentProofModuleResponse`
     */
    recordsProblemReportCreate: (presExId: string, body: V20PresProblemReportRequest, params: RequestParams = {}) =>
      this.request<V20PresentProofModuleResponse, any>({
        path: `/present-proof-2.0/records/${presExId}/problem-report`,
        method: 'POST',
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags present-proof v2.0
     * @name RecordsSendPresentationCreate
     * @summary Sends a proof presentation
     * @request POST:/present-proof-2.0/records/{pres_ex_id}/send-presentation
     * @secure
     * @response `200` `V20PresExRecord`
     */
    recordsSendPresentationCreate: (presExId: string, body: V20PresSpecByFormatRequest, params: RequestParams = {}) =>
      this.request<V20PresExRecord, any>({
        path: `/present-proof-2.0/records/${presExId}/send-presentation`,
        method: 'POST',
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags present-proof v2.0
     * @name RecordsSendRequestCreate
     * @summary Sends a presentation request in reference to a proposal
     * @request POST:/present-proof-2.0/records/{pres_ex_id}/send-request
     * @secure
     * @response `200` `V20PresExRecord`
     */
    recordsSendRequestCreate: (
      presExId: string,
      body: V20PresentationSendRequestToProposal,
      params: RequestParams = {},
    ) =>
      this.request<V20PresExRecord, any>({
        path: `/present-proof-2.0/records/${presExId}/send-request`,
        method: 'POST',
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags present-proof v2.0
     * @name RecordsVerifyPresentationCreate
     * @summary Verify a received presentation
     * @request POST:/present-proof-2.0/records/{pres_ex_id}/verify-presentation
     * @secure
     * @response `200` `V20PresExRecord`
     */
    recordsVerifyPresentationCreate: (presExId: string, params: RequestParams = {}) =>
      this.request<V20PresExRecord, any>({
        path: `/present-proof-2.0/records/${presExId}/verify-presentation`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags present-proof v2.0
     * @name SendProposalCreate
     * @summary Sends a presentation proposal
     * @request POST:/present-proof-2.0/send-proposal
     * @secure
     * @response `200` `V20PresExRecord`
     */
    sendProposalCreate: (body: V20PresProposalRequest, params: RequestParams = {}) =>
      this.request<V20PresExRecord, any>({
        path: `/present-proof-2.0/send-proposal`,
        method: 'POST',
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags present-proof v2.0
     * @name SendRequestCreate
     * @summary Sends a free presentation request not bound to any proposal
     * @request POST:/present-proof-2.0/send-request
     * @secure
     * @response `200` `V20PresExRecord`
     */
    sendRequestCreate: (body: V20PresSendRequestRequest, params: RequestParams = {}) =>
      this.request<V20PresExRecord, any>({
        path: `/present-proof-2.0/send-request`,
        method: 'POST',
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),
  };
  presentProof = {
    /**
     * No description
     *
     * @tags present-proof v1.0
     * @name CreateRequestCreate
     * @summary Creates a presentation request not bound to any proposal or connection
     * @request POST:/present-proof/create-request
     * @secure
     * @response `200` `V10PresentationExchange`
     */
    createRequestCreate: (body: V10PresentationCreateRequestRequest, params: RequestParams = {}) =>
      this.request<V10PresentationExchange, any>({
        path: `/present-proof/create-request`,
        method: 'POST',
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags present-proof v1.0
     * @name RecordsList
     * @summary Fetch all present-proof exchange records
     * @request GET:/present-proof/records
     * @secure
     * @response `200` `V10PresentationExchangeList`
     */
    recordsList: (
      query?: {
        /**
         * Connection identifier
         * @format uuid
         * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
         */
        connection_id?: string;
        /** Role assigned in presentation exchange */
        role?: 'prover' | 'verifier';
        /** Presentation exchange state */
        state?:
          | 'proposal_sent'
          | 'proposal_received'
          | 'request_sent'
          | 'request_received'
          | 'presentation_sent'
          | 'presentation_received'
          | 'verified'
          | 'presentation_acked'
          | 'abandoned';
        /**
         * Thread identifier
         * @format uuid
         * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
         */
        thread_id?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<V10PresentationExchangeList, any>({
        path: `/present-proof/records`,
        method: 'GET',
        query: query,
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags present-proof v1.0
     * @name RecordsDetail
     * @summary Fetch a single presentation exchange record
     * @request GET:/present-proof/records/{pres_ex_id}
     * @secure
     * @response `200` `V10PresentationExchange`
     */
    recordsDetail: (presExId: string, params: RequestParams = {}) =>
      this.request<V10PresentationExchange, any>({
        path: `/present-proof/records/${presExId}`,
        method: 'GET',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags present-proof v1.0
     * @name RecordsDelete
     * @summary Remove an existing presentation exchange record
     * @request DELETE:/present-proof/records/{pres_ex_id}
     * @secure
     * @response `200` `V10PresentProofModuleResponse`
     */
    recordsDelete: (presExId: string, params: RequestParams = {}) =>
      this.request<V10PresentProofModuleResponse, any>({
        path: `/present-proof/records/${presExId}`,
        method: 'DELETE',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags present-proof v1.0
     * @name RecordsCredentialsDetail
     * @summary Fetch credentials for a presentation request from wallet
     * @request GET:/present-proof/records/{pres_ex_id}/credentials
     * @secure
     * @response `200` `(IndyCredPrecis)[]`
     */
    recordsCredentialsDetail: (
      presExId: string,
      query?: {
        /**
         * Maximum number to retrieve
         * @pattern ^[1-9][0-9]*$
         * @example "1"
         */
        count?: string;
        /**
         * (JSON) object mapping referents to extra WQL queries
         * @pattern ^{\s*".*?"\s*:\s*{.*?}\s*(,\s*".*?"\s*:\s*{.*?}\s*)*\s*}$
         * @example "{"0_drink_uuid": {"attr::drink::value": "martini"}}"
         */
        extra_query?: string;
        /**
         * Proof request referents of interest, comma-separated
         * @example "1_name_uuid,2_score_uuid"
         */
        referent?: string;
        /**
         * Start index
         * @pattern ^[0-9]*$
         * @example "0"
         */
        start?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<IndyCredPrecis[], any>({
        path: `/present-proof/records/${presExId}/credentials`,
        method: 'GET',
        query: query,
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags present-proof v1.0
     * @name RecordsProblemReportCreate
     * @summary Send a problem report for presentation exchange
     * @request POST:/present-proof/records/{pres_ex_id}/problem-report
     * @secure
     * @response `200` `V10PresentProofModuleResponse`
     */
    recordsProblemReportCreate: (
      presExId: string,
      body: V10PresentationProblemReportRequest,
      params: RequestParams = {},
    ) =>
      this.request<V10PresentProofModuleResponse, any>({
        path: `/present-proof/records/${presExId}/problem-report`,
        method: 'POST',
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags present-proof v1.0
     * @name RecordsSendPresentationCreate
     * @summary Sends a proof presentation
     * @request POST:/present-proof/records/{pres_ex_id}/send-presentation
     * @secure
     * @response `200` `V10PresentationExchange`
     */
    recordsSendPresentationCreate: (presExId: string, body: IndyPresSpec, params: RequestParams = {}) =>
      this.request<V10PresentationExchange, any>({
        path: `/present-proof/records/${presExId}/send-presentation`,
        method: 'POST',
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags present-proof v1.0
     * @name RecordsSendRequestCreate
     * @summary Sends a presentation request in reference to a proposal
     * @request POST:/present-proof/records/{pres_ex_id}/send-request
     * @secure
     * @response `200` `V10PresentationExchange`
     */
    recordsSendRequestCreate: (
      presExId: string,
      body: V10PresentationSendRequestToProposal,
      params: RequestParams = {},
    ) =>
      this.request<V10PresentationExchange, any>({
        path: `/present-proof/records/${presExId}/send-request`,
        method: 'POST',
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags present-proof v1.0
     * @name RecordsVerifyPresentationCreate
     * @summary Verify a received presentation
     * @request POST:/present-proof/records/{pres_ex_id}/verify-presentation
     * @secure
     * @response `200` `V10PresentationExchange`
     */
    recordsVerifyPresentationCreate: (presExId: string, params: RequestParams = {}) =>
      this.request<V10PresentationExchange, any>({
        path: `/present-proof/records/${presExId}/verify-presentation`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags present-proof v1.0
     * @name SendProposalCreate
     * @summary Sends a presentation proposal
     * @request POST:/present-proof/send-proposal
     * @secure
     * @response `200` `V10PresentationExchange`
     */
    sendProposalCreate: (body: V10PresentationProposalRequest, params: RequestParams = {}) =>
      this.request<V10PresentationExchange, any>({
        path: `/present-proof/send-proposal`,
        method: 'POST',
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags present-proof v1.0
     * @name SendRequestCreate
     * @summary Sends a free presentation request not bound to any proposal
     * @request POST:/present-proof/send-request
     * @secure
     * @response `200` `V10PresentationExchange`
     */
    sendRequestCreate: (body: V10PresentationSendRequestRequest, params: RequestParams = {}) =>
      this.request<V10PresentationExchange, any>({
        path: `/present-proof/send-request`,
        method: 'POST',
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),
  };
  resolver = {
    /**
     * No description
     *
     * @tags resolver
     * @name ResolveDetail
     * @summary Retrieve doc for requested did
     * @request GET:/resolver/resolve/{did}
     * @secure
     * @response `200` `ResolutionResult`
     */
    resolveDetail: (did: string, params: RequestParams = {}) =>
      this.request<ResolutionResult, any>({
        path: `/resolver/resolve/${did}`,
        method: 'GET',
        secure: true,
        format: 'json',
        ...params,
      }),
  };
  revocation = {
    /**
     * No description
     *
     * @tags revocation
     * @name ActiveRegistryDetail
     * @summary Get current active revocation registry by credential definition id
     * @request GET:/revocation/active-registry/{cred_def_id}
     * @secure
     * @response `200` `RevRegResult`
     */
    activeRegistryDetail: (credDefId: string, params: RequestParams = {}) =>
      this.request<RevRegResult, any>({
        path: `/revocation/active-registry/${credDefId}`,
        method: 'GET',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags revocation
     * @name ClearPendingRevocationsCreate
     * @summary Clear pending revocations
     * @request POST:/revocation/clear-pending-revocations
     * @secure
     * @response `200` `PublishRevocations`
     */
    clearPendingRevocationsCreate: (body: ClearPendingRevocationsRequest, params: RequestParams = {}) =>
      this.request<PublishRevocations, any>({
        path: `/revocation/clear-pending-revocations`,
        method: 'POST',
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags revocation
     * @name CreateRegistryCreate
     * @summary Creates a new revocation registry
     * @request POST:/revocation/create-registry
     * @secure
     * @response `200` `RevRegResult`
     */
    createRegistryCreate: (body: RevRegCreateRequest, params: RequestParams = {}) =>
      this.request<RevRegResult, any>({
        path: `/revocation/create-registry`,
        method: 'POST',
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags revocation
     * @name CredentialRecordList
     * @summary Get credential revocation status
     * @request GET:/revocation/credential-record
     * @secure
     * @response `200` `CredRevRecordResult`
     */
    credentialRecordList: (
      query?: {
        /**
         * Credential exchange identifier
         * @pattern [a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}
         * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
         */
        cred_ex_id?: string;
        /**
         * Credential revocation identifier
         * @pattern ^[1-9][0-9]*$
         * @example "12345"
         */
        cred_rev_id?: string;
        /**
         * Revocation registry identifier
         * @pattern ^([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):4:([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):3:CL:(([1-9][0-9]*)|([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+))(:.+)?:CL_ACCUM:(.+$)
         * @example "WgWxqztrNooG92RXvxSTWv:4:WgWxqztrNooG92RXvxSTWv:3:CL:20:tag:CL_ACCUM:0"
         */
        rev_reg_id?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<CredRevRecordResult, any>({
        path: `/revocation/credential-record`,
        method: 'GET',
        query: query,
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags revocation
     * @name PublishRevocationsCreate
     * @summary Publish pending revocations to ledger
     * @request POST:/revocation/publish-revocations
     * @secure
     * @response `200` `TxnOrPublishRevocationsResult`
     */
    publishRevocationsCreate: (body: PublishRevocations, params: RequestParams = {}) =>
      this.request<TxnOrPublishRevocationsResult, any>({
        path: `/revocation/publish-revocations`,
        method: 'POST',
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags revocation
     * @name RegistriesCreatedList
     * @summary Search for matching revocation registries that current agent created
     * @request GET:/revocation/registries/created
     * @secure
     * @response `200` `RevRegsCreated`
     */
    registriesCreatedList: (
      query?: {
        /**
         * Credential definition identifier
         * @pattern ^([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):3:CL:(([1-9][0-9]*)|([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+)):(.+)?$
         * @example "WgWxqztrNooG92RXvxSTWv:3:CL:20:tag"
         */
        cred_def_id?: string;
        /** Revocation registry state */
        state?: 'init' | 'generated' | 'posted' | 'active' | 'full';
      },
      params: RequestParams = {},
    ) =>
      this.request<RevRegsCreated, any>({
        path: `/revocation/registries/created`,
        method: 'GET',
        query: query,
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags revocation
     * @name RegistryDetail
     * @summary Get revocation registry by revocation registry id
     * @request GET:/revocation/registry/{rev_reg_id}
     * @secure
     * @response `200` `RevRegResult`
     */
    registryDetail: (revRegId: string, params: RequestParams = {}) =>
      this.request<RevRegResult, any>({
        path: `/revocation/registry/${revRegId}`,
        method: 'GET',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags revocation
     * @name RegistryPartialUpdate
     * @summary Update revocation registry with new public URI to its tails file
     * @request PATCH:/revocation/registry/{rev_reg_id}
     * @secure
     * @response `200` `RevRegResult`
     */
    registryPartialUpdate: (revRegId: string, body: RevRegUpdateTailsFileUri, params: RequestParams = {}) =>
      this.request<RevRegResult, any>({
        path: `/revocation/registry/${revRegId}`,
        method: 'PATCH',
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags revocation
     * @name RegistryDefinitionCreate
     * @summary Send revocation registry definition to ledger
     * @request POST:/revocation/registry/{rev_reg_id}/definition
     * @secure
     * @response `200` `TxnOrRevRegResult`
     */
    registryDefinitionCreate: (
      revRegId: string,
      query?: {
        /**
         * Connection identifier
         * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
         */
        conn_id?: string;
        /** Create Transaction For Endorser's signature */
        create_transaction_for_endorser?: boolean;
      },
      params: RequestParams = {},
    ) =>
      this.request<TxnOrRevRegResult, any>({
        path: `/revocation/registry/${revRegId}/definition`,
        method: 'POST',
        query: query,
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags revocation
     * @name RegistryEntryCreate
     * @summary Send revocation registry entry to ledger
     * @request POST:/revocation/registry/{rev_reg_id}/entry
     * @secure
     * @response `200` `RevRegResult`
     */
    registryEntryCreate: (
      revRegId: string,
      query?: {
        /**
         * Connection identifier
         * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
         */
        conn_id?: string;
        /** Create Transaction For Endorser's signature */
        create_transaction_for_endorser?: boolean;
      },
      params: RequestParams = {},
    ) =>
      this.request<RevRegResult, any>({
        path: `/revocation/registry/${revRegId}/entry`,
        method: 'POST',
        query: query,
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags revocation
     * @name RegistryFixRevocationEntryStateUpdate
     * @summary Fix revocation state in wallet and return number of updated entries
     * @request PUT:/revocation/registry/{rev_reg_id}/fix-revocation-entry-state
     * @secure
     * @response `200` `RevRegWalletUpdatedResult`
     */
    registryFixRevocationEntryStateUpdate: (
      revRegId: string,
      query: {
        /** Apply updated accumulator transaction to ledger */
        apply_ledger_update: boolean;
      },
      params: RequestParams = {},
    ) =>
      this.request<RevRegWalletUpdatedResult, any>({
        path: `/revocation/registry/${revRegId}/fix-revocation-entry-state`,
        method: 'PUT',
        query: query,
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags revocation
     * @name RegistryIssuedDetail
     * @summary Get number of credentials issued against revocation registry
     * @request GET:/revocation/registry/{rev_reg_id}/issued
     * @secure
     * @response `200` `RevRegIssuedResult`
     */
    registryIssuedDetail: (revRegId: string, params: RequestParams = {}) =>
      this.request<RevRegIssuedResult, any>({
        path: `/revocation/registry/${revRegId}/issued`,
        method: 'GET',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags revocation
     * @name RegistryIssuedDetailsDetail
     * @summary Get details of credentials issued against revocation registry
     * @request GET:/revocation/registry/{rev_reg_id}/issued/details
     * @secure
     * @response `200` `CredRevRecordDetailsResult`
     */
    registryIssuedDetailsDetail: (revRegId: string, params: RequestParams = {}) =>
      this.request<CredRevRecordDetailsResult, any>({
        path: `/revocation/registry/${revRegId}/issued/details`,
        method: 'GET',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags revocation
     * @name RegistryIssuedIndyRecsDetail
     * @summary Get details of revoked credentials from ledger
     * @request GET:/revocation/registry/{rev_reg_id}/issued/indy_recs
     * @secure
     * @response `200` `CredRevIndyRecordsResult`
     */
    registryIssuedIndyRecsDetail: (revRegId: string, params: RequestParams = {}) =>
      this.request<CredRevIndyRecordsResult, any>({
        path: `/revocation/registry/${revRegId}/issued/indy_recs`,
        method: 'GET',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags revocation
     * @name RegistrySetStatePartialUpdate
     * @summary Set revocation registry state manually
     * @request PATCH:/revocation/registry/{rev_reg_id}/set-state
     * @secure
     * @response `200` `RevRegResult`
     */
    registrySetStatePartialUpdate: (
      revRegId: string,
      query: {
        /** Revocation registry state to set */
        state: 'init' | 'generated' | 'posted' | 'active' | 'full';
      },
      params: RequestParams = {},
    ) =>
      this.request<RevRegResult, any>({
        path: `/revocation/registry/${revRegId}/set-state`,
        method: 'PATCH',
        query: query,
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags revocation
     * @name RegistryTailsFileUpdate
     * @summary Upload local tails file to server
     * @request PUT:/revocation/registry/{rev_reg_id}/tails-file
     * @secure
     * @response `200` `RevocationModuleResponse`
     */
    registryTailsFileUpdate: (revRegId: string, params: RequestParams = {}) =>
      this.request<RevocationModuleResponse, any>({
        path: `/revocation/registry/${revRegId}/tails-file`,
        method: 'PUT',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags revocation
     * @name RegistryTailsFileDetail
     * @summary Download tails file
     * @request GET:/revocation/registry/{rev_reg_id}/tails-file
     * @secure
     * @response `200` `File` tails file
     */
    registryTailsFileDetail: (revRegId: string, params: RequestParams = {}) =>
      this.request<File, any>({
        path: `/revocation/registry/${revRegId}/tails-file`,
        method: 'GET',
        secure: true,
        ...params,
      }),

    /**
     * No description
     *
     * @tags revocation
     * @name RevokeCreate
     * @summary Revoke an issued credential
     * @request POST:/revocation/revoke
     * @secure
     * @response `200` `RevocationModuleResponse`
     */
    revokeCreate: (body: RevokeRequest, params: RequestParams = {}) =>
      this.request<RevocationModuleResponse, any>({
        path: `/revocation/revoke`,
        method: 'POST',
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),
  };
  schemas = {
    /**
     * No description
     *
     * @tags schema
     * @name SchemasCreate
     * @summary Sends a schema to the ledger
     * @request POST:/schemas
     * @secure
     * @response `200` `TxnOrSchemaSendResult`
     */
    schemasCreate: (
      body: SchemaSendRequest,
      query?: {
        /**
         * Connection identifier
         * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
         */
        conn_id?: string;
        /** Create Transaction For Endorser's signature */
        create_transaction_for_endorser?: boolean;
      },
      params: RequestParams = {},
    ) =>
      this.request<TxnOrSchemaSendResult, any>({
        path: `/schemas`,
        method: 'POST',
        query: query,
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags schema
     * @name CreatedList
     * @summary Search for matching schema that agent originated
     * @request GET:/schemas/created
     * @secure
     * @response `200` `SchemasCreatedResult`
     */
    createdList: (
      query?: {
        /**
         * Schema identifier
         * @pattern ^[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+$
         * @example "WgWxqztrNooG92RXvxSTWv:2:schema_name:1.0"
         */
        schema_id?: string;
        /**
         * Schema issuer DID
         * @pattern ^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$
         * @example "WgWxqztrNooG92RXvxSTWv"
         */
        schema_issuer_did?: string;
        /**
         * Schema name
         * @example "membership"
         */
        schema_name?: string;
        /**
         * Schema version
         * @pattern ^[0-9.]+$
         * @example "1.0"
         */
        schema_version?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<SchemasCreatedResult, any>({
        path: `/schemas/created`,
        method: 'GET',
        query: query,
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags schema
     * @name SchemasDetail
     * @summary Gets a schema from the ledger
     * @request GET:/schemas/{schema_id}
     * @secure
     * @response `200` `SchemaGetResult`
     */
    schemasDetail: (schemaId: string, params: RequestParams = {}) =>
      this.request<SchemaGetResult, any>({
        path: `/schemas/${schemaId}`,
        method: 'GET',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags schema
     * @name WriteRecordCreate
     * @summary Writes a schema non-secret record to the wallet
     * @request POST:/schemas/{schema_id}/write_record
     * @secure
     * @response `200` `SchemaGetResult`
     */
    writeRecordCreate: (schemaId: string, params: RequestParams = {}) =>
      this.request<SchemaGetResult, any>({
        path: `/schemas/${schemaId}/write_record`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),
  };
  shutdown = {
    /**
     * No description
     *
     * @tags server
     * @name ShutdownList
     * @summary Shut down server
     * @request GET:/shutdown
     * @secure
     * @response `200` `AdminShutdown`
     */
    shutdownList: (params: RequestParams = {}) =>
      this.request<AdminShutdown, any>({
        path: `/shutdown`,
        method: 'GET',
        secure: true,
        format: 'json',
        ...params,
      }),
  };
  status = {
    /**
     * No description
     *
     * @tags server
     * @name StatusList
     * @summary Fetch the server status
     * @request GET:/status
     * @secure
     * @response `200` `AdminStatus`
     */
    statusList: (params: RequestParams = {}) =>
      this.request<AdminStatus, any>({
        path: `/status`,
        method: 'GET',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags server
     * @name ConfigList
     * @summary Fetch the server configuration
     * @request GET:/status/config
     * @secure
     * @response `200` `AdminConfig`
     */
    configList: (params: RequestParams = {}) =>
      this.request<AdminConfig, any>({
        path: `/status/config`,
        method: 'GET',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags server
     * @name LiveList
     * @summary Liveliness check
     * @request GET:/status/live
     * @secure
     * @response `200` `AdminStatusLiveliness`
     */
    liveList: (params: RequestParams = {}) =>
      this.request<AdminStatusLiveliness, any>({
        path: `/status/live`,
        method: 'GET',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags server
     * @name ReadyList
     * @summary Readiness check
     * @request GET:/status/ready
     * @secure
     * @response `200` `AdminStatusReadiness`
     */
    readyList: (params: RequestParams = {}) =>
      this.request<AdminStatusReadiness, any>({
        path: `/status/ready`,
        method: 'GET',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags server
     * @name ResetCreate
     * @summary Reset statistics
     * @request POST:/status/reset
     * @secure
     * @response `200` `AdminReset`
     */
    resetCreate: (params: RequestParams = {}) =>
      this.request<AdminReset, any>({
        path: `/status/reset`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),
  };
  transaction = {
    /**
     * No description
     *
     * @tags endorse-transaction
     * @name ResendCreate
     * @summary For Author to resend a particular transaction request
     * @request POST:/transaction/{tran_id}/resend
     * @secure
     * @response `200` `TransactionRecord`
     */
    resendCreate: (tranId: string, params: RequestParams = {}) =>
      this.request<TransactionRecord, any>({
        path: `/transaction/${tranId}/resend`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),
  };
  transactions = {
    /**
     * No description
     *
     * @tags endorse-transaction
     * @name TransactionsList
     * @summary Query transactions
     * @request GET:/transactions
     * @secure
     * @response `200` `TransactionList`
     */
    transactionsList: (params: RequestParams = {}) =>
      this.request<TransactionList, any>({
        path: `/transactions`,
        method: 'GET',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags endorse-transaction
     * @name CreateRequestCreate
     * @summary For author to send a transaction request
     * @request POST:/transactions/create-request
     * @secure
     * @response `200` `TransactionRecord`
     */
    createRequestCreate: (
      query: {
        /**
         * Transaction identifier
         * @example "3fa85f64-5717-4562-b3fc-2c963f66afa6"
         */
        tran_id: string;
        /** Endorser will write the transaction after endorsing it */
        endorser_write_txn?: boolean;
      },
      body: Date,
      params: RequestParams = {},
    ) =>
      this.request<TransactionRecord, any>({
        path: `/transactions/create-request`,
        method: 'POST',
        query: query,
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags endorse-transaction
     * @name SetEndorserInfoCreate
     * @summary Set Endorser Info
     * @request POST:/transactions/{conn_id}/set-endorser-info
     * @secure
     * @response `200` `EndorserInfo`
     */
    setEndorserInfoCreate: (
      connId: string,
      query: {
        /** Endorser DID */
        endorser_did: string;
        /** Endorser Name */
        endorser_name?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<EndorserInfo, any>({
        path: `/transactions/${connId}/set-endorser-info`,
        method: 'POST',
        query: query,
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags endorse-transaction
     * @name SetEndorserRoleCreate
     * @summary Set transaction jobs
     * @request POST:/transactions/{conn_id}/set-endorser-role
     * @secure
     * @response `200` `TransactionJobs`
     */
    setEndorserRoleCreate: (
      connId: string,
      query?: {
        /** Transaction related jobs */
        transaction_my_job?: 'TRANSACTION_AUTHOR' | 'TRANSACTION_ENDORSER' | 'reset';
      },
      params: RequestParams = {},
    ) =>
      this.request<TransactionJobs, any>({
        path: `/transactions/${connId}/set-endorser-role`,
        method: 'POST',
        query: query,
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags endorse-transaction
     * @name TransactionsDetail
     * @summary Fetch a single transaction record
     * @request GET:/transactions/{tran_id}
     * @secure
     * @response `200` `TransactionRecord`
     */
    transactionsDetail: (tranId: string, params: RequestParams = {}) =>
      this.request<TransactionRecord, any>({
        path: `/transactions/${tranId}`,
        method: 'GET',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags endorse-transaction
     * @name CancelCreate
     * @summary For Author to cancel a particular transaction request
     * @request POST:/transactions/{tran_id}/cancel
     * @secure
     * @response `200` `TransactionRecord`
     */
    cancelCreate: (tranId: string, params: RequestParams = {}) =>
      this.request<TransactionRecord, any>({
        path: `/transactions/${tranId}/cancel`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags endorse-transaction
     * @name EndorseCreate
     * @summary For Endorser to endorse a particular transaction record
     * @request POST:/transactions/{tran_id}/endorse
     * @secure
     * @response `200` `TransactionRecord`
     */
    endorseCreate: (
      tranId: string,
      query?: {
        /** Endorser DID */
        endorser_did?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<TransactionRecord, any>({
        path: `/transactions/${tranId}/endorse`,
        method: 'POST',
        query: query,
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags endorse-transaction
     * @name RefuseCreate
     * @summary For Endorser to refuse a particular transaction record
     * @request POST:/transactions/{tran_id}/refuse
     * @secure
     * @response `200` `TransactionRecord`
     */
    refuseCreate: (tranId: string, params: RequestParams = {}) =>
      this.request<TransactionRecord, any>({
        path: `/transactions/${tranId}/refuse`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags endorse-transaction
     * @name WriteCreate
     * @summary For Author / Endorser to write an endorsed transaction to the ledger
     * @request POST:/transactions/{tran_id}/write
     * @secure
     * @response `200` `TransactionRecord`
     */
    writeCreate: (tranId: string, params: RequestParams = {}) =>
      this.request<TransactionRecord, any>({
        path: `/transactions/${tranId}/write`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),
  };
  wallet = {
    /**
     * No description
     *
     * @tags wallet
     * @name GetWallet
     * @summary List wallet DIDs
     * @request GET:/wallet/did
     * @secure
     * @response `200` `DIDList`
     */
    getWallet: (
      query?: {
        /**
         * DID of interest
         * @pattern ^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$|^did:([a-zA-Z0-9_]+):([a-zA-Z0-9_.%-]+(:[a-zA-Z0-9_.%-]+)*)((;[a-zA-Z0-9_.:%-]+=[a-zA-Z0-9_.:%-]*)*)(\/[^#?]*)?([?][^#]*)?(\#.*)?$$
         * @example "did:peer:WgWxqztrNooG92RXvxSTWv"
         */
        did?: string;
        /**
         * Key type to query for.
         * @example "ed25519"
         */
        key_type?: 'ed25519' | 'bls12381g2';
        /**
         * DID method to query for. e.g. sov to only fetch indy/sov DIDs
         * @example "key"
         */
        method?: 'key' | 'sov';
        /**
         * Whether DID is current public DID, posted to ledger but current public DID, or local to the wallet
         * @example "wallet_only"
         */
        posture?: 'public' | 'posted' | 'wallet_only';
        /**
         * Verification key of interest
         * @pattern ^[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{43,44}$
         * @example "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV"
         */
        verkey?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<DIDList, any>({
        path: `/wallet/did`,
        method: 'GET',
        query: query,
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags wallet
     * @name DidCreateCreate
     * @summary Create a local DID
     * @request POST:/wallet/did/create
     * @secure
     * @response `200` `DIDResult`
     */
    didCreateCreate: (body: DIDCreate, params: RequestParams = {}) =>
      this.request<DIDResult, any>({
        path: `/wallet/did/create`,
        method: 'POST',
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags wallet
     * @name DidLocalRotateKeypairPartialUpdate
     * @summary Rotate keypair for a DID not posted to the ledger
     * @request PATCH:/wallet/did/local/rotate-keypair
     * @secure
     * @response `200` `WalletModuleResponse`
     */
    didLocalRotateKeypairPartialUpdate: (
      query: {
        /**
         * DID of interest
         * @pattern ^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$
         * @example "WgWxqztrNooG92RXvxSTWv"
         */
        did: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<WalletModuleResponse, any>({
        path: `/wallet/did/local/rotate-keypair`,
        method: 'PATCH',
        query: query,
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags wallet
     * @name DidPublicList
     * @summary Fetch the current public DID
     * @request GET:/wallet/did/public
     * @secure
     * @response `200` `DIDResult`
     */
    didPublicList: (params: RequestParams = {}) =>
      this.request<DIDResult, any>({
        path: `/wallet/did/public`,
        method: 'GET',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags wallet
     * @name DidPublicCreate
     * @summary Assign the current public DID
     * @request POST:/wallet/did/public
     * @secure
     * @response `200` `DIDResult`
     */
    didPublicCreate: (
      query: {
        /**
         * DID of interest
         * @pattern ^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$
         * @example "WgWxqztrNooG92RXvxSTWv"
         */
        did: string;
        /** Connection identifier */
        conn_id?: string;
        /** Create Transaction For Endorser's signature */
        create_transaction_for_endorser?: boolean;
        /** Mediation identifier */
        mediation_id?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<DIDResult, any>({
        path: `/wallet/did/public`,
        method: 'POST',
        query: query,
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags wallet
     * @name GetDidEndpointList
     * @summary Query DID endpoint in wallet
     * @request GET:/wallet/get-did-endpoint
     * @secure
     * @response `200` `DIDEndpoint`
     */
    getDidEndpointList: (
      query: {
        /**
         * DID of interest
         * @pattern ^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$
         * @example "WgWxqztrNooG92RXvxSTWv"
         */
        did: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<DIDEndpoint, any>({
        path: `/wallet/get-did-endpoint`,
        method: 'GET',
        query: query,
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags wallet
     * @name SetDidEndpointCreate
     * @summary Update endpoint in wallet and on ledger if posted to it
     * @request POST:/wallet/set-did-endpoint
     * @secure
     * @response `200` `WalletModuleResponse`
     */
    setDidEndpointCreate: (
      body: DIDEndpointWithType,
      query?: {
        /** Connection identifier */
        conn_id?: string;
        /** Create Transaction For Endorser's signature */
        create_transaction_for_endorser?: boolean;
      },
      params: RequestParams = {},
    ) =>
      this.request<WalletModuleResponse, any>({
        path: `/wallet/set-did-endpoint`,
        method: 'POST',
        query: query,
        body: body,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),
  };
}
