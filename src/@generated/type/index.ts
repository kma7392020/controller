import { Field } from '@nestjs/graphql';
import { ObjectType } from '@nestjs/graphql';
import { ArgsType } from '@nestjs/graphql';
import { Type } from 'class-transformer';
import { Int } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { GraphQLJSON } from 'graphql-type-json';
import { registerEnumType } from '@nestjs/graphql';
import { ID } from '@nestjs/graphql';
import { HideField } from '@nestjs/graphql';
import { Float } from '@nestjs/graphql';
import * as Validator from 'class-validator';

export enum WalletScalarFieldEnum {
    id = "id",
    createdAt = "createdAt",
    updatedAt = "updatedAt",
    email = "email",
    role = "role",
    passwordHash = "passwordHash",
    accessToken = "accessToken"
}

export enum VerifierSettingScalarFieldEnum {
    id = "id",
    createdAt = "createdAt",
    updatedAt = "updatedAt",
    verifierWalletId = "verifierWalletId",
    proofRequestSetting = "proofRequestSetting"
}

export enum ProofExchangeScalarFieldEnum {
    id = "id",
    name = "name",
    metadata = "metadata",
    createdAt = "createdAt",
    attributes = "attributes",
    presentationAttributes = "presentationAttributes",
    deviceId = "deviceId",
    verifierWalletId = "verifierWalletId",
    proverWalletId = "proverWalletId",
    verifierConnectionId = "verifierConnectionId",
    proverConnectionId = "proverConnectionId",
    verifierPresExId = "verifierPresExId",
    proverPresExId = "proverPresExId",
    presRequest = "presRequest",
    state = "state",
    webhookUrl = "webhookUrl"
}

export enum WalletRole {
    ISSUER = "ISSUER",
    VERIFIER = "VERIFIER",
    HOLDER = "HOLDER"
}

export enum TransactionIsolationLevel {
    ReadUncommitted = "ReadUncommitted",
    ReadCommitted = "ReadCommitted",
    RepeatableRead = "RepeatableRead",
    Serializable = "Serializable"
}

export enum SortOrder {
    asc = "asc",
    desc = "desc"
}

export enum QueryMode {
    'default' = "default",
    insensitive = "insensitive"
}

export enum ProofExchangeState {
    WAITING = "WAITING",
    REQUEST_SENT = "REQUEST_SENT",
    REQUEST_RECEIVED = "REQUEST_RECEIVED",
    PRESENTATION_SENT = "PRESENTATION_SENT",
    VERIFIED = "VERIFIED",
    REJECTED = "REJECTED"
}

export enum OtpAlgorithm {
    HOTP = "HOTP",
    TOTP = "TOTP"
}

export enum JsonNullValueInput {
    JsonNull = "JsonNull"
}

export enum JsonNullValueFilter {
    DbNull = "DbNull",
    JsonNull = "JsonNull",
    AnyNull = "AnyNull"
}

export enum OtpScalarFieldEnum {
    id = "id",
    createdAt = "createdAt",
    updatedAt = "updatedAt",
    algorithm = "algorithm",
    secret = "secret",
    digits = "digits",
    issuer = "issuer",
    accountName = "accountName",
    walletId = "walletId"
}

export enum MessagePayloadScalarFieldEnum {
    id = "id",
    payload = "payload"
}

export enum DeviceScalarFieldEnum {
    token = "token",
    walletId = "walletId"
}

export enum CredentialExchangeScalarFieldEnum {
    id = "id",
    createdAt = "createdAt",
    issuerWalletId = "issuerWalletId",
    attributes = "attributes",
    indyFilter = "indyFilter",
    issuerConnectionId = "issuerConnectionId"
}

registerEnumType(CredentialExchangeScalarFieldEnum, { name: 'CredentialExchangeScalarFieldEnum', description: undefined })
registerEnumType(DeviceScalarFieldEnum, { name: 'DeviceScalarFieldEnum', description: undefined })
registerEnumType(MessagePayloadScalarFieldEnum, { name: 'MessagePayloadScalarFieldEnum', description: undefined })
registerEnumType(OtpScalarFieldEnum, { name: 'OtpScalarFieldEnum', description: undefined })
registerEnumType(JsonNullValueFilter, { name: 'JsonNullValueFilter', description: undefined })
registerEnumType(JsonNullValueInput, { name: 'JsonNullValueInput', description: undefined })
registerEnumType(OtpAlgorithm, { name: 'OtpAlgorithm', description: undefined })
registerEnumType(ProofExchangeState, { name: 'ProofExchangeState', description: undefined })
registerEnumType(QueryMode, { name: 'QueryMode', description: undefined })
registerEnumType(SortOrder, { name: 'SortOrder', description: undefined })
registerEnumType(TransactionIsolationLevel, { name: 'TransactionIsolationLevel', description: undefined })
registerEnumType(WalletRole, { name: 'WalletRole', description: undefined })
registerEnumType(ProofExchangeScalarFieldEnum, { name: 'ProofExchangeScalarFieldEnum', description: undefined })
registerEnumType(VerifierSettingScalarFieldEnum, { name: 'VerifierSettingScalarFieldEnum', description: undefined })
registerEnumType(WalletScalarFieldEnum, { name: 'WalletScalarFieldEnum', description: undefined })

@ObjectType()
export class AggregateCredentialExchange {
    @Field(() => CredentialExchangeCountAggregate, {nullable:true})
    _count?: InstanceType<typeof CredentialExchangeCountAggregate>;
    @Field(() => CredentialExchangeMinAggregate, {nullable:true})
    _min?: InstanceType<typeof CredentialExchangeMinAggregate>;
    @Field(() => CredentialExchangeMaxAggregate, {nullable:true})
    _max?: InstanceType<typeof CredentialExchangeMaxAggregate>;
}

@ArgsType()
export class CreateManyCredentialExchangeArgs {
    @Field(() => [CredentialExchangeCreateManyInput], {nullable:false})
    @Type(() => CredentialExchangeCreateManyInput)
    data!: Array<CredentialExchangeCreateManyInput>;
    @Field(() => Boolean, {nullable:true})
    skipDuplicates?: boolean;
}

@ArgsType()
export class CreateOneCredentialExchangeArgs {
    @Field(() => CredentialExchangeCreateInput, {nullable:false})
    @Type(() => CredentialExchangeCreateInput)
    data!: InstanceType<typeof CredentialExchangeCreateInput>;
}

@ArgsType()
export class CredentialExchangeAggregateArgs {
    @Field(() => CredentialExchangeWhereInput, {nullable:true})
    @Type(() => CredentialExchangeWhereInput)
    where?: InstanceType<typeof CredentialExchangeWhereInput>;
    @Field(() => [CredentialExchangeOrderByWithRelationInput], {nullable:true})
    orderBy?: Array<CredentialExchangeOrderByWithRelationInput>;
    @Field(() => CredentialExchangeWhereUniqueInput, {nullable:true})
    cursor?: InstanceType<typeof CredentialExchangeWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => CredentialExchangeCountAggregateInput, {nullable:true})
    _count?: InstanceType<typeof CredentialExchangeCountAggregateInput>;
    @Field(() => CredentialExchangeMinAggregateInput, {nullable:true})
    _min?: InstanceType<typeof CredentialExchangeMinAggregateInput>;
    @Field(() => CredentialExchangeMaxAggregateInput, {nullable:true})
    _max?: InstanceType<typeof CredentialExchangeMaxAggregateInput>;
}

@InputType()
export class CredentialExchangeCountAggregateInput {
    @Field(() => Boolean, {nullable:true})
    id?: true;
    @Field(() => Boolean, {nullable:true})
    createdAt?: true;
    @Field(() => Boolean, {nullable:true})
    issuerWalletId?: true;
    @Field(() => Boolean, {nullable:true})
    attributes?: true;
    @Field(() => Boolean, {nullable:true})
    indyFilter?: true;
    @Field(() => Boolean, {nullable:true})
    issuerConnectionId?: true;
    @Field(() => Boolean, {nullable:true})
    _all?: true;
}

@ObjectType()
export class CredentialExchangeCountAggregate {
    @Field(() => Int, {nullable:false})
    id!: number;
    @Field(() => Int, {nullable:false})
    createdAt!: number;
    @Field(() => Int, {nullable:false})
    issuerWalletId!: number;
    @Field(() => Int, {nullable:false})
    attributes!: number;
    @Field(() => Int, {nullable:false})
    indyFilter!: number;
    @Field(() => Int, {nullable:false})
    issuerConnectionId!: number;
    @Field(() => Int, {nullable:false})
    _all!: number;
}

@InputType()
export class CredentialExchangeCountOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    createdAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    issuerWalletId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    attributes?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    indyFilter?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    issuerConnectionId?: keyof typeof SortOrder;
}

@InputType()
export class CredentialExchangeCreateManyInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => String, {nullable:false})
    issuerWalletId!: string;
    @Field(() => GraphQLJSON, {nullable:false})
    attributes!: any;
    @Field(() => GraphQLJSON, {nullable:false})
    indyFilter!: any;
    @Field(() => String, {nullable:true})
    issuerConnectionId?: string;
}

@InputType()
export class CredentialExchangeCreateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => String, {nullable:false})
    issuerWalletId!: string;
    @Field(() => GraphQLJSON, {nullable:false})
    attributes!: any;
    @Field(() => GraphQLJSON, {nullable:false})
    indyFilter!: any;
    @Field(() => String, {nullable:true})
    issuerConnectionId?: string;
}

@ArgsType()
export class CredentialExchangeGroupByArgs {
    @Field(() => CredentialExchangeWhereInput, {nullable:true})
    @Type(() => CredentialExchangeWhereInput)
    where?: InstanceType<typeof CredentialExchangeWhereInput>;
    @Field(() => [CredentialExchangeOrderByWithAggregationInput], {nullable:true})
    orderBy?: Array<CredentialExchangeOrderByWithAggregationInput>;
    @Field(() => [CredentialExchangeScalarFieldEnum], {nullable:false})
    by!: Array<keyof typeof CredentialExchangeScalarFieldEnum>;
    @Field(() => CredentialExchangeScalarWhereWithAggregatesInput, {nullable:true})
    having?: InstanceType<typeof CredentialExchangeScalarWhereWithAggregatesInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => CredentialExchangeCountAggregateInput, {nullable:true})
    _count?: InstanceType<typeof CredentialExchangeCountAggregateInput>;
    @Field(() => CredentialExchangeMinAggregateInput, {nullable:true})
    _min?: InstanceType<typeof CredentialExchangeMinAggregateInput>;
    @Field(() => CredentialExchangeMaxAggregateInput, {nullable:true})
    _max?: InstanceType<typeof CredentialExchangeMaxAggregateInput>;
}

@ObjectType()
export class CredentialExchangeGroupBy {
    @Field(() => String, {nullable:false})
    id!: string;
    @Field(() => Date, {nullable:false})
    createdAt!: Date | string;
    @Field(() => String, {nullable:false})
    issuerWalletId!: string;
    @Field(() => GraphQLJSON, {nullable:false})
    attributes!: any;
    @Field(() => GraphQLJSON, {nullable:false})
    indyFilter!: any;
    @Field(() => String, {nullable:true})
    issuerConnectionId?: string;
    @Field(() => CredentialExchangeCountAggregate, {nullable:true})
    _count?: InstanceType<typeof CredentialExchangeCountAggregate>;
    @Field(() => CredentialExchangeMinAggregate, {nullable:true})
    _min?: InstanceType<typeof CredentialExchangeMinAggregate>;
    @Field(() => CredentialExchangeMaxAggregate, {nullable:true})
    _max?: InstanceType<typeof CredentialExchangeMaxAggregate>;
}

@InputType()
export class CredentialExchangeMaxAggregateInput {
    @Field(() => Boolean, {nullable:true})
    id?: true;
    @Field(() => Boolean, {nullable:true})
    createdAt?: true;
    @Field(() => Boolean, {nullable:true})
    issuerWalletId?: true;
    @Field(() => Boolean, {nullable:true})
    issuerConnectionId?: true;
}

@ObjectType()
export class CredentialExchangeMaxAggregate {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => String, {nullable:true})
    issuerWalletId?: string;
    @Field(() => String, {nullable:true})
    issuerConnectionId?: string;
}

@InputType()
export class CredentialExchangeMaxOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    createdAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    issuerWalletId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    issuerConnectionId?: keyof typeof SortOrder;
}

@InputType()
export class CredentialExchangeMinAggregateInput {
    @Field(() => Boolean, {nullable:true})
    id?: true;
    @Field(() => Boolean, {nullable:true})
    createdAt?: true;
    @Field(() => Boolean, {nullable:true})
    issuerWalletId?: true;
    @Field(() => Boolean, {nullable:true})
    issuerConnectionId?: true;
}

@ObjectType()
export class CredentialExchangeMinAggregate {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => String, {nullable:true})
    issuerWalletId?: string;
    @Field(() => String, {nullable:true})
    issuerConnectionId?: string;
}

@InputType()
export class CredentialExchangeMinOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    createdAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    issuerWalletId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    issuerConnectionId?: keyof typeof SortOrder;
}

@InputType()
export class CredentialExchangeOrderByWithAggregationInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    createdAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    issuerWalletId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    attributes?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    indyFilter?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    issuerConnectionId?: keyof typeof SortOrder;
    @Field(() => CredentialExchangeCountOrderByAggregateInput, {nullable:true})
    _count?: InstanceType<typeof CredentialExchangeCountOrderByAggregateInput>;
    @Field(() => CredentialExchangeMaxOrderByAggregateInput, {nullable:true})
    _max?: InstanceType<typeof CredentialExchangeMaxOrderByAggregateInput>;
    @Field(() => CredentialExchangeMinOrderByAggregateInput, {nullable:true})
    _min?: InstanceType<typeof CredentialExchangeMinOrderByAggregateInput>;
}

@InputType()
export class CredentialExchangeOrderByWithRelationInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    createdAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    issuerWalletId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    attributes?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    indyFilter?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    issuerConnectionId?: keyof typeof SortOrder;
}

@InputType()
export class CredentialExchangeScalarWhereWithAggregatesInput {
    @Field(() => [CredentialExchangeScalarWhereWithAggregatesInput], {nullable:true})
    AND?: Array<CredentialExchangeScalarWhereWithAggregatesInput>;
    @Field(() => [CredentialExchangeScalarWhereWithAggregatesInput], {nullable:true})
    OR?: Array<CredentialExchangeScalarWhereWithAggregatesInput>;
    @Field(() => [CredentialExchangeScalarWhereWithAggregatesInput], {nullable:true})
    NOT?: Array<CredentialExchangeScalarWhereWithAggregatesInput>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    id?: InstanceType<typeof StringWithAggregatesFilter>;
    @Field(() => DateTimeWithAggregatesFilter, {nullable:true})
    createdAt?: InstanceType<typeof DateTimeWithAggregatesFilter>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    issuerWalletId?: InstanceType<typeof StringWithAggregatesFilter>;
    @Field(() => JsonWithAggregatesFilter, {nullable:true})
    attributes?: InstanceType<typeof JsonWithAggregatesFilter>;
    @Field(() => JsonWithAggregatesFilter, {nullable:true})
    indyFilter?: InstanceType<typeof JsonWithAggregatesFilter>;
    @Field(() => StringNullableWithAggregatesFilter, {nullable:true})
    issuerConnectionId?: InstanceType<typeof StringNullableWithAggregatesFilter>;
}

@InputType()
export class CredentialExchangeUncheckedCreateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => String, {nullable:false})
    issuerWalletId!: string;
    @Field(() => GraphQLJSON, {nullable:false})
    attributes!: any;
    @Field(() => GraphQLJSON, {nullable:false})
    indyFilter!: any;
    @Field(() => String, {nullable:true})
    issuerConnectionId?: string;
}

@InputType()
export class CredentialExchangeUncheckedUpdateManyInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => String, {nullable:true})
    issuerWalletId?: string;
    @Field(() => GraphQLJSON, {nullable:true})
    attributes?: any;
    @Field(() => GraphQLJSON, {nullable:true})
    indyFilter?: any;
    @Field(() => String, {nullable:true})
    issuerConnectionId?: string;
}

@InputType()
export class CredentialExchangeUncheckedUpdateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => String, {nullable:true})
    issuerWalletId?: string;
    @Field(() => GraphQLJSON, {nullable:true})
    attributes?: any;
    @Field(() => GraphQLJSON, {nullable:true})
    indyFilter?: any;
    @Field(() => String, {nullable:true})
    issuerConnectionId?: string;
}

@InputType()
export class CredentialExchangeUpdateManyMutationInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => String, {nullable:true})
    issuerWalletId?: string;
    @Field(() => GraphQLJSON, {nullable:true})
    attributes?: any;
    @Field(() => GraphQLJSON, {nullable:true})
    indyFilter?: any;
    @Field(() => String, {nullable:true})
    issuerConnectionId?: string;
}

@InputType()
export class CredentialExchangeUpdateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => String, {nullable:true})
    issuerWalletId?: string;
    @Field(() => GraphQLJSON, {nullable:true})
    attributes?: any;
    @Field(() => GraphQLJSON, {nullable:true})
    indyFilter?: any;
    @Field(() => String, {nullable:true})
    issuerConnectionId?: string;
}

@InputType()
export class CredentialExchangeWhereUniqueInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => String, {nullable:true})
    issuerConnectionId?: string;
}

@InputType()
export class CredentialExchangeWhereInput {
    @Field(() => [CredentialExchangeWhereInput], {nullable:true})
    AND?: Array<CredentialExchangeWhereInput>;
    @Field(() => [CredentialExchangeWhereInput], {nullable:true})
    OR?: Array<CredentialExchangeWhereInput>;
    @Field(() => [CredentialExchangeWhereInput], {nullable:true})
    NOT?: Array<CredentialExchangeWhereInput>;
    @Field(() => StringFilter, {nullable:true})
    id?: InstanceType<typeof StringFilter>;
    @Field(() => DateTimeFilter, {nullable:true})
    createdAt?: InstanceType<typeof DateTimeFilter>;
    @Field(() => StringFilter, {nullable:true})
    issuerWalletId?: InstanceType<typeof StringFilter>;
    @Field(() => JsonFilter, {nullable:true})
    attributes?: InstanceType<typeof JsonFilter>;
    @Field(() => JsonFilter, {nullable:true})
    indyFilter?: InstanceType<typeof JsonFilter>;
    @Field(() => StringNullableFilter, {nullable:true})
    issuerConnectionId?: InstanceType<typeof StringNullableFilter>;
}

@ObjectType()
export class CredentialExchange {
    @Field(() => ID, {nullable:false})
    id!: string;
    @Field(() => Date, {nullable:false})
    createdAt!: Date;
    @Field(() => String, {nullable:false})
    issuerWalletId!: string;
    @Field(() => GraphQLJSON, {nullable:false})
    attributes!: any;
    @Field(() => GraphQLJSON, {nullable:false})
    indyFilter!: any;
    @Field(() => String, {nullable:true})
    issuerConnectionId!: string | null;
}

@ArgsType()
export class DeleteManyCredentialExchangeArgs {
    @Field(() => CredentialExchangeWhereInput, {nullable:true})
    @Type(() => CredentialExchangeWhereInput)
    where?: InstanceType<typeof CredentialExchangeWhereInput>;
}

@ArgsType()
export class DeleteOneCredentialExchangeArgs {
    @Field(() => CredentialExchangeWhereUniqueInput, {nullable:false})
    @Type(() => CredentialExchangeWhereUniqueInput)
    where!: InstanceType<typeof CredentialExchangeWhereUniqueInput>;
}

@ArgsType()
export class FindFirstCredentialExchangeOrThrowArgs {
    @Field(() => CredentialExchangeWhereInput, {nullable:true})
    @Type(() => CredentialExchangeWhereInput)
    where?: InstanceType<typeof CredentialExchangeWhereInput>;
    @Field(() => [CredentialExchangeOrderByWithRelationInput], {nullable:true})
    orderBy?: Array<CredentialExchangeOrderByWithRelationInput>;
    @Field(() => CredentialExchangeWhereUniqueInput, {nullable:true})
    cursor?: InstanceType<typeof CredentialExchangeWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => [CredentialExchangeScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof CredentialExchangeScalarFieldEnum>;
}

@ArgsType()
export class FindFirstCredentialExchangeArgs {
    @Field(() => CredentialExchangeWhereInput, {nullable:true})
    @Type(() => CredentialExchangeWhereInput)
    where?: InstanceType<typeof CredentialExchangeWhereInput>;
    @Field(() => [CredentialExchangeOrderByWithRelationInput], {nullable:true})
    orderBy?: Array<CredentialExchangeOrderByWithRelationInput>;
    @Field(() => CredentialExchangeWhereUniqueInput, {nullable:true})
    cursor?: InstanceType<typeof CredentialExchangeWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => [CredentialExchangeScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof CredentialExchangeScalarFieldEnum>;
}

@ArgsType()
export class FindManyCredentialExchangeArgs {
    @Field(() => CredentialExchangeWhereInput, {nullable:true})
    @Type(() => CredentialExchangeWhereInput)
    where?: InstanceType<typeof CredentialExchangeWhereInput>;
    @Field(() => [CredentialExchangeOrderByWithRelationInput], {nullable:true})
    orderBy?: Array<CredentialExchangeOrderByWithRelationInput>;
    @Field(() => CredentialExchangeWhereUniqueInput, {nullable:true})
    cursor?: InstanceType<typeof CredentialExchangeWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => [CredentialExchangeScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof CredentialExchangeScalarFieldEnum>;
}

@ArgsType()
export class FindUniqueCredentialExchangeOrThrowArgs {
    @Field(() => CredentialExchangeWhereUniqueInput, {nullable:false})
    @Type(() => CredentialExchangeWhereUniqueInput)
    where!: InstanceType<typeof CredentialExchangeWhereUniqueInput>;
}

@ArgsType()
export class FindUniqueCredentialExchangeArgs {
    @Field(() => CredentialExchangeWhereUniqueInput, {nullable:false})
    @Type(() => CredentialExchangeWhereUniqueInput)
    where!: InstanceType<typeof CredentialExchangeWhereUniqueInput>;
}

@ArgsType()
export class UpdateManyCredentialExchangeArgs {
    @Field(() => CredentialExchangeUpdateManyMutationInput, {nullable:false})
    @Type(() => CredentialExchangeUpdateManyMutationInput)
    data!: InstanceType<typeof CredentialExchangeUpdateManyMutationInput>;
    @Field(() => CredentialExchangeWhereInput, {nullable:true})
    @Type(() => CredentialExchangeWhereInput)
    where?: InstanceType<typeof CredentialExchangeWhereInput>;
}

@ArgsType()
export class UpdateOneCredentialExchangeArgs {
    @Field(() => CredentialExchangeUpdateInput, {nullable:false})
    @Type(() => CredentialExchangeUpdateInput)
    data!: InstanceType<typeof CredentialExchangeUpdateInput>;
    @Field(() => CredentialExchangeWhereUniqueInput, {nullable:false})
    @Type(() => CredentialExchangeWhereUniqueInput)
    where!: InstanceType<typeof CredentialExchangeWhereUniqueInput>;
}

@ArgsType()
export class UpsertOneCredentialExchangeArgs {
    @Field(() => CredentialExchangeWhereUniqueInput, {nullable:false})
    @Type(() => CredentialExchangeWhereUniqueInput)
    where!: InstanceType<typeof CredentialExchangeWhereUniqueInput>;
    @Field(() => CredentialExchangeCreateInput, {nullable:false})
    @Type(() => CredentialExchangeCreateInput)
    create!: InstanceType<typeof CredentialExchangeCreateInput>;
    @Field(() => CredentialExchangeUpdateInput, {nullable:false})
    @Type(() => CredentialExchangeUpdateInput)
    update!: InstanceType<typeof CredentialExchangeUpdateInput>;
}

@ObjectType()
export class AggregateDevice {
    @Field(() => DeviceCountAggregate, {nullable:true})
    _count?: InstanceType<typeof DeviceCountAggregate>;
    @Field(() => DeviceMinAggregate, {nullable:true})
    _min?: InstanceType<typeof DeviceMinAggregate>;
    @Field(() => DeviceMaxAggregate, {nullable:true})
    _max?: InstanceType<typeof DeviceMaxAggregate>;
}

@ArgsType()
export class CreateManyDeviceArgs {
    @Field(() => [DeviceCreateManyInput], {nullable:false})
    @Type(() => DeviceCreateManyInput)
    data!: Array<DeviceCreateManyInput>;
    @Field(() => Boolean, {nullable:true})
    skipDuplicates?: boolean;
}

@ArgsType()
export class CreateOneDeviceArgs {
    @Field(() => DeviceCreateInput, {nullable:false})
    @Type(() => DeviceCreateInput)
    data!: InstanceType<typeof DeviceCreateInput>;
}

@ArgsType()
export class DeleteManyDeviceArgs {
    @Field(() => DeviceWhereInput, {nullable:true})
    @Type(() => DeviceWhereInput)
    where?: InstanceType<typeof DeviceWhereInput>;
}

@ArgsType()
export class DeleteOneDeviceArgs {
    @Field(() => DeviceWhereUniqueInput, {nullable:false})
    @Type(() => DeviceWhereUniqueInput)
    where!: InstanceType<typeof DeviceWhereUniqueInput>;
}

@ArgsType()
export class DeviceAggregateArgs {
    @Field(() => DeviceWhereInput, {nullable:true})
    @Type(() => DeviceWhereInput)
    where?: InstanceType<typeof DeviceWhereInput>;
    @Field(() => [DeviceOrderByWithRelationInput], {nullable:true})
    orderBy?: Array<DeviceOrderByWithRelationInput>;
    @Field(() => DeviceWhereUniqueInput, {nullable:true})
    cursor?: InstanceType<typeof DeviceWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => DeviceCountAggregateInput, {nullable:true})
    _count?: InstanceType<typeof DeviceCountAggregateInput>;
    @Field(() => DeviceMinAggregateInput, {nullable:true})
    _min?: InstanceType<typeof DeviceMinAggregateInput>;
    @Field(() => DeviceMaxAggregateInput, {nullable:true})
    _max?: InstanceType<typeof DeviceMaxAggregateInput>;
}

@InputType()
export class DeviceCountAggregateInput {
    @Field(() => Boolean, {nullable:true})
    token?: true;
    @HideField()
    walletId?: true;
    @Field(() => Boolean, {nullable:true})
    _all?: true;
}

@ObjectType()
export class DeviceCountAggregate {
    @Field(() => Int, {nullable:false})
    token!: number;
    @Field(() => Int, {nullable:false})
    walletId!: number;
    @Field(() => Int, {nullable:false})
    _all!: number;
}

@InputType()
export class DeviceCountOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    token?: keyof typeof SortOrder;
    @HideField()
    walletId?: keyof typeof SortOrder;
}

@InputType()
export class DeviceCreateManyWalletInputEnvelope {
    @Field(() => [DeviceCreateManyWalletInput], {nullable:false})
    @Type(() => DeviceCreateManyWalletInput)
    data!: Array<DeviceCreateManyWalletInput>;
    @Field(() => Boolean, {nullable:true})
    skipDuplicates?: boolean;
}

@InputType()
export class DeviceCreateManyWalletInput {
    @Field(() => String, {nullable:false})
    token!: string;
}

@InputType()
export class DeviceCreateManyInput {
    @Field(() => String, {nullable:false})
    token!: string;
    @HideField()
    walletId!: string;
}

@InputType()
export class DeviceCreateNestedManyWithoutWalletInput {
    @Field(() => [DeviceCreateWithoutWalletInput], {nullable:true})
    @Type(() => DeviceCreateWithoutWalletInput)
    create?: Array<DeviceCreateWithoutWalletInput>;
    @Field(() => [DeviceCreateOrConnectWithoutWalletInput], {nullable:true})
    @Type(() => DeviceCreateOrConnectWithoutWalletInput)
    connectOrCreate?: Array<DeviceCreateOrConnectWithoutWalletInput>;
    @Field(() => DeviceCreateManyWalletInputEnvelope, {nullable:true})
    @Type(() => DeviceCreateManyWalletInputEnvelope)
    createMany?: InstanceType<typeof DeviceCreateManyWalletInputEnvelope>;
    @Field(() => [DeviceWhereUniqueInput], {nullable:true})
    @Type(() => DeviceWhereUniqueInput)
    connect?: Array<DeviceWhereUniqueInput>;
}

@InputType()
export class DeviceCreateOrConnectWithoutWalletInput {
    @Field(() => DeviceWhereUniqueInput, {nullable:false})
    @Type(() => DeviceWhereUniqueInput)
    where!: InstanceType<typeof DeviceWhereUniqueInput>;
    @Field(() => DeviceCreateWithoutWalletInput, {nullable:false})
    @Type(() => DeviceCreateWithoutWalletInput)
    create!: InstanceType<typeof DeviceCreateWithoutWalletInput>;
}

@InputType()
export class DeviceCreateWithoutWalletInput {
    @Field(() => String, {nullable:false})
    token!: string;
}

@InputType()
export class DeviceCreateInput {
    @Field(() => String, {nullable:false})
    token!: string;
    @HideField()
    wallet!: InstanceType<typeof WalletCreateNestedOneWithoutDevicesInput>;
}

@ArgsType()
export class DeviceGroupByArgs {
    @Field(() => DeviceWhereInput, {nullable:true})
    @Type(() => DeviceWhereInput)
    where?: InstanceType<typeof DeviceWhereInput>;
    @Field(() => [DeviceOrderByWithAggregationInput], {nullable:true})
    orderBy?: Array<DeviceOrderByWithAggregationInput>;
    @Field(() => [DeviceScalarFieldEnum], {nullable:false})
    by!: Array<keyof typeof DeviceScalarFieldEnum>;
    @Field(() => DeviceScalarWhereWithAggregatesInput, {nullable:true})
    having?: InstanceType<typeof DeviceScalarWhereWithAggregatesInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => DeviceCountAggregateInput, {nullable:true})
    _count?: InstanceType<typeof DeviceCountAggregateInput>;
    @Field(() => DeviceMinAggregateInput, {nullable:true})
    _min?: InstanceType<typeof DeviceMinAggregateInput>;
    @Field(() => DeviceMaxAggregateInput, {nullable:true})
    _max?: InstanceType<typeof DeviceMaxAggregateInput>;
}

@ObjectType()
export class DeviceGroupBy {
    @Field(() => String, {nullable:false})
    token!: string;
    @Field(() => String, {nullable:false})
    walletId!: string;
    @Field(() => DeviceCountAggregate, {nullable:true})
    _count?: InstanceType<typeof DeviceCountAggregate>;
    @Field(() => DeviceMinAggregate, {nullable:true})
    _min?: InstanceType<typeof DeviceMinAggregate>;
    @Field(() => DeviceMaxAggregate, {nullable:true})
    _max?: InstanceType<typeof DeviceMaxAggregate>;
}

@InputType()
export class DeviceListRelationFilter {
    @Field(() => DeviceWhereInput, {nullable:true})
    every?: InstanceType<typeof DeviceWhereInput>;
    @Field(() => DeviceWhereInput, {nullable:true})
    some?: InstanceType<typeof DeviceWhereInput>;
    @Field(() => DeviceWhereInput, {nullable:true})
    none?: InstanceType<typeof DeviceWhereInput>;
}

@InputType()
export class DeviceMaxAggregateInput {
    @Field(() => Boolean, {nullable:true})
    token?: true;
    @HideField()
    walletId?: true;
}

@ObjectType()
export class DeviceMaxAggregate {
    @Field(() => String, {nullable:true})
    token?: string;
    @Field(() => String, {nullable:true})
    walletId?: string;
}

@InputType()
export class DeviceMaxOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    token?: keyof typeof SortOrder;
    @HideField()
    walletId?: keyof typeof SortOrder;
}

@InputType()
export class DeviceMinAggregateInput {
    @Field(() => Boolean, {nullable:true})
    token?: true;
    @HideField()
    walletId?: true;
}

@ObjectType()
export class DeviceMinAggregate {
    @Field(() => String, {nullable:true})
    token?: string;
    @Field(() => String, {nullable:true})
    walletId?: string;
}

@InputType()
export class DeviceMinOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    token?: keyof typeof SortOrder;
    @HideField()
    walletId?: keyof typeof SortOrder;
}

@InputType()
export class DeviceOrderByRelationAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    _count?: keyof typeof SortOrder;
}

@InputType()
export class DeviceOrderByWithAggregationInput {
    @Field(() => SortOrder, {nullable:true})
    token?: keyof typeof SortOrder;
    @HideField()
    walletId?: keyof typeof SortOrder;
    @Field(() => DeviceCountOrderByAggregateInput, {nullable:true})
    _count?: InstanceType<typeof DeviceCountOrderByAggregateInput>;
    @Field(() => DeviceMaxOrderByAggregateInput, {nullable:true})
    _max?: InstanceType<typeof DeviceMaxOrderByAggregateInput>;
    @Field(() => DeviceMinOrderByAggregateInput, {nullable:true})
    _min?: InstanceType<typeof DeviceMinOrderByAggregateInput>;
}

@InputType()
export class DeviceOrderByWithRelationInput {
    @Field(() => SortOrder, {nullable:true})
    token?: keyof typeof SortOrder;
    @HideField()
    walletId?: keyof typeof SortOrder;
    @HideField()
    wallet?: InstanceType<typeof WalletOrderByWithRelationInput>;
}

@InputType()
export class DeviceScalarWhereWithAggregatesInput {
    @Field(() => [DeviceScalarWhereWithAggregatesInput], {nullable:true})
    AND?: Array<DeviceScalarWhereWithAggregatesInput>;
    @Field(() => [DeviceScalarWhereWithAggregatesInput], {nullable:true})
    OR?: Array<DeviceScalarWhereWithAggregatesInput>;
    @Field(() => [DeviceScalarWhereWithAggregatesInput], {nullable:true})
    NOT?: Array<DeviceScalarWhereWithAggregatesInput>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    token?: InstanceType<typeof StringWithAggregatesFilter>;
    @HideField()
    walletId?: InstanceType<typeof StringWithAggregatesFilter>;
}

@InputType()
export class DeviceScalarWhereInput {
    @Field(() => [DeviceScalarWhereInput], {nullable:true})
    AND?: Array<DeviceScalarWhereInput>;
    @Field(() => [DeviceScalarWhereInput], {nullable:true})
    OR?: Array<DeviceScalarWhereInput>;
    @Field(() => [DeviceScalarWhereInput], {nullable:true})
    NOT?: Array<DeviceScalarWhereInput>;
    @Field(() => StringFilter, {nullable:true})
    token?: InstanceType<typeof StringFilter>;
    @HideField()
    walletId?: InstanceType<typeof StringFilter>;
}

@InputType()
export class DeviceTokenWalletIdCompoundUniqueInput {
    @Field(() => String, {nullable:false})
    token!: string;
    @HideField()
    walletId!: string;
}

@InputType()
export class DeviceUncheckedCreateNestedManyWithoutWalletInput {
    @Field(() => [DeviceCreateWithoutWalletInput], {nullable:true})
    @Type(() => DeviceCreateWithoutWalletInput)
    create?: Array<DeviceCreateWithoutWalletInput>;
    @Field(() => [DeviceCreateOrConnectWithoutWalletInput], {nullable:true})
    @Type(() => DeviceCreateOrConnectWithoutWalletInput)
    connectOrCreate?: Array<DeviceCreateOrConnectWithoutWalletInput>;
    @Field(() => DeviceCreateManyWalletInputEnvelope, {nullable:true})
    @Type(() => DeviceCreateManyWalletInputEnvelope)
    createMany?: InstanceType<typeof DeviceCreateManyWalletInputEnvelope>;
    @Field(() => [DeviceWhereUniqueInput], {nullable:true})
    @Type(() => DeviceWhereUniqueInput)
    connect?: Array<DeviceWhereUniqueInput>;
}

@InputType()
export class DeviceUncheckedCreateWithoutWalletInput {
    @Field(() => String, {nullable:false})
    token!: string;
}

@InputType()
export class DeviceUncheckedCreateInput {
    @Field(() => String, {nullable:false})
    token!: string;
    @HideField()
    walletId!: string;
}

@InputType()
export class DeviceUncheckedUpdateManyWithoutDevicesInput {
    @Field(() => String, {nullable:true})
    token?: string;
}

@InputType()
export class DeviceUncheckedUpdateManyWithoutWalletNestedInput {
    @Field(() => [DeviceCreateWithoutWalletInput], {nullable:true})
    @Type(() => DeviceCreateWithoutWalletInput)
    create?: Array<DeviceCreateWithoutWalletInput>;
    @Field(() => [DeviceCreateOrConnectWithoutWalletInput], {nullable:true})
    @Type(() => DeviceCreateOrConnectWithoutWalletInput)
    connectOrCreate?: Array<DeviceCreateOrConnectWithoutWalletInput>;
    @Field(() => [DeviceUpsertWithWhereUniqueWithoutWalletInput], {nullable:true})
    @Type(() => DeviceUpsertWithWhereUniqueWithoutWalletInput)
    upsert?: Array<DeviceUpsertWithWhereUniqueWithoutWalletInput>;
    @Field(() => DeviceCreateManyWalletInputEnvelope, {nullable:true})
    @Type(() => DeviceCreateManyWalletInputEnvelope)
    createMany?: InstanceType<typeof DeviceCreateManyWalletInputEnvelope>;
    @Field(() => [DeviceWhereUniqueInput], {nullable:true})
    @Type(() => DeviceWhereUniqueInput)
    set?: Array<DeviceWhereUniqueInput>;
    @Field(() => [DeviceWhereUniqueInput], {nullable:true})
    @Type(() => DeviceWhereUniqueInput)
    disconnect?: Array<DeviceWhereUniqueInput>;
    @Field(() => [DeviceWhereUniqueInput], {nullable:true})
    @Type(() => DeviceWhereUniqueInput)
    delete?: Array<DeviceWhereUniqueInput>;
    @Field(() => [DeviceWhereUniqueInput], {nullable:true})
    @Type(() => DeviceWhereUniqueInput)
    connect?: Array<DeviceWhereUniqueInput>;
    @Field(() => [DeviceUpdateWithWhereUniqueWithoutWalletInput], {nullable:true})
    @Type(() => DeviceUpdateWithWhereUniqueWithoutWalletInput)
    update?: Array<DeviceUpdateWithWhereUniqueWithoutWalletInput>;
    @Field(() => [DeviceUpdateManyWithWhereWithoutWalletInput], {nullable:true})
    @Type(() => DeviceUpdateManyWithWhereWithoutWalletInput)
    updateMany?: Array<DeviceUpdateManyWithWhereWithoutWalletInput>;
    @Field(() => [DeviceScalarWhereInput], {nullable:true})
    @Type(() => DeviceScalarWhereInput)
    deleteMany?: Array<DeviceScalarWhereInput>;
}

@InputType()
export class DeviceUncheckedUpdateManyInput {
    @Field(() => String, {nullable:true})
    token?: string;
    @HideField()
    walletId?: string;
}

@InputType()
export class DeviceUncheckedUpdateWithoutWalletInput {
    @Field(() => String, {nullable:true})
    token?: string;
}

@InputType()
export class DeviceUncheckedUpdateInput {
    @Field(() => String, {nullable:true})
    token?: string;
    @HideField()
    walletId?: string;
}

@InputType()
export class DeviceUpdateManyMutationInput {
    @Field(() => String, {nullable:true})
    token?: string;
}

@InputType()
export class DeviceUpdateManyWithWhereWithoutWalletInput {
    @Field(() => DeviceScalarWhereInput, {nullable:false})
    @Type(() => DeviceScalarWhereInput)
    where!: InstanceType<typeof DeviceScalarWhereInput>;
    @Field(() => DeviceUpdateManyMutationInput, {nullable:false})
    @Type(() => DeviceUpdateManyMutationInput)
    data!: InstanceType<typeof DeviceUpdateManyMutationInput>;
}

@InputType()
export class DeviceUpdateManyWithoutWalletNestedInput {
    @Field(() => [DeviceCreateWithoutWalletInput], {nullable:true})
    @Type(() => DeviceCreateWithoutWalletInput)
    create?: Array<DeviceCreateWithoutWalletInput>;
    @Field(() => [DeviceCreateOrConnectWithoutWalletInput], {nullable:true})
    @Type(() => DeviceCreateOrConnectWithoutWalletInput)
    connectOrCreate?: Array<DeviceCreateOrConnectWithoutWalletInput>;
    @Field(() => [DeviceUpsertWithWhereUniqueWithoutWalletInput], {nullable:true})
    @Type(() => DeviceUpsertWithWhereUniqueWithoutWalletInput)
    upsert?: Array<DeviceUpsertWithWhereUniqueWithoutWalletInput>;
    @Field(() => DeviceCreateManyWalletInputEnvelope, {nullable:true})
    @Type(() => DeviceCreateManyWalletInputEnvelope)
    createMany?: InstanceType<typeof DeviceCreateManyWalletInputEnvelope>;
    @Field(() => [DeviceWhereUniqueInput], {nullable:true})
    @Type(() => DeviceWhereUniqueInput)
    set?: Array<DeviceWhereUniqueInput>;
    @Field(() => [DeviceWhereUniqueInput], {nullable:true})
    @Type(() => DeviceWhereUniqueInput)
    disconnect?: Array<DeviceWhereUniqueInput>;
    @Field(() => [DeviceWhereUniqueInput], {nullable:true})
    @Type(() => DeviceWhereUniqueInput)
    delete?: Array<DeviceWhereUniqueInput>;
    @Field(() => [DeviceWhereUniqueInput], {nullable:true})
    @Type(() => DeviceWhereUniqueInput)
    connect?: Array<DeviceWhereUniqueInput>;
    @Field(() => [DeviceUpdateWithWhereUniqueWithoutWalletInput], {nullable:true})
    @Type(() => DeviceUpdateWithWhereUniqueWithoutWalletInput)
    update?: Array<DeviceUpdateWithWhereUniqueWithoutWalletInput>;
    @Field(() => [DeviceUpdateManyWithWhereWithoutWalletInput], {nullable:true})
    @Type(() => DeviceUpdateManyWithWhereWithoutWalletInput)
    updateMany?: Array<DeviceUpdateManyWithWhereWithoutWalletInput>;
    @Field(() => [DeviceScalarWhereInput], {nullable:true})
    @Type(() => DeviceScalarWhereInput)
    deleteMany?: Array<DeviceScalarWhereInput>;
}

@InputType()
export class DeviceUpdateWithWhereUniqueWithoutWalletInput {
    @Field(() => DeviceWhereUniqueInput, {nullable:false})
    @Type(() => DeviceWhereUniqueInput)
    where!: InstanceType<typeof DeviceWhereUniqueInput>;
    @Field(() => DeviceUpdateWithoutWalletInput, {nullable:false})
    @Type(() => DeviceUpdateWithoutWalletInput)
    data!: InstanceType<typeof DeviceUpdateWithoutWalletInput>;
}

@InputType()
export class DeviceUpdateWithoutWalletInput {
    @Field(() => String, {nullable:true})
    token?: string;
}

@InputType()
export class DeviceUpdateInput {
    @Field(() => String, {nullable:true})
    token?: string;
    @HideField()
    wallet?: InstanceType<typeof WalletUpdateOneRequiredWithoutDevicesNestedInput>;
}

@InputType()
export class DeviceUpsertWithWhereUniqueWithoutWalletInput {
    @Field(() => DeviceWhereUniqueInput, {nullable:false})
    @Type(() => DeviceWhereUniqueInput)
    where!: InstanceType<typeof DeviceWhereUniqueInput>;
    @Field(() => DeviceUpdateWithoutWalletInput, {nullable:false})
    @Type(() => DeviceUpdateWithoutWalletInput)
    update!: InstanceType<typeof DeviceUpdateWithoutWalletInput>;
    @Field(() => DeviceCreateWithoutWalletInput, {nullable:false})
    @Type(() => DeviceCreateWithoutWalletInput)
    create!: InstanceType<typeof DeviceCreateWithoutWalletInput>;
}

@InputType()
export class DeviceWhereUniqueInput {
    @Field(() => DeviceTokenWalletIdCompoundUniqueInput, {nullable:true})
    token_walletId?: InstanceType<typeof DeviceTokenWalletIdCompoundUniqueInput>;
}

@InputType()
export class DeviceWhereInput {
    @Field(() => [DeviceWhereInput], {nullable:true})
    AND?: Array<DeviceWhereInput>;
    @Field(() => [DeviceWhereInput], {nullable:true})
    OR?: Array<DeviceWhereInput>;
    @Field(() => [DeviceWhereInput], {nullable:true})
    NOT?: Array<DeviceWhereInput>;
    @Field(() => StringFilter, {nullable:true})
    token?: InstanceType<typeof StringFilter>;
    @HideField()
    walletId?: InstanceType<typeof StringFilter>;
    @HideField()
    wallet?: InstanceType<typeof WalletRelationFilter>;
}

@ObjectType()
export class Device {
    @Field(() => String, {nullable:false})
    token!: string;
    @Field(() => String, {nullable:false})
    walletId!: string;
    @HideField()
    wallet?: InstanceType<typeof Wallet>;
}

@ArgsType()
export class FindFirstDeviceOrThrowArgs {
    @Field(() => DeviceWhereInput, {nullable:true})
    @Type(() => DeviceWhereInput)
    where?: InstanceType<typeof DeviceWhereInput>;
    @Field(() => [DeviceOrderByWithRelationInput], {nullable:true})
    orderBy?: Array<DeviceOrderByWithRelationInput>;
    @Field(() => DeviceWhereUniqueInput, {nullable:true})
    cursor?: InstanceType<typeof DeviceWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => [DeviceScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof DeviceScalarFieldEnum>;
}

@ArgsType()
export class FindFirstDeviceArgs {
    @Field(() => DeviceWhereInput, {nullable:true})
    @Type(() => DeviceWhereInput)
    where?: InstanceType<typeof DeviceWhereInput>;
    @Field(() => [DeviceOrderByWithRelationInput], {nullable:true})
    orderBy?: Array<DeviceOrderByWithRelationInput>;
    @Field(() => DeviceWhereUniqueInput, {nullable:true})
    cursor?: InstanceType<typeof DeviceWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => [DeviceScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof DeviceScalarFieldEnum>;
}

@ArgsType()
export class FindManyDeviceArgs {
    @Field(() => DeviceWhereInput, {nullable:true})
    @Type(() => DeviceWhereInput)
    where?: InstanceType<typeof DeviceWhereInput>;
    @Field(() => [DeviceOrderByWithRelationInput], {nullable:true})
    orderBy?: Array<DeviceOrderByWithRelationInput>;
    @Field(() => DeviceWhereUniqueInput, {nullable:true})
    cursor?: InstanceType<typeof DeviceWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => [DeviceScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof DeviceScalarFieldEnum>;
}

@ArgsType()
export class FindUniqueDeviceOrThrowArgs {
    @Field(() => DeviceWhereUniqueInput, {nullable:false})
    @Type(() => DeviceWhereUniqueInput)
    where!: InstanceType<typeof DeviceWhereUniqueInput>;
}

@ArgsType()
export class FindUniqueDeviceArgs {
    @Field(() => DeviceWhereUniqueInput, {nullable:false})
    @Type(() => DeviceWhereUniqueInput)
    where!: InstanceType<typeof DeviceWhereUniqueInput>;
}

@ArgsType()
export class UpdateManyDeviceArgs {
    @Field(() => DeviceUpdateManyMutationInput, {nullable:false})
    @Type(() => DeviceUpdateManyMutationInput)
    data!: InstanceType<typeof DeviceUpdateManyMutationInput>;
    @Field(() => DeviceWhereInput, {nullable:true})
    @Type(() => DeviceWhereInput)
    where?: InstanceType<typeof DeviceWhereInput>;
}

@ArgsType()
export class UpdateOneDeviceArgs {
    @Field(() => DeviceUpdateInput, {nullable:false})
    @Type(() => DeviceUpdateInput)
    data!: InstanceType<typeof DeviceUpdateInput>;
    @Field(() => DeviceWhereUniqueInput, {nullable:false})
    @Type(() => DeviceWhereUniqueInput)
    where!: InstanceType<typeof DeviceWhereUniqueInput>;
}

@ArgsType()
export class UpsertOneDeviceArgs {
    @Field(() => DeviceWhereUniqueInput, {nullable:false})
    @Type(() => DeviceWhereUniqueInput)
    where!: InstanceType<typeof DeviceWhereUniqueInput>;
    @Field(() => DeviceCreateInput, {nullable:false})
    @Type(() => DeviceCreateInput)
    create!: InstanceType<typeof DeviceCreateInput>;
    @Field(() => DeviceUpdateInput, {nullable:false})
    @Type(() => DeviceUpdateInput)
    update!: InstanceType<typeof DeviceUpdateInput>;
}

@ObjectType()
export class AggregateMessagePayload {
    @Field(() => MessagePayloadCountAggregate, {nullable:true})
    _count?: InstanceType<typeof MessagePayloadCountAggregate>;
    @Field(() => MessagePayloadMinAggregate, {nullable:true})
    _min?: InstanceType<typeof MessagePayloadMinAggregate>;
    @Field(() => MessagePayloadMaxAggregate, {nullable:true})
    _max?: InstanceType<typeof MessagePayloadMaxAggregate>;
}

@ArgsType()
export class CreateManyMessagePayloadArgs {
    @Field(() => [MessagePayloadCreateManyInput], {nullable:false})
    @Type(() => MessagePayloadCreateManyInput)
    data!: Array<MessagePayloadCreateManyInput>;
    @Field(() => Boolean, {nullable:true})
    skipDuplicates?: boolean;
}

@ArgsType()
export class CreateOneMessagePayloadArgs {
    @Field(() => MessagePayloadCreateInput, {nullable:false})
    @Type(() => MessagePayloadCreateInput)
    data!: InstanceType<typeof MessagePayloadCreateInput>;
}

@ArgsType()
export class DeleteManyMessagePayloadArgs {
    @Field(() => MessagePayloadWhereInput, {nullable:true})
    @Type(() => MessagePayloadWhereInput)
    where?: InstanceType<typeof MessagePayloadWhereInput>;
}

@ArgsType()
export class DeleteOneMessagePayloadArgs {
    @Field(() => MessagePayloadWhereUniqueInput, {nullable:false})
    @Type(() => MessagePayloadWhereUniqueInput)
    where!: InstanceType<typeof MessagePayloadWhereUniqueInput>;
}

@ArgsType()
export class FindFirstMessagePayloadOrThrowArgs {
    @Field(() => MessagePayloadWhereInput, {nullable:true})
    @Type(() => MessagePayloadWhereInput)
    where?: InstanceType<typeof MessagePayloadWhereInput>;
    @Field(() => [MessagePayloadOrderByWithRelationInput], {nullable:true})
    orderBy?: Array<MessagePayloadOrderByWithRelationInput>;
    @Field(() => MessagePayloadWhereUniqueInput, {nullable:true})
    cursor?: InstanceType<typeof MessagePayloadWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => [MessagePayloadScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof MessagePayloadScalarFieldEnum>;
}

@ArgsType()
export class FindFirstMessagePayloadArgs {
    @Field(() => MessagePayloadWhereInput, {nullable:true})
    @Type(() => MessagePayloadWhereInput)
    where?: InstanceType<typeof MessagePayloadWhereInput>;
    @Field(() => [MessagePayloadOrderByWithRelationInput], {nullable:true})
    orderBy?: Array<MessagePayloadOrderByWithRelationInput>;
    @Field(() => MessagePayloadWhereUniqueInput, {nullable:true})
    cursor?: InstanceType<typeof MessagePayloadWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => [MessagePayloadScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof MessagePayloadScalarFieldEnum>;
}

@ArgsType()
export class FindManyMessagePayloadArgs {
    @Field(() => MessagePayloadWhereInput, {nullable:true})
    @Type(() => MessagePayloadWhereInput)
    where?: InstanceType<typeof MessagePayloadWhereInput>;
    @Field(() => [MessagePayloadOrderByWithRelationInput], {nullable:true})
    orderBy?: Array<MessagePayloadOrderByWithRelationInput>;
    @Field(() => MessagePayloadWhereUniqueInput, {nullable:true})
    cursor?: InstanceType<typeof MessagePayloadWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => [MessagePayloadScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof MessagePayloadScalarFieldEnum>;
}

@ArgsType()
export class FindUniqueMessagePayloadOrThrowArgs {
    @Field(() => MessagePayloadWhereUniqueInput, {nullable:false})
    @Type(() => MessagePayloadWhereUniqueInput)
    where!: InstanceType<typeof MessagePayloadWhereUniqueInput>;
}

@ArgsType()
export class FindUniqueMessagePayloadArgs {
    @Field(() => MessagePayloadWhereUniqueInput, {nullable:false})
    @Type(() => MessagePayloadWhereUniqueInput)
    where!: InstanceType<typeof MessagePayloadWhereUniqueInput>;
}

@ArgsType()
export class MessagePayloadAggregateArgs {
    @Field(() => MessagePayloadWhereInput, {nullable:true})
    @Type(() => MessagePayloadWhereInput)
    where?: InstanceType<typeof MessagePayloadWhereInput>;
    @Field(() => [MessagePayloadOrderByWithRelationInput], {nullable:true})
    orderBy?: Array<MessagePayloadOrderByWithRelationInput>;
    @Field(() => MessagePayloadWhereUniqueInput, {nullable:true})
    cursor?: InstanceType<typeof MessagePayloadWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => MessagePayloadCountAggregateInput, {nullable:true})
    _count?: InstanceType<typeof MessagePayloadCountAggregateInput>;
    @Field(() => MessagePayloadMinAggregateInput, {nullable:true})
    _min?: InstanceType<typeof MessagePayloadMinAggregateInput>;
    @Field(() => MessagePayloadMaxAggregateInput, {nullable:true})
    _max?: InstanceType<typeof MessagePayloadMaxAggregateInput>;
}

@InputType()
export class MessagePayloadCountAggregateInput {
    @Field(() => Boolean, {nullable:true})
    id?: true;
    @Field(() => Boolean, {nullable:true})
    payload?: true;
    @Field(() => Boolean, {nullable:true})
    _all?: true;
}

@ObjectType()
export class MessagePayloadCountAggregate {
    @Field(() => Int, {nullable:false})
    id!: number;
    @Field(() => Int, {nullable:false})
    payload!: number;
    @Field(() => Int, {nullable:false})
    _all!: number;
}

@InputType()
export class MessagePayloadCountOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    payload?: keyof typeof SortOrder;
}

@InputType()
export class MessagePayloadCreateManyInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => GraphQLJSON, {nullable:false})
    payload!: any;
}

@InputType()
export class MessagePayloadCreateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => GraphQLJSON, {nullable:false})
    payload!: any;
}

@ArgsType()
export class MessagePayloadGroupByArgs {
    @Field(() => MessagePayloadWhereInput, {nullable:true})
    @Type(() => MessagePayloadWhereInput)
    where?: InstanceType<typeof MessagePayloadWhereInput>;
    @Field(() => [MessagePayloadOrderByWithAggregationInput], {nullable:true})
    orderBy?: Array<MessagePayloadOrderByWithAggregationInput>;
    @Field(() => [MessagePayloadScalarFieldEnum], {nullable:false})
    by!: Array<keyof typeof MessagePayloadScalarFieldEnum>;
    @Field(() => MessagePayloadScalarWhereWithAggregatesInput, {nullable:true})
    having?: InstanceType<typeof MessagePayloadScalarWhereWithAggregatesInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => MessagePayloadCountAggregateInput, {nullable:true})
    _count?: InstanceType<typeof MessagePayloadCountAggregateInput>;
    @Field(() => MessagePayloadMinAggregateInput, {nullable:true})
    _min?: InstanceType<typeof MessagePayloadMinAggregateInput>;
    @Field(() => MessagePayloadMaxAggregateInput, {nullable:true})
    _max?: InstanceType<typeof MessagePayloadMaxAggregateInput>;
}

@ObjectType()
export class MessagePayloadGroupBy {
    @Field(() => String, {nullable:false})
    id!: string;
    @Field(() => GraphQLJSON, {nullable:false})
    payload!: any;
    @Field(() => MessagePayloadCountAggregate, {nullable:true})
    _count?: InstanceType<typeof MessagePayloadCountAggregate>;
    @Field(() => MessagePayloadMinAggregate, {nullable:true})
    _min?: InstanceType<typeof MessagePayloadMinAggregate>;
    @Field(() => MessagePayloadMaxAggregate, {nullable:true})
    _max?: InstanceType<typeof MessagePayloadMaxAggregate>;
}

@InputType()
export class MessagePayloadMaxAggregateInput {
    @Field(() => Boolean, {nullable:true})
    id?: true;
}

@ObjectType()
export class MessagePayloadMaxAggregate {
    @Field(() => String, {nullable:true})
    id?: string;
}

@InputType()
export class MessagePayloadMaxOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
}

@InputType()
export class MessagePayloadMinAggregateInput {
    @Field(() => Boolean, {nullable:true})
    id?: true;
}

@ObjectType()
export class MessagePayloadMinAggregate {
    @Field(() => String, {nullable:true})
    id?: string;
}

@InputType()
export class MessagePayloadMinOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
}

@InputType()
export class MessagePayloadOrderByWithAggregationInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    payload?: keyof typeof SortOrder;
    @Field(() => MessagePayloadCountOrderByAggregateInput, {nullable:true})
    _count?: InstanceType<typeof MessagePayloadCountOrderByAggregateInput>;
    @Field(() => MessagePayloadMaxOrderByAggregateInput, {nullable:true})
    _max?: InstanceType<typeof MessagePayloadMaxOrderByAggregateInput>;
    @Field(() => MessagePayloadMinOrderByAggregateInput, {nullable:true})
    _min?: InstanceType<typeof MessagePayloadMinOrderByAggregateInput>;
}

@InputType()
export class MessagePayloadOrderByWithRelationInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    payload?: keyof typeof SortOrder;
}

@InputType()
export class MessagePayloadScalarWhereWithAggregatesInput {
    @Field(() => [MessagePayloadScalarWhereWithAggregatesInput], {nullable:true})
    AND?: Array<MessagePayloadScalarWhereWithAggregatesInput>;
    @Field(() => [MessagePayloadScalarWhereWithAggregatesInput], {nullable:true})
    OR?: Array<MessagePayloadScalarWhereWithAggregatesInput>;
    @Field(() => [MessagePayloadScalarWhereWithAggregatesInput], {nullable:true})
    NOT?: Array<MessagePayloadScalarWhereWithAggregatesInput>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    id?: InstanceType<typeof StringWithAggregatesFilter>;
    @Field(() => JsonWithAggregatesFilter, {nullable:true})
    payload?: InstanceType<typeof JsonWithAggregatesFilter>;
}

@InputType()
export class MessagePayloadUncheckedCreateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => GraphQLJSON, {nullable:false})
    payload!: any;
}

@InputType()
export class MessagePayloadUncheckedUpdateManyInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => GraphQLJSON, {nullable:true})
    payload?: any;
}

@InputType()
export class MessagePayloadUncheckedUpdateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => GraphQLJSON, {nullable:true})
    payload?: any;
}

@InputType()
export class MessagePayloadUpdateManyMutationInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => GraphQLJSON, {nullable:true})
    payload?: any;
}

@InputType()
export class MessagePayloadUpdateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => GraphQLJSON, {nullable:true})
    payload?: any;
}

@InputType()
export class MessagePayloadWhereUniqueInput {
    @Field(() => String, {nullable:true})
    id?: string;
}

@InputType()
export class MessagePayloadWhereInput {
    @Field(() => [MessagePayloadWhereInput], {nullable:true})
    AND?: Array<MessagePayloadWhereInput>;
    @Field(() => [MessagePayloadWhereInput], {nullable:true})
    OR?: Array<MessagePayloadWhereInput>;
    @Field(() => [MessagePayloadWhereInput], {nullable:true})
    NOT?: Array<MessagePayloadWhereInput>;
    @Field(() => StringFilter, {nullable:true})
    id?: InstanceType<typeof StringFilter>;
    @Field(() => JsonFilter, {nullable:true})
    payload?: InstanceType<typeof JsonFilter>;
}

@ObjectType()
export class MessagePayload {
    @Field(() => ID, {nullable:false})
    id!: string;
    @Field(() => GraphQLJSON, {nullable:false})
    payload!: any;
}

@ArgsType()
export class UpdateManyMessagePayloadArgs {
    @Field(() => MessagePayloadUpdateManyMutationInput, {nullable:false})
    @Type(() => MessagePayloadUpdateManyMutationInput)
    data!: InstanceType<typeof MessagePayloadUpdateManyMutationInput>;
    @Field(() => MessagePayloadWhereInput, {nullable:true})
    @Type(() => MessagePayloadWhereInput)
    where?: InstanceType<typeof MessagePayloadWhereInput>;
}

@ArgsType()
export class UpdateOneMessagePayloadArgs {
    @Field(() => MessagePayloadUpdateInput, {nullable:false})
    @Type(() => MessagePayloadUpdateInput)
    data!: InstanceType<typeof MessagePayloadUpdateInput>;
    @Field(() => MessagePayloadWhereUniqueInput, {nullable:false})
    @Type(() => MessagePayloadWhereUniqueInput)
    where!: InstanceType<typeof MessagePayloadWhereUniqueInput>;
}

@ArgsType()
export class UpsertOneMessagePayloadArgs {
    @Field(() => MessagePayloadWhereUniqueInput, {nullable:false})
    @Type(() => MessagePayloadWhereUniqueInput)
    where!: InstanceType<typeof MessagePayloadWhereUniqueInput>;
    @Field(() => MessagePayloadCreateInput, {nullable:false})
    @Type(() => MessagePayloadCreateInput)
    create!: InstanceType<typeof MessagePayloadCreateInput>;
    @Field(() => MessagePayloadUpdateInput, {nullable:false})
    @Type(() => MessagePayloadUpdateInput)
    update!: InstanceType<typeof MessagePayloadUpdateInput>;
}

@ObjectType()
export class AggregateOtp {
    @Field(() => OtpCountAggregate, {nullable:true})
    _count?: InstanceType<typeof OtpCountAggregate>;
    @Field(() => OtpAvgAggregate, {nullable:true})
    _avg?: InstanceType<typeof OtpAvgAggregate>;
    @Field(() => OtpSumAggregate, {nullable:true})
    _sum?: InstanceType<typeof OtpSumAggregate>;
    @Field(() => OtpMinAggregate, {nullable:true})
    _min?: InstanceType<typeof OtpMinAggregate>;
    @Field(() => OtpMaxAggregate, {nullable:true})
    _max?: InstanceType<typeof OtpMaxAggregate>;
}

@ArgsType()
export class CreateManyOtpArgs {
    @Field(() => [OtpCreateManyInput], {nullable:false})
    @Type(() => OtpCreateManyInput)
    data!: Array<OtpCreateManyInput>;
    @Field(() => Boolean, {nullable:true})
    skipDuplicates?: boolean;
}

@ArgsType()
export class CreateOneOtpArgs {
    @Field(() => OtpCreateInput, {nullable:false})
    @Type(() => OtpCreateInput)
    data!: InstanceType<typeof OtpCreateInput>;
}

@ArgsType()
export class DeleteManyOtpArgs {
    @Field(() => OtpWhereInput, {nullable:true})
    @Type(() => OtpWhereInput)
    where?: InstanceType<typeof OtpWhereInput>;
}

@ArgsType()
export class DeleteOneOtpArgs {
    @Field(() => OtpWhereUniqueInput, {nullable:false})
    @Type(() => OtpWhereUniqueInput)
    where!: InstanceType<typeof OtpWhereUniqueInput>;
}

@ArgsType()
export class FindFirstOtpOrThrowArgs {
    @Field(() => OtpWhereInput, {nullable:true})
    @Type(() => OtpWhereInput)
    where?: InstanceType<typeof OtpWhereInput>;
    @Field(() => [OtpOrderByWithRelationInput], {nullable:true})
    orderBy?: Array<OtpOrderByWithRelationInput>;
    @Field(() => OtpWhereUniqueInput, {nullable:true})
    cursor?: InstanceType<typeof OtpWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => [OtpScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof OtpScalarFieldEnum>;
}

@ArgsType()
export class FindFirstOtpArgs {
    @Field(() => OtpWhereInput, {nullable:true})
    @Type(() => OtpWhereInput)
    where?: InstanceType<typeof OtpWhereInput>;
    @Field(() => [OtpOrderByWithRelationInput], {nullable:true})
    orderBy?: Array<OtpOrderByWithRelationInput>;
    @Field(() => OtpWhereUniqueInput, {nullable:true})
    cursor?: InstanceType<typeof OtpWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => [OtpScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof OtpScalarFieldEnum>;
}

@ArgsType()
export class FindManyOtpArgs {
    @Field(() => OtpWhereInput, {nullable:true})
    @Type(() => OtpWhereInput)
    where?: InstanceType<typeof OtpWhereInput>;
    @Field(() => [OtpOrderByWithRelationInput], {nullable:true})
    orderBy?: Array<OtpOrderByWithRelationInput>;
    @Field(() => OtpWhereUniqueInput, {nullable:true})
    cursor?: InstanceType<typeof OtpWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => [OtpScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof OtpScalarFieldEnum>;
}

@ArgsType()
export class FindUniqueOtpOrThrowArgs {
    @Field(() => OtpWhereUniqueInput, {nullable:false})
    @Type(() => OtpWhereUniqueInput)
    where!: InstanceType<typeof OtpWhereUniqueInput>;
}

@ArgsType()
export class FindUniqueOtpArgs {
    @Field(() => OtpWhereUniqueInput, {nullable:false})
    @Type(() => OtpWhereUniqueInput)
    where!: InstanceType<typeof OtpWhereUniqueInput>;
}

@ArgsType()
export class OtpAggregateArgs {
    @Field(() => OtpWhereInput, {nullable:true})
    @Type(() => OtpWhereInput)
    where?: InstanceType<typeof OtpWhereInput>;
    @Field(() => [OtpOrderByWithRelationInput], {nullable:true})
    orderBy?: Array<OtpOrderByWithRelationInput>;
    @Field(() => OtpWhereUniqueInput, {nullable:true})
    cursor?: InstanceType<typeof OtpWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => OtpCountAggregateInput, {nullable:true})
    _count?: InstanceType<typeof OtpCountAggregateInput>;
    @Field(() => OtpAvgAggregateInput, {nullable:true})
    _avg?: InstanceType<typeof OtpAvgAggregateInput>;
    @Field(() => OtpSumAggregateInput, {nullable:true})
    _sum?: InstanceType<typeof OtpSumAggregateInput>;
    @Field(() => OtpMinAggregateInput, {nullable:true})
    _min?: InstanceType<typeof OtpMinAggregateInput>;
    @Field(() => OtpMaxAggregateInput, {nullable:true})
    _max?: InstanceType<typeof OtpMaxAggregateInput>;
}

@InputType()
export class OtpAvgAggregateInput {
    @Field(() => Boolean, {nullable:true})
    digits?: true;
}

@ObjectType()
export class OtpAvgAggregate {
    @Field(() => Float, {nullable:true})
    digits?: number;
}

@InputType()
export class OtpAvgOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    digits?: keyof typeof SortOrder;
}

@InputType()
export class OtpCountAggregateInput {
    @Field(() => Boolean, {nullable:true})
    id?: true;
    @HideField()
    createdAt?: true;
    @HideField()
    updatedAt?: true;
    @Field(() => Boolean, {nullable:true})
    algorithm?: true;
    @Field(() => Boolean, {nullable:true})
    secret?: true;
    @Field(() => Boolean, {nullable:true})
    digits?: true;
    @Field(() => Boolean, {nullable:true})
    issuer?: true;
    @Field(() => Boolean, {nullable:true})
    accountName?: true;
    @HideField()
    walletId?: true;
    @Field(() => Boolean, {nullable:true})
    _all?: true;
}

@ObjectType()
export class OtpCountAggregate {
    @Field(() => Int, {nullable:false})
    id!: number;
    @Field(() => Int, {nullable:false})
    createdAt!: number;
    @Field(() => Int, {nullable:false})
    updatedAt!: number;
    @Field(() => Int, {nullable:false})
    algorithm!: number;
    @Field(() => Int, {nullable:false})
    secret!: number;
    @Field(() => Int, {nullable:false})
    digits!: number;
    @Field(() => Int, {nullable:false})
    issuer!: number;
    @Field(() => Int, {nullable:false})
    accountName!: number;
    @Field(() => Int, {nullable:false})
    walletId!: number;
    @Field(() => Int, {nullable:false})
    _all!: number;
}

@InputType()
export class OtpCountOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    algorithm?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    secret?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    digits?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    issuer?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    accountName?: keyof typeof SortOrder;
    @HideField()
    walletId?: keyof typeof SortOrder;
}

@InputType()
export class OtpCreateManyWalletInputEnvelope {
    @Field(() => [OtpCreateManyWalletInput], {nullable:false})
    @Type(() => OtpCreateManyWalletInput)
    data!: Array<OtpCreateManyWalletInput>;
    @Field(() => Boolean, {nullable:true})
    skipDuplicates?: boolean;
}

@InputType()
export class OtpCreateManyWalletInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => OtpAlgorithm, {nullable:false})
    algorithm!: keyof typeof OtpAlgorithm;
    @Field(() => String, {nullable:false})
    secret!: string;
    @Field(() => Int, {nullable:false})
    digits!: number;
    @Field(() => String, {nullable:false})
    issuer!: string;
    @Field(() => String, {nullable:false})
    accountName!: string;
}

@InputType()
export class OtpCreateManyInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => OtpAlgorithm, {nullable:false})
    algorithm!: keyof typeof OtpAlgorithm;
    @Field(() => String, {nullable:false})
    secret!: string;
    @Field(() => Int, {nullable:false})
    digits!: number;
    @Field(() => String, {nullable:false})
    issuer!: string;
    @Field(() => String, {nullable:false})
    accountName!: string;
    @HideField()
    walletId!: string;
}

@InputType()
export class OtpCreateNestedManyWithoutWalletInput {
    @Field(() => [OtpCreateWithoutWalletInput], {nullable:true})
    @Type(() => OtpCreateWithoutWalletInput)
    create?: Array<OtpCreateWithoutWalletInput>;
    @Field(() => [OtpCreateOrConnectWithoutWalletInput], {nullable:true})
    @Type(() => OtpCreateOrConnectWithoutWalletInput)
    connectOrCreate?: Array<OtpCreateOrConnectWithoutWalletInput>;
    @Field(() => OtpCreateManyWalletInputEnvelope, {nullable:true})
    @Type(() => OtpCreateManyWalletInputEnvelope)
    createMany?: InstanceType<typeof OtpCreateManyWalletInputEnvelope>;
    @Field(() => [OtpWhereUniqueInput], {nullable:true})
    @Type(() => OtpWhereUniqueInput)
    connect?: Array<OtpWhereUniqueInput>;
}

@InputType()
export class OtpCreateOrConnectWithoutWalletInput {
    @Field(() => OtpWhereUniqueInput, {nullable:false})
    @Type(() => OtpWhereUniqueInput)
    where!: InstanceType<typeof OtpWhereUniqueInput>;
    @Field(() => OtpCreateWithoutWalletInput, {nullable:false})
    @Type(() => OtpCreateWithoutWalletInput)
    create!: InstanceType<typeof OtpCreateWithoutWalletInput>;
}

@InputType()
export class OtpCreateWithoutWalletInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => OtpAlgorithm, {nullable:false})
    algorithm!: keyof typeof OtpAlgorithm;
    @Field(() => String, {nullable:false})
    secret!: string;
    @Field(() => Int, {nullable:false})
    digits!: number;
    @Field(() => String, {nullable:false})
    issuer!: string;
    @Field(() => String, {nullable:false})
    accountName!: string;
}

@InputType()
export class OtpCreateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => OtpAlgorithm, {nullable:false})
    algorithm!: keyof typeof OtpAlgorithm;
    @Field(() => String, {nullable:false})
    secret!: string;
    @Field(() => Int, {nullable:false})
    digits!: number;
    @Field(() => String, {nullable:false})
    issuer!: string;
    @Field(() => String, {nullable:false})
    accountName!: string;
    @HideField()
    wallet!: InstanceType<typeof WalletCreateNestedOneWithoutOtpsInput>;
}

@ArgsType()
export class OtpGroupByArgs {
    @Field(() => OtpWhereInput, {nullable:true})
    @Type(() => OtpWhereInput)
    where?: InstanceType<typeof OtpWhereInput>;
    @Field(() => [OtpOrderByWithAggregationInput], {nullable:true})
    orderBy?: Array<OtpOrderByWithAggregationInput>;
    @Field(() => [OtpScalarFieldEnum], {nullable:false})
    by!: Array<keyof typeof OtpScalarFieldEnum>;
    @Field(() => OtpScalarWhereWithAggregatesInput, {nullable:true})
    having?: InstanceType<typeof OtpScalarWhereWithAggregatesInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => OtpCountAggregateInput, {nullable:true})
    _count?: InstanceType<typeof OtpCountAggregateInput>;
    @Field(() => OtpAvgAggregateInput, {nullable:true})
    _avg?: InstanceType<typeof OtpAvgAggregateInput>;
    @Field(() => OtpSumAggregateInput, {nullable:true})
    _sum?: InstanceType<typeof OtpSumAggregateInput>;
    @Field(() => OtpMinAggregateInput, {nullable:true})
    _min?: InstanceType<typeof OtpMinAggregateInput>;
    @Field(() => OtpMaxAggregateInput, {nullable:true})
    _max?: InstanceType<typeof OtpMaxAggregateInput>;
}

@ObjectType()
export class OtpGroupBy {
    @Field(() => String, {nullable:false})
    id!: string;
    @Field(() => Date, {nullable:false})
    createdAt!: Date | string;
    @Field(() => Date, {nullable:false})
    updatedAt!: Date | string;
    @Field(() => OtpAlgorithm, {nullable:false})
    algorithm!: keyof typeof OtpAlgorithm;
    @Field(() => String, {nullable:false})
    secret!: string;
    @Field(() => Int, {nullable:false})
    digits!: number;
    @Field(() => String, {nullable:false})
    issuer!: string;
    @Field(() => String, {nullable:false})
    accountName!: string;
    @Field(() => String, {nullable:false})
    walletId!: string;
    @Field(() => OtpCountAggregate, {nullable:true})
    _count?: InstanceType<typeof OtpCountAggregate>;
    @Field(() => OtpAvgAggregate, {nullable:true})
    _avg?: InstanceType<typeof OtpAvgAggregate>;
    @Field(() => OtpSumAggregate, {nullable:true})
    _sum?: InstanceType<typeof OtpSumAggregate>;
    @Field(() => OtpMinAggregate, {nullable:true})
    _min?: InstanceType<typeof OtpMinAggregate>;
    @Field(() => OtpMaxAggregate, {nullable:true})
    _max?: InstanceType<typeof OtpMaxAggregate>;
}

@InputType()
export class OtpListRelationFilter {
    @Field(() => OtpWhereInput, {nullable:true})
    every?: InstanceType<typeof OtpWhereInput>;
    @Field(() => OtpWhereInput, {nullable:true})
    some?: InstanceType<typeof OtpWhereInput>;
    @Field(() => OtpWhereInput, {nullable:true})
    none?: InstanceType<typeof OtpWhereInput>;
}

@InputType()
export class OtpMaxAggregateInput {
    @Field(() => Boolean, {nullable:true})
    id?: true;
    @HideField()
    createdAt?: true;
    @HideField()
    updatedAt?: true;
    @Field(() => Boolean, {nullable:true})
    algorithm?: true;
    @Field(() => Boolean, {nullable:true})
    secret?: true;
    @Field(() => Boolean, {nullable:true})
    digits?: true;
    @Field(() => Boolean, {nullable:true})
    issuer?: true;
    @Field(() => Boolean, {nullable:true})
    accountName?: true;
    @HideField()
    walletId?: true;
}

@ObjectType()
export class OtpMaxAggregate {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;
    @Field(() => OtpAlgorithm, {nullable:true})
    algorithm?: keyof typeof OtpAlgorithm;
    @Field(() => String, {nullable:true})
    secret?: string;
    @Field(() => Int, {nullable:true})
    digits?: number;
    @Field(() => String, {nullable:true})
    issuer?: string;
    @Field(() => String, {nullable:true})
    accountName?: string;
    @Field(() => String, {nullable:true})
    walletId?: string;
}

@InputType()
export class OtpMaxOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    algorithm?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    secret?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    digits?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    issuer?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    accountName?: keyof typeof SortOrder;
    @HideField()
    walletId?: keyof typeof SortOrder;
}

@InputType()
export class OtpMinAggregateInput {
    @Field(() => Boolean, {nullable:true})
    id?: true;
    @HideField()
    createdAt?: true;
    @HideField()
    updatedAt?: true;
    @Field(() => Boolean, {nullable:true})
    algorithm?: true;
    @Field(() => Boolean, {nullable:true})
    secret?: true;
    @Field(() => Boolean, {nullable:true})
    digits?: true;
    @Field(() => Boolean, {nullable:true})
    issuer?: true;
    @Field(() => Boolean, {nullable:true})
    accountName?: true;
    @HideField()
    walletId?: true;
}

@ObjectType()
export class OtpMinAggregate {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;
    @Field(() => OtpAlgorithm, {nullable:true})
    algorithm?: keyof typeof OtpAlgorithm;
    @Field(() => String, {nullable:true})
    secret?: string;
    @Field(() => Int, {nullable:true})
    digits?: number;
    @Field(() => String, {nullable:true})
    issuer?: string;
    @Field(() => String, {nullable:true})
    accountName?: string;
    @Field(() => String, {nullable:true})
    walletId?: string;
}

@InputType()
export class OtpMinOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    algorithm?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    secret?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    digits?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    issuer?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    accountName?: keyof typeof SortOrder;
    @HideField()
    walletId?: keyof typeof SortOrder;
}

@InputType()
export class OtpOrderByRelationAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    _count?: keyof typeof SortOrder;
}

@InputType()
export class OtpOrderByWithAggregationInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    algorithm?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    secret?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    digits?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    issuer?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    accountName?: keyof typeof SortOrder;
    @HideField()
    walletId?: keyof typeof SortOrder;
    @Field(() => OtpCountOrderByAggregateInput, {nullable:true})
    _count?: InstanceType<typeof OtpCountOrderByAggregateInput>;
    @Field(() => OtpAvgOrderByAggregateInput, {nullable:true})
    _avg?: InstanceType<typeof OtpAvgOrderByAggregateInput>;
    @Field(() => OtpMaxOrderByAggregateInput, {nullable:true})
    _max?: InstanceType<typeof OtpMaxOrderByAggregateInput>;
    @Field(() => OtpMinOrderByAggregateInput, {nullable:true})
    _min?: InstanceType<typeof OtpMinOrderByAggregateInput>;
    @Field(() => OtpSumOrderByAggregateInput, {nullable:true})
    _sum?: InstanceType<typeof OtpSumOrderByAggregateInput>;
}

@InputType()
export class OtpOrderByWithRelationInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    algorithm?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    secret?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    digits?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    issuer?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    accountName?: keyof typeof SortOrder;
    @HideField()
    walletId?: keyof typeof SortOrder;
    @HideField()
    wallet?: InstanceType<typeof WalletOrderByWithRelationInput>;
}

@InputType()
export class OtpScalarWhereWithAggregatesInput {
    @Field(() => [OtpScalarWhereWithAggregatesInput], {nullable:true})
    AND?: Array<OtpScalarWhereWithAggregatesInput>;
    @Field(() => [OtpScalarWhereWithAggregatesInput], {nullable:true})
    OR?: Array<OtpScalarWhereWithAggregatesInput>;
    @Field(() => [OtpScalarWhereWithAggregatesInput], {nullable:true})
    NOT?: Array<OtpScalarWhereWithAggregatesInput>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    id?: InstanceType<typeof StringWithAggregatesFilter>;
    @HideField()
    createdAt?: InstanceType<typeof DateTimeWithAggregatesFilter>;
    @HideField()
    updatedAt?: InstanceType<typeof DateTimeWithAggregatesFilter>;
    @Field(() => EnumOtpAlgorithmWithAggregatesFilter, {nullable:true})
    algorithm?: InstanceType<typeof EnumOtpAlgorithmWithAggregatesFilter>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    secret?: InstanceType<typeof StringWithAggregatesFilter>;
    @Field(() => IntWithAggregatesFilter, {nullable:true})
    digits?: InstanceType<typeof IntWithAggregatesFilter>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    issuer?: InstanceType<typeof StringWithAggregatesFilter>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    accountName?: InstanceType<typeof StringWithAggregatesFilter>;
    @HideField()
    walletId?: InstanceType<typeof StringWithAggregatesFilter>;
}

@InputType()
export class OtpScalarWhereInput {
    @Field(() => [OtpScalarWhereInput], {nullable:true})
    AND?: Array<OtpScalarWhereInput>;
    @Field(() => [OtpScalarWhereInput], {nullable:true})
    OR?: Array<OtpScalarWhereInput>;
    @Field(() => [OtpScalarWhereInput], {nullable:true})
    NOT?: Array<OtpScalarWhereInput>;
    @Field(() => StringFilter, {nullable:true})
    id?: InstanceType<typeof StringFilter>;
    @HideField()
    createdAt?: InstanceType<typeof DateTimeFilter>;
    @HideField()
    updatedAt?: InstanceType<typeof DateTimeFilter>;
    @Field(() => EnumOtpAlgorithmFilter, {nullable:true})
    algorithm?: InstanceType<typeof EnumOtpAlgorithmFilter>;
    @Field(() => StringFilter, {nullable:true})
    secret?: InstanceType<typeof StringFilter>;
    @Field(() => IntFilter, {nullable:true})
    digits?: InstanceType<typeof IntFilter>;
    @Field(() => StringFilter, {nullable:true})
    issuer?: InstanceType<typeof StringFilter>;
    @Field(() => StringFilter, {nullable:true})
    accountName?: InstanceType<typeof StringFilter>;
    @HideField()
    walletId?: InstanceType<typeof StringFilter>;
}

@InputType()
export class OtpSumAggregateInput {
    @Field(() => Boolean, {nullable:true})
    digits?: true;
}

@ObjectType()
export class OtpSumAggregate {
    @Field(() => Int, {nullable:true})
    digits?: number;
}

@InputType()
export class OtpSumOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    digits?: keyof typeof SortOrder;
}

@InputType()
export class OtpUncheckedCreateNestedManyWithoutWalletInput {
    @Field(() => [OtpCreateWithoutWalletInput], {nullable:true})
    @Type(() => OtpCreateWithoutWalletInput)
    create?: Array<OtpCreateWithoutWalletInput>;
    @Field(() => [OtpCreateOrConnectWithoutWalletInput], {nullable:true})
    @Type(() => OtpCreateOrConnectWithoutWalletInput)
    connectOrCreate?: Array<OtpCreateOrConnectWithoutWalletInput>;
    @Field(() => OtpCreateManyWalletInputEnvelope, {nullable:true})
    @Type(() => OtpCreateManyWalletInputEnvelope)
    createMany?: InstanceType<typeof OtpCreateManyWalletInputEnvelope>;
    @Field(() => [OtpWhereUniqueInput], {nullable:true})
    @Type(() => OtpWhereUniqueInput)
    connect?: Array<OtpWhereUniqueInput>;
}

@InputType()
export class OtpUncheckedCreateWithoutWalletInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => OtpAlgorithm, {nullable:false})
    algorithm!: keyof typeof OtpAlgorithm;
    @Field(() => String, {nullable:false})
    secret!: string;
    @Field(() => Int, {nullable:false})
    digits!: number;
    @Field(() => String, {nullable:false})
    issuer!: string;
    @Field(() => String, {nullable:false})
    accountName!: string;
}

@InputType()
export class OtpUncheckedCreateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => OtpAlgorithm, {nullable:false})
    algorithm!: keyof typeof OtpAlgorithm;
    @Field(() => String, {nullable:false})
    secret!: string;
    @Field(() => Int, {nullable:false})
    digits!: number;
    @Field(() => String, {nullable:false})
    issuer!: string;
    @Field(() => String, {nullable:false})
    accountName!: string;
    @HideField()
    walletId!: string;
}

@InputType()
export class OtpUncheckedUpdateManyWithoutOtpsInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => OtpAlgorithm, {nullable:true})
    algorithm?: keyof typeof OtpAlgorithm;
    @Field(() => String, {nullable:true})
    secret?: string;
    @Field(() => Int, {nullable:true})
    digits?: number;
    @Field(() => String, {nullable:true})
    issuer?: string;
    @Field(() => String, {nullable:true})
    accountName?: string;
}

@InputType()
export class OtpUncheckedUpdateManyWithoutWalletNestedInput {
    @Field(() => [OtpCreateWithoutWalletInput], {nullable:true})
    @Type(() => OtpCreateWithoutWalletInput)
    create?: Array<OtpCreateWithoutWalletInput>;
    @Field(() => [OtpCreateOrConnectWithoutWalletInput], {nullable:true})
    @Type(() => OtpCreateOrConnectWithoutWalletInput)
    connectOrCreate?: Array<OtpCreateOrConnectWithoutWalletInput>;
    @Field(() => [OtpUpsertWithWhereUniqueWithoutWalletInput], {nullable:true})
    @Type(() => OtpUpsertWithWhereUniqueWithoutWalletInput)
    upsert?: Array<OtpUpsertWithWhereUniqueWithoutWalletInput>;
    @Field(() => OtpCreateManyWalletInputEnvelope, {nullable:true})
    @Type(() => OtpCreateManyWalletInputEnvelope)
    createMany?: InstanceType<typeof OtpCreateManyWalletInputEnvelope>;
    @Field(() => [OtpWhereUniqueInput], {nullable:true})
    @Type(() => OtpWhereUniqueInput)
    set?: Array<OtpWhereUniqueInput>;
    @Field(() => [OtpWhereUniqueInput], {nullable:true})
    @Type(() => OtpWhereUniqueInput)
    disconnect?: Array<OtpWhereUniqueInput>;
    @Field(() => [OtpWhereUniqueInput], {nullable:true})
    @Type(() => OtpWhereUniqueInput)
    delete?: Array<OtpWhereUniqueInput>;
    @Field(() => [OtpWhereUniqueInput], {nullable:true})
    @Type(() => OtpWhereUniqueInput)
    connect?: Array<OtpWhereUniqueInput>;
    @Field(() => [OtpUpdateWithWhereUniqueWithoutWalletInput], {nullable:true})
    @Type(() => OtpUpdateWithWhereUniqueWithoutWalletInput)
    update?: Array<OtpUpdateWithWhereUniqueWithoutWalletInput>;
    @Field(() => [OtpUpdateManyWithWhereWithoutWalletInput], {nullable:true})
    @Type(() => OtpUpdateManyWithWhereWithoutWalletInput)
    updateMany?: Array<OtpUpdateManyWithWhereWithoutWalletInput>;
    @Field(() => [OtpScalarWhereInput], {nullable:true})
    @Type(() => OtpScalarWhereInput)
    deleteMany?: Array<OtpScalarWhereInput>;
}

@InputType()
export class OtpUncheckedUpdateManyInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => OtpAlgorithm, {nullable:true})
    algorithm?: keyof typeof OtpAlgorithm;
    @Field(() => String, {nullable:true})
    secret?: string;
    @Field(() => Int, {nullable:true})
    digits?: number;
    @Field(() => String, {nullable:true})
    issuer?: string;
    @Field(() => String, {nullable:true})
    accountName?: string;
    @HideField()
    walletId?: string;
}

@InputType()
export class OtpUncheckedUpdateWithoutWalletInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => OtpAlgorithm, {nullable:true})
    algorithm?: keyof typeof OtpAlgorithm;
    @Field(() => String, {nullable:true})
    secret?: string;
    @Field(() => Int, {nullable:true})
    digits?: number;
    @Field(() => String, {nullable:true})
    issuer?: string;
    @Field(() => String, {nullable:true})
    accountName?: string;
}

@InputType()
export class OtpUncheckedUpdateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => OtpAlgorithm, {nullable:true})
    algorithm?: keyof typeof OtpAlgorithm;
    @Field(() => String, {nullable:true})
    secret?: string;
    @Field(() => Int, {nullable:true})
    digits?: number;
    @Field(() => String, {nullable:true})
    issuer?: string;
    @Field(() => String, {nullable:true})
    accountName?: string;
    @HideField()
    walletId?: string;
}

@InputType()
export class OtpUpdateManyMutationInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => OtpAlgorithm, {nullable:true})
    algorithm?: keyof typeof OtpAlgorithm;
    @Field(() => String, {nullable:true})
    secret?: string;
    @Field(() => Int, {nullable:true})
    digits?: number;
    @Field(() => String, {nullable:true})
    issuer?: string;
    @Field(() => String, {nullable:true})
    accountName?: string;
}

@InputType()
export class OtpUpdateManyWithWhereWithoutWalletInput {
    @Field(() => OtpScalarWhereInput, {nullable:false})
    @Type(() => OtpScalarWhereInput)
    where!: InstanceType<typeof OtpScalarWhereInput>;
    @Field(() => OtpUpdateManyMutationInput, {nullable:false})
    @Type(() => OtpUpdateManyMutationInput)
    data!: InstanceType<typeof OtpUpdateManyMutationInput>;
}

@InputType()
export class OtpUpdateManyWithoutWalletNestedInput {
    @Field(() => [OtpCreateWithoutWalletInput], {nullable:true})
    @Type(() => OtpCreateWithoutWalletInput)
    create?: Array<OtpCreateWithoutWalletInput>;
    @Field(() => [OtpCreateOrConnectWithoutWalletInput], {nullable:true})
    @Type(() => OtpCreateOrConnectWithoutWalletInput)
    connectOrCreate?: Array<OtpCreateOrConnectWithoutWalletInput>;
    @Field(() => [OtpUpsertWithWhereUniqueWithoutWalletInput], {nullable:true})
    @Type(() => OtpUpsertWithWhereUniqueWithoutWalletInput)
    upsert?: Array<OtpUpsertWithWhereUniqueWithoutWalletInput>;
    @Field(() => OtpCreateManyWalletInputEnvelope, {nullable:true})
    @Type(() => OtpCreateManyWalletInputEnvelope)
    createMany?: InstanceType<typeof OtpCreateManyWalletInputEnvelope>;
    @Field(() => [OtpWhereUniqueInput], {nullable:true})
    @Type(() => OtpWhereUniqueInput)
    set?: Array<OtpWhereUniqueInput>;
    @Field(() => [OtpWhereUniqueInput], {nullable:true})
    @Type(() => OtpWhereUniqueInput)
    disconnect?: Array<OtpWhereUniqueInput>;
    @Field(() => [OtpWhereUniqueInput], {nullable:true})
    @Type(() => OtpWhereUniqueInput)
    delete?: Array<OtpWhereUniqueInput>;
    @Field(() => [OtpWhereUniqueInput], {nullable:true})
    @Type(() => OtpWhereUniqueInput)
    connect?: Array<OtpWhereUniqueInput>;
    @Field(() => [OtpUpdateWithWhereUniqueWithoutWalletInput], {nullable:true})
    @Type(() => OtpUpdateWithWhereUniqueWithoutWalletInput)
    update?: Array<OtpUpdateWithWhereUniqueWithoutWalletInput>;
    @Field(() => [OtpUpdateManyWithWhereWithoutWalletInput], {nullable:true})
    @Type(() => OtpUpdateManyWithWhereWithoutWalletInput)
    updateMany?: Array<OtpUpdateManyWithWhereWithoutWalletInput>;
    @Field(() => [OtpScalarWhereInput], {nullable:true})
    @Type(() => OtpScalarWhereInput)
    deleteMany?: Array<OtpScalarWhereInput>;
}

@InputType()
export class OtpUpdateWithWhereUniqueWithoutWalletInput {
    @Field(() => OtpWhereUniqueInput, {nullable:false})
    @Type(() => OtpWhereUniqueInput)
    where!: InstanceType<typeof OtpWhereUniqueInput>;
    @Field(() => OtpUpdateWithoutWalletInput, {nullable:false})
    @Type(() => OtpUpdateWithoutWalletInput)
    data!: InstanceType<typeof OtpUpdateWithoutWalletInput>;
}

@InputType()
export class OtpUpdateWithoutWalletInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => OtpAlgorithm, {nullable:true})
    algorithm?: keyof typeof OtpAlgorithm;
    @Field(() => String, {nullable:true})
    secret?: string;
    @Field(() => Int, {nullable:true})
    digits?: number;
    @Field(() => String, {nullable:true})
    issuer?: string;
    @Field(() => String, {nullable:true})
    accountName?: string;
}

@InputType()
export class OtpUpdateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => OtpAlgorithm, {nullable:true})
    algorithm?: keyof typeof OtpAlgorithm;
    @Field(() => String, {nullable:true})
    secret?: string;
    @Field(() => Int, {nullable:true})
    digits?: number;
    @Field(() => String, {nullable:true})
    issuer?: string;
    @Field(() => String, {nullable:true})
    accountName?: string;
    @HideField()
    wallet?: InstanceType<typeof WalletUpdateOneRequiredWithoutOtpsNestedInput>;
}

@InputType()
export class OtpUpsertWithWhereUniqueWithoutWalletInput {
    @Field(() => OtpWhereUniqueInput, {nullable:false})
    @Type(() => OtpWhereUniqueInput)
    where!: InstanceType<typeof OtpWhereUniqueInput>;
    @Field(() => OtpUpdateWithoutWalletInput, {nullable:false})
    @Type(() => OtpUpdateWithoutWalletInput)
    update!: InstanceType<typeof OtpUpdateWithoutWalletInput>;
    @Field(() => OtpCreateWithoutWalletInput, {nullable:false})
    @Type(() => OtpCreateWithoutWalletInput)
    create!: InstanceType<typeof OtpCreateWithoutWalletInput>;
}

@InputType()
export class OtpWhereUniqueInput {
    @Field(() => String, {nullable:true})
    id?: string;
}

@InputType()
export class OtpWhereInput {
    @Field(() => [OtpWhereInput], {nullable:true})
    AND?: Array<OtpWhereInput>;
    @Field(() => [OtpWhereInput], {nullable:true})
    OR?: Array<OtpWhereInput>;
    @Field(() => [OtpWhereInput], {nullable:true})
    NOT?: Array<OtpWhereInput>;
    @Field(() => StringFilter, {nullable:true})
    id?: InstanceType<typeof StringFilter>;
    @HideField()
    createdAt?: InstanceType<typeof DateTimeFilter>;
    @HideField()
    updatedAt?: InstanceType<typeof DateTimeFilter>;
    @Field(() => EnumOtpAlgorithmFilter, {nullable:true})
    algorithm?: InstanceType<typeof EnumOtpAlgorithmFilter>;
    @Field(() => StringFilter, {nullable:true})
    secret?: InstanceType<typeof StringFilter>;
    @Field(() => IntFilter, {nullable:true})
    digits?: InstanceType<typeof IntFilter>;
    @Field(() => StringFilter, {nullable:true})
    issuer?: InstanceType<typeof StringFilter>;
    @Field(() => StringFilter, {nullable:true})
    accountName?: InstanceType<typeof StringFilter>;
    @HideField()
    walletId?: InstanceType<typeof StringFilter>;
    @HideField()
    wallet?: InstanceType<typeof WalletRelationFilter>;
}

@ObjectType()
export class Otp {
    @Field(() => ID, {nullable:false})
    id!: string;
    @Field(() => Date, {nullable:false})
    createdAt!: Date;
    @Field(() => Date, {nullable:false})
    updatedAt!: Date;
    @Field(() => OtpAlgorithm, {nullable:false})
    algorithm!: keyof typeof OtpAlgorithm;
    @Field(() => String, {nullable:false})
    secret!: string;
    @Field(() => Int, {nullable:false})
    digits!: number;
    @Field(() => String, {nullable:false})
    issuer!: string;
    @Field(() => String, {nullable:false})
    accountName!: string;
    @Field(() => String, {nullable:false})
    walletId!: string;
    @HideField()
    wallet?: InstanceType<typeof Wallet>;
}

@ArgsType()
export class UpdateManyOtpArgs {
    @Field(() => OtpUpdateManyMutationInput, {nullable:false})
    @Type(() => OtpUpdateManyMutationInput)
    data!: InstanceType<typeof OtpUpdateManyMutationInput>;
    @Field(() => OtpWhereInput, {nullable:true})
    @Type(() => OtpWhereInput)
    where?: InstanceType<typeof OtpWhereInput>;
}

@ArgsType()
export class UpdateOneOtpArgs {
    @Field(() => OtpUpdateInput, {nullable:false})
    @Type(() => OtpUpdateInput)
    data!: InstanceType<typeof OtpUpdateInput>;
    @Field(() => OtpWhereUniqueInput, {nullable:false})
    @Type(() => OtpWhereUniqueInput)
    where!: InstanceType<typeof OtpWhereUniqueInput>;
}

@ArgsType()
export class UpsertOneOtpArgs {
    @Field(() => OtpWhereUniqueInput, {nullable:false})
    @Type(() => OtpWhereUniqueInput)
    where!: InstanceType<typeof OtpWhereUniqueInput>;
    @Field(() => OtpCreateInput, {nullable:false})
    @Type(() => OtpCreateInput)
    create!: InstanceType<typeof OtpCreateInput>;
    @Field(() => OtpUpdateInput, {nullable:false})
    @Type(() => OtpUpdateInput)
    update!: InstanceType<typeof OtpUpdateInput>;
}

@ObjectType()
export class AffectedRows {
    @Field(() => Int, {nullable:false})
    count!: number;
}

@InputType()
export class DateTimeFilter {
    @Field(() => Date, {nullable:true})
    equals?: Date | string;
    @Field(() => [Date], {nullable:true})
    in?: Array<Date> | Array<string>;
    @Field(() => [Date], {nullable:true})
    notIn?: Array<Date> | Array<string>;
    @Field(() => Date, {nullable:true})
    lt?: Date | string;
    @Field(() => Date, {nullable:true})
    lte?: Date | string;
    @Field(() => Date, {nullable:true})
    gt?: Date | string;
    @Field(() => Date, {nullable:true})
    gte?: Date | string;
    @Field(() => NestedDateTimeFilter, {nullable:true})
    not?: InstanceType<typeof NestedDateTimeFilter>;
}

@InputType()
export class DateTimeWithAggregatesFilter {
    @Field(() => Date, {nullable:true})
    equals?: Date | string;
    @Field(() => [Date], {nullable:true})
    in?: Array<Date> | Array<string>;
    @Field(() => [Date], {nullable:true})
    notIn?: Array<Date> | Array<string>;
    @Field(() => Date, {nullable:true})
    lt?: Date | string;
    @Field(() => Date, {nullable:true})
    lte?: Date | string;
    @Field(() => Date, {nullable:true})
    gt?: Date | string;
    @Field(() => Date, {nullable:true})
    gte?: Date | string;
    @Field(() => NestedDateTimeWithAggregatesFilter, {nullable:true})
    not?: InstanceType<typeof NestedDateTimeWithAggregatesFilter>;
    @Field(() => NestedIntFilter, {nullable:true})
    _count?: InstanceType<typeof NestedIntFilter>;
    @Field(() => NestedDateTimeFilter, {nullable:true})
    _min?: InstanceType<typeof NestedDateTimeFilter>;
    @Field(() => NestedDateTimeFilter, {nullable:true})
    _max?: InstanceType<typeof NestedDateTimeFilter>;
}

@InputType()
export class EnumOtpAlgorithmFilter {
    @Field(() => OtpAlgorithm, {nullable:true})
    equals?: keyof typeof OtpAlgorithm;
    @Field(() => [OtpAlgorithm], {nullable:true})
    in?: Array<keyof typeof OtpAlgorithm>;
    @Field(() => [OtpAlgorithm], {nullable:true})
    notIn?: Array<keyof typeof OtpAlgorithm>;
    @Field(() => NestedEnumOtpAlgorithmFilter, {nullable:true})
    not?: InstanceType<typeof NestedEnumOtpAlgorithmFilter>;
}

@InputType()
export class EnumOtpAlgorithmWithAggregatesFilter {
    @Field(() => OtpAlgorithm, {nullable:true})
    equals?: keyof typeof OtpAlgorithm;
    @Field(() => [OtpAlgorithm], {nullable:true})
    in?: Array<keyof typeof OtpAlgorithm>;
    @Field(() => [OtpAlgorithm], {nullable:true})
    notIn?: Array<keyof typeof OtpAlgorithm>;
    @Field(() => NestedEnumOtpAlgorithmWithAggregatesFilter, {nullable:true})
    not?: InstanceType<typeof NestedEnumOtpAlgorithmWithAggregatesFilter>;
    @Field(() => NestedIntFilter, {nullable:true})
    _count?: InstanceType<typeof NestedIntFilter>;
    @Field(() => NestedEnumOtpAlgorithmFilter, {nullable:true})
    _min?: InstanceType<typeof NestedEnumOtpAlgorithmFilter>;
    @Field(() => NestedEnumOtpAlgorithmFilter, {nullable:true})
    _max?: InstanceType<typeof NestedEnumOtpAlgorithmFilter>;
}

@InputType()
export class EnumProofExchangeStateFilter {
    @Field(() => ProofExchangeState, {nullable:true})
    equals?: keyof typeof ProofExchangeState;
    @Field(() => [ProofExchangeState], {nullable:true})
    in?: Array<keyof typeof ProofExchangeState>;
    @Field(() => [ProofExchangeState], {nullable:true})
    notIn?: Array<keyof typeof ProofExchangeState>;
    @Field(() => NestedEnumProofExchangeStateFilter, {nullable:true})
    not?: InstanceType<typeof NestedEnumProofExchangeStateFilter>;
}

@InputType()
export class EnumProofExchangeStateWithAggregatesFilter {
    @Field(() => ProofExchangeState, {nullable:true})
    equals?: keyof typeof ProofExchangeState;
    @Field(() => [ProofExchangeState], {nullable:true})
    in?: Array<keyof typeof ProofExchangeState>;
    @Field(() => [ProofExchangeState], {nullable:true})
    notIn?: Array<keyof typeof ProofExchangeState>;
    @Field(() => NestedEnumProofExchangeStateWithAggregatesFilter, {nullable:true})
    not?: InstanceType<typeof NestedEnumProofExchangeStateWithAggregatesFilter>;
    @Field(() => NestedIntFilter, {nullable:true})
    _count?: InstanceType<typeof NestedIntFilter>;
    @Field(() => NestedEnumProofExchangeStateFilter, {nullable:true})
    _min?: InstanceType<typeof NestedEnumProofExchangeStateFilter>;
    @Field(() => NestedEnumProofExchangeStateFilter, {nullable:true})
    _max?: InstanceType<typeof NestedEnumProofExchangeStateFilter>;
}

@InputType()
export class EnumWalletRoleFilter {
    @Field(() => WalletRole, {nullable:true})
    equals?: keyof typeof WalletRole;
    @Field(() => [WalletRole], {nullable:true})
    in?: Array<keyof typeof WalletRole>;
    @Field(() => [WalletRole], {nullable:true})
    notIn?: Array<keyof typeof WalletRole>;
    @Field(() => NestedEnumWalletRoleFilter, {nullable:true})
    not?: InstanceType<typeof NestedEnumWalletRoleFilter>;
}

@InputType()
export class EnumWalletRoleWithAggregatesFilter {
    @Field(() => WalletRole, {nullable:true})
    equals?: keyof typeof WalletRole;
    @Field(() => [WalletRole], {nullable:true})
    in?: Array<keyof typeof WalletRole>;
    @Field(() => [WalletRole], {nullable:true})
    notIn?: Array<keyof typeof WalletRole>;
    @Field(() => NestedEnumWalletRoleWithAggregatesFilter, {nullable:true})
    not?: InstanceType<typeof NestedEnumWalletRoleWithAggregatesFilter>;
    @Field(() => NestedIntFilter, {nullable:true})
    _count?: InstanceType<typeof NestedIntFilter>;
    @Field(() => NestedEnumWalletRoleFilter, {nullable:true})
    _min?: InstanceType<typeof NestedEnumWalletRoleFilter>;
    @Field(() => NestedEnumWalletRoleFilter, {nullable:true})
    _max?: InstanceType<typeof NestedEnumWalletRoleFilter>;
}

@InputType()
export class IntFilter {
    @Field(() => Int, {nullable:true})
    equals?: number;
    @Field(() => [Int], {nullable:true})
    in?: Array<number>;
    @Field(() => [Int], {nullable:true})
    notIn?: Array<number>;
    @Field(() => Int, {nullable:true})
    lt?: number;
    @Field(() => Int, {nullable:true})
    lte?: number;
    @Field(() => Int, {nullable:true})
    gt?: number;
    @Field(() => Int, {nullable:true})
    gte?: number;
    @Field(() => NestedIntFilter, {nullable:true})
    not?: InstanceType<typeof NestedIntFilter>;
}

@InputType()
export class IntWithAggregatesFilter {
    @Field(() => Int, {nullable:true})
    equals?: number;
    @Field(() => [Int], {nullable:true})
    in?: Array<number>;
    @Field(() => [Int], {nullable:true})
    notIn?: Array<number>;
    @Field(() => Int, {nullable:true})
    lt?: number;
    @Field(() => Int, {nullable:true})
    lte?: number;
    @Field(() => Int, {nullable:true})
    gt?: number;
    @Field(() => Int, {nullable:true})
    gte?: number;
    @Field(() => NestedIntWithAggregatesFilter, {nullable:true})
    not?: InstanceType<typeof NestedIntWithAggregatesFilter>;
    @Field(() => NestedIntFilter, {nullable:true})
    _count?: InstanceType<typeof NestedIntFilter>;
    @Field(() => NestedFloatFilter, {nullable:true})
    _avg?: InstanceType<typeof NestedFloatFilter>;
    @Field(() => NestedIntFilter, {nullable:true})
    _sum?: InstanceType<typeof NestedIntFilter>;
    @Field(() => NestedIntFilter, {nullable:true})
    _min?: InstanceType<typeof NestedIntFilter>;
    @Field(() => NestedIntFilter, {nullable:true})
    _max?: InstanceType<typeof NestedIntFilter>;
}

@InputType()
export class JsonFilter {
    @Field(() => GraphQLJSON, {nullable:true})
    equals?: any;
    @Field(() => [String], {nullable:true})
    path?: Array<string>;
    @Field(() => String, {nullable:true})
    string_contains?: string;
    @Field(() => String, {nullable:true})
    string_starts_with?: string;
    @Field(() => String, {nullable:true})
    string_ends_with?: string;
    @Field(() => GraphQLJSON, {nullable:true})
    array_contains?: any;
    @Field(() => GraphQLJSON, {nullable:true})
    array_starts_with?: any;
    @Field(() => GraphQLJSON, {nullable:true})
    array_ends_with?: any;
    @Field(() => GraphQLJSON, {nullable:true})
    lt?: any;
    @Field(() => GraphQLJSON, {nullable:true})
    lte?: any;
    @Field(() => GraphQLJSON, {nullable:true})
    gt?: any;
    @Field(() => GraphQLJSON, {nullable:true})
    gte?: any;
    @Field(() => GraphQLJSON, {nullable:true})
    not?: any;
}

@InputType()
export class JsonWithAggregatesFilter {
    @Field(() => GraphQLJSON, {nullable:true})
    equals?: any;
    @Field(() => [String], {nullable:true})
    path?: Array<string>;
    @Field(() => String, {nullable:true})
    string_contains?: string;
    @Field(() => String, {nullable:true})
    string_starts_with?: string;
    @Field(() => String, {nullable:true})
    string_ends_with?: string;
    @Field(() => GraphQLJSON, {nullable:true})
    array_contains?: any;
    @Field(() => GraphQLJSON, {nullable:true})
    array_starts_with?: any;
    @Field(() => GraphQLJSON, {nullable:true})
    array_ends_with?: any;
    @Field(() => GraphQLJSON, {nullable:true})
    lt?: any;
    @Field(() => GraphQLJSON, {nullable:true})
    lte?: any;
    @Field(() => GraphQLJSON, {nullable:true})
    gt?: any;
    @Field(() => GraphQLJSON, {nullable:true})
    gte?: any;
    @Field(() => GraphQLJSON, {nullable:true})
    not?: any;
    @Field(() => NestedIntFilter, {nullable:true})
    _count?: InstanceType<typeof NestedIntFilter>;
    @Field(() => NestedJsonFilter, {nullable:true})
    _min?: InstanceType<typeof NestedJsonFilter>;
    @Field(() => NestedJsonFilter, {nullable:true})
    _max?: InstanceType<typeof NestedJsonFilter>;
}

@InputType()
export class NestedDateTimeFilter {
    @Field(() => Date, {nullable:true})
    equals?: Date | string;
    @Field(() => [Date], {nullable:true})
    in?: Array<Date> | Array<string>;
    @Field(() => [Date], {nullable:true})
    notIn?: Array<Date> | Array<string>;
    @Field(() => Date, {nullable:true})
    lt?: Date | string;
    @Field(() => Date, {nullable:true})
    lte?: Date | string;
    @Field(() => Date, {nullable:true})
    gt?: Date | string;
    @Field(() => Date, {nullable:true})
    gte?: Date | string;
    @Field(() => NestedDateTimeFilter, {nullable:true})
    not?: InstanceType<typeof NestedDateTimeFilter>;
}

@InputType()
export class NestedDateTimeWithAggregatesFilter {
    @Field(() => Date, {nullable:true})
    equals?: Date | string;
    @Field(() => [Date], {nullable:true})
    in?: Array<Date> | Array<string>;
    @Field(() => [Date], {nullable:true})
    notIn?: Array<Date> | Array<string>;
    @Field(() => Date, {nullable:true})
    lt?: Date | string;
    @Field(() => Date, {nullable:true})
    lte?: Date | string;
    @Field(() => Date, {nullable:true})
    gt?: Date | string;
    @Field(() => Date, {nullable:true})
    gte?: Date | string;
    @Field(() => NestedDateTimeWithAggregatesFilter, {nullable:true})
    not?: InstanceType<typeof NestedDateTimeWithAggregatesFilter>;
    @Field(() => NestedIntFilter, {nullable:true})
    _count?: InstanceType<typeof NestedIntFilter>;
    @Field(() => NestedDateTimeFilter, {nullable:true})
    _min?: InstanceType<typeof NestedDateTimeFilter>;
    @Field(() => NestedDateTimeFilter, {nullable:true})
    _max?: InstanceType<typeof NestedDateTimeFilter>;
}

@InputType()
export class NestedEnumOtpAlgorithmFilter {
    @Field(() => OtpAlgorithm, {nullable:true})
    equals?: keyof typeof OtpAlgorithm;
    @Field(() => [OtpAlgorithm], {nullable:true})
    in?: Array<keyof typeof OtpAlgorithm>;
    @Field(() => [OtpAlgorithm], {nullable:true})
    notIn?: Array<keyof typeof OtpAlgorithm>;
    @Field(() => NestedEnumOtpAlgorithmFilter, {nullable:true})
    not?: InstanceType<typeof NestedEnumOtpAlgorithmFilter>;
}

@InputType()
export class NestedEnumOtpAlgorithmWithAggregatesFilter {
    @Field(() => OtpAlgorithm, {nullable:true})
    equals?: keyof typeof OtpAlgorithm;
    @Field(() => [OtpAlgorithm], {nullable:true})
    in?: Array<keyof typeof OtpAlgorithm>;
    @Field(() => [OtpAlgorithm], {nullable:true})
    notIn?: Array<keyof typeof OtpAlgorithm>;
    @Field(() => NestedEnumOtpAlgorithmWithAggregatesFilter, {nullable:true})
    not?: InstanceType<typeof NestedEnumOtpAlgorithmWithAggregatesFilter>;
    @Field(() => NestedIntFilter, {nullable:true})
    _count?: InstanceType<typeof NestedIntFilter>;
    @Field(() => NestedEnumOtpAlgorithmFilter, {nullable:true})
    _min?: InstanceType<typeof NestedEnumOtpAlgorithmFilter>;
    @Field(() => NestedEnumOtpAlgorithmFilter, {nullable:true})
    _max?: InstanceType<typeof NestedEnumOtpAlgorithmFilter>;
}

@InputType()
export class NestedEnumProofExchangeStateWithAggregatesFilter {
    @Field(() => ProofExchangeState, {nullable:true})
    equals?: keyof typeof ProofExchangeState;
    @Field(() => [ProofExchangeState], {nullable:true})
    in?: Array<keyof typeof ProofExchangeState>;
    @Field(() => [ProofExchangeState], {nullable:true})
    notIn?: Array<keyof typeof ProofExchangeState>;
    @Field(() => NestedEnumProofExchangeStateWithAggregatesFilter, {nullable:true})
    not?: InstanceType<typeof NestedEnumProofExchangeStateWithAggregatesFilter>;
    @Field(() => NestedIntFilter, {nullable:true})
    _count?: InstanceType<typeof NestedIntFilter>;
    @Field(() => NestedEnumProofExchangeStateFilter, {nullable:true})
    _min?: InstanceType<typeof NestedEnumProofExchangeStateFilter>;
    @Field(() => NestedEnumProofExchangeStateFilter, {nullable:true})
    _max?: InstanceType<typeof NestedEnumProofExchangeStateFilter>;
}

@InputType()
export class NestedEnumWalletRoleFilter {
    @Field(() => WalletRole, {nullable:true})
    equals?: keyof typeof WalletRole;
    @Field(() => [WalletRole], {nullable:true})
    in?: Array<keyof typeof WalletRole>;
    @Field(() => [WalletRole], {nullable:true})
    notIn?: Array<keyof typeof WalletRole>;
    @Field(() => NestedEnumWalletRoleFilter, {nullable:true})
    not?: InstanceType<typeof NestedEnumWalletRoleFilter>;
}

@InputType()
export class NestedEnumWalletRoleWithAggregatesFilter {
    @Field(() => WalletRole, {nullable:true})
    equals?: keyof typeof WalletRole;
    @Field(() => [WalletRole], {nullable:true})
    in?: Array<keyof typeof WalletRole>;
    @Field(() => [WalletRole], {nullable:true})
    notIn?: Array<keyof typeof WalletRole>;
    @Field(() => NestedEnumWalletRoleWithAggregatesFilter, {nullable:true})
    not?: InstanceType<typeof NestedEnumWalletRoleWithAggregatesFilter>;
    @Field(() => NestedIntFilter, {nullable:true})
    _count?: InstanceType<typeof NestedIntFilter>;
    @Field(() => NestedEnumWalletRoleFilter, {nullable:true})
    _min?: InstanceType<typeof NestedEnumWalletRoleFilter>;
    @Field(() => NestedEnumWalletRoleFilter, {nullable:true})
    _max?: InstanceType<typeof NestedEnumWalletRoleFilter>;
}

@InputType()
export class NestedFloatFilter {
    @Field(() => Float, {nullable:true})
    equals?: number;
    @Field(() => [Float], {nullable:true})
    in?: Array<number>;
    @Field(() => [Float], {nullable:true})
    notIn?: Array<number>;
    @Field(() => Float, {nullable:true})
    lt?: number;
    @Field(() => Float, {nullable:true})
    lte?: number;
    @Field(() => Float, {nullable:true})
    gt?: number;
    @Field(() => Float, {nullable:true})
    gte?: number;
    @Field(() => NestedFloatFilter, {nullable:true})
    not?: InstanceType<typeof NestedFloatFilter>;
}

@InputType()
export class NestedIntFilter {
    @Field(() => Int, {nullable:true})
    equals?: number;
    @Field(() => [Int], {nullable:true})
    in?: Array<number>;
    @Field(() => [Int], {nullable:true})
    notIn?: Array<number>;
    @Field(() => Int, {nullable:true})
    lt?: number;
    @Field(() => Int, {nullable:true})
    lte?: number;
    @Field(() => Int, {nullable:true})
    gt?: number;
    @Field(() => Int, {nullable:true})
    gte?: number;
    @Field(() => NestedIntFilter, {nullable:true})
    not?: InstanceType<typeof NestedIntFilter>;
}

@InputType()
export class NestedIntNullableFilter {
    @Field(() => Int, {nullable:true})
    equals?: number;
    @Field(() => [Int], {nullable:true})
    in?: Array<number>;
    @Field(() => [Int], {nullable:true})
    notIn?: Array<number>;
    @Field(() => Int, {nullable:true})
    lt?: number;
    @Field(() => Int, {nullable:true})
    lte?: number;
    @Field(() => Int, {nullable:true})
    gt?: number;
    @Field(() => Int, {nullable:true})
    gte?: number;
    @Field(() => NestedIntNullableFilter, {nullable:true})
    not?: InstanceType<typeof NestedIntNullableFilter>;
}

@InputType()
export class NestedIntWithAggregatesFilter {
    @Field(() => Int, {nullable:true})
    equals?: number;
    @Field(() => [Int], {nullable:true})
    in?: Array<number>;
    @Field(() => [Int], {nullable:true})
    notIn?: Array<number>;
    @Field(() => Int, {nullable:true})
    lt?: number;
    @Field(() => Int, {nullable:true})
    lte?: number;
    @Field(() => Int, {nullable:true})
    gt?: number;
    @Field(() => Int, {nullable:true})
    gte?: number;
    @Field(() => NestedIntWithAggregatesFilter, {nullable:true})
    not?: InstanceType<typeof NestedIntWithAggregatesFilter>;
    @Field(() => NestedIntFilter, {nullable:true})
    _count?: InstanceType<typeof NestedIntFilter>;
    @Field(() => NestedFloatFilter, {nullable:true})
    _avg?: InstanceType<typeof NestedFloatFilter>;
    @Field(() => NestedIntFilter, {nullable:true})
    _sum?: InstanceType<typeof NestedIntFilter>;
    @Field(() => NestedIntFilter, {nullable:true})
    _min?: InstanceType<typeof NestedIntFilter>;
    @Field(() => NestedIntFilter, {nullable:true})
    _max?: InstanceType<typeof NestedIntFilter>;
}

@InputType()
export class NestedJsonFilter {
    @Field(() => GraphQLJSON, {nullable:true})
    equals?: any;
    @Field(() => [String], {nullable:true})
    path?: Array<string>;
    @Field(() => String, {nullable:true})
    string_contains?: string;
    @Field(() => String, {nullable:true})
    string_starts_with?: string;
    @Field(() => String, {nullable:true})
    string_ends_with?: string;
    @Field(() => GraphQLJSON, {nullable:true})
    array_contains?: any;
    @Field(() => GraphQLJSON, {nullable:true})
    array_starts_with?: any;
    @Field(() => GraphQLJSON, {nullable:true})
    array_ends_with?: any;
    @Field(() => GraphQLJSON, {nullable:true})
    lt?: any;
    @Field(() => GraphQLJSON, {nullable:true})
    lte?: any;
    @Field(() => GraphQLJSON, {nullable:true})
    gt?: any;
    @Field(() => GraphQLJSON, {nullable:true})
    gte?: any;
    @Field(() => GraphQLJSON, {nullable:true})
    not?: any;
}

@InputType()
export class NestedStringFilter {
    @Field(() => String, {nullable:true})
    equals?: string;
    @Field(() => [String], {nullable:true})
    in?: Array<string>;
    @Field(() => [String], {nullable:true})
    notIn?: Array<string>;
    @Field(() => String, {nullable:true})
    lt?: string;
    @Field(() => String, {nullable:true})
    lte?: string;
    @Field(() => String, {nullable:true})
    gt?: string;
    @Field(() => String, {nullable:true})
    gte?: string;
    @Field(() => String, {nullable:true})
    contains?: string;
    @Field(() => String, {nullable:true})
    startsWith?: string;
    @Field(() => String, {nullable:true})
    endsWith?: string;
    @Field(() => NestedStringFilter, {nullable:true})
    not?: InstanceType<typeof NestedStringFilter>;
}

@InputType()
export class NestedStringNullableFilter {
    @Field(() => String, {nullable:true})
    equals?: string;
    @Field(() => [String], {nullable:true})
    in?: Array<string>;
    @Field(() => [String], {nullable:true})
    notIn?: Array<string>;
    @Field(() => String, {nullable:true})
    lt?: string;
    @Field(() => String, {nullable:true})
    lte?: string;
    @Field(() => String, {nullable:true})
    gt?: string;
    @Field(() => String, {nullable:true})
    gte?: string;
    @Field(() => String, {nullable:true})
    contains?: string;
    @Field(() => String, {nullable:true})
    startsWith?: string;
    @Field(() => String, {nullable:true})
    endsWith?: string;
    @Field(() => NestedStringNullableFilter, {nullable:true})
    not?: InstanceType<typeof NestedStringNullableFilter>;
}

@InputType()
export class NestedStringNullableWithAggregatesFilter {
    @Field(() => String, {nullable:true})
    equals?: string;
    @Field(() => [String], {nullable:true})
    in?: Array<string>;
    @Field(() => [String], {nullable:true})
    notIn?: Array<string>;
    @Field(() => String, {nullable:true})
    lt?: string;
    @Field(() => String, {nullable:true})
    lte?: string;
    @Field(() => String, {nullable:true})
    gt?: string;
    @Field(() => String, {nullable:true})
    gte?: string;
    @Field(() => String, {nullable:true})
    contains?: string;
    @Field(() => String, {nullable:true})
    startsWith?: string;
    @Field(() => String, {nullable:true})
    endsWith?: string;
    @Field(() => NestedStringNullableWithAggregatesFilter, {nullable:true})
    not?: InstanceType<typeof NestedStringNullableWithAggregatesFilter>;
    @Field(() => NestedIntNullableFilter, {nullable:true})
    _count?: InstanceType<typeof NestedIntNullableFilter>;
    @Field(() => NestedStringNullableFilter, {nullable:true})
    _min?: InstanceType<typeof NestedStringNullableFilter>;
    @Field(() => NestedStringNullableFilter, {nullable:true})
    _max?: InstanceType<typeof NestedStringNullableFilter>;
}

@InputType()
export class NestedStringWithAggregatesFilter {
    @Field(() => String, {nullable:true})
    equals?: string;
    @Field(() => [String], {nullable:true})
    in?: Array<string>;
    @Field(() => [String], {nullable:true})
    notIn?: Array<string>;
    @Field(() => String, {nullable:true})
    lt?: string;
    @Field(() => String, {nullable:true})
    lte?: string;
    @Field(() => String, {nullable:true})
    gt?: string;
    @Field(() => String, {nullable:true})
    gte?: string;
    @Field(() => String, {nullable:true})
    contains?: string;
    @Field(() => String, {nullable:true})
    startsWith?: string;
    @Field(() => String, {nullable:true})
    endsWith?: string;
    @Field(() => NestedStringWithAggregatesFilter, {nullable:true})
    not?: InstanceType<typeof NestedStringWithAggregatesFilter>;
    @Field(() => NestedIntFilter, {nullable:true})
    _count?: InstanceType<typeof NestedIntFilter>;
    @Field(() => NestedStringFilter, {nullable:true})
    _min?: InstanceType<typeof NestedStringFilter>;
    @Field(() => NestedStringFilter, {nullable:true})
    _max?: InstanceType<typeof NestedStringFilter>;
}

@InputType()
export class StringFilter {
    @Field(() => String, {nullable:true})
    equals?: string;
    @Field(() => [String], {nullable:true})
    in?: Array<string>;
    @Field(() => [String], {nullable:true})
    notIn?: Array<string>;
    @Field(() => String, {nullable:true})
    lt?: string;
    @Field(() => String, {nullable:true})
    lte?: string;
    @Field(() => String, {nullable:true})
    gt?: string;
    @Field(() => String, {nullable:true})
    gte?: string;
    @Field(() => String, {nullable:true})
    contains?: string;
    @Field(() => String, {nullable:true})
    startsWith?: string;
    @Field(() => String, {nullable:true})
    endsWith?: string;
    @Field(() => QueryMode, {nullable:true})
    mode?: keyof typeof QueryMode;
    @Field(() => NestedStringFilter, {nullable:true})
    not?: InstanceType<typeof NestedStringFilter>;
}

@InputType()
export class StringNullableFilter {
    @Field(() => String, {nullable:true})
    equals?: string;
    @Field(() => [String], {nullable:true})
    in?: Array<string>;
    @Field(() => [String], {nullable:true})
    notIn?: Array<string>;
    @Field(() => String, {nullable:true})
    lt?: string;
    @Field(() => String, {nullable:true})
    lte?: string;
    @Field(() => String, {nullable:true})
    gt?: string;
    @Field(() => String, {nullable:true})
    gte?: string;
    @Field(() => String, {nullable:true})
    contains?: string;
    @Field(() => String, {nullable:true})
    startsWith?: string;
    @Field(() => String, {nullable:true})
    endsWith?: string;
    @Field(() => QueryMode, {nullable:true})
    mode?: keyof typeof QueryMode;
    @Field(() => NestedStringNullableFilter, {nullable:true})
    not?: InstanceType<typeof NestedStringNullableFilter>;
}

@InputType()
export class StringNullableWithAggregatesFilter {
    @Field(() => String, {nullable:true})
    equals?: string;
    @Field(() => [String], {nullable:true})
    in?: Array<string>;
    @Field(() => [String], {nullable:true})
    notIn?: Array<string>;
    @Field(() => String, {nullable:true})
    lt?: string;
    @Field(() => String, {nullable:true})
    lte?: string;
    @Field(() => String, {nullable:true})
    gt?: string;
    @Field(() => String, {nullable:true})
    gte?: string;
    @Field(() => String, {nullable:true})
    contains?: string;
    @Field(() => String, {nullable:true})
    startsWith?: string;
    @Field(() => String, {nullable:true})
    endsWith?: string;
    @Field(() => QueryMode, {nullable:true})
    mode?: keyof typeof QueryMode;
    @Field(() => NestedStringNullableWithAggregatesFilter, {nullable:true})
    not?: InstanceType<typeof NestedStringNullableWithAggregatesFilter>;
    @Field(() => NestedIntNullableFilter, {nullable:true})
    _count?: InstanceType<typeof NestedIntNullableFilter>;
    @Field(() => NestedStringNullableFilter, {nullable:true})
    _min?: InstanceType<typeof NestedStringNullableFilter>;
    @Field(() => NestedStringNullableFilter, {nullable:true})
    _max?: InstanceType<typeof NestedStringNullableFilter>;
}

@InputType()
export class StringWithAggregatesFilter {
    @Field(() => String, {nullable:true})
    equals?: string;
    @Field(() => [String], {nullable:true})
    in?: Array<string>;
    @Field(() => [String], {nullable:true})
    notIn?: Array<string>;
    @Field(() => String, {nullable:true})
    lt?: string;
    @Field(() => String, {nullable:true})
    lte?: string;
    @Field(() => String, {nullable:true})
    gt?: string;
    @Field(() => String, {nullable:true})
    gte?: string;
    @Field(() => String, {nullable:true})
    contains?: string;
    @Field(() => String, {nullable:true})
    startsWith?: string;
    @Field(() => String, {nullable:true})
    endsWith?: string;
    @Field(() => QueryMode, {nullable:true})
    mode?: keyof typeof QueryMode;
    @Field(() => NestedStringWithAggregatesFilter, {nullable:true})
    not?: InstanceType<typeof NestedStringWithAggregatesFilter>;
    @Field(() => NestedIntFilter, {nullable:true})
    _count?: InstanceType<typeof NestedIntFilter>;
    @Field(() => NestedStringFilter, {nullable:true})
    _min?: InstanceType<typeof NestedStringFilter>;
    @Field(() => NestedStringFilter, {nullable:true})
    _max?: InstanceType<typeof NestedStringFilter>;
}

@ObjectType()
export class AggregateProofExchange {
    @Field(() => ProofExchangeCountAggregate, {nullable:true})
    _count?: InstanceType<typeof ProofExchangeCountAggregate>;
    @Field(() => ProofExchangeMinAggregate, {nullable:true})
    _min?: InstanceType<typeof ProofExchangeMinAggregate>;
    @Field(() => ProofExchangeMaxAggregate, {nullable:true})
    _max?: InstanceType<typeof ProofExchangeMaxAggregate>;
}

@ArgsType()
export class CreateManyProofExchangeArgs {
    @Field(() => [ProofExchangeCreateManyInput], {nullable:false})
    @Type(() => ProofExchangeCreateManyInput)
    data!: Array<ProofExchangeCreateManyInput>;
    @Field(() => Boolean, {nullable:true})
    skipDuplicates?: boolean;
}

@ArgsType()
export class CreateOneProofExchangeArgs {
    @Field(() => ProofExchangeCreateInput, {nullable:false})
    @Type(() => ProofExchangeCreateInput)
    data!: InstanceType<typeof ProofExchangeCreateInput>;
}

@ArgsType()
export class DeleteManyProofExchangeArgs {
    @Field(() => ProofExchangeWhereInput, {nullable:true})
    @Type(() => ProofExchangeWhereInput)
    where?: InstanceType<typeof ProofExchangeWhereInput>;
}

@ArgsType()
export class DeleteOneProofExchangeArgs {
    @Field(() => ProofExchangeWhereUniqueInput, {nullable:false})
    @Type(() => ProofExchangeWhereUniqueInput)
    where!: InstanceType<typeof ProofExchangeWhereUniqueInput>;
}

@ArgsType()
export class FindFirstProofExchangeOrThrowArgs {
    @Field(() => ProofExchangeWhereInput, {nullable:true})
    @Type(() => ProofExchangeWhereInput)
    where?: InstanceType<typeof ProofExchangeWhereInput>;
    @Field(() => [ProofExchangeOrderByWithRelationInput], {nullable:true})
    orderBy?: Array<ProofExchangeOrderByWithRelationInput>;
    @Field(() => ProofExchangeWhereUniqueInput, {nullable:true})
    cursor?: InstanceType<typeof ProofExchangeWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => [ProofExchangeScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof ProofExchangeScalarFieldEnum>;
}

@ArgsType()
export class FindFirstProofExchangeArgs {
    @Field(() => ProofExchangeWhereInput, {nullable:true})
    @Type(() => ProofExchangeWhereInput)
    where?: InstanceType<typeof ProofExchangeWhereInput>;
    @Field(() => [ProofExchangeOrderByWithRelationInput], {nullable:true})
    orderBy?: Array<ProofExchangeOrderByWithRelationInput>;
    @Field(() => ProofExchangeWhereUniqueInput, {nullable:true})
    cursor?: InstanceType<typeof ProofExchangeWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => [ProofExchangeScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof ProofExchangeScalarFieldEnum>;
}

@ArgsType()
export class FindManyProofExchangeArgs {
    @Field(() => ProofExchangeWhereInput, {nullable:true})
    @Type(() => ProofExchangeWhereInput)
    where?: InstanceType<typeof ProofExchangeWhereInput>;
    @Field(() => [ProofExchangeOrderByWithRelationInput], {nullable:true})
    orderBy?: Array<ProofExchangeOrderByWithRelationInput>;
    @Field(() => ProofExchangeWhereUniqueInput, {nullable:true})
    cursor?: InstanceType<typeof ProofExchangeWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => [ProofExchangeScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof ProofExchangeScalarFieldEnum>;
}

@ArgsType()
export class FindUniqueProofExchangeOrThrowArgs {
    @Field(() => ProofExchangeWhereUniqueInput, {nullable:false})
    @Type(() => ProofExchangeWhereUniqueInput)
    where!: InstanceType<typeof ProofExchangeWhereUniqueInput>;
}

@ArgsType()
export class FindUniqueProofExchangeArgs {
    @Field(() => ProofExchangeWhereUniqueInput, {nullable:false})
    @Type(() => ProofExchangeWhereUniqueInput)
    where!: InstanceType<typeof ProofExchangeWhereUniqueInput>;
}

@InputType()
export class NestedEnumProofExchangeStateFilter {
    @Field(() => ProofExchangeState, {nullable:true})
    equals?: keyof typeof ProofExchangeState;
    @Field(() => [ProofExchangeState], {nullable:true})
    in?: Array<keyof typeof ProofExchangeState>;
    @Field(() => [ProofExchangeState], {nullable:true})
    notIn?: Array<keyof typeof ProofExchangeState>;
    @Field(() => NestedEnumProofExchangeStateFilter, {nullable:true})
    not?: InstanceType<typeof NestedEnumProofExchangeStateFilter>;
}

@ArgsType()
export class ProofExchangeAggregateArgs {
    @Field(() => ProofExchangeWhereInput, {nullable:true})
    @Type(() => ProofExchangeWhereInput)
    where?: InstanceType<typeof ProofExchangeWhereInput>;
    @Field(() => [ProofExchangeOrderByWithRelationInput], {nullable:true})
    orderBy?: Array<ProofExchangeOrderByWithRelationInput>;
    @Field(() => ProofExchangeWhereUniqueInput, {nullable:true})
    cursor?: InstanceType<typeof ProofExchangeWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => ProofExchangeCountAggregateInput, {nullable:true})
    _count?: InstanceType<typeof ProofExchangeCountAggregateInput>;
    @Field(() => ProofExchangeMinAggregateInput, {nullable:true})
    _min?: InstanceType<typeof ProofExchangeMinAggregateInput>;
    @Field(() => ProofExchangeMaxAggregateInput, {nullable:true})
    _max?: InstanceType<typeof ProofExchangeMaxAggregateInput>;
}

@InputType()
export class ProofExchangeCountAggregateInput {
    @Field(() => Boolean, {nullable:true})
    id?: true;
    @Field(() => Boolean, {nullable:true})
    name?: true;
    @Field(() => Boolean, {nullable:true})
    metadata?: true;
    @Field(() => Boolean, {nullable:true})
    createdAt?: true;
    @Field(() => Boolean, {nullable:true})
    attributes?: true;
    @Field(() => Boolean, {nullable:true})
    presentationAttributes?: true;
    @Field(() => Boolean, {nullable:true})
    deviceId?: true;
    @Field(() => Boolean, {nullable:true})
    verifierWalletId?: true;
    @Field(() => Boolean, {nullable:true})
    proverWalletId?: true;
    @Field(() => Boolean, {nullable:true})
    verifierConnectionId?: true;
    @Field(() => Boolean, {nullable:true})
    proverConnectionId?: true;
    @Field(() => Boolean, {nullable:true})
    verifierPresExId?: true;
    @Field(() => Boolean, {nullable:true})
    proverPresExId?: true;
    @Field(() => Boolean, {nullable:true})
    presRequest?: true;
    @Field(() => Boolean, {nullable:true})
    state?: true;
    @Field(() => Boolean, {nullable:true})
    webhookUrl?: true;
    @Field(() => Boolean, {nullable:true})
    _all?: true;
}

@ObjectType()
export class ProofExchangeCountAggregate {
    @Field(() => Int, {nullable:false})
    id!: number;
    @Field(() => Int, {nullable:false})
    name!: number;
    @Field(() => Int, {nullable:false})
    metadata!: number;
    @Field(() => Int, {nullable:false})
    createdAt!: number;
    @Field(() => Int, {nullable:false})
    attributes!: number;
    @Field(() => Int, {nullable:false})
    presentationAttributes!: number;
    @Field(() => Int, {nullable:false})
    deviceId!: number;
    @Field(() => Int, {nullable:false})
    verifierWalletId!: number;
    @Field(() => Int, {nullable:false})
    proverWalletId!: number;
    @Field(() => Int, {nullable:false})
    verifierConnectionId!: number;
    @Field(() => Int, {nullable:false})
    proverConnectionId!: number;
    @Field(() => Int, {nullable:false})
    verifierPresExId!: number;
    @Field(() => Int, {nullable:false})
    proverPresExId!: number;
    @Field(() => Int, {nullable:false})
    presRequest!: number;
    @Field(() => Int, {nullable:false})
    state!: number;
    @Field(() => Int, {nullable:false})
    webhookUrl!: number;
    @Field(() => Int, {nullable:false})
    _all!: number;
}

@InputType()
export class ProofExchangeCountOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    name?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    metadata?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    createdAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    attributes?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    presentationAttributes?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    deviceId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    verifierWalletId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    proverWalletId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    verifierConnectionId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    proverConnectionId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    verifierPresExId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    proverPresExId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    presRequest?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    state?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    webhookUrl?: keyof typeof SortOrder;
}

@InputType()
export class ProofExchangeCreateManyInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => String, {nullable:false})
    name!: string;
    @Field(() => GraphQLJSON, {nullable:true})
    metadata?: any;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => GraphQLJSON, {nullable:true})
    attributes?: any;
    @Field(() => GraphQLJSON, {nullable:true})
    presentationAttributes?: any;
    @Field(() => String, {nullable:false})
    deviceId!: string;
    @Field(() => String, {nullable:false})
    verifierWalletId!: string;
    @Field(() => String, {nullable:true})
    proverWalletId?: string;
    @Field(() => String, {nullable:true})
    verifierConnectionId?: string;
    @Field(() => String, {nullable:true})
    proverConnectionId?: string;
    @Field(() => String, {nullable:true})
    verifierPresExId?: string;
    @Field(() => String, {nullable:true})
    proverPresExId?: string;
    @Field(() => GraphQLJSON, {nullable:false})
    presRequest!: any;
    @Field(() => ProofExchangeState, {nullable:true})
    state?: keyof typeof ProofExchangeState;
    @Field(() => String, {nullable:false})
    webhookUrl!: string;
}

@InputType()
export class ProofExchangeCreateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => String, {nullable:false})
    name!: string;
    @Field(() => GraphQLJSON, {nullable:true})
    metadata?: any;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => GraphQLJSON, {nullable:true})
    attributes?: any;
    @Field(() => GraphQLJSON, {nullable:true})
    presentationAttributes?: any;
    @Field(() => String, {nullable:false})
    deviceId!: string;
    @Field(() => String, {nullable:false})
    verifierWalletId!: string;
    @Field(() => String, {nullable:true})
    proverWalletId?: string;
    @Field(() => String, {nullable:true})
    verifierConnectionId?: string;
    @Field(() => String, {nullable:true})
    proverConnectionId?: string;
    @Field(() => String, {nullable:true})
    verifierPresExId?: string;
    @Field(() => String, {nullable:true})
    proverPresExId?: string;
    @Field(() => GraphQLJSON, {nullable:false})
    presRequest!: any;
    @Field(() => ProofExchangeState, {nullable:true})
    state?: keyof typeof ProofExchangeState;
    @Field(() => String, {nullable:false})
    webhookUrl!: string;
}

@ArgsType()
export class ProofExchangeGroupByArgs {
    @Field(() => ProofExchangeWhereInput, {nullable:true})
    @Type(() => ProofExchangeWhereInput)
    where?: InstanceType<typeof ProofExchangeWhereInput>;
    @Field(() => [ProofExchangeOrderByWithAggregationInput], {nullable:true})
    orderBy?: Array<ProofExchangeOrderByWithAggregationInput>;
    @Field(() => [ProofExchangeScalarFieldEnum], {nullable:false})
    by!: Array<keyof typeof ProofExchangeScalarFieldEnum>;
    @Field(() => ProofExchangeScalarWhereWithAggregatesInput, {nullable:true})
    having?: InstanceType<typeof ProofExchangeScalarWhereWithAggregatesInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => ProofExchangeCountAggregateInput, {nullable:true})
    _count?: InstanceType<typeof ProofExchangeCountAggregateInput>;
    @Field(() => ProofExchangeMinAggregateInput, {nullable:true})
    _min?: InstanceType<typeof ProofExchangeMinAggregateInput>;
    @Field(() => ProofExchangeMaxAggregateInput, {nullable:true})
    _max?: InstanceType<typeof ProofExchangeMaxAggregateInput>;
}

@ObjectType()
export class ProofExchangeGroupBy {
    @Field(() => String, {nullable:false})
    id!: string;
    @Field(() => String, {nullable:false})
    name!: string;
    @Field(() => GraphQLJSON, {nullable:false})
    metadata!: any;
    @Field(() => Date, {nullable:false})
    createdAt!: Date | string;
    @Field(() => GraphQLJSON, {nullable:false})
    attributes!: any;
    @Field(() => GraphQLJSON, {nullable:false})
    presentationAttributes!: any;
    @Field(() => String, {nullable:false})
    deviceId!: string;
    @Field(() => String, {nullable:false})
    verifierWalletId!: string;
    @Field(() => String, {nullable:true})
    proverWalletId?: string;
    @Field(() => String, {nullable:true})
    verifierConnectionId?: string;
    @Field(() => String, {nullable:true})
    proverConnectionId?: string;
    @Field(() => String, {nullable:true})
    verifierPresExId?: string;
    @Field(() => String, {nullable:true})
    proverPresExId?: string;
    @Field(() => GraphQLJSON, {nullable:false})
    presRequest!: any;
    @Field(() => ProofExchangeState, {nullable:false})
    state!: keyof typeof ProofExchangeState;
    @Field(() => String, {nullable:false})
    webhookUrl!: string;
    @Field(() => ProofExchangeCountAggregate, {nullable:true})
    _count?: InstanceType<typeof ProofExchangeCountAggregate>;
    @Field(() => ProofExchangeMinAggregate, {nullable:true})
    _min?: InstanceType<typeof ProofExchangeMinAggregate>;
    @Field(() => ProofExchangeMaxAggregate, {nullable:true})
    _max?: InstanceType<typeof ProofExchangeMaxAggregate>;
}

@InputType()
export class ProofExchangeMaxAggregateInput {
    @Field(() => Boolean, {nullable:true})
    id?: true;
    @Field(() => Boolean, {nullable:true})
    name?: true;
    @Field(() => Boolean, {nullable:true})
    createdAt?: true;
    @Field(() => Boolean, {nullable:true})
    deviceId?: true;
    @Field(() => Boolean, {nullable:true})
    verifierWalletId?: true;
    @Field(() => Boolean, {nullable:true})
    proverWalletId?: true;
    @Field(() => Boolean, {nullable:true})
    verifierConnectionId?: true;
    @Field(() => Boolean, {nullable:true})
    proverConnectionId?: true;
    @Field(() => Boolean, {nullable:true})
    verifierPresExId?: true;
    @Field(() => Boolean, {nullable:true})
    proverPresExId?: true;
    @Field(() => Boolean, {nullable:true})
    state?: true;
    @Field(() => Boolean, {nullable:true})
    webhookUrl?: true;
}

@ObjectType()
export class ProofExchangeMaxAggregate {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => String, {nullable:true})
    name?: string;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => String, {nullable:true})
    deviceId?: string;
    @Field(() => String, {nullable:true})
    verifierWalletId?: string;
    @Field(() => String, {nullable:true})
    proverWalletId?: string;
    @Field(() => String, {nullable:true})
    verifierConnectionId?: string;
    @Field(() => String, {nullable:true})
    proverConnectionId?: string;
    @Field(() => String, {nullable:true})
    verifierPresExId?: string;
    @Field(() => String, {nullable:true})
    proverPresExId?: string;
    @Field(() => ProofExchangeState, {nullable:true})
    state?: keyof typeof ProofExchangeState;
    @Field(() => String, {nullable:true})
    webhookUrl?: string;
}

@InputType()
export class ProofExchangeMaxOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    name?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    createdAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    deviceId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    verifierWalletId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    proverWalletId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    verifierConnectionId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    proverConnectionId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    verifierPresExId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    proverPresExId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    state?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    webhookUrl?: keyof typeof SortOrder;
}

@InputType()
export class ProofExchangeMinAggregateInput {
    @Field(() => Boolean, {nullable:true})
    id?: true;
    @Field(() => Boolean, {nullable:true})
    name?: true;
    @Field(() => Boolean, {nullable:true})
    createdAt?: true;
    @Field(() => Boolean, {nullable:true})
    deviceId?: true;
    @Field(() => Boolean, {nullable:true})
    verifierWalletId?: true;
    @Field(() => Boolean, {nullable:true})
    proverWalletId?: true;
    @Field(() => Boolean, {nullable:true})
    verifierConnectionId?: true;
    @Field(() => Boolean, {nullable:true})
    proverConnectionId?: true;
    @Field(() => Boolean, {nullable:true})
    verifierPresExId?: true;
    @Field(() => Boolean, {nullable:true})
    proverPresExId?: true;
    @Field(() => Boolean, {nullable:true})
    state?: true;
    @Field(() => Boolean, {nullable:true})
    webhookUrl?: true;
}

@ObjectType()
export class ProofExchangeMinAggregate {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => String, {nullable:true})
    name?: string;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => String, {nullable:true})
    deviceId?: string;
    @Field(() => String, {nullable:true})
    verifierWalletId?: string;
    @Field(() => String, {nullable:true})
    proverWalletId?: string;
    @Field(() => String, {nullable:true})
    verifierConnectionId?: string;
    @Field(() => String, {nullable:true})
    proverConnectionId?: string;
    @Field(() => String, {nullable:true})
    verifierPresExId?: string;
    @Field(() => String, {nullable:true})
    proverPresExId?: string;
    @Field(() => ProofExchangeState, {nullable:true})
    state?: keyof typeof ProofExchangeState;
    @Field(() => String, {nullable:true})
    webhookUrl?: string;
}

@InputType()
export class ProofExchangeMinOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    name?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    createdAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    deviceId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    verifierWalletId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    proverWalletId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    verifierConnectionId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    proverConnectionId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    verifierPresExId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    proverPresExId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    state?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    webhookUrl?: keyof typeof SortOrder;
}

@InputType()
export class ProofExchangeOrderByWithAggregationInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    name?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    metadata?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    createdAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    attributes?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    presentationAttributes?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    deviceId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    verifierWalletId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    proverWalletId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    verifierConnectionId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    proverConnectionId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    verifierPresExId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    proverPresExId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    presRequest?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    state?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    webhookUrl?: keyof typeof SortOrder;
    @Field(() => ProofExchangeCountOrderByAggregateInput, {nullable:true})
    _count?: InstanceType<typeof ProofExchangeCountOrderByAggregateInput>;
    @Field(() => ProofExchangeMaxOrderByAggregateInput, {nullable:true})
    _max?: InstanceType<typeof ProofExchangeMaxOrderByAggregateInput>;
    @Field(() => ProofExchangeMinOrderByAggregateInput, {nullable:true})
    _min?: InstanceType<typeof ProofExchangeMinOrderByAggregateInput>;
}

@InputType()
export class ProofExchangeOrderByWithRelationInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    name?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    metadata?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    createdAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    attributes?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    presentationAttributes?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    deviceId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    verifierWalletId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    proverWalletId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    verifierConnectionId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    proverConnectionId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    verifierPresExId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    proverPresExId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    presRequest?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    state?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    webhookUrl?: keyof typeof SortOrder;
}

@InputType()
export class ProofExchangeScalarWhereWithAggregatesInput {
    @Field(() => [ProofExchangeScalarWhereWithAggregatesInput], {nullable:true})
    AND?: Array<ProofExchangeScalarWhereWithAggregatesInput>;
    @Field(() => [ProofExchangeScalarWhereWithAggregatesInput], {nullable:true})
    OR?: Array<ProofExchangeScalarWhereWithAggregatesInput>;
    @Field(() => [ProofExchangeScalarWhereWithAggregatesInput], {nullable:true})
    NOT?: Array<ProofExchangeScalarWhereWithAggregatesInput>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    id?: InstanceType<typeof StringWithAggregatesFilter>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    name?: InstanceType<typeof StringWithAggregatesFilter>;
    @Field(() => JsonWithAggregatesFilter, {nullable:true})
    metadata?: InstanceType<typeof JsonWithAggregatesFilter>;
    @Field(() => DateTimeWithAggregatesFilter, {nullable:true})
    createdAt?: InstanceType<typeof DateTimeWithAggregatesFilter>;
    @Field(() => JsonWithAggregatesFilter, {nullable:true})
    attributes?: InstanceType<typeof JsonWithAggregatesFilter>;
    @Field(() => JsonWithAggregatesFilter, {nullable:true})
    presentationAttributes?: InstanceType<typeof JsonWithAggregatesFilter>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    deviceId?: InstanceType<typeof StringWithAggregatesFilter>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    verifierWalletId?: InstanceType<typeof StringWithAggregatesFilter>;
    @Field(() => StringNullableWithAggregatesFilter, {nullable:true})
    proverWalletId?: InstanceType<typeof StringNullableWithAggregatesFilter>;
    @Field(() => StringNullableWithAggregatesFilter, {nullable:true})
    verifierConnectionId?: InstanceType<typeof StringNullableWithAggregatesFilter>;
    @Field(() => StringNullableWithAggregatesFilter, {nullable:true})
    proverConnectionId?: InstanceType<typeof StringNullableWithAggregatesFilter>;
    @Field(() => StringNullableWithAggregatesFilter, {nullable:true})
    verifierPresExId?: InstanceType<typeof StringNullableWithAggregatesFilter>;
    @Field(() => StringNullableWithAggregatesFilter, {nullable:true})
    proverPresExId?: InstanceType<typeof StringNullableWithAggregatesFilter>;
    @Field(() => JsonWithAggregatesFilter, {nullable:true})
    presRequest?: InstanceType<typeof JsonWithAggregatesFilter>;
    @Field(() => EnumProofExchangeStateWithAggregatesFilter, {nullable:true})
    state?: InstanceType<typeof EnumProofExchangeStateWithAggregatesFilter>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    webhookUrl?: InstanceType<typeof StringWithAggregatesFilter>;
}

@InputType()
export class ProofExchangeUncheckedCreateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => String, {nullable:false})
    name!: string;
    @Field(() => GraphQLJSON, {nullable:true})
    metadata?: any;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => GraphQLJSON, {nullable:true})
    attributes?: any;
    @Field(() => GraphQLJSON, {nullable:true})
    presentationAttributes?: any;
    @Field(() => String, {nullable:false})
    deviceId!: string;
    @Field(() => String, {nullable:false})
    verifierWalletId!: string;
    @Field(() => String, {nullable:true})
    proverWalletId?: string;
    @Field(() => String, {nullable:true})
    verifierConnectionId?: string;
    @Field(() => String, {nullable:true})
    proverConnectionId?: string;
    @Field(() => String, {nullable:true})
    verifierPresExId?: string;
    @Field(() => String, {nullable:true})
    proverPresExId?: string;
    @Field(() => GraphQLJSON, {nullable:false})
    presRequest!: any;
    @Field(() => ProofExchangeState, {nullable:true})
    state?: keyof typeof ProofExchangeState;
    @Field(() => String, {nullable:false})
    webhookUrl!: string;
}

@InputType()
export class ProofExchangeUncheckedUpdateManyInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => String, {nullable:true})
    name?: string;
    @Field(() => GraphQLJSON, {nullable:true})
    metadata?: any;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => GraphQLJSON, {nullable:true})
    attributes?: any;
    @Field(() => GraphQLJSON, {nullable:true})
    presentationAttributes?: any;
    @Field(() => String, {nullable:true})
    deviceId?: string;
    @Field(() => String, {nullable:true})
    verifierWalletId?: string;
    @Field(() => String, {nullable:true})
    proverWalletId?: string;
    @Field(() => String, {nullable:true})
    verifierConnectionId?: string;
    @Field(() => String, {nullable:true})
    proverConnectionId?: string;
    @Field(() => String, {nullable:true})
    verifierPresExId?: string;
    @Field(() => String, {nullable:true})
    proverPresExId?: string;
    @Field(() => GraphQLJSON, {nullable:true})
    presRequest?: any;
    @Field(() => ProofExchangeState, {nullable:true})
    state?: keyof typeof ProofExchangeState;
    @Field(() => String, {nullable:true})
    webhookUrl?: string;
}

@InputType()
export class ProofExchangeUncheckedUpdateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => String, {nullable:true})
    name?: string;
    @Field(() => GraphQLJSON, {nullable:true})
    metadata?: any;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => GraphQLJSON, {nullable:true})
    attributes?: any;
    @Field(() => GraphQLJSON, {nullable:true})
    presentationAttributes?: any;
    @Field(() => String, {nullable:true})
    deviceId?: string;
    @Field(() => String, {nullable:true})
    verifierWalletId?: string;
    @Field(() => String, {nullable:true})
    proverWalletId?: string;
    @Field(() => String, {nullable:true})
    verifierConnectionId?: string;
    @Field(() => String, {nullable:true})
    proverConnectionId?: string;
    @Field(() => String, {nullable:true})
    verifierPresExId?: string;
    @Field(() => String, {nullable:true})
    proverPresExId?: string;
    @Field(() => GraphQLJSON, {nullable:true})
    presRequest?: any;
    @Field(() => ProofExchangeState, {nullable:true})
    state?: keyof typeof ProofExchangeState;
    @Field(() => String, {nullable:true})
    webhookUrl?: string;
}

@InputType()
export class ProofExchangeUpdateManyMutationInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => String, {nullable:true})
    name?: string;
    @Field(() => GraphQLJSON, {nullable:true})
    metadata?: any;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => GraphQLJSON, {nullable:true})
    attributes?: any;
    @Field(() => GraphQLJSON, {nullable:true})
    presentationAttributes?: any;
    @Field(() => String, {nullable:true})
    deviceId?: string;
    @Field(() => String, {nullable:true})
    verifierWalletId?: string;
    @Field(() => String, {nullable:true})
    proverWalletId?: string;
    @Field(() => String, {nullable:true})
    verifierConnectionId?: string;
    @Field(() => String, {nullable:true})
    proverConnectionId?: string;
    @Field(() => String, {nullable:true})
    verifierPresExId?: string;
    @Field(() => String, {nullable:true})
    proverPresExId?: string;
    @Field(() => GraphQLJSON, {nullable:true})
    presRequest?: any;
    @Field(() => ProofExchangeState, {nullable:true})
    state?: keyof typeof ProofExchangeState;
    @Field(() => String, {nullable:true})
    webhookUrl?: string;
}

@InputType()
export class ProofExchangeUpdateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => String, {nullable:true})
    name?: string;
    @Field(() => GraphQLJSON, {nullable:true})
    metadata?: any;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => GraphQLJSON, {nullable:true})
    attributes?: any;
    @Field(() => GraphQLJSON, {nullable:true})
    presentationAttributes?: any;
    @Field(() => String, {nullable:true})
    deviceId?: string;
    @Field(() => String, {nullable:true})
    verifierWalletId?: string;
    @Field(() => String, {nullable:true})
    proverWalletId?: string;
    @Field(() => String, {nullable:true})
    verifierConnectionId?: string;
    @Field(() => String, {nullable:true})
    proverConnectionId?: string;
    @Field(() => String, {nullable:true})
    verifierPresExId?: string;
    @Field(() => String, {nullable:true})
    proverPresExId?: string;
    @Field(() => GraphQLJSON, {nullable:true})
    presRequest?: any;
    @Field(() => ProofExchangeState, {nullable:true})
    state?: keyof typeof ProofExchangeState;
    @Field(() => String, {nullable:true})
    webhookUrl?: string;
}

@InputType()
export class ProofExchangeWhereUniqueInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => String, {nullable:true})
    name?: string;
    @Field(() => String, {nullable:true})
    verifierPresExId?: string;
    @Field(() => String, {nullable:true})
    proverPresExId?: string;
}

@InputType()
export class ProofExchangeWhereInput {
    @Field(() => [ProofExchangeWhereInput], {nullable:true})
    AND?: Array<ProofExchangeWhereInput>;
    @Field(() => [ProofExchangeWhereInput], {nullable:true})
    OR?: Array<ProofExchangeWhereInput>;
    @Field(() => [ProofExchangeWhereInput], {nullable:true})
    NOT?: Array<ProofExchangeWhereInput>;
    @Field(() => StringFilter, {nullable:true})
    id?: InstanceType<typeof StringFilter>;
    @Field(() => StringFilter, {nullable:true})
    name?: InstanceType<typeof StringFilter>;
    @Field(() => JsonFilter, {nullable:true})
    metadata?: InstanceType<typeof JsonFilter>;
    @Field(() => DateTimeFilter, {nullable:true})
    createdAt?: InstanceType<typeof DateTimeFilter>;
    @Field(() => JsonFilter, {nullable:true})
    attributes?: InstanceType<typeof JsonFilter>;
    @Field(() => JsonFilter, {nullable:true})
    presentationAttributes?: InstanceType<typeof JsonFilter>;
    @Field(() => StringFilter, {nullable:true})
    deviceId?: InstanceType<typeof StringFilter>;
    @Field(() => StringFilter, {nullable:true})
    verifierWalletId?: InstanceType<typeof StringFilter>;
    @Field(() => StringNullableFilter, {nullable:true})
    proverWalletId?: InstanceType<typeof StringNullableFilter>;
    @Field(() => StringNullableFilter, {nullable:true})
    verifierConnectionId?: InstanceType<typeof StringNullableFilter>;
    @Field(() => StringNullableFilter, {nullable:true})
    proverConnectionId?: InstanceType<typeof StringNullableFilter>;
    @Field(() => StringNullableFilter, {nullable:true})
    verifierPresExId?: InstanceType<typeof StringNullableFilter>;
    @Field(() => StringNullableFilter, {nullable:true})
    proverPresExId?: InstanceType<typeof StringNullableFilter>;
    @Field(() => JsonFilter, {nullable:true})
    presRequest?: InstanceType<typeof JsonFilter>;
    @Field(() => EnumProofExchangeStateFilter, {nullable:true})
    state?: InstanceType<typeof EnumProofExchangeStateFilter>;
    @Field(() => StringFilter, {nullable:true})
    webhookUrl?: InstanceType<typeof StringFilter>;
}

@ObjectType()
export class ProofExchange {
    @Field(() => ID, {nullable:false})
    id!: string;
    @Field(() => String, {nullable:false})
    name!: string;
    @Field(() => GraphQLJSON, {nullable:false,defaultValue:'{}'})
    metadata!: any;
    @Field(() => Date, {nullable:false})
    createdAt!: Date;
    @Field(() => GraphQLJSON, {nullable:false,defaultValue:'[]'})
    attributes!: any;
    @Field(() => GraphQLJSON, {nullable:false,defaultValue:'[]'})
    presentationAttributes!: any;
    @Field(() => String, {nullable:false})
    deviceId!: string;
    @Field(() => String, {nullable:false})
    verifierWalletId!: string;
    @Field(() => String, {nullable:true})
    proverWalletId!: string | null;
    @Field(() => String, {nullable:true})
    verifierConnectionId!: string | null;
    @Field(() => String, {nullable:true})
    proverConnectionId!: string | null;
    @Field(() => String, {nullable:true})
    verifierPresExId!: string | null;
    @Field(() => String, {nullable:true})
    proverPresExId!: string | null;
    @Field(() => GraphQLJSON, {nullable:false})
    presRequest!: any;
    @Field(() => ProofExchangeState, {nullable:false,defaultValue:'WAITING'})
    state!: keyof typeof ProofExchangeState;
    @Field(() => String, {nullable:false})
    webhookUrl!: string;
}

@ArgsType()
export class UpdateManyProofExchangeArgs {
    @Field(() => ProofExchangeUpdateManyMutationInput, {nullable:false})
    @Type(() => ProofExchangeUpdateManyMutationInput)
    data!: InstanceType<typeof ProofExchangeUpdateManyMutationInput>;
    @Field(() => ProofExchangeWhereInput, {nullable:true})
    @Type(() => ProofExchangeWhereInput)
    where?: InstanceType<typeof ProofExchangeWhereInput>;
}

@ArgsType()
export class UpdateOneProofExchangeArgs {
    @Field(() => ProofExchangeUpdateInput, {nullable:false})
    @Type(() => ProofExchangeUpdateInput)
    data!: InstanceType<typeof ProofExchangeUpdateInput>;
    @Field(() => ProofExchangeWhereUniqueInput, {nullable:false})
    @Type(() => ProofExchangeWhereUniqueInput)
    where!: InstanceType<typeof ProofExchangeWhereUniqueInput>;
}

@ArgsType()
export class UpsertOneProofExchangeArgs {
    @Field(() => ProofExchangeWhereUniqueInput, {nullable:false})
    @Type(() => ProofExchangeWhereUniqueInput)
    where!: InstanceType<typeof ProofExchangeWhereUniqueInput>;
    @Field(() => ProofExchangeCreateInput, {nullable:false})
    @Type(() => ProofExchangeCreateInput)
    create!: InstanceType<typeof ProofExchangeCreateInput>;
    @Field(() => ProofExchangeUpdateInput, {nullable:false})
    @Type(() => ProofExchangeUpdateInput)
    update!: InstanceType<typeof ProofExchangeUpdateInput>;
}

@ObjectType()
export class AggregateVerifierSetting {
    @Field(() => VerifierSettingCountAggregate, {nullable:true})
    _count?: InstanceType<typeof VerifierSettingCountAggregate>;
    @Field(() => VerifierSettingMinAggregate, {nullable:true})
    _min?: InstanceType<typeof VerifierSettingMinAggregate>;
    @Field(() => VerifierSettingMaxAggregate, {nullable:true})
    _max?: InstanceType<typeof VerifierSettingMaxAggregate>;
}

@ArgsType()
export class CreateManyVerifierSettingArgs {
    @Field(() => [VerifierSettingCreateManyInput], {nullable:false})
    @Type(() => VerifierSettingCreateManyInput)
    data!: Array<VerifierSettingCreateManyInput>;
    @Field(() => Boolean, {nullable:true})
    skipDuplicates?: boolean;
}

@ArgsType()
export class CreateOneVerifierSettingArgs {
    @Field(() => VerifierSettingCreateInput, {nullable:false})
    @Type(() => VerifierSettingCreateInput)
    data!: InstanceType<typeof VerifierSettingCreateInput>;
}

@ArgsType()
export class DeleteManyVerifierSettingArgs {
    @Field(() => VerifierSettingWhereInput, {nullable:true})
    @Type(() => VerifierSettingWhereInput)
    where?: InstanceType<typeof VerifierSettingWhereInput>;
}

@ArgsType()
export class DeleteOneVerifierSettingArgs {
    @Field(() => VerifierSettingWhereUniqueInput, {nullable:false})
    @Type(() => VerifierSettingWhereUniqueInput)
    where!: InstanceType<typeof VerifierSettingWhereUniqueInput>;
}

@ArgsType()
export class FindFirstVerifierSettingOrThrowArgs {
    @Field(() => VerifierSettingWhereInput, {nullable:true})
    @Type(() => VerifierSettingWhereInput)
    where?: InstanceType<typeof VerifierSettingWhereInput>;
    @Field(() => [VerifierSettingOrderByWithRelationInput], {nullable:true})
    orderBy?: Array<VerifierSettingOrderByWithRelationInput>;
    @Field(() => VerifierSettingWhereUniqueInput, {nullable:true})
    cursor?: InstanceType<typeof VerifierSettingWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => [VerifierSettingScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof VerifierSettingScalarFieldEnum>;
}

@ArgsType()
export class FindFirstVerifierSettingArgs {
    @Field(() => VerifierSettingWhereInput, {nullable:true})
    @Type(() => VerifierSettingWhereInput)
    where?: InstanceType<typeof VerifierSettingWhereInput>;
    @Field(() => [VerifierSettingOrderByWithRelationInput], {nullable:true})
    orderBy?: Array<VerifierSettingOrderByWithRelationInput>;
    @Field(() => VerifierSettingWhereUniqueInput, {nullable:true})
    cursor?: InstanceType<typeof VerifierSettingWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => [VerifierSettingScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof VerifierSettingScalarFieldEnum>;
}

@ArgsType()
export class FindManyVerifierSettingArgs {
    @Field(() => VerifierSettingWhereInput, {nullable:true})
    @Type(() => VerifierSettingWhereInput)
    where?: InstanceType<typeof VerifierSettingWhereInput>;
    @Field(() => [VerifierSettingOrderByWithRelationInput], {nullable:true})
    orderBy?: Array<VerifierSettingOrderByWithRelationInput>;
    @Field(() => VerifierSettingWhereUniqueInput, {nullable:true})
    cursor?: InstanceType<typeof VerifierSettingWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => [VerifierSettingScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof VerifierSettingScalarFieldEnum>;
}

@ArgsType()
export class FindUniqueVerifierSettingOrThrowArgs {
    @Field(() => VerifierSettingWhereUniqueInput, {nullable:false})
    @Type(() => VerifierSettingWhereUniqueInput)
    where!: InstanceType<typeof VerifierSettingWhereUniqueInput>;
}

@ArgsType()
export class FindUniqueVerifierSettingArgs {
    @Field(() => VerifierSettingWhereUniqueInput, {nullable:false})
    @Type(() => VerifierSettingWhereUniqueInput)
    where!: InstanceType<typeof VerifierSettingWhereUniqueInput>;
}

@ArgsType()
export class UpdateManyVerifierSettingArgs {
    @Field(() => VerifierSettingUpdateManyMutationInput, {nullable:false})
    @Type(() => VerifierSettingUpdateManyMutationInput)
    data!: InstanceType<typeof VerifierSettingUpdateManyMutationInput>;
    @Field(() => VerifierSettingWhereInput, {nullable:true})
    @Type(() => VerifierSettingWhereInput)
    where?: InstanceType<typeof VerifierSettingWhereInput>;
}

@ArgsType()
export class UpdateOneVerifierSettingArgs {
    @Field(() => VerifierSettingUpdateInput, {nullable:false})
    @Type(() => VerifierSettingUpdateInput)
    data!: InstanceType<typeof VerifierSettingUpdateInput>;
    @Field(() => VerifierSettingWhereUniqueInput, {nullable:false})
    @Type(() => VerifierSettingWhereUniqueInput)
    where!: InstanceType<typeof VerifierSettingWhereUniqueInput>;
}

@ArgsType()
export class UpsertOneVerifierSettingArgs {
    @Field(() => VerifierSettingWhereUniqueInput, {nullable:false})
    @Type(() => VerifierSettingWhereUniqueInput)
    where!: InstanceType<typeof VerifierSettingWhereUniqueInput>;
    @Field(() => VerifierSettingCreateInput, {nullable:false})
    @Type(() => VerifierSettingCreateInput)
    create!: InstanceType<typeof VerifierSettingCreateInput>;
    @Field(() => VerifierSettingUpdateInput, {nullable:false})
    @Type(() => VerifierSettingUpdateInput)
    update!: InstanceType<typeof VerifierSettingUpdateInput>;
}

@ArgsType()
export class VerifierSettingAggregateArgs {
    @Field(() => VerifierSettingWhereInput, {nullable:true})
    @Type(() => VerifierSettingWhereInput)
    where?: InstanceType<typeof VerifierSettingWhereInput>;
    @Field(() => [VerifierSettingOrderByWithRelationInput], {nullable:true})
    orderBy?: Array<VerifierSettingOrderByWithRelationInput>;
    @Field(() => VerifierSettingWhereUniqueInput, {nullable:true})
    cursor?: InstanceType<typeof VerifierSettingWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => VerifierSettingCountAggregateInput, {nullable:true})
    _count?: InstanceType<typeof VerifierSettingCountAggregateInput>;
    @Field(() => VerifierSettingMinAggregateInput, {nullable:true})
    _min?: InstanceType<typeof VerifierSettingMinAggregateInput>;
    @Field(() => VerifierSettingMaxAggregateInput, {nullable:true})
    _max?: InstanceType<typeof VerifierSettingMaxAggregateInput>;
}

@InputType()
export class VerifierSettingCountAggregateInput {
    @Field(() => Boolean, {nullable:true})
    id?: true;
    @HideField()
    createdAt?: true;
    @HideField()
    updatedAt?: true;
    @Field(() => Boolean, {nullable:true})
    verifierWalletId?: true;
    @Field(() => Boolean, {nullable:true})
    proofRequestSetting?: true;
    @Field(() => Boolean, {nullable:true})
    _all?: true;
}

@ObjectType()
export class VerifierSettingCountAggregate {
    @Field(() => Int, {nullable:false})
    id!: number;
    @Field(() => Int, {nullable:false})
    createdAt!: number;
    @Field(() => Int, {nullable:false})
    updatedAt!: number;
    @Field(() => Int, {nullable:false})
    verifierWalletId!: number;
    @Field(() => Int, {nullable:false})
    proofRequestSetting!: number;
    @Field(() => Int, {nullable:false})
    _all!: number;
}

@InputType()
export class VerifierSettingCountOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    verifierWalletId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    proofRequestSetting?: keyof typeof SortOrder;
}

@InputType()
export class VerifierSettingCreateManyInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    verifierWalletId!: string;
    @Field(() => GraphQLJSON, {nullable:false})
    proofRequestSetting!: any;
}

@InputType()
export class VerifierSettingCreateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    verifierWalletId!: string;
    @Field(() => GraphQLJSON, {nullable:false})
    proofRequestSetting!: any;
}

@ArgsType()
export class VerifierSettingGroupByArgs {
    @Field(() => VerifierSettingWhereInput, {nullable:true})
    @Type(() => VerifierSettingWhereInput)
    where?: InstanceType<typeof VerifierSettingWhereInput>;
    @Field(() => [VerifierSettingOrderByWithAggregationInput], {nullable:true})
    orderBy?: Array<VerifierSettingOrderByWithAggregationInput>;
    @Field(() => [VerifierSettingScalarFieldEnum], {nullable:false})
    by!: Array<keyof typeof VerifierSettingScalarFieldEnum>;
    @Field(() => VerifierSettingScalarWhereWithAggregatesInput, {nullable:true})
    having?: InstanceType<typeof VerifierSettingScalarWhereWithAggregatesInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => VerifierSettingCountAggregateInput, {nullable:true})
    _count?: InstanceType<typeof VerifierSettingCountAggregateInput>;
    @Field(() => VerifierSettingMinAggregateInput, {nullable:true})
    _min?: InstanceType<typeof VerifierSettingMinAggregateInput>;
    @Field(() => VerifierSettingMaxAggregateInput, {nullable:true})
    _max?: InstanceType<typeof VerifierSettingMaxAggregateInput>;
}

@ObjectType()
export class VerifierSettingGroupBy {
    @Field(() => String, {nullable:false})
    id!: string;
    @Field(() => Date, {nullable:false})
    createdAt!: Date | string;
    @Field(() => Date, {nullable:false})
    updatedAt!: Date | string;
    @Field(() => String, {nullable:false})
    verifierWalletId!: string;
    @Field(() => GraphQLJSON, {nullable:false})
    proofRequestSetting!: any;
    @Field(() => VerifierSettingCountAggregate, {nullable:true})
    _count?: InstanceType<typeof VerifierSettingCountAggregate>;
    @Field(() => VerifierSettingMinAggregate, {nullable:true})
    _min?: InstanceType<typeof VerifierSettingMinAggregate>;
    @Field(() => VerifierSettingMaxAggregate, {nullable:true})
    _max?: InstanceType<typeof VerifierSettingMaxAggregate>;
}

@InputType()
export class VerifierSettingMaxAggregateInput {
    @Field(() => Boolean, {nullable:true})
    id?: true;
    @HideField()
    createdAt?: true;
    @HideField()
    updatedAt?: true;
    @Field(() => Boolean, {nullable:true})
    verifierWalletId?: true;
}

@ObjectType()
export class VerifierSettingMaxAggregate {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    verifierWalletId?: string;
}

@InputType()
export class VerifierSettingMaxOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    verifierWalletId?: keyof typeof SortOrder;
}

@InputType()
export class VerifierSettingMinAggregateInput {
    @Field(() => Boolean, {nullable:true})
    id?: true;
    @HideField()
    createdAt?: true;
    @HideField()
    updatedAt?: true;
    @Field(() => Boolean, {nullable:true})
    verifierWalletId?: true;
}

@ObjectType()
export class VerifierSettingMinAggregate {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    verifierWalletId?: string;
}

@InputType()
export class VerifierSettingMinOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    verifierWalletId?: keyof typeof SortOrder;
}

@InputType()
export class VerifierSettingOrderByWithAggregationInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    verifierWalletId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    proofRequestSetting?: keyof typeof SortOrder;
    @Field(() => VerifierSettingCountOrderByAggregateInput, {nullable:true})
    _count?: InstanceType<typeof VerifierSettingCountOrderByAggregateInput>;
    @Field(() => VerifierSettingMaxOrderByAggregateInput, {nullable:true})
    _max?: InstanceType<typeof VerifierSettingMaxOrderByAggregateInput>;
    @Field(() => VerifierSettingMinOrderByAggregateInput, {nullable:true})
    _min?: InstanceType<typeof VerifierSettingMinOrderByAggregateInput>;
}

@InputType()
export class VerifierSettingOrderByWithRelationInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    verifierWalletId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    proofRequestSetting?: keyof typeof SortOrder;
}

@InputType()
export class VerifierSettingScalarWhereWithAggregatesInput {
    @Field(() => [VerifierSettingScalarWhereWithAggregatesInput], {nullable:true})
    AND?: Array<VerifierSettingScalarWhereWithAggregatesInput>;
    @Field(() => [VerifierSettingScalarWhereWithAggregatesInput], {nullable:true})
    OR?: Array<VerifierSettingScalarWhereWithAggregatesInput>;
    @Field(() => [VerifierSettingScalarWhereWithAggregatesInput], {nullable:true})
    NOT?: Array<VerifierSettingScalarWhereWithAggregatesInput>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    id?: InstanceType<typeof StringWithAggregatesFilter>;
    @HideField()
    createdAt?: InstanceType<typeof DateTimeWithAggregatesFilter>;
    @HideField()
    updatedAt?: InstanceType<typeof DateTimeWithAggregatesFilter>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    verifierWalletId?: InstanceType<typeof StringWithAggregatesFilter>;
    @Field(() => JsonWithAggregatesFilter, {nullable:true})
    proofRequestSetting?: InstanceType<typeof JsonWithAggregatesFilter>;
}

@InputType()
export class VerifierSettingUncheckedCreateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    verifierWalletId!: string;
    @Field(() => GraphQLJSON, {nullable:false})
    proofRequestSetting!: any;
}

@InputType()
export class VerifierSettingUncheckedUpdateManyInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    verifierWalletId?: string;
    @Field(() => GraphQLJSON, {nullable:true})
    proofRequestSetting?: any;
}

@InputType()
export class VerifierSettingUncheckedUpdateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    verifierWalletId?: string;
    @Field(() => GraphQLJSON, {nullable:true})
    proofRequestSetting?: any;
}

@InputType()
export class VerifierSettingUpdateManyMutationInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    verifierWalletId?: string;
    @Field(() => GraphQLJSON, {nullable:true})
    proofRequestSetting?: any;
}

@InputType()
export class VerifierSettingUpdateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    verifierWalletId?: string;
    @Field(() => GraphQLJSON, {nullable:true})
    proofRequestSetting?: any;
}

@InputType()
export class VerifierSettingWhereUniqueInput {
    @Field(() => String, {nullable:true})
    id?: string;
}

@InputType()
export class VerifierSettingWhereInput {
    @Field(() => [VerifierSettingWhereInput], {nullable:true})
    AND?: Array<VerifierSettingWhereInput>;
    @Field(() => [VerifierSettingWhereInput], {nullable:true})
    OR?: Array<VerifierSettingWhereInput>;
    @Field(() => [VerifierSettingWhereInput], {nullable:true})
    NOT?: Array<VerifierSettingWhereInput>;
    @Field(() => StringFilter, {nullable:true})
    id?: InstanceType<typeof StringFilter>;
    @HideField()
    createdAt?: InstanceType<typeof DateTimeFilter>;
    @HideField()
    updatedAt?: InstanceType<typeof DateTimeFilter>;
    @Field(() => StringFilter, {nullable:true})
    verifierWalletId?: InstanceType<typeof StringFilter>;
    @Field(() => JsonFilter, {nullable:true})
    proofRequestSetting?: InstanceType<typeof JsonFilter>;
}

@ObjectType()
export class VerifierSetting {
    @Field(() => ID, {nullable:false})
    id!: string;
    @Field(() => Date, {nullable:false})
    createdAt!: Date;
    @Field(() => Date, {nullable:false})
    updatedAt!: Date;
    @Field(() => String, {nullable:false})
    verifierWalletId!: string;
    @Field(() => GraphQLJSON, {nullable:false})
    proofRequestSetting!: any;
}

@ObjectType()
export class AggregateWallet {
    @Field(() => WalletCountAggregate, {nullable:true})
    _count?: InstanceType<typeof WalletCountAggregate>;
    @Field(() => WalletMinAggregate, {nullable:true})
    _min?: InstanceType<typeof WalletMinAggregate>;
    @Field(() => WalletMaxAggregate, {nullable:true})
    _max?: InstanceType<typeof WalletMaxAggregate>;
}

@ArgsType()
export class CreateManyWalletArgs {
    @Field(() => [WalletCreateManyInput], {nullable:false})
    @Type(() => WalletCreateManyInput)
    data!: Array<WalletCreateManyInput>;
    @Field(() => Boolean, {nullable:true})
    skipDuplicates?: boolean;
}

@ArgsType()
export class CreateOneWalletArgs {
    @Field(() => WalletCreateInput, {nullable:false})
    @Type(() => WalletCreateInput)
    data!: InstanceType<typeof WalletCreateInput>;
}

@ArgsType()
export class DeleteManyWalletArgs {
    @Field(() => WalletWhereInput, {nullable:true})
    @Type(() => WalletWhereInput)
    where?: InstanceType<typeof WalletWhereInput>;
}

@ArgsType()
export class DeleteOneWalletArgs {
    @Field(() => WalletWhereUniqueInput, {nullable:false})
    @Type(() => WalletWhereUniqueInput)
    where!: InstanceType<typeof WalletWhereUniqueInput>;
}

@ArgsType()
export class FindFirstWalletOrThrowArgs {
    @Field(() => WalletWhereInput, {nullable:true})
    @Type(() => WalletWhereInput)
    where?: InstanceType<typeof WalletWhereInput>;
    @Field(() => [WalletOrderByWithRelationInput], {nullable:true})
    orderBy?: Array<WalletOrderByWithRelationInput>;
    @Field(() => WalletWhereUniqueInput, {nullable:true})
    cursor?: InstanceType<typeof WalletWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => [WalletScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof WalletScalarFieldEnum>;
}

@ArgsType()
export class FindFirstWalletArgs {
    @Field(() => WalletWhereInput, {nullable:true})
    @Type(() => WalletWhereInput)
    where?: InstanceType<typeof WalletWhereInput>;
    @Field(() => [WalletOrderByWithRelationInput], {nullable:true})
    orderBy?: Array<WalletOrderByWithRelationInput>;
    @Field(() => WalletWhereUniqueInput, {nullable:true})
    cursor?: InstanceType<typeof WalletWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => [WalletScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof WalletScalarFieldEnum>;
}

@ArgsType()
export class FindManyWalletArgs {
    @Field(() => WalletWhereInput, {nullable:true})
    @Type(() => WalletWhereInput)
    where?: InstanceType<typeof WalletWhereInput>;
    @Field(() => [WalletOrderByWithRelationInput], {nullable:true})
    orderBy?: Array<WalletOrderByWithRelationInput>;
    @Field(() => WalletWhereUniqueInput, {nullable:true})
    cursor?: InstanceType<typeof WalletWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => [WalletScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof WalletScalarFieldEnum>;
}

@ArgsType()
export class FindUniqueWalletOrThrowArgs {
    @Field(() => WalletWhereUniqueInput, {nullable:false})
    @Type(() => WalletWhereUniqueInput)
    where!: InstanceType<typeof WalletWhereUniqueInput>;
}

@ArgsType()
export class FindUniqueWalletArgs {
    @Field(() => WalletWhereUniqueInput, {nullable:false})
    @Type(() => WalletWhereUniqueInput)
    where!: InstanceType<typeof WalletWhereUniqueInput>;
}

@ArgsType()
export class UpdateManyWalletArgs {
    @Field(() => WalletUpdateManyMutationInput, {nullable:false})
    @Type(() => WalletUpdateManyMutationInput)
    data!: InstanceType<typeof WalletUpdateManyMutationInput>;
    @Field(() => WalletWhereInput, {nullable:true})
    @Type(() => WalletWhereInput)
    where?: InstanceType<typeof WalletWhereInput>;
}

@ArgsType()
export class UpdateOneWalletArgs {
    @Field(() => WalletUpdateInput, {nullable:false})
    @Type(() => WalletUpdateInput)
    data!: InstanceType<typeof WalletUpdateInput>;
    @Field(() => WalletWhereUniqueInput, {nullable:false})
    @Type(() => WalletWhereUniqueInput)
    where!: InstanceType<typeof WalletWhereUniqueInput>;
}

@ArgsType()
export class UpsertOneWalletArgs {
    @Field(() => WalletWhereUniqueInput, {nullable:false})
    @Type(() => WalletWhereUniqueInput)
    where!: InstanceType<typeof WalletWhereUniqueInput>;
    @Field(() => WalletCreateInput, {nullable:false})
    @Type(() => WalletCreateInput)
    create!: InstanceType<typeof WalletCreateInput>;
    @Field(() => WalletUpdateInput, {nullable:false})
    @Type(() => WalletUpdateInput)
    update!: InstanceType<typeof WalletUpdateInput>;
}

@ArgsType()
export class WalletAggregateArgs {
    @Field(() => WalletWhereInput, {nullable:true})
    @Type(() => WalletWhereInput)
    where?: InstanceType<typeof WalletWhereInput>;
    @Field(() => [WalletOrderByWithRelationInput], {nullable:true})
    orderBy?: Array<WalletOrderByWithRelationInput>;
    @Field(() => WalletWhereUniqueInput, {nullable:true})
    cursor?: InstanceType<typeof WalletWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => WalletCountAggregateInput, {nullable:true})
    _count?: InstanceType<typeof WalletCountAggregateInput>;
    @Field(() => WalletMinAggregateInput, {nullable:true})
    _min?: InstanceType<typeof WalletMinAggregateInput>;
    @Field(() => WalletMaxAggregateInput, {nullable:true})
    _max?: InstanceType<typeof WalletMaxAggregateInput>;
}

@InputType()
export class WalletCountAggregateInput {
    @Field(() => Boolean, {nullable:true})
    id?: true;
    @HideField()
    createdAt?: true;
    @HideField()
    updatedAt?: true;
    @Field(() => Boolean, {nullable:true})
    email?: true;
    @HideField()
    role?: true;
    @HideField()
    passwordHash?: true;
    @HideField()
    accessToken?: true;
    @Field(() => Boolean, {nullable:true})
    _all?: true;
}

@ObjectType()
export class WalletCountAggregate {
    @Field(() => Int, {nullable:false})
    id!: number;
    @Field(() => Int, {nullable:false})
    createdAt!: number;
    @Field(() => Int, {nullable:false})
    updatedAt!: number;
    @Field(() => Int, {nullable:false})
    email!: number;
    @Field(() => Int, {nullable:false})
    role!: number;
    @HideField()
    passwordHash!: number;
    @HideField()
    accessToken!: number;
    @Field(() => Int, {nullable:false})
    _all!: number;
}

@InputType()
export class WalletCountOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    email?: keyof typeof SortOrder;
    @HideField()
    role?: keyof typeof SortOrder;
    @HideField()
    passwordHash?: keyof typeof SortOrder;
    @HideField()
    accessToken?: keyof typeof SortOrder;
}

@ObjectType()
export class WalletCount {
    @Field(() => Int, {nullable:false})
    otps?: number;
    @Field(() => Int, {nullable:false})
    devices?: number;
}

@InputType()
export class WalletCreateManyInput {
    @Field(() => String, {nullable:false})
    id!: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsEmail()
    email!: string;
    @HideField()
    role?: keyof typeof WalletRole;
    @HideField()
    passwordHash!: string;
    @HideField()
    accessToken!: string;
}

@InputType()
export class WalletCreateNestedOneWithoutDevicesInput {
    @Field(() => WalletCreateWithoutDevicesInput, {nullable:true})
    @Type(() => WalletCreateWithoutDevicesInput)
    create?: InstanceType<typeof WalletCreateWithoutDevicesInput>;
    @Field(() => WalletCreateOrConnectWithoutDevicesInput, {nullable:true})
    @Type(() => WalletCreateOrConnectWithoutDevicesInput)
    connectOrCreate?: InstanceType<typeof WalletCreateOrConnectWithoutDevicesInput>;
    @Field(() => WalletWhereUniqueInput, {nullable:true})
    @Type(() => WalletWhereUniqueInput)
    connect?: InstanceType<typeof WalletWhereUniqueInput>;
}

@InputType()
export class WalletCreateNestedOneWithoutOtpsInput {
    @Field(() => WalletCreateWithoutOtpsInput, {nullable:true})
    @Type(() => WalletCreateWithoutOtpsInput)
    create?: InstanceType<typeof WalletCreateWithoutOtpsInput>;
    @Field(() => WalletCreateOrConnectWithoutOtpsInput, {nullable:true})
    @Type(() => WalletCreateOrConnectWithoutOtpsInput)
    connectOrCreate?: InstanceType<typeof WalletCreateOrConnectWithoutOtpsInput>;
    @Field(() => WalletWhereUniqueInput, {nullable:true})
    @Type(() => WalletWhereUniqueInput)
    connect?: InstanceType<typeof WalletWhereUniqueInput>;
}

@InputType()
export class WalletCreateOrConnectWithoutDevicesInput {
    @Field(() => WalletWhereUniqueInput, {nullable:false})
    @Type(() => WalletWhereUniqueInput)
    where!: InstanceType<typeof WalletWhereUniqueInput>;
    @Field(() => WalletCreateWithoutDevicesInput, {nullable:false})
    @Type(() => WalletCreateWithoutDevicesInput)
    create!: InstanceType<typeof WalletCreateWithoutDevicesInput>;
}

@InputType()
export class WalletCreateOrConnectWithoutOtpsInput {
    @Field(() => WalletWhereUniqueInput, {nullable:false})
    @Type(() => WalletWhereUniqueInput)
    where!: InstanceType<typeof WalletWhereUniqueInput>;
    @Field(() => WalletCreateWithoutOtpsInput, {nullable:false})
    @Type(() => WalletCreateWithoutOtpsInput)
    create!: InstanceType<typeof WalletCreateWithoutOtpsInput>;
}

@InputType()
export class WalletCreateWithoutDevicesInput {
    @Field(() => String, {nullable:false})
    id!: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsEmail()
    email!: string;
    @HideField()
    role?: keyof typeof WalletRole;
    @HideField()
    passwordHash!: string;
    @HideField()
    accessToken!: string;
    @Field(() => OtpCreateNestedManyWithoutWalletInput, {nullable:true})
    otps?: InstanceType<typeof OtpCreateNestedManyWithoutWalletInput>;
}

@InputType()
export class WalletCreateWithoutOtpsInput {
    @Field(() => String, {nullable:false})
    id!: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsEmail()
    email!: string;
    @HideField()
    role?: keyof typeof WalletRole;
    @HideField()
    passwordHash!: string;
    @HideField()
    accessToken!: string;
    @HideField()
    devices?: InstanceType<typeof DeviceCreateNestedManyWithoutWalletInput>;
}

@InputType()
export class WalletCreateInput {
    @Field(() => String, {nullable:false})
    id!: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsEmail()
    email!: string;
    @HideField()
    role?: keyof typeof WalletRole;
    @HideField()
    passwordHash!: string;
    @HideField()
    accessToken!: string;
    @Field(() => OtpCreateNestedManyWithoutWalletInput, {nullable:true})
    otps?: InstanceType<typeof OtpCreateNestedManyWithoutWalletInput>;
    @HideField()
    devices?: InstanceType<typeof DeviceCreateNestedManyWithoutWalletInput>;
}

@ArgsType()
export class WalletGroupByArgs {
    @Field(() => WalletWhereInput, {nullable:true})
    @Type(() => WalletWhereInput)
    where?: InstanceType<typeof WalletWhereInput>;
    @Field(() => [WalletOrderByWithAggregationInput], {nullable:true})
    orderBy?: Array<WalletOrderByWithAggregationInput>;
    @Field(() => [WalletScalarFieldEnum], {nullable:false})
    by!: Array<keyof typeof WalletScalarFieldEnum>;
    @Field(() => WalletScalarWhereWithAggregatesInput, {nullable:true})
    having?: InstanceType<typeof WalletScalarWhereWithAggregatesInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => WalletCountAggregateInput, {nullable:true})
    _count?: InstanceType<typeof WalletCountAggregateInput>;
    @Field(() => WalletMinAggregateInput, {nullable:true})
    _min?: InstanceType<typeof WalletMinAggregateInput>;
    @Field(() => WalletMaxAggregateInput, {nullable:true})
    _max?: InstanceType<typeof WalletMaxAggregateInput>;
}

@ObjectType()
export class WalletGroupBy {
    @Field(() => String, {nullable:false})
    id!: string;
    @Field(() => Date, {nullable:false})
    createdAt!: Date | string;
    @Field(() => Date, {nullable:false})
    updatedAt!: Date | string;
    @Field(() => String, {nullable:false})
    email!: string;
    @Field(() => WalletRole, {nullable:false})
    role!: keyof typeof WalletRole;
    @HideField()
    passwordHash!: string;
    @HideField()
    accessToken!: string;
    @Field(() => WalletCountAggregate, {nullable:true})
    _count?: InstanceType<typeof WalletCountAggregate>;
    @Field(() => WalletMinAggregate, {nullable:true})
    _min?: InstanceType<typeof WalletMinAggregate>;
    @Field(() => WalletMaxAggregate, {nullable:true})
    _max?: InstanceType<typeof WalletMaxAggregate>;
}

@InputType()
export class WalletMaxAggregateInput {
    @Field(() => Boolean, {nullable:true})
    id?: true;
    @HideField()
    createdAt?: true;
    @HideField()
    updatedAt?: true;
    @Field(() => Boolean, {nullable:true})
    email?: true;
    @HideField()
    role?: true;
    @HideField()
    passwordHash?: true;
    @HideField()
    accessToken?: true;
}

@ObjectType()
export class WalletMaxAggregate {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    email?: string;
    @Field(() => WalletRole, {nullable:true})
    role?: keyof typeof WalletRole;
    @HideField()
    passwordHash?: string;
    @HideField()
    accessToken?: string;
}

@InputType()
export class WalletMaxOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    email?: keyof typeof SortOrder;
    @HideField()
    role?: keyof typeof SortOrder;
    @HideField()
    passwordHash?: keyof typeof SortOrder;
    @HideField()
    accessToken?: keyof typeof SortOrder;
}

@InputType()
export class WalletMinAggregateInput {
    @Field(() => Boolean, {nullable:true})
    id?: true;
    @HideField()
    createdAt?: true;
    @HideField()
    updatedAt?: true;
    @Field(() => Boolean, {nullable:true})
    email?: true;
    @HideField()
    role?: true;
    @HideField()
    passwordHash?: true;
    @HideField()
    accessToken?: true;
}

@ObjectType()
export class WalletMinAggregate {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    email?: string;
    @Field(() => WalletRole, {nullable:true})
    role?: keyof typeof WalletRole;
    @HideField()
    passwordHash?: string;
    @HideField()
    accessToken?: string;
}

@InputType()
export class WalletMinOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    email?: keyof typeof SortOrder;
    @HideField()
    role?: keyof typeof SortOrder;
    @HideField()
    passwordHash?: keyof typeof SortOrder;
    @HideField()
    accessToken?: keyof typeof SortOrder;
}

@InputType()
export class WalletOrderByWithAggregationInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    email?: keyof typeof SortOrder;
    @HideField()
    role?: keyof typeof SortOrder;
    @HideField()
    passwordHash?: keyof typeof SortOrder;
    @HideField()
    accessToken?: keyof typeof SortOrder;
    @Field(() => WalletCountOrderByAggregateInput, {nullable:true})
    _count?: InstanceType<typeof WalletCountOrderByAggregateInput>;
    @Field(() => WalletMaxOrderByAggregateInput, {nullable:true})
    _max?: InstanceType<typeof WalletMaxOrderByAggregateInput>;
    @Field(() => WalletMinOrderByAggregateInput, {nullable:true})
    _min?: InstanceType<typeof WalletMinOrderByAggregateInput>;
}

@InputType()
export class WalletOrderByWithRelationInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    email?: keyof typeof SortOrder;
    @HideField()
    role?: keyof typeof SortOrder;
    @HideField()
    passwordHash?: keyof typeof SortOrder;
    @HideField()
    accessToken?: keyof typeof SortOrder;
    @Field(() => OtpOrderByRelationAggregateInput, {nullable:true})
    otps?: InstanceType<typeof OtpOrderByRelationAggregateInput>;
    @HideField()
    devices?: InstanceType<typeof DeviceOrderByRelationAggregateInput>;
}

@InputType()
export class WalletRelationFilter {
    @Field(() => WalletWhereInput, {nullable:true})
    is?: InstanceType<typeof WalletWhereInput>;
    @Field(() => WalletWhereInput, {nullable:true})
    isNot?: InstanceType<typeof WalletWhereInput>;
}

@InputType()
export class WalletScalarWhereWithAggregatesInput {
    @Field(() => [WalletScalarWhereWithAggregatesInput], {nullable:true})
    AND?: Array<WalletScalarWhereWithAggregatesInput>;
    @Field(() => [WalletScalarWhereWithAggregatesInput], {nullable:true})
    OR?: Array<WalletScalarWhereWithAggregatesInput>;
    @Field(() => [WalletScalarWhereWithAggregatesInput], {nullable:true})
    NOT?: Array<WalletScalarWhereWithAggregatesInput>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    id?: InstanceType<typeof StringWithAggregatesFilter>;
    @HideField()
    createdAt?: InstanceType<typeof DateTimeWithAggregatesFilter>;
    @HideField()
    updatedAt?: InstanceType<typeof DateTimeWithAggregatesFilter>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    email?: InstanceType<typeof StringWithAggregatesFilter>;
    @HideField()
    role?: InstanceType<typeof EnumWalletRoleWithAggregatesFilter>;
    @HideField()
    passwordHash?: InstanceType<typeof StringWithAggregatesFilter>;
    @HideField()
    accessToken?: InstanceType<typeof StringWithAggregatesFilter>;
}

@InputType()
export class WalletUncheckedCreateWithoutDevicesInput {
    @Field(() => String, {nullable:false})
    id!: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsEmail()
    email!: string;
    @HideField()
    role?: keyof typeof WalletRole;
    @HideField()
    passwordHash!: string;
    @HideField()
    accessToken!: string;
    @Field(() => OtpUncheckedCreateNestedManyWithoutWalletInput, {nullable:true})
    otps?: InstanceType<typeof OtpUncheckedCreateNestedManyWithoutWalletInput>;
}

@InputType()
export class WalletUncheckedCreateWithoutOtpsInput {
    @Field(() => String, {nullable:false})
    id!: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsEmail()
    email!: string;
    @HideField()
    role?: keyof typeof WalletRole;
    @HideField()
    passwordHash!: string;
    @HideField()
    accessToken!: string;
    @HideField()
    devices?: InstanceType<typeof DeviceUncheckedCreateNestedManyWithoutWalletInput>;
}

@InputType()
export class WalletUncheckedCreateInput {
    @Field(() => String, {nullable:false})
    id!: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsEmail()
    email!: string;
    @HideField()
    role?: keyof typeof WalletRole;
    @HideField()
    passwordHash!: string;
    @HideField()
    accessToken!: string;
    @Field(() => OtpUncheckedCreateNestedManyWithoutWalletInput, {nullable:true})
    otps?: InstanceType<typeof OtpUncheckedCreateNestedManyWithoutWalletInput>;
    @HideField()
    devices?: InstanceType<typeof DeviceUncheckedCreateNestedManyWithoutWalletInput>;
}

@InputType()
export class WalletUncheckedUpdateManyInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsEmail()
    email?: string;
    @HideField()
    role?: keyof typeof WalletRole;
    @HideField()
    passwordHash?: string;
    @HideField()
    accessToken?: string;
}

@InputType()
export class WalletUncheckedUpdateWithoutDevicesInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsEmail()
    email?: string;
    @HideField()
    role?: keyof typeof WalletRole;
    @HideField()
    passwordHash?: string;
    @HideField()
    accessToken?: string;
    @Field(() => OtpUncheckedUpdateManyWithoutWalletNestedInput, {nullable:true})
    otps?: InstanceType<typeof OtpUncheckedUpdateManyWithoutWalletNestedInput>;
}

@InputType()
export class WalletUncheckedUpdateWithoutOtpsInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsEmail()
    email?: string;
    @HideField()
    role?: keyof typeof WalletRole;
    @HideField()
    passwordHash?: string;
    @HideField()
    accessToken?: string;
    @HideField()
    devices?: InstanceType<typeof DeviceUncheckedUpdateManyWithoutWalletNestedInput>;
}

@InputType()
export class WalletUncheckedUpdateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsEmail()
    email?: string;
    @HideField()
    role?: keyof typeof WalletRole;
    @HideField()
    passwordHash?: string;
    @HideField()
    accessToken?: string;
    @Field(() => OtpUncheckedUpdateManyWithoutWalletNestedInput, {nullable:true})
    otps?: InstanceType<typeof OtpUncheckedUpdateManyWithoutWalletNestedInput>;
    @HideField()
    devices?: InstanceType<typeof DeviceUncheckedUpdateManyWithoutWalletNestedInput>;
}

@InputType()
export class WalletUpdateManyMutationInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsEmail()
    email?: string;
    @HideField()
    role?: keyof typeof WalletRole;
    @HideField()
    passwordHash?: string;
    @HideField()
    accessToken?: string;
}

@InputType()
export class WalletUpdateOneRequiredWithoutDevicesNestedInput {
    @Field(() => WalletCreateWithoutDevicesInput, {nullable:true})
    @Type(() => WalletCreateWithoutDevicesInput)
    create?: InstanceType<typeof WalletCreateWithoutDevicesInput>;
    @Field(() => WalletCreateOrConnectWithoutDevicesInput, {nullable:true})
    @Type(() => WalletCreateOrConnectWithoutDevicesInput)
    connectOrCreate?: InstanceType<typeof WalletCreateOrConnectWithoutDevicesInput>;
    @Field(() => WalletUpsertWithoutDevicesInput, {nullable:true})
    @Type(() => WalletUpsertWithoutDevicesInput)
    upsert?: InstanceType<typeof WalletUpsertWithoutDevicesInput>;
    @Field(() => WalletWhereUniqueInput, {nullable:true})
    @Type(() => WalletWhereUniqueInput)
    connect?: InstanceType<typeof WalletWhereUniqueInput>;
    @Field(() => WalletUpdateWithoutDevicesInput, {nullable:true})
    @Type(() => WalletUpdateWithoutDevicesInput)
    update?: InstanceType<typeof WalletUpdateWithoutDevicesInput>;
}

@InputType()
export class WalletUpdateOneRequiredWithoutOtpsNestedInput {
    @Field(() => WalletCreateWithoutOtpsInput, {nullable:true})
    @Type(() => WalletCreateWithoutOtpsInput)
    create?: InstanceType<typeof WalletCreateWithoutOtpsInput>;
    @Field(() => WalletCreateOrConnectWithoutOtpsInput, {nullable:true})
    @Type(() => WalletCreateOrConnectWithoutOtpsInput)
    connectOrCreate?: InstanceType<typeof WalletCreateOrConnectWithoutOtpsInput>;
    @Field(() => WalletUpsertWithoutOtpsInput, {nullable:true})
    @Type(() => WalletUpsertWithoutOtpsInput)
    upsert?: InstanceType<typeof WalletUpsertWithoutOtpsInput>;
    @Field(() => WalletWhereUniqueInput, {nullable:true})
    @Type(() => WalletWhereUniqueInput)
    connect?: InstanceType<typeof WalletWhereUniqueInput>;
    @Field(() => WalletUpdateWithoutOtpsInput, {nullable:true})
    @Type(() => WalletUpdateWithoutOtpsInput)
    update?: InstanceType<typeof WalletUpdateWithoutOtpsInput>;
}

@InputType()
export class WalletUpdateWithoutDevicesInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsEmail()
    email?: string;
    @HideField()
    role?: keyof typeof WalletRole;
    @HideField()
    passwordHash?: string;
    @HideField()
    accessToken?: string;
    @Field(() => OtpUpdateManyWithoutWalletNestedInput, {nullable:true})
    otps?: InstanceType<typeof OtpUpdateManyWithoutWalletNestedInput>;
}

@InputType()
export class WalletUpdateWithoutOtpsInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsEmail()
    email?: string;
    @HideField()
    role?: keyof typeof WalletRole;
    @HideField()
    passwordHash?: string;
    @HideField()
    accessToken?: string;
    @HideField()
    devices?: InstanceType<typeof DeviceUpdateManyWithoutWalletNestedInput>;
}

@InputType()
export class WalletUpdateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsEmail()
    email?: string;
    @HideField()
    role?: keyof typeof WalletRole;
    @HideField()
    passwordHash?: string;
    @HideField()
    accessToken?: string;
    @Field(() => OtpUpdateManyWithoutWalletNestedInput, {nullable:true})
    otps?: InstanceType<typeof OtpUpdateManyWithoutWalletNestedInput>;
    @HideField()
    devices?: InstanceType<typeof DeviceUpdateManyWithoutWalletNestedInput>;
}

@InputType()
export class WalletUpsertWithoutDevicesInput {
    @Field(() => WalletUpdateWithoutDevicesInput, {nullable:false})
    @Type(() => WalletUpdateWithoutDevicesInput)
    update!: InstanceType<typeof WalletUpdateWithoutDevicesInput>;
    @Field(() => WalletCreateWithoutDevicesInput, {nullable:false})
    @Type(() => WalletCreateWithoutDevicesInput)
    create!: InstanceType<typeof WalletCreateWithoutDevicesInput>;
}

@InputType()
export class WalletUpsertWithoutOtpsInput {
    @Field(() => WalletUpdateWithoutOtpsInput, {nullable:false})
    @Type(() => WalletUpdateWithoutOtpsInput)
    update!: InstanceType<typeof WalletUpdateWithoutOtpsInput>;
    @Field(() => WalletCreateWithoutOtpsInput, {nullable:false})
    @Type(() => WalletCreateWithoutOtpsInput)
    create!: InstanceType<typeof WalletCreateWithoutOtpsInput>;
}

@InputType()
export class WalletWhereUniqueInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsEmail()
    email?: string;
}

@InputType()
export class WalletWhereInput {
    @Field(() => [WalletWhereInput], {nullable:true})
    AND?: Array<WalletWhereInput>;
    @Field(() => [WalletWhereInput], {nullable:true})
    OR?: Array<WalletWhereInput>;
    @Field(() => [WalletWhereInput], {nullable:true})
    NOT?: Array<WalletWhereInput>;
    @Field(() => StringFilter, {nullable:true})
    id?: InstanceType<typeof StringFilter>;
    @HideField()
    createdAt?: InstanceType<typeof DateTimeFilter>;
    @HideField()
    updatedAt?: InstanceType<typeof DateTimeFilter>;
    @Field(() => StringFilter, {nullable:true})
    email?: InstanceType<typeof StringFilter>;
    @HideField()
    role?: InstanceType<typeof EnumWalletRoleFilter>;
    @HideField()
    passwordHash?: InstanceType<typeof StringFilter>;
    @HideField()
    accessToken?: InstanceType<typeof StringFilter>;
    @Field(() => OtpListRelationFilter, {nullable:true})
    otps?: InstanceType<typeof OtpListRelationFilter>;
    @HideField()
    devices?: InstanceType<typeof DeviceListRelationFilter>;
}

@ObjectType()
export class Wallet {
    @Field(() => ID, {nullable:false})
    id!: string;
    @Field(() => Date, {nullable:false})
    createdAt!: Date;
    @Field(() => Date, {nullable:false})
    updatedAt!: Date;
    @Field(() => String, {nullable:false})
    email!: string;
    @Field(() => WalletRole, {nullable:false,defaultValue:'HOLDER'})
    role!: keyof typeof WalletRole;
    @HideField()
    passwordHash!: string;
    @HideField()
    accessToken!: string;
    @Field(() => [Otp], {nullable:true})
    otps?: Array<Otp>;
    @HideField()
    devices?: Array<Device>;
    @Field(() => WalletCount, {nullable:false})
    _count?: InstanceType<typeof WalletCount>;
}
