export const removePropertyRecursive = <
  T extends Record<string, any>,
  K extends keyof T,
>(
  obj: T,
  prop: K,
): Omit<T, K> => {
  return Object.keys(obj).reduce((memo, key) => {
    if (key === prop) {
      return memo;
    }
    if (typeof obj[key] === 'object') {
      memo[key] = removePropertyRecursive(obj[key], prop);
    } else {
      memo[key] = obj[key];
    }
    return memo;
  }, {} as any);
};

export const lowercaseObjectKeys = <T extends Record<string, any>>(map: T) =>
  Object.fromEntries(Object.entries(map).map(([k, v]) => [k.toLowerCase(), v]));
