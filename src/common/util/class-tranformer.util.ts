import { Transform } from 'class-transformer';
import { isNumeric } from './type-validate.util';

export const ToInt = () =>
  Transform(({ value }) => (isNumeric(value) ? parseInt(value) : value));
