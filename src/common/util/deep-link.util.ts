export const toDeepLink = (url: string, data: Record<string, any>) => {
  const urlObj = new URL(url);
  urlObj.searchParams.set(
    'data',
    Buffer.from(JSON.stringify(data)).toString('base64'),
  );
  return urlObj.toString();
};
