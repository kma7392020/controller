export const authorizationHeader = (token: string) => ({
  authorization: `Bearer ${token}`,
});

export const tokenFromAuthorizationHeader = (authorization: string) => {
  const [, token] = authorization.split(' ');
  return token;
};
