import { applyDecorators } from '@nestjs/common';
import { IsInt } from 'class-validator';
import { ToInt } from './class-tranformer.util';

export const IsIntEnv = () => applyDecorators(IsInt(), ToInt());
