import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { AllowUnauthenticated } from './module/auth/decorator/allow-unauthenticated.decorator';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  @AllowUnauthenticated()
  getHello(): string {
    return this.appService.getHello();
  }
}
