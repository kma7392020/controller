import { Mutation, Resolver, Subscription } from '@nestjs/graphql';
import { AppService } from './app.service';
import { Query } from '@nestjs/graphql';
import { PubSubService } from './module/pub-sub/pub-sub.service';
import { HELLO_SAID } from './module/pub-sub/pub-sub.trigger';

@Resolver()
export class AppResolver {
  constructor(
    private readonly service: AppService,
    private readonly pubSub: PubSubService,
  ) {}

  @Query(() => String)
  hello() {
    return this.service.getHello();
  }

  @Subscription(() => String, {
    name: HELLO_SAID,
  })
  helloSaid() {
    return this.pubSub.asyncIterator(HELLO_SAID);
  }

  @Mutation(() => String)
  async sayHello() {
    const wordToSay = 'Hello World!';

    await this.pubSub.publish(HELLO_SAID, {
      [HELLO_SAID]: wordToSay,
    });
    return wordToSay;
  }
}
