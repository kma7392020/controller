import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import {
  ApolloServerPluginLandingPageLocalDefault,
  Context,
} from 'apollo-server-core';
import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';
import { GraphQLModule } from '@nestjs/graphql';
import { AppResolver } from './app.resolver';
import { loggingMiddleware, PrismaModule } from 'nestjs-prisma';
import { AuthModule } from './module/auth/auth.module';
import { AxiosModule } from './module/axios/axios.module';
import { lowercaseObjectKeys } from './common/util/object.util';
import { AgentAdminModule } from './module/agent-admin/agent-admin.module';
import { AppEnvModule } from './module/app-env/app-env.module';
import { ConfigService } from '@nestjs/config';
import { getAppEnvFrom } from './module/app-env/app-env.factory';
import { WalletWebhookModule } from './module/wallet-webhook/wallet-webhook.module';
import { CredentialExchangeModule } from './module/credential-exchange/credential-exchange.module';
import { SchemaModule } from './module/schema/schema.module';
import { CredentialDefinitionModule } from './module/credential-definition/credential-definition.module';
import { AgentConnectionModule } from './module/agent-connection/agent-connection.module';
import { DeeplinkModule } from './module/deeplink/deeplink.module';
import { ScanLinkModule } from './module/scan-link/scan-link.module';
import { ProofExchangeModule } from './module/proof-exchange/proof-exchange.module';
import { PubSubModule } from './module/pub-sub/pub-sub.module';
import { OtpModule } from './module/otp/otp.module';
import { AccessTokenModule } from './module/access-token/access-token.module';
import { WalletModule } from './module/wallet/wallet.module';
import { DeviceModule } from './module/device/device.module';
import { FirebaseModule } from './module/firebase/firebase.module';

@Module({
  imports: [
    GraphQLModule.forRoot<ApolloDriverConfig>({
      driver: ApolloDriver,
      playground: false,
      autoSchemaFile: true,
      introspection: true,
      plugins: [
        ApolloServerPluginLandingPageLocalDefault({
          embed: true,
        }),
      ],
      sortSchema: true,
      subscriptions: {
        'graphql-ws': {
          onConnect: (context: Context<any>) => {
            const { connectionParams, extra } = context;
            const params = lowercaseObjectKeys(connectionParams);
            const { authorization } = params;

            extra.authorization = authorization;
          },
        },
      },
      context: (context) => {
        const authorization = context?.extra?.authorization
          ? {
              authorization: context.extra.authorization,
            }
          : {};
        const req = context.req || {};
        const headers = req.headers || {};

        context.req = {
          ...req,
          headers: {
            ...headers,
            ...authorization,
          },
        };

        return context;
      },
    }),
    PrismaModule.forRootAsync({
      isGlobal: true,
      useFactory: (configService: ConfigService) => {
        const { isDev } = getAppEnvFrom(configService);

        return {
          prismaOptions: {
            log: isDev
              ? [
                  'info',
                  'warn',
                  'error',
                  {
                    emit: 'event',
                    level: 'query',
                  },
                ]
              : ['warn', 'error'],
          },
          explicitConnect: !isDev,
          middlewares: [loggingMiddleware()],
        };
      },
      inject: [ConfigService],
    }),
    AuthModule,
    AxiosModule,
    AgentAdminModule,
    AppEnvModule,
    WalletWebhookModule,
    CredentialExchangeModule,
    SchemaModule,
    CredentialDefinitionModule,
    AgentConnectionModule,
    DeeplinkModule,
    ScanLinkModule,
    ProofExchangeModule,
    PubSubModule,
    OtpModule,
    AccessTokenModule,
    WalletModule,
    DeviceModule,
    FirebaseModule,
  ],
  controllers: [AppController],
  providers: [AppService, AppResolver],
})
export class AppModule {}
