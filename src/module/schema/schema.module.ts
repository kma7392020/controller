import { Module } from '@nestjs/common';
import { SchemaService } from './schema.service';
import { AgentAdminModule } from '../agent-admin/agent-admin.module';

@Module({
  imports: [AgentAdminModule],
  providers: [SchemaService],
  exports: [SchemaService],
})
export class SchemaModule {}
