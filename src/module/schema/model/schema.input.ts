import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class SchemaInput {
  @Field(() => [String])
  attrNames!: string[];

  @Field()
  name!: string;

  @Field()
  version!: string;
}
