import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class SchemaObject {
  @Field(() => [String], {
    nullable: true,
  })
  attrNames?: string[];

  @Field({
    nullable: true,
  })
  id?: string;

  @Field({
    nullable: true,
  })
  name?: string;

  @Field({
    nullable: true,
  })
  seqNo?: number;

  @Field({
    nullable: true,
  })
  version?: string;

  @Field({
    nullable: true,
  })
  ver?: string;
}
