import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { AgentAdminService } from '../agent-admin/agent-admin.service';
import { SchemaInput } from './model/schema.input';
import { SchemaObject } from './model/schema.object';

@Injectable()
export class SchemaService {
  constructor(private readonly agentAdmin: AgentAdminService) {}

  async get(authorization: string, schemaId: string) {
    const {
      data: { schema },
    } = await this.agentAdmin.schemas.schemasDetail(schemaId, {
      headers: {
        authorization,
      },
    });
    if (!schema) {
      throw new InternalServerErrorException('Failed to get schema');
    }

    return schema;
  }

  async create(
    authorization: string,
    { attrNames, name, version }: SchemaInput,
  ): Promise<SchemaObject> {
    const {
      data: { sent },
    } = await this.agentAdmin.schemas.schemasCreate(
      {
        attributes: attrNames,
        schema_name: name,
        schema_version: version,
      },
      {},
      {
        headers: {
          authorization,
        },
      },
    );
    const { schema_id, schema } = sent || {};
    if (!schema_id || !schema) {
      throw new InternalServerErrorException('Failed to create schema');
    }

    return schema;
  }

  async getAll(authorization: string): Promise<SchemaObject[]> {
    const {
      data: { schema_ids },
    } = await this.agentAdmin.schemas.createdList(
      {},
      {
        headers: {
          authorization,
        },
      },
    );
    if (!schema_ids) {
      throw new InternalServerErrorException('Failed to get schemas');
    }

    return Promise.all(
      schema_ids.map(async (id) => {
        const {
          data: { schema },
        } = await this.agentAdmin.schemas.schemasDetail(id, {
          headers: {
            authorization,
          },
        });

        if (!schema) {
          throw new InternalServerErrorException('Failed to get schema');
        }

        return schema;
      }),
    );
  }
}
