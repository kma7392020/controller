import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { JwtPayloadType } from '../type/jwt-payload.type';
import { AppEnvType, InjectAppEnv } from '../../app-env/app-env.factory';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(@InjectAppEnv() appEnv: AppEnvType) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: appEnv.JWT_SECRET,
    });
  }

  async validate({ wallet_id }: JwtPayloadType): Promise<string> {
    return wallet_id;
  }
}
