import {
  HttpException,
  Injectable,
  InternalServerErrorException,
  Logger,
  UnauthorizedException,
} from '@nestjs/common';
import { PrismaService } from 'nestjs-prisma';
import { LoginInput } from './model/login.input';
import { AuthorizableObject } from './model/authorizable.object';
import { RegisterInput } from './model/register.input';
import { compare } from 'bcrypt';
import { hashPassword } from './auth.util';
import { AgentAdminService } from '../agent-admin/agent-admin.service';
import { authorizationHeader } from '../../common/util/http-header.util';
import { AxiosService } from '../axios/axios.service';
import { AppEnvType, InjectAppEnv } from '../app-env/app-env.factory';
import { AccessTokenService } from '../access-token/access-token.service';

@Injectable()
export class AuthService {
  private readonly logger = new Logger(AuthService.name);

  constructor(
    private readonly prisma: PrismaService,
    private readonly agentAdmin: AgentAdminService,
    private readonly axios: AxiosService,
    @InjectAppEnv() private readonly appEnv: AppEnvType,
    private readonly accessTokenService: AccessTokenService,
  ) {}

  async login({ email, password }: LoginInput): Promise<AuthorizableObject> {
    const wallet = await this.prisma.wallet.findUnique({
      where: {
        email,
      },
    });
    if (!wallet) {
      throw new UnauthorizedException();
    }

    const isPasswordMatch = await compare(password, wallet.passwordHash);
    if (!isPasswordMatch) {
      throw new UnauthorizedException();
    }

    return {
      wallet,
      accessToken: await this.accessTokenService.getOrCreateValidToken(wallet),
    };
  }

  async register(input: RegisterInput): Promise<AuthorizableObject> {
    const { email, password } = input;

    const { BASE_WEBHOOK_URL } = this.appEnv;
    const {
      data: { token, wallet_id },
    } = await this.agentAdmin.multitenancy.walletCreate({
      image_url: 'https://aries.ca/images/sample.png',
      key_management_mode: 'managed',
      label: `${email}-wallet`,
      wallet_dispatch_type: 'default',
      wallet_key: password,
      wallet_name: email,
      wallet_type: 'askar',
      wallet_webhook_urls: [`${BASE_WEBHOOK_URL}/wallet/${email}`],
    });

    if (!token || !wallet_id) {
      throw new InternalServerErrorException();
    }

    const {
      data: { result: localDid },
    } = await this.agentAdmin.wallet.didCreateCreate(
      {},
      {
        headers: {
          ...authorizationHeader(token),
        },
      },
    );

    const { did, verkey } = localDid || {};
    if (!did || !verkey) {
      throw new InternalServerErrorException();
    }
    await this.registerPublicDid(did, verkey);
    await this.agentAdmin.wallet.didPublicCreate(
      {
        did,
      },
      {
        headers: {
          ...authorizationHeader(token),
        },
      },
    );

    const wallet = await this.prisma.wallet.create({
      data: {
        id: wallet_id,
        email,
        passwordHash: await hashPassword(password),
        accessToken: token,
      },
    });

    return {
      wallet,
      accessToken: token,
    };
  }

  async registerPublicDid(did: string, verkey: string): Promise<void> {
    try {
      const { AGENT_LEDGER_URL } = this.appEnv;
      await this.axios.instance.post(
        `${AGENT_LEDGER_URL}/register`,
        {
          did,
          verkey,
          role: 'ENDORSER',
          alias: null,
        },
        {
          baseURL: '',
        },
      );
    } catch (e) {
      this.logger.error('Failed to register public did');
      console.log(e?.error);
      throw new HttpException('Failed to register public did', 500);
    }
  }
}
