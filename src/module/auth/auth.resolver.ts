import { Args, Mutation, Resolver } from '@nestjs/graphql';
import { AuthService } from './auth.service';
import { AllowUnauthenticated } from './decorator/allow-unauthenticated.decorator';
import { AuthorizableObject } from './model/authorizable.object';
import { LoginInput } from './model/login.input';
import { RegisterInput } from './model/register.input';

@Resolver()
export class AuthResolver {
  constructor(private readonly service: AuthService) {}

  @AllowUnauthenticated()
  @Mutation(() => AuthorizableObject)
  async login(@Args('data') data: LoginInput) {
    return this.service.login(data);
  }

  @AllowUnauthenticated()
  @Mutation(() => AuthorizableObject)
  async register(@Args('data') data: RegisterInput) {
    return this.service.register(data);
  }
}
