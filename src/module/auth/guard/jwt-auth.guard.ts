import {
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { GqlExecutionContext } from '@nestjs/graphql';
import { Reflector } from '@nestjs/core';
import { ALLOW_UNAUTHENTICATED } from '../auth.const';

@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') {
  constructor(private readonly reflector: Reflector) {
    super();
  }

  getRequest(context: ExecutionContext) {
    const ctx = GqlExecutionContext.create(context);

    return ctx.getContext().req;
  }

  handleRequest(err: any, user: any, info: any, context: ExecutionContext) {
    const allowAny = this.reflector.get<string[]>(
      ALLOW_UNAUTHENTICATED,
      context.getHandler(),
    );

    if (user) return user;
    if (allowAny) return true;

    throw new UnauthorizedException();
  }
}
