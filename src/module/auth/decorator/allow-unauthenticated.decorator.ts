import { SetMetadata } from '@nestjs/common';
import { ALLOW_UNAUTHENTICATED } from '../auth.const';

export const AllowUnauthenticated = () =>
  SetMetadata(ALLOW_UNAUTHENTICATED, true);
