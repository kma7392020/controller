import { createParamDecorator } from '@nestjs/common';
import { walletIdFromContext } from '../auth.util';

export const CurrentWalletId = createParamDecorator((data, ctx) =>
  walletIdFromContext(ctx),
);
