import { hash } from 'bcrypt';
import { GqlExecutionContext } from '@nestjs/graphql';
import { ExecutionContext } from '@nestjs/common';

export const hashPassword = async (password: string) => hash(password, 10);
export const walletIdFromContext = (context: ExecutionContext) =>
  walletIdFromGqlContext(GqlExecutionContext.create(context).getContext());

export const walletIdFromGqlContext = (context: any) => context.req.user;
