import { Field, InputType, PickType } from '@nestjs/graphql';
import { WalletCreateInput } from '../../../@generated/type';

@InputType()
export class RegisterInput extends PickType(WalletCreateInput, [
  'email',
] as const) {
  @Field()
  password!: string;
}
