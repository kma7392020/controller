import { Field, ObjectType } from '@nestjs/graphql';
import { Wallet } from '../../../@generated/type';

@ObjectType()
export class AuthorizableObject {
  @Field(() => Wallet)
  wallet: Wallet;

  @Field()
  accessToken: string;
}
