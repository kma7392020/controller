import { Module } from '@nestjs/common';
import { APP_GUARD, Reflector } from '@nestjs/core';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { AuthService } from './auth.service';
import { JwtAuthGuard } from './guard/jwt-auth.guard';
import { JwtStrategy } from './strategy/jwt.strategy';
import { AuthResolver } from './auth.resolver';
import { ConfigService } from '@nestjs/config';
import { getAppEnvFrom } from '../app-env/app-env.factory';
import { AgentAdminModule } from '../agent-admin/agent-admin.module';
import { AccessTokenModule } from '../access-token/access-token.module';

@Module({
  imports: [
    PassportModule,
    JwtModule.registerAsync({
      useFactory: (configService: ConfigService) => {
        const { JWT_SECRET } = getAppEnvFrom(configService);

        return {
          secret: JWT_SECRET,
        };
      },
      inject: [ConfigService],
    }),
    AgentAdminModule,
    AccessTokenModule,
  ],
  providers: [
    AuthService,
    AuthResolver,
    JwtStrategy,
    {
      provide: APP_GUARD,
      useFactory: (ref) => new JwtAuthGuard(ref),
      inject: [Reflector],
    },
  ],
  exports: [AuthService],
})
export class AuthModule {}
