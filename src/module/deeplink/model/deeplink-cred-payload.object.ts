import { Field, ObjectType } from '@nestjs/graphql';
import { DeeplinkPayloadInterface } from './deeplink-payload.interface';
import { DeeplinkTypeEnum } from './deeplink-type.enum';
import { CredentialExchangeObject } from './credential-exchange.object';

@ObjectType({
  implements: () => [DeeplinkPayloadInterface],
})
export class DeeplinkCredPayloadObject implements DeeplinkPayloadInterface {
  id!: string;
  type!: DeeplinkTypeEnum;

  @Field(() => CredentialExchangeObject)
  credentialExchange!: CredentialExchangeObject;
}
