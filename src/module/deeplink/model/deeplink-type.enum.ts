import { registerEnumType } from '@nestjs/graphql';

export enum DeeplinkTypeEnum {
  CREDENTIAL = 'CREDENTIAL',
  PROOF = 'PROOF',
}

registerEnumType(DeeplinkTypeEnum, {
  name: 'DeeplinkType',
});
