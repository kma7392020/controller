import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class CredentialExchangeAttributeObject {
  @Field()
  name: string;

  @Field()
  value: string;
}
