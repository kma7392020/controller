import { Field, ObjectType } from '@nestjs/graphql';
import { CredentialExchange } from '../../../@generated/type';
import { CredentialExchangeAttributeObject } from './credential-exchange-attribute.object';

@ObjectType()
export class CredentialExchangeObject extends CredentialExchange {
  @Field(() => [CredentialExchangeAttributeObject])
  attributes: CredentialExchangeAttributeObject[];
}
