import { Field, InterfaceType } from '@nestjs/graphql';
import { DeeplinkTypeEnum } from './deeplink-type.enum';

@InterfaceType()
export abstract class DeeplinkPayloadInterface {
  @Field()
  id!: string;

  @Field(() => DeeplinkTypeEnum)
  type!: DeeplinkTypeEnum;
}
