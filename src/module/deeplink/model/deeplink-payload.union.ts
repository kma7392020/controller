import { createUnionType } from '@nestjs/graphql';
import { DeeplinkCredPayloadObject } from './deeplink-cred-payload.object';
import { DeeplinkProofPayloadObject } from './deeplink-proof-payload.object';
import { DeeplinkTypeEnum } from './deeplink-type.enum';

export const DeeplinkPayloadUnion = createUnionType({
  name: 'DeeplinkPayloadUnion',
  types: () => [DeeplinkCredPayloadObject, DeeplinkProofPayloadObject] as const,
  resolveType({ type }: { type: DeeplinkTypeEnum }) {
    if (type === DeeplinkTypeEnum.CREDENTIAL) {
      return DeeplinkCredPayloadObject;
    }
    if (type === DeeplinkTypeEnum.PROOF) {
      return DeeplinkProofPayloadObject;
    }
    return null;
  },
});
