import { ObjectType } from '@nestjs/graphql';
import { DeeplinkPayloadInterface } from './deeplink-payload.interface';
import { DeeplinkTypeEnum } from './deeplink-type.enum';

@ObjectType({
  implements: () => [DeeplinkPayloadInterface],
})
export class DeeplinkProofPayloadObject implements DeeplinkPayloadInterface {
  id!: string;
  type!: DeeplinkTypeEnum;
}
