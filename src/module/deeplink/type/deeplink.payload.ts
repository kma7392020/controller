export type DeeplinkPayload = {
  type: 'credential' | 'proof';
  id: string;
};
