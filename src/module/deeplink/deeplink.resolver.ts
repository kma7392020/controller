import { Args, Query, Resolver } from '@nestjs/graphql';
import { DeeplinkService } from './deeplink.service';
import { DeeplinkPayloadUnion } from './model/deeplink-payload.union';

@Resolver()
export class DeeplinkResolver {
  constructor(private readonly service: DeeplinkService) {}

  @Query(() => DeeplinkPayloadUnion)
  async parseDeeplink(
    @Args('deeplink') deeplink: string,
  ): Promise<typeof DeeplinkPayloadUnion> {
    return this.service.parse(deeplink);
  }
}
