import { Injectable } from '@nestjs/common';
import { AppEnvType, InjectAppEnv } from '../app-env/app-env.factory';
import { DeeplinkPayloadUnion } from './model/deeplink-payload.union';
import { PrismaService } from 'nestjs-prisma';
import { CredentialExchangeObject } from './model/credential-exchange.object';

@Injectable()
export class DeeplinkService {
  constructor(
    @InjectAppEnv() private readonly appEnv: AppEnvType,
    private readonly prisma: PrismaService,
  ) {}

  create(data: typeof DeeplinkPayloadUnion) {
    const { DEEPLINK_URL } = this.appEnv;
    const urlObj = new URL(DEEPLINK_URL);
    urlObj.searchParams.set(
      'data',
      Buffer.from(JSON.stringify(data)).toString('base64'),
    );
    return urlObj.toString();
  }

  async parse(deeplink: string): Promise<typeof DeeplinkPayloadUnion> {
    const urlObj = new URL(deeplink);
    const data = urlObj.searchParams.get('data');
    if (!data) {
      throw new Error('Invalid deeplink');
    }

    const { id, type }: typeof DeeplinkPayloadUnion = JSON.parse(
      Buffer.from(data, 'base64').toString(),
    );

    if (type === 'PROOF') {
      return {
        id,
        type,
      };
    }

    const credentialExchange =
      (await this.prisma.credentialExchange.findUniqueOrThrow({
        where: {
          id,
        },
      })) as unknown as CredentialExchangeObject;

    return {
      id,
      type,
      credentialExchange,
    };
  }
}
