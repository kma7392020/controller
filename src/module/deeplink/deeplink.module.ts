import { Module } from '@nestjs/common';
import { DeeplinkService } from './deeplink.service';
import { DeeplinkResolver } from './deeplink.resolver';

@Module({
  providers: [DeeplinkService, DeeplinkResolver],
  exports: [DeeplinkService],
})
export class DeeplinkModule {}
