import { Injectable } from '@nestjs/common';
import axios, { Axios } from 'axios';

@Injectable()
export class AxiosService {
  public readonly instance: Axios;
  constructor() {
    this.instance = axios.create();
  }
}
