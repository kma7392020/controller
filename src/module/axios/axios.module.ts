import { Global, Module } from '@nestjs/common';
import { AxiosService } from './axios.service';

@Module({
  providers: [AxiosService],
  exports: [AxiosService],
})
@Global()
export class AxiosModule {}
