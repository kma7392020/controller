export const HELLO_SAID = 'helloSaid';
export const CREDENTIAL_ISSUED = 'credentialIssued';
export const PROOF_SENT = 'proofSent';
export const PROOF_EXCHANGE_REQ_RECEIVED = 'proofExchangeReqReceived';
