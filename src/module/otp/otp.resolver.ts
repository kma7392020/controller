import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { OtpService } from './otp.service';
import { Otp } from '../../@generated/type';
import { CurrentWalletId } from '../auth/decorator/current-wallet-id.decorator';

@Resolver()
export class OtpResolver {
  constructor(private readonly service: OtpService) {}

  @Mutation(() => Otp)
  async deleteOtp(@Args('id') id: string): Promise<Otp> {
    return this.service.delete(id);
  }

  @Mutation(() => Otp)
  async scanOtpDeeplink(
    @CurrentWalletId() walletId: string,
    @Args('deeplink') deeplink: string,
  ): Promise<Otp> {
    return this.service.createOtp(walletId, deeplink);
  }

  @Query(() => [Otp])
  async allOtps(@CurrentWalletId() walletId: string): Promise<Otp[]> {
    return this.service.all(walletId);
  }
}
