import { OtpAlgorithm, OtpCreateInput } from '../../@generated/type';

export const parseOtpDeeplink = (
  deeplink: string,
): Pick<
  OtpCreateInput,
  'algorithm' | 'digits' | 'secret' | 'issuer' | 'accountName'
> => {
  const url = new URL(deeplink);
  const alg = url.host.toLowerCase();
  const algorithm = alg === 'totp' ? OtpAlgorithm.TOTP : OtpAlgorithm.HOTP;
  const secret = url.searchParams.get('secret');
  if (!secret) {
    throw new Error('secret is required');
  }
  const digits = url.searchParams.get('digits') || '6';
  if (digits && !/^\d+$/.test(digits)) {
    throw new Error('digits must be a number');
  }
  const issuer = url.searchParams.get('issuer');
  if (!issuer) {
    throw new Error('issuer is required');
  }
  const accountName = url.pathname.replace('/', '');

  return {
    algorithm,
    digits: Number(digits),
    secret,
    issuer,
    accountName,
  };
};
