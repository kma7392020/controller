import { Injectable } from '@nestjs/common';
import { PrismaService } from 'nestjs-prisma';
import { Otp } from '../../@generated/type';
import { parseOtpDeeplink } from './otp.util';

@Injectable()
export class OtpService {
  constructor(private readonly prisma: PrismaService) {}

  async all(walletId: string): Promise<Otp[]> {
    return this.prisma.otp.findMany({
      where: {
        walletId,
      },
    });
  }

  async delete(id: string): Promise<Otp> {
    return this.prisma.otp.delete({
      where: { id },
    });
  }

  async createOtp(walletId: string, deeplink: string): Promise<Otp> {
    const { algorithm, digits, secret, issuer, accountName } =
      parseOtpDeeplink(deeplink);

    return this.prisma.otp.create({
      data: {
        algorithm,
        digits,
        secret,
        issuer,
        accountName,
        wallet: {
          connect: {
            id: walletId,
          },
        },
      },
    });
  }
}
