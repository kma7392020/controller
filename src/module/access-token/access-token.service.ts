import { Injectable, UnauthorizedException } from '@nestjs/common';
import { Wallet } from '../../@generated/type';
import { authorizationHeader } from '../../common/util/http-header.util';
import { PrismaService } from 'nestjs-prisma';
import { AgentAdminService } from '../agent-admin/agent-admin.service';

@Injectable()
export class AccessTokenService {
  constructor(
    private readonly prisma: PrismaService,
    private readonly agentAdmin: AgentAdminService,
  ) {}

  async tokenFromWalletId(walletId: string): Promise<string> {
    const wallet = await this.prisma.wallet.findUnique({
      where: {
        id: walletId,
      },
    });
    if (!wallet) {
      throw new UnauthorizedException();
    }

    return await this.getOrCreateValidToken(wallet);
  }

  async getOrCreateValidToken(wallet: Wallet): Promise<string> {
    const { accessToken } = wallet;

    if (await this.isTokenValid(accessToken)) {
      return accessToken;
    }

    const token = await this.createToken(wallet.id);

    await this.prisma.wallet.update({
      where: {
        id: wallet.id,
      },
      data: {
        accessToken: token,
      },
    });

    return token;
  }

  async createToken(walletId: string): Promise<string> {
    const token = (
      await this.agentAdmin.multitenancy.walletTokenCreate(walletId, {})
    ).data?.token;

    if (!token) {
      throw new UnauthorizedException();
    }

    return token;
  }

  async isTokenValid(accessToken: string): Promise<boolean> {
    try {
      await this.agentAdmin.wallet.didPublicList({
        headers: {
          ...authorizationHeader(accessToken),
        },
      });

      return true;
    } catch (e) {
      return false;
    }
  }
}
