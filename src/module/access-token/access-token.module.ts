import { Module } from '@nestjs/common';
import { AccessTokenService } from './access-token.service';
import { AgentAdminModule } from '../agent-admin/agent-admin.module';

@Module({
  imports: [AgentAdminModule],
  providers: [AccessTokenService],
  exports: [AccessTokenService],
})
export class AccessTokenModule {}
