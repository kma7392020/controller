import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { AgentAdminService } from '../agent-admin/agent-admin.service';
import { CredentialDefinitionInput } from './model/credential-definition.input';
import { CredentialDefinitionObject } from './model/credential-definition.object';
import { SchemaService } from '../schema/schema.service';
import { AgentConnectionService } from '../agent-connection/agent-connection.service';

@Injectable()
export class CredentialDefinitionService {
  constructor(
    private readonly agentAdmin: AgentAdminService,
    private readonly schemaService: SchemaService,
    private readonly agentConnectionService: AgentConnectionService,
  ) {}

  async create(
    authorization: string,
    {
      schema,
      supportRevocation,
      revocationRegistrySize,
      tag,
    }: CredentialDefinitionInput,
  ): Promise<CredentialDefinitionObject> {
    const createdSchema = await this.schemaService.create(
      authorization,
      schema,
    );
    const {
      data: { sent },
    } = await this.agentAdmin.credentialDefinitions.credentialDefinitionsCreate(
      {
        tag,
        revocation_registry_size: revocationRegistrySize,
        support_revocation: supportRevocation,
        schema_id: createdSchema.id,
      },
      {},
      {
        headers: {
          authorization,
        },
      },
    );
    const { credential_definition_id } = sent || {};
    if (!credential_definition_id) {
      throw new InternalServerErrorException(
        'Failed to create credential definition',
      );
    }

    return {
      id: credential_definition_id,
      tag,
      schema: createdSchema,
    };
  }

  async getAll(authorization: string): Promise<CredentialDefinitionObject[]> {
    const {
      data: { credential_definition_ids },
    } = await this.agentAdmin.credentialDefinitions.createdList(
      {},
      {
        headers: {
          authorization,
        },
      },
    );
    if (!credential_definition_ids) {
      throw new InternalServerErrorException(
        'Failed to get credential definitions',
      );
    }

    const allSchemas = await this.schemaService.getAll(authorization);

    return Promise.all(
      credential_definition_ids.map(
        async (credId): Promise<CredentialDefinitionObject> => {
          const {
            data: { credential_definition },
          } =
            await this.agentAdmin.credentialDefinitions.credentialDefinitionsDetail(
              credId,
              {
                headers: {
                  authorization,
                },
              },
            );
          if (!credential_definition) {
            throw new InternalServerErrorException(
              'Failed to get credential definition',
            );
          }

          const { schemaId } = credential_definition;

          return {
            ...credential_definition,
            schema: allSchemas.find((s) => s.seqNo?.toString() === schemaId),
          };
        },
      ),
    );
  }
}
