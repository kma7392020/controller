import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { CredentialDefinitionService } from './credential-definition.service';
import { CredentialDefinitionObject } from './model/credential-definition.object';
import { CredentialDefinitionInput } from './model/credential-definition.input';
import { AuthorizationHeader } from '../auth/decorator/authorization-header.decorator';

@Resolver()
export class CredentialDefinitionResolver {
  constructor(private readonly service: CredentialDefinitionService) {}

  @Query(() => [CredentialDefinitionObject])
  async allCredentialDefinitions(@AuthorizationHeader() authorization: string) {
    return this.service.getAll(authorization);
  }

  @Mutation(() => CredentialDefinitionObject)
  async createCredentialDefinition(
    @AuthorizationHeader() authorization: string,
    @Args('data') input: CredentialDefinitionInput,
  ) {
    return this.service.create(authorization, input);
  }
}
