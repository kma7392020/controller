import { Field, ObjectType } from '@nestjs/graphql';
import { SchemaObject } from '../../schema/model/schema.object';

@ObjectType()
export class CredentialDefinitionObject {
  @Field({
    nullable: true,
  })
  id?: string;

  @Field({
    nullable: true,
  })
  schema?: SchemaObject;

  @Field({
    nullable: true,
  })
  tag?: string;
}
