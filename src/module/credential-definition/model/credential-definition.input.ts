import { Field, InputType } from '@nestjs/graphql';
import { SchemaInput } from '../../schema/model/schema.input';

@InputType()
export class CredentialDefinitionInput {
  @Field({
    defaultValue: 1000,
  })
  revocationRegistrySize!: number;

  @Field({
    defaultValue: false,
  })
  supportRevocation!: boolean;

  @Field({
    defaultValue: 'default',
  })
  tag!: string;

  @Field(() => SchemaInput)
  schema!: SchemaInput;
}
