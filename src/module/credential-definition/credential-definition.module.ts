import { Module } from '@nestjs/common';
import { CredentialDefinitionService } from './credential-definition.service';
import { CredentialDefinitionResolver } from './credential-definition.resolver';
import { SchemaModule } from '../schema/schema.module';
import { AgentAdminModule } from '../agent-admin/agent-admin.module';
import { AgentConnectionModule } from '../agent-connection/agent-connection.module';

@Module({
  imports: [SchemaModule, AgentAdminModule, AgentConnectionModule],
  providers: [CredentialDefinitionService, CredentialDefinitionResolver],
})
export class CredentialDefinitionModule {}
