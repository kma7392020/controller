import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class CredentialAttributeInput {
  @Field()
  name!: string;

  @Field()
  value!: string;
}
