import { Field, ObjectType } from '@nestjs/graphql';
import { GraphQLJSON } from 'graphql-type-json';

@ObjectType()
export class IndyCredentialObject {
  @Field({
    nullable: true,
    description: 'credId - Credential identifier',
  })
  referent?: string;

  @Field({
    nullable: true,
  })
  schema_id?: string;

  @Field({
    nullable: true,
  })
  cred_def_id?: string;

  @Field(() => GraphQLJSON, {
    nullable: true,
  })
  attrs?: Record<string, string>;
}
