import { Field, InputType } from '@nestjs/graphql';
import { CredentialAttributeInput } from './credential-attribute.input';

@InputType()
export class CredentialExchangeInput {
  @Field()
  schemaId!: string;

  @Field()
  defCredId!: string;

  @Field(() => [CredentialAttributeInput])
  attributes!: CredentialAttributeInput[];
}
