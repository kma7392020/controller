import { CredentialExchange } from '../../../@generated/type';
import { CredentialAttributeInput } from '../model/credential-attribute.input';
import { V20CredFilterIndy } from '../../../@generated/agent-admin-api/type';

export class CredentialExchangeRecord extends CredentialExchange {
  attributes: CredentialAttributeInput[];

  indyFilter: Required<V20CredFilterIndy>;
}
