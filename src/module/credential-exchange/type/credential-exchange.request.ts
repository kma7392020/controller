import { CredentialAttributeInput } from '../model/credential-attribute.input';
import { V20CredFilterIndy } from '../../../@generated/agent-admin-api/type';
import { CredentialExchangeCreateInput } from '../../../@generated/type';

export class CredentialExchangeRequest extends CredentialExchangeCreateInput {
  attributes: CredentialAttributeInput[];

  indyFilter: Required<V20CredFilterIndy>;
}
