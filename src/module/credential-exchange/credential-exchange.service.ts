import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { AgentAdminService } from '../agent-admin/agent-admin.service';
import { PrismaService } from 'nestjs-prisma';
import { CredentialExchangeInput } from './model/credential-exchange.input';
import { DeeplinkService } from '../deeplink/deeplink.service';
import { AgentConnectionService } from '../agent-connection/agent-connection.service';
import { CredentialExchangeRequest } from './type/credential-exchange.request';
import { SchemaService } from '../schema/schema.service';
import { CredentialExchangeRecord } from './type/credential-exchange.record';
import { authorizationHeader } from '../../common/util/http-header.util';
import { DeeplinkTypeEnum } from '../deeplink/model/deeplink-type.enum';
import { AccessTokenService } from '../access-token/access-token.service';
import { ConnRecord } from '../../@generated/agent-admin-api/type';
import { DeeplinkPayloadUnion } from '../deeplink/model/deeplink-payload.union';

@Injectable()
export class CredentialExchangeService {
  constructor(
    private readonly agentAdmin: AgentAdminService,
    private readonly prisma: PrismaService,
    private readonly deepLinkService: DeeplinkService,
    private readonly agentConnectionService: AgentConnectionService,
    private readonly schemaService: SchemaService,
    private readonly accessTokenService: AccessTokenService,
  ) {}

  async sendCredential(credentialExchangeId: string) {
    const { issuerConnectionId, attributes, indyFilter, issuerWalletId } =
      (await this.prisma.credentialExchange.findUniqueOrThrow({
        where: {
          id: credentialExchangeId,
        },
      })) as unknown as CredentialExchangeRecord;
    if (!issuerConnectionId) {
      throw new InternalServerErrorException('Connection not established');
    }

    const issuerToken = await this.accessTokenService.tokenFromWalletId(
      issuerWalletId,
    );

    await this.agentAdmin.issueCredential20.sendCreate(
      {
        auto_remove: true,
        comment: `Credential exchange ${credentialExchangeId}`,
        connection_id: issuerConnectionId,
        credential_preview: {
          '@type': 'issue-credential/2.0/credential-preview',
          attributes,
        },
        filter: {
          indy: indyFilter,
        },
        trace: false,
      },
      {
        headers: {
          ...authorizationHeader(issuerToken),
        },
      },
    );

    await this.prisma.credentialExchange.delete({
      where: {
        id: credentialExchangeId,
      },
    });
  }

  async handleAcceptCredentialExchange(
    holderAuthorization: string,
    holderWalletId: string,
    credentialExchangeId: string,
  ) {
    const { issuerWalletId } =
      (await this.prisma.credentialExchange.findUniqueOrThrow({
        where: {
          id: credentialExchangeId,
        },
      })) as unknown as CredentialExchangeRecord;

    await this.setupConnection(
      issuerWalletId,
      holderAuthorization,
      holderWalletId,
      credentialExchangeId,
    );
  }

  async setupConnection(
    issuerWalletId: string,
    holderAuthorization: string,
    holderWalletId: string,
    credentialExchangeId: string,
  ) {
    const activeConnection =
      await this.agentConnectionService.getActiveConnection(
        issuerWalletId,
        holderWalletId,
      );

    if (activeConnection) {
      return await this.setupActiveConnection(
        activeConnection,
        issuerWalletId,
        holderAuthorization,
        holderWalletId,
        credentialExchangeId,
      );
    }

    return await this.setupNewConnection(
      issuerWalletId,
      holderAuthorization,
      holderWalletId,
      credentialExchangeId,
    );
  }

  async setupActiveConnection(
    activeConnection: ConnRecord,
    issuerWalletId: string,
    holderAuthorization: string,
    holderWalletId: string,
    credentialExchangeId: string,
  ) {
    await this.prisma.credentialExchange.update({
      where: {
        id: credentialExchangeId,
      },
      data: {
        issuerConnectionId: activeConnection.connection_id,
      },
    });

    await this.sendCredential(credentialExchangeId);
  }

  async setupNewConnection(
    issuerWalletId: string,
    holderAuthorization: string,
    holderWalletId: string,
    credentialExchangeId: string,
  ) {
    const issuerToken = await this.accessTokenService.tokenFromWalletId(
      issuerWalletId,
    );

    const {
      data: { invitation, connection_id },
    } = await this.agentAdmin.connections.createInvitationCreate(
      {},
      {},
      {
        headers: {
          ...authorizationHeader(issuerToken),
        },
      },
    );
    if (!invitation) {
      throw new InternalServerErrorException('Failed to create invitation');
    }

    await this.prisma.credentialExchange.update({
      where: {
        id: credentialExchangeId,
      },
      data: {
        issuerConnectionId: connection_id,
      },
    });

    const { data } = await this.agentAdmin.connections.receiveInvitationCreate(
      invitation,
      {},
      {
        headers: {
          authorization: holderAuthorization,
        },
      },
    );
    if (!data) {
      throw new InternalServerErrorException('Failed to receive invitation');
    }

    return data;
  }

  async createCredentialExchange(
    walletId: string,
    authorization: string,
    { schemaId, attributes, defCredId }: CredentialExchangeInput,
  ) {
    const did = await this.agentConnectionService.getPublicDid(authorization);
    const { name: schemaName, version: schemaVersion } =
      await this.schemaService.get(authorization, schemaId);
    if (!schemaName || !schemaVersion) {
      throw new InternalServerErrorException('Schema not found');
    }

    const createData: CredentialExchangeRequest = {
      attributes,
      indyFilter: {
        schema_issuer_did: did,
        issuer_did: did,
        cred_def_id: defCredId,
        schema_id: schemaId,
        schema_name: schemaName,
        schema_version: schemaVersion,
      },
      issuerWalletId: walletId,
    };
    const credExchange = await this.prisma.credentialExchange.create({
      data: createData as any,
    });

    const deeplinkPayload: typeof DeeplinkPayloadUnion = {
      id: credExchange.id,
      type: DeeplinkTypeEnum.CREDENTIAL,
    };

    return this.deepLinkService.create(deeplinkPayload);
  }

  async credentials(authorization: string) {
    const {
      data: { results },
    } = await this.agentAdmin.credentials.credentialsList(
      {
        count: '100',
      },
      {
        headers: {
          authorization,
        },
      },
    );

    return results || [];
  }
}
