import { Module } from '@nestjs/common';
import { CredentialExchangeService } from './credential-exchange.service';
import { AgentAdminModule } from '../agent-admin/agent-admin.module';
import { DeeplinkModule } from '../deeplink/deeplink.module';
import { CredentialExchangeResolver } from './credential-exchange.resolver';
import { AgentConnectionModule } from '../agent-connection/agent-connection.module';
import { SchemaModule } from '../schema/schema.module';
import { AccessTokenModule } from '../access-token/access-token.module';

@Module({
  imports: [
    AgentAdminModule,
    DeeplinkModule,
    AgentConnectionModule,
    SchemaModule,
    AccessTokenModule,
  ],
  providers: [CredentialExchangeService, CredentialExchangeResolver],
  exports: [CredentialExchangeService],
})
export class CredentialExchangeModule {}
