import { Args, Mutation, Query, Resolver, Subscription } from '@nestjs/graphql';
import { CredentialExchangeService } from './credential-exchange.service';
import { CurrentWalletId } from '../auth/decorator/current-wallet-id.decorator';
import { CredentialExchangeInput } from './model/credential-exchange.input';
import { IndyCredentialObject } from './model/indy-credential.object';
import { PubSubService } from '../pub-sub/pub-sub.service';
import { CREDENTIAL_ISSUED } from '../pub-sub/pub-sub.trigger';
import { walletIdFromGqlContext } from '../auth/auth.util';
import { AuthorizationHeader } from '../auth/decorator/authorization-header.decorator';

@Resolver()
export class CredentialExchangeResolver {
  constructor(
    private readonly service: CredentialExchangeService,
    private readonly pubSub: PubSubService,
  ) {}

  @Mutation(() => String)
  async createCredentialExchangeUrl(
    @CurrentWalletId() walletId: string,
    @AuthorizationHeader() authorization: string,
    @Args('data') data: CredentialExchangeInput,
  ) {
    return this.service.createCredentialExchange(walletId, authorization, data);
  }

  @Query(() => [IndyCredentialObject])
  async credentials(@AuthorizationHeader() authorization: string) {
    return this.service.credentials(authorization);
  }

  @Subscription(() => IndyCredentialObject, {
    name: CREDENTIAL_ISSUED,
    filter: (payload, variables, context) => {
      return (
        payload[CREDENTIAL_ISSUED].walletId === walletIdFromGqlContext(context)
      );
    },
  })
  async credentialIssued() {
    return this.pubSub.asyncIterator(CREDENTIAL_ISSUED);
  }
}
