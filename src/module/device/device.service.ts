import { Injectable } from '@nestjs/common';
import { PrismaService } from 'nestjs-prisma';

@Injectable()
export class DeviceService {
  constructor(private readonly prisma: PrismaService) {}

  async addDevice(walletId: string, token: string) {
    const device = await this.prisma.device.upsert({
      where: {
        token_walletId: {
          token,
          walletId,
        },
      },
      create: {
        token,
        walletId,
      },
      update: {},
    });

    return device;
  }
}
