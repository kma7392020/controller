import { Args, Mutation, Resolver } from '@nestjs/graphql';
import { Device } from 'src/@generated/type';
import { CurrentWalletId } from '../auth/decorator/current-wallet-id.decorator';
import { DeviceService } from './device.service';

@Resolver()
export class DeviceResolver {
  constructor(private readonly service: DeviceService) {}

  @Mutation(() => Device)
  async addDevice(
    @Args('token') token: string,
    @CurrentWalletId() walletId: string,
  ) {
    return this.service.addDevice(walletId, token);
  }
}
