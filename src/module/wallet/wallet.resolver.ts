import { Query, Resolver } from '@nestjs/graphql';
import { WalletService } from './wallet.service';
import { Wallet } from '../../@generated/type';
import { CurrentWalletId } from '../auth/decorator/current-wallet-id.decorator';
import { UnauthorizedException } from '@nestjs/common';

@Resolver()
export class WalletResolver {
  constructor(private readonly service: WalletService) {}

  @Query(() => [Wallet])
  async wallets(): Promise<Wallet[]> {
    return this.service.findAll();
  }

  @Query(() => Wallet)
  async authenticatedWallet(
    @CurrentWalletId() currentWalletId: string,
  ): Promise<Wallet> {
    const wallet = await this.service.findOne(currentWalletId);
    if (!wallet) {
      throw new UnauthorizedException();
    }

    return wallet;
  }
}
