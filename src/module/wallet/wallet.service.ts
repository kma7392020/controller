import { Injectable } from '@nestjs/common';
import { PrismaService } from 'nestjs-prisma';

@Injectable()
export class WalletService {
  constructor(private readonly prisma: PrismaService) {}

  async findOne(id: string) {
    return this.prisma.wallet.findUnique({
      where: {
        id,
      },
    });
  }

  async findAll() {
    return this.prisma.wallet.findMany();
  }
}
