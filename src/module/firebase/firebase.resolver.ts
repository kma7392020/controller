import { Args, Query, Resolver } from '@nestjs/graphql';
import { MessagePayload } from 'src/@generated/type';
import { FirebaseService } from './firebase.service';

@Resolver()
export class FirebaseResolver {
  constructor(private readonly service: FirebaseService) {}

  @Query(() => MessagePayload, {
    nullable: true,
  })
  async retrieveMessagePayload(
    @Args('payloadId') id: string,
  ): Promise<MessagePayload | null> {
    return this.service.retrieveMessagePayload(id);
  }
}
