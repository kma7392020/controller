import { Module } from '@nestjs/common';
import { FirebaseResolver } from './firebase.resolver';
import { FirebaseService } from './firebase.service';

@Module({
  providers: [FirebaseService, FirebaseResolver],
  exports: [FirebaseService],
})
export class FirebaseModule {}
