import { Injectable, Logger } from '@nestjs/common';
import * as admin from 'firebase-admin';
import { applicationDefault } from 'firebase-admin/app';
import { PrismaService } from 'nestjs-prisma';
import { MessagePayload } from './firebase.type';

@Injectable()
export class FirebaseService {
  private readonly logger = new Logger(FirebaseService.name);
  private readonly messaging: admin.messaging.Messaging;

  constructor(private readonly prisma: PrismaService) {
    admin.initializeApp({
      credential: applicationDefault(),
    });
    this.messaging = admin.messaging();
  }

  async retrieveMessagePayload(payloadId: string) {
    const payload = await this.prisma.messagePayload.findUnique({
      where: {
        id: payloadId,
      },
    });
    await this.prisma.messagePayload.delete({
      where: {
        id: payloadId,
      },
    });
    return payload;
  }

  async sendMessageToWallet(walletId: string, payload: MessagePayload) {
    const devices = await this.prisma.device.findMany({
      where: {
        walletId,
      },
    });

    if (!devices.length) {
      return;
    }

    const payloadRecord = await this.prisma.messagePayload.create({
      data: {
        payload: payload as any,
      },
    });

    const tokens = devices.map((device) => device.token);

    await this.messaging.sendMulticast({
      tokens,
      data: {
        payloadId: payloadRecord.id,
      },
    });

    this.logger.log(
      `Sent message to ${tokens.join(',')} devices of wallet ${walletId}`,
      payload,
    );
  }
}
