import { IndyCredentialObject } from '../credential-exchange/model/indy-credential.object';
import { ProofExchangeReqReceivedEventObject } from '../proof-exchange/model/proof-exchange-req-received-event.object';
import { ProofObject } from '../proof-exchange/model/proof.object';

export type MessageEventType =
  | 'CREDENTIAL_ISSUED'
  | 'PROOF_EXCHANGE_REQ_RECEIVED'
  | 'PROOF_SENT';

export type MessagePayload = {
  type: MessageEventType;
  data:
    | IndyCredentialObject
    | ProofExchangeReqReceivedEventObject
    | ProofObject;
};
