import { Module } from '@nestjs/common';
import { ScanLinkService } from './scan-link.service';
import { DeeplinkModule } from '../deeplink/deeplink.module';
import { CredentialExchangeModule } from '../credential-exchange/credential-exchange.module';
import { ScanLinkResolver } from './scan-link.resolver';
import { AgentConnectionModule } from '../agent-connection/agent-connection.module';
import { ProofExchangeModule } from '../proof-exchange/proof-exchange.module';

@Module({
  imports: [
    DeeplinkModule,
    CredentialExchangeModule,
    AgentConnectionModule,
    ProofExchangeModule,
  ],
  providers: [ScanLinkService, ScanLinkResolver],
})
export class ScanLinkModule {}
