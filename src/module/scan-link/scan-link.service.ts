import { Injectable } from '@nestjs/common';
import { DeeplinkService } from '../deeplink/deeplink.service';
import { CredentialExchangeService } from '../credential-exchange/credential-exchange.service';
import { AgentConnectionService } from '../agent-connection/agent-connection.service';
import { ProofExchangeService } from '../proof-exchange/proof-exchange.service';

@Injectable()
export class ScanLinkService {
  constructor(
    private readonly deepLinkService: DeeplinkService,
    private readonly credentialExchangeService: CredentialExchangeService,
    private readonly agentConnectionService: AgentConnectionService,
    private readonly proofExchangeService: ProofExchangeService,
  ) {}

  async handleCredentialDeepLink(
    holderWalletId: string,
    holderAuthorization: string,
    deepLink: string,
  ) {
    const { id } = await this.deepLinkService.parse(deepLink);

    return await this.credentialExchangeService.handleAcceptCredentialExchange(
      holderAuthorization,
      holderWalletId,
      id,
    );
  }

  async handleProofDeepLink(
    holderWalletId: string,
    holderAuthorization: string,
    deepLink: string,
  ) {
    const { id } = await this.deepLinkService.parse(deepLink);

    return await this.proofExchangeService.handleAcceptProofRequest(
      holderAuthorization,
      holderWalletId,
      id,
    );
  }

  async handleDeepLink(
    holderWalletId: string,
    holderAuthorization: string,
    deepLink: string,
  ) {
    const { id, type } = await this.deepLinkService.parse(deepLink);

    if (type === 'CREDENTIAL') {
      return await this.credentialExchangeService.handleAcceptCredentialExchange(
        holderAuthorization,
        holderWalletId,
        id,
      );
    }

    return await this.proofExchangeService.handleAcceptProofRequest(
      holderAuthorization,
      holderWalletId,
      id,
    );
  }
}
