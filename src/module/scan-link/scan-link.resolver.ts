import { Args, Mutation, Resolver } from '@nestjs/graphql';
import { ScanLinkService } from './scan-link.service';
import { CurrentWalletId } from '../auth/decorator/current-wallet-id.decorator';
import { AuthorizationHeader } from '../auth/decorator/authorization-header.decorator';

@Resolver()
export class ScanLinkResolver {
  constructor(private readonly service: ScanLinkService) {}

  @Mutation(() => Boolean)
  async handleAcceptCredentialDeepLink(
    @Args('url') deepLink: string,
    @CurrentWalletId() walletId: string,
    @AuthorizationHeader() authorization: string,
  ) {
    await this.service.handleCredentialDeepLink(
      walletId,
      authorization,
      deepLink,
    );
    return true;
  }

  @Mutation(() => Boolean)
  async handleProofDeepLink(
    @Args('url') deepLink: string,
    @CurrentWalletId() walletId: string,
    @AuthorizationHeader() authorization: string,
  ) {
    await this.service.handleProofDeepLink(walletId, authorization, deepLink);
    return true;
  }

  @Mutation(() => Boolean)
  async handleDeepLink(
    @Args('url') deepLink: string,
    @CurrentWalletId() walletId: string,
    @AuthorizationHeader() authorization: string,
  ) {
    await this.service.handleDeepLink(walletId, authorization, deepLink);
    return true;
  }
}
