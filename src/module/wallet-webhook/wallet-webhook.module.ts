import { Module } from '@nestjs/common';
import { WalletWebhookService } from './wallet-webhook.service';
import { WalletWebhookController } from './wallet-webhook.controller';
import { CredentialExchangeModule } from '../credential-exchange/credential-exchange.module';
import { ProofExchangeModule } from '../proof-exchange/proof-exchange.module';
import { AgentAdminModule } from '../agent-admin/agent-admin.module';
import { AccessTokenModule } from '../access-token/access-token.module';
import { FirebaseModule } from '../firebase/firebase.module';

@Module({
  imports: [
    CredentialExchangeModule,
    ProofExchangeModule,
    AgentAdminModule,
    AccessTokenModule,
    FirebaseModule,
  ],
  providers: [WalletWebhookService],
  controllers: [WalletWebhookController],
})
export class WalletWebhookModule {}
