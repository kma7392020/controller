import { Injectable, Logger } from '@nestjs/common';
import {
  ConnRecord,
  V20CredExRecordIndy,
  V20PresExRecord,
} from '../../@generated/agent-admin-api/type';
import { PrismaService } from 'nestjs-prisma';
import { CredentialExchangeService } from '../credential-exchange/credential-exchange.service';
import { ProofExchangeService } from '../proof-exchange/proof-exchange.service';
import { CREDENTIAL_ISSUED, PROOF_SENT } from '../pub-sub/pub-sub.trigger';
import { PubSubService } from '../pub-sub/pub-sub.service';
import { AccessTokenService } from '../access-token/access-token.service';
import { ProofExchangeState } from '../../@generated/type';
import { AgentAdminService } from '../agent-admin/agent-admin.service';
import { authorizationHeader } from 'src/common/util/http-header.util';
import { FirebaseService } from '../firebase/firebase.service';

@Injectable()
export class WalletWebhookService {
  private readonly logger = new Logger(WalletWebhookService.name);

  constructor(
    private readonly prisma: PrismaService,
    private readonly credentialExchangeService: CredentialExchangeService,
    private readonly proofExchangeService: ProofExchangeService,
    private readonly pubSub: PubSubService,
    private readonly accessTokenService: AccessTokenService,
    private readonly agentAdmin: AgentAdminService,
    private readonly firebase: FirebaseService,
  ) {}

  async handleConnectionRequest(walletName: string, connection: ConnRecord) {
    const { state, connection_id } = connection;
    if (state !== 'active' || !connection_id) {
      return;
    }

    const credentialExchange = await this.prisma.credentialExchange.findUnique({
      where: {
        issuerConnectionId: connection_id,
      },
    });
    if (credentialExchange) {
      return await this.credentialExchangeService.sendCredential(
        credentialExchange.id,
      );
    }

    const proofExchange = await this.prisma.proofExchange.findFirst({
      where: {
        verifierConnectionId: connection_id,
        state: ProofExchangeState.WAITING,
      },
    });
    if (proofExchange) {
      return await this.proofExchangeService.sendProofRequest(proofExchange.id);
    }
  }

  async handleCredentialStored(walletName: string, body: V20CredExRecordIndy) {
    const wallet = await this.prisma.wallet.findUnique({
      where: {
        email: walletName,
      },
    });
    if (!wallet) {
      return;
    }

    const { cred_id_stored } = body;
    if (!cred_id_stored) {
      return;
    }

    const token = await this.accessTokenService.tokenFromWalletId(wallet.id);
    const creds = await this.credentialExchangeService.credentials(
      `Bearer ${token}`,
    );
    const cred = creds.find((c) => c.referent === cred_id_stored) || {};

    await this.firebase.sendMessageToWallet(wallet.id, {
      type: 'CREDENTIAL_ISSUED',
      data: cred,
    });

    await this.pubSub.publish(CREDENTIAL_ISSUED, {
      [CREDENTIAL_ISSUED]: {
        walletId: wallet.id,
        ...cred,
      },
    });
  }

  async handlePresentProof(walletName: string, body: V20PresExRecord) {
    const { state } = body;

    const { connection_id, pres_ex_id } = body;
    if (!connection_id || !pres_ex_id) {
      return;
    }

    if (state === 'request-received') {
      const proofExchange = await this.prisma.proofExchange.findFirst({
        where: {
          proverConnectionId: connection_id,
          state: ProofExchangeState.REQUEST_SENT,
        },
      });
      if (!proofExchange) {
        return;
      }

      return await this.proofExchangeService.handlePresentRequestReceived(
        proofExchange,
        body,
      );
    }

    if (state !== 'done' && state !== 'abandoned') {
      return;
    }

    const proofExchange = await this.prisma.proofExchange.findFirst({
      where: {
        verifierConnectionId: connection_id,
        state: ProofExchangeState.PRESENTATION_SENT,
      },
    });
    if (!proofExchange) {
      return;
    }

    const rawProof = await this.agentAdmin.presentProof20.recordsDetail(
      pres_ex_id,
      {
        headers: {
          ...authorizationHeader(
            await this.accessTokenService.tokenFromWalletId(
              proofExchange.verifierWalletId,
            ),
          ),
        },
      },
    );
    const proof = this.proofExchangeService.transformProof(rawProof.data);
    const { proverWalletId } = proofExchange;
    if (!proverWalletId) {
      this.logger.error('Prover wallet id is not set');
      return;
    }
    await this.firebase.sendMessageToWallet(proverWalletId, {
      type: 'PROOF_SENT',
      data: proof,
    });
    await this.pubSub.publish(PROOF_SENT, {
      [PROOF_SENT]: {
        ...proof,
        proverWalletId: proverWalletId,
      },
    });

    await this.prisma.proofExchange.update({
      where: {
        id: proofExchange.id,
      },
      data: {
        attributes: proof.attributes as any,
      },
    });

    await this.proofExchangeService.verify(proofExchange.id);
  }
}
