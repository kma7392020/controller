import { Body, Controller, Logger, Param, Post } from '@nestjs/common';
import { AllowUnauthenticated } from '../auth/decorator/allow-unauthenticated.decorator';
import {
  ConnRecord,
  V20PresExRecord,
} from '../../@generated/agent-admin-api/type';
import { WalletWebhookService } from './wallet-webhook.service';
import { doNothing } from '../../common/util/lint.util';

const WALLET_NAME = 'walletName';

@Controller(`/webhook/wallet/:${WALLET_NAME}/topic`)
export class WalletWebhookController {
  private readonly logger = new Logger(WalletWebhookController.name);

  constructor(private readonly service: WalletWebhookService) {}

  @Post('connections')
  @AllowUnauthenticated()
  async connections(
    @Body() body: ConnRecord,
    @Param(WALLET_NAME) walletName: string,
  ) {
    try {
      await this.service.handleConnectionRequest(walletName, body);
    } catch (e) {
      this.logger.error('connections webhook error');
      console.log(e);
    }
  }

  @Post('issue_credential_v2_0')
  @AllowUnauthenticated()
  async issueCredentialV2(
    @Body() body: any,
    @Param(WALLET_NAME) walletName: string,
  ) {
    // this.logger.log('issue_credential_v2_0');
    // console.log(walletName);
    // console.log(body);
    doNothing(body, walletName);
  }

  @Post('issue_credential_v2_0_indy')
  @AllowUnauthenticated()
  async issueCredentialV2Store(
    @Body() body: any,
    @Param(WALLET_NAME) walletName: string,
  ) {
    try {
      await this.service.handleCredentialStored(walletName, body);
    } catch (e) {
      this.logger.error('issue_credential_v2_0_indy webhook error');
      console.log(e);
    }
  }

  @Post('credential')
  @AllowUnauthenticated()
  async credential(@Body() body: any, @Param(WALLET_NAME) walletName: string) {
    doNothing(body, walletName);
  }

  @Post('present_proof_v2_0')
  @AllowUnauthenticated()
  async presentProofV2(
    @Body() body: V20PresExRecord,
    @Param(WALLET_NAME) walletName: string,
  ) {
    this.logger.log('present_proof_v2_0', body);
    try {
      await this.service.handlePresentProof(walletName, body);
    } catch (e) {
      this.logger.error('present_proof_v2_0 webhook error');
      console.log(e);
    }
  }
}
