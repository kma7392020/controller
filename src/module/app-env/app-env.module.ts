import { Global, Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { appEnvFactory } from './app-env.factory';

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [appEnvFactory],
      isGlobal: true,
    }),
  ],
})
@Global()
export class AppEnvModule {}
