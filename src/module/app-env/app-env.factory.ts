import { ConfigService, ConfigType, registerAs } from '@nestjs/config';
import { Inject } from '@nestjs/common';
import { cleanEnv } from 'envalid';
import { appEnvSchema } from './app-env.schema';

const TOKEN = 'app';

export const appEnvFactory = registerAs(TOKEN, () => {
  const { isDev, isProd, isTest, ...envs } = cleanEnv(
    process.env,
    appEnvSchema,
  );

  return {
    ...envs,
    isDev,
    isProd,
    isTest,
  };
});

export type AppEnvType = Omit<
  ConfigType<typeof appEnvFactory>,
  'isDevelopment' | 'isProduction'
>;
export const InjectAppEnv = () => Inject(appEnvFactory.KEY);
export const getAppEnvFrom = (configService: ConfigService): AppEnvType =>
  configService.get(TOKEN) as any;
