import { num, str, url } from 'envalid';

export const appEnvSchema = {
  NODE_ENV: str({
    choices: ['development', 'production'],
    default: 'development',
  }),
  SERVER_PORT: num(),
  DATABASE_URL: url(),
  JWT_SECRET: str(),
  AGENT_ADMIN_URL: url(),
  AGENT_LEDGER_URL: url(),
  BASE_WEBHOOK_URL: url(),
  DEEPLINK_URL: str(),
};
