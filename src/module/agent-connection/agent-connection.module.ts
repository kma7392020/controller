import { Module } from '@nestjs/common';
import { AgentConnectionService } from './agent-connection.service';
import { AgentAdminModule } from '../agent-admin/agent-admin.module';
import { AgentConnectionResolver } from './agent-connection.resolver';
import { AccessTokenModule } from '../access-token/access-token.module';

@Module({
  imports: [AgentAdminModule, AccessTokenModule],
  providers: [AgentConnectionService, AgentConnectionResolver],
  exports: [AgentConnectionService],
})
export class AgentConnectionModule {}
