import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class ConnectionObject {
  @Field({
    nullable: true,
  })
  alias?: string;

  @Field({
    nullable: true,
  })
  connection_id?: string;

  @Field({
    nullable: true,
  })
  connection_protocol?: string;

  @Field({
    nullable: true,
  })
  created_at?: string;
  @Field({
    nullable: true,
  })
  my_did: string;

  @Field({
    nullable: true,
  })
  state: string;

  @Field({
    nullable: true,
  })
  their_did: string;

  @Field({
    nullable: true,
  })
  their_label: string;

  @Field({
    nullable: true,
  })
  their_public_did: string;

  @Field({
    nullable: true,
  })
  updated_at: string;
}
