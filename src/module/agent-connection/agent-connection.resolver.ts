import { Query, Resolver } from '@nestjs/graphql';
import { AgentConnectionService } from './agent-connection.service';
import { ConnectionObject } from './model/connection.object';
import { AuthorizationHeader } from '../auth/decorator/authorization-header.decorator';

@Resolver()
export class AgentConnectionResolver {
  constructor(private readonly service: AgentConnectionService) {}

  @Query(() => [ConnectionObject])
  async connections(@AuthorizationHeader() authorization: string) {
    return this.service.getConnections(authorization);
  }
}
