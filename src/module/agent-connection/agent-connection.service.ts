import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { AgentAdminService } from '../agent-admin/agent-admin.service';
import { PrismaService } from 'nestjs-prisma';
import { AccessTokenService } from '../access-token/access-token.service';

@Injectable()
export class AgentConnectionService {
  constructor(
    private readonly agentAdmin: AgentAdminService,
    private readonly prisma: PrismaService,
    private readonly accessTokenService: AccessTokenService,
  ) {}

  async getActiveConnection(senderWalletId: string, receiverWalletId: string) {
    const [senderToken, receiverToken] = await Promise.all([
      this.accessTokenService.tokenFromWalletId(senderWalletId),
      this.accessTokenService.tokenFromWalletId(receiverWalletId),
    ]);

    const [senderConnections, receiverConnections] = await Promise.all([
      this.getConnections(`Bearer ${senderToken}`),
      this.getConnections(`Bearer ${receiverToken}`),
    ]);

    return senderConnections.find((senderConnection) => {
      return (
        senderConnection.state === 'active' &&
        receiverConnections.some(
          (receiverConnection) =>
            senderConnection.invitation_key ===
            receiverConnection.invitation_key,
        )
      );
    });
  }

  async getConnections(authorization: string) {
    const {
      data: { results },
    } = await this.agentAdmin.connections.connectionsList(
      {},
      {
        headers: {
          authorization,
        },
      },
    );
    if (!results) {
      throw new InternalServerErrorException('Failed to get connections');
    }

    return results;
  }

  async getPublicDid(authorization: string) {
    const {
      data: { result },
    } = await this.agentAdmin.wallet.didPublicList({
      headers: {
        authorization,
      },
    });
    const { did } = result || {};
    if (!did) {
      throw new InternalServerErrorException('Failed to get public did');
    }

    return did;
  }
}
