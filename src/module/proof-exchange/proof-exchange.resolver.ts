import { Args, Mutation, Query, Resolver, Subscription } from '@nestjs/graphql';
import { ProofExchangeService } from './proof-exchange.service';
import { PresentationInput } from './model/presentation.input';
import { ProofExchange } from '../../@generated/type';
import { AllowUnauthenticated } from '../auth/decorator/allow-unauthenticated.decorator';
import { PubSubService } from '../pub-sub/pub-sub.service';
import { ProofExchangeReqReceivedEventObject } from './model/proof-exchange-req-received-event.object';
import {
  PROOF_EXCHANGE_REQ_RECEIVED,
  PROOF_SENT,
} from '../pub-sub/pub-sub.trigger';
import { walletIdFromGqlContext } from '../auth/auth.util';
import { ProofObject } from './model/proof.object';
import { ProofExchangeInput } from './model/proof-exchange.input';
import { ProofExchangeWithDeeplinkObject } from './model/proof-exchange-with-deeplink.object';

@Resolver()
export class ProofExchangeResolver {
  constructor(
    private readonly service: ProofExchangeService,
    private readonly pubSub: PubSubService,
  ) {}

  @Mutation(() => Boolean)
  async sendPresentation(@Args('data') data: PresentationInput) {
    await this.service.sendPresentation(data);

    return true;
  }

  @Subscription(() => ProofExchangeReqReceivedEventObject, {
    name: PROOF_EXCHANGE_REQ_RECEIVED,
    filter: (payload, variables, context) =>
      payload[PROOF_EXCHANGE_REQ_RECEIVED].proverWalletId ===
      walletIdFromGqlContext(context),
  })
  async proofExchangeReqReceived() {
    return this.pubSub.asyncIterator(PROOF_EXCHANGE_REQ_RECEIVED);
  }

  @Subscription(() => ProofObject, {
    name: PROOF_SENT,
    filter: (payload, variables, context) => {
      return (
        payload[PROOF_SENT].proverWalletId === walletIdFromGqlContext(context)
      );
    },
  })
  async proofSent() {
    return this.pubSub.asyncIterator(PROOF_SENT);
  }

  @Mutation(() => ProofExchangeWithDeeplinkObject)
  @AllowUnauthenticated()
  async createProofExchange(@Args('data') data: ProofExchangeInput) {
    return this.service.createProofExchangeWithDeeplink(data);
  }

  @Query(() => ProofExchange)
  @AllowUnauthenticated()
  async proofExchange(@Args('id') id: string) {
    return this.service.findOne(id);
  }
}
