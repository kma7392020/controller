import { Field, InputType } from '@nestjs/graphql';
import { AttributeRestrictionInput } from './attribute-restriction.input';

@InputType()
export class ProofAttributeInput {
  @Field()
  name: string;

  @Field(() => [AttributeRestrictionInput])
  restrictions: AttributeRestrictionInput[];
}
