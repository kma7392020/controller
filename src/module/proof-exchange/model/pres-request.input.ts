import { Field, InputType } from '@nestjs/graphql';
import { ProofReqAttrSpecInput } from './proof-req-attr-spec.input';
import { GraphQLJSON } from 'graphql-type-json';
import { IndyProofReqPredSpec } from '../../../@generated/agent-admin-api/type';

@InputType()
export class PresRequestInput {
  @Field(() => [ProofReqAttrSpecInput])
  attributes: ProofReqAttrSpecInput[];

  @Field(() => [GraphQLJSON])
  predicates: IndyProofReqPredSpec[];
}
