import { Field, InputType } from '@nestjs/graphql';
import { PresentationAttributeSpecInput } from './presentation-attribute-spec.input';

@InputType()
export class PresentationInput {
  @Field()
  proofExchangeId: string;

  @Field(() => [PresentationAttributeSpecInput])
  attributes: PresentationAttributeSpecInput[];
}
