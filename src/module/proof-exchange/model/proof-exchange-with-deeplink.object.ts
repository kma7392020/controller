import { Field, ObjectType } from '@nestjs/graphql';
import { ProofExchange } from '../../../@generated/type';

@ObjectType()
export class ProofExchangeWithDeeplinkObject extends ProofExchange {
  @Field()
  deeplink: string;
}
