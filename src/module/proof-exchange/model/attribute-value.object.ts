import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class AttributeValueObject {
  @Field()
  name: string;

  @Field()
  value: string;
}
