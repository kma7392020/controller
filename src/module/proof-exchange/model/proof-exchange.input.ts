import { Field, InputType, PickType } from '@nestjs/graphql';
import { PresRequestInput } from './pres-request.input';
import { ProofExchangeCreateInput } from '../../../@generated/type';

@InputType()
export class ProofExchangeInput extends PickType(ProofExchangeCreateInput, [
  'verifierWalletId',
  'webhookUrl',
  'deviceId',
] as const) {
  @Field(() => PresRequestInput)
  presRequest: PresRequestInput;
}
