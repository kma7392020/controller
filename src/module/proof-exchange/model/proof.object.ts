import { Field, ObjectType } from '@nestjs/graphql';
import { GraphQLJSON } from 'graphql-type-json';
import { AttributeValueObject } from './attribute-value.object';
import { V20PresExRecord } from '../../../@generated/agent-admin-api/type';

@ObjectType()
export class ProofObject {
  @Field({
    nullable: true,
  })
  state?: string;

  @Field(() => [AttributeValueObject])
  attributes: AttributeValueObject[];

  @Field(() => GraphQLJSON)
  raw: V20PresExRecord;
}
