import { Field, InputType } from '@nestjs/graphql';
import { AttributeRestrictionInput } from './attribute-restriction.input';

@InputType()
export class ProofReqAttrSpecInput {
  @Field()
  name: string;

  @Field(() => [AttributeRestrictionInput], {
    nullable: true,
  })
  restrictions?: AttributeRestrictionInput[];
}
