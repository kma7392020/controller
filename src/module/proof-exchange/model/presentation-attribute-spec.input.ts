import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class PresentationAttributeSpecInput {
  @Field()
  credId: string;

  @Field()
  ruleId: string;
}
