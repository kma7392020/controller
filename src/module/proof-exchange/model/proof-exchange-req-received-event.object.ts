import { Field, ObjectType } from '@nestjs/graphql';
import { IndyCredentialObject } from '../../credential-exchange/model/indy-credential.object';

@ObjectType()
export class ProofCredAttributeObject {
  @Field()
  ruleId: string;

  @Field()
  name: string;

  @Field(() => [IndyCredentialObject])
  credentials: IndyCredentialObject[];
}

@ObjectType()
export class ProofExchangeReqReceivedEventObject {
  @Field({
    nullable: true,
  })
  proofExchangeId: string;

  @Field(() => [ProofCredAttributeObject])
  attributes: ProofCredAttributeObject[];
}
