import { Module } from '@nestjs/common';
import { ProofExchangeService } from './proof-exchange.service';
import { AgentAdminModule } from '../agent-admin/agent-admin.module';
import { DeeplinkModule } from '../deeplink/deeplink.module';
import { AgentConnectionModule } from '../agent-connection/agent-connection.module';
import { SchemaModule } from '../schema/schema.module';
import { ProofExchangeResolver } from './proof-exchange.resolver';
import { AccessTokenModule } from '../access-token/access-token.module';
import { FirebaseModule } from '../firebase/firebase.module';

@Module({
  imports: [
    AgentAdminModule,
    DeeplinkModule,
    AgentConnectionModule,
    SchemaModule,
    AccessTokenModule,
    FirebaseModule,
  ],
  providers: [ProofExchangeService, ProofExchangeResolver],
  exports: [ProofExchangeService],
})
export class ProofExchangeModule {}
