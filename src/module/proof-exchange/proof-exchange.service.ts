import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { AgentAdminService } from '../agent-admin/agent-admin.service';
import { PrismaService } from 'nestjs-prisma';
import { DeeplinkService } from '../deeplink/deeplink.service';
import { AgentConnectionService } from '../agent-connection/agent-connection.service';
import { SchemaService } from '../schema/schema.service';
import { ProofExchangeRequest } from './type/proof-exchange.request';
import {
  ConnRecord,
  IndyPresSpec,
  IndyProof,
  IndyProofReqAttrSpec,
  V20PresExRecord,
  V20PresRequestByFormat,
} from '../../@generated/agent-admin-api/type';
import { v4 } from 'uuid';
import { ProofExchangeRecord } from './type/proof-exchange.record';
import { authorizationHeader } from '../../common/util/http-header.util';
import { ProofObject } from './model/proof.object';
import { AttributeValueObject } from './model/attribute-value.object';
import { AppEnvType, InjectAppEnv } from '../app-env/app-env.factory';
import { AccessTokenService } from '../access-token/access-token.service';
import { PresentationInput } from './model/presentation.input';
import { ProofExchange, ProofExchangeState } from '../../@generated/type';
import {
  ProofCredAttributeObject,
  ProofExchangeReqReceivedEventObject,
} from './model/proof-exchange-req-received-event.object';
import slugify from 'slugify';
import { FirebaseService } from '../firebase/firebase.service';
import { ProofExchangeInput } from './model/proof-exchange.input';
import axios from 'axios';
import { DeeplinkProofPayloadObject } from '../deeplink/model/deeplink-proof-payload.object';
import { DeeplinkTypeEnum } from '../deeplink/model/deeplink-type.enum';
import { ProofExchangeWithDeeplinkObject } from './model/proof-exchange-with-deeplink.object';
import { PubSubService } from '../pub-sub/pub-sub.service';
import { PROOF_EXCHANGE_REQ_RECEIVED } from '../pub-sub/pub-sub.trigger';

@Injectable()
export class ProofExchangeService {
  constructor(
    private readonly agentAdmin: AgentAdminService,
    private readonly prisma: PrismaService,
    private readonly deepLinkService: DeeplinkService,
    private readonly agentConnectionService: AgentConnectionService,
    private readonly schemaService: SchemaService,
    @InjectAppEnv() private readonly appEnv: AppEnvType,
    private readonly accessTokenService: AccessTokenService,
    private readonly firebase: FirebaseService,
    private readonly pubSub: PubSubService,
  ) {}

  async verify(id: string) {
    const { webhookUrl, attributes, deviceId } =
      await this.prisma.proofExchange.findUniqueOrThrow({
        where: {
          id,
        },
      });

    try {
      await axios.post(
        webhookUrl,
        {
          id,
          attributes,
          deviceId,
        },
        {
          headers: {
            'Content-Type': 'application/json',
          },
        },
      );

      await this.prisma.proofExchange.update({
        where: {
          id,
        },
        data: {
          state: ProofExchangeState.VERIFIED,
        },
      });
    } catch (e) {
      await this.prisma.proofExchange.update({
        where: {
          id,
        },
        data: {
          state: ProofExchangeState.REJECTED,
        },
      });
    }
  }

  async handlePresentRequestReceived(
    proofExchange: ProofExchange,
    presEx: V20PresExRecord,
  ) {
    const { pres_ex_id } = presEx;
    if (!pres_ex_id) {
      throw new InternalServerErrorException('Proof Exchange not found');
    }

    const { proverWalletId } = proofExchange;
    if (!proverWalletId) {
      throw new InternalServerErrorException('Prover wallet not found');
    }
    const proverToken = await this.accessTokenService.tokenFromWalletId(
      proverWalletId,
    );
    const { data = [] } =
      await this.agentAdmin.presentProof20.recordsCredentialsDetail(
        pres_ex_id,
        {},
        {
          headers: {
            ...authorizationHeader(proverToken),
          },
        },
      );
    const {
      data: { by_format },
    } = await this.agentAdmin.presentProof20.recordsDetail(pres_ex_id, {
      headers: {
        ...authorizationHeader(proverToken),
      },
    });

    const { pres_request } = by_format || {};

    const attributesHash = {
      ...((pres_request as any)?.indy?.requested_attributes || {}),
      ...((pres_request as any)?.indy?.requested_predicates || {}),
    };
    const requestedAttributes = Object.keys(attributesHash).map((key) => ({
      ruleId: key,
      ...attributesHash[key],
    }));

    const presentationAttributes: ProofCredAttributeObject[] =
      requestedAttributes.map((attr) => ({
        name: attr.name,
        ruleId: attr.ruleId,
        credentials: data
          .filter((presCred) =>
            presCred?.presentation_referents?.includes(attr.ruleId),
          )
          .map(
            (presCred) =>
              ({
                ...presCred?.cred_info,
              } || {}),
          ),
      }));

    const subscriptionPayload: ProofExchangeReqReceivedEventObject = {
      proofExchangeId: proofExchange.id,
      attributes: presentationAttributes,
    };

    await this.prisma.proofExchange.update({
      where: {
        id: proofExchange.id,
      },
      data: {
        proverPresExId: pres_ex_id,
        state: ProofExchangeState.REQUEST_RECEIVED,
        presentationAttributes: presentationAttributes as any,
      },
    });

    await this.firebase.sendMessageToWallet(proverWalletId, {
      type: 'PROOF_EXCHANGE_REQ_RECEIVED',
      data: subscriptionPayload,
    });
    await this.pubSub.publish(PROOF_EXCHANGE_REQ_RECEIVED, {
      [PROOF_EXCHANGE_REQ_RECEIVED]: {
        proverWalletId,
        ...subscriptionPayload,
      },
    });
  }

  async sendPresentation({ attributes, proofExchangeId }: PresentationInput) {
    const { proverPresExId, proverWalletId } =
      await this.prisma.proofExchange.findUniqueOrThrow({
        where: {
          id: proofExchangeId,
        },
      });
    if (!proverPresExId || !proverWalletId) {
      throw new InternalServerErrorException('Proof Exchange not found');
    }

    const proverToken = await this.accessTokenService.tokenFromWalletId(
      proverWalletId,
    );

    const {
      data: { by_format },
    } = await this.agentAdmin.presentProof20.recordsDetail(proverPresExId, {
      headers: {
        ...authorizationHeader(proverToken),
      },
    });

    const { pres_request } = by_format || {};

    const attributesHash =
      (pres_request as any)?.indy?.requested_attributes || {};
    const predicatesHash =
      (pres_request as any)?.indy?.requested_predicates || {};

    const requestedAttributes = Object.keys(attributesHash).map((key) => ({
      ruleId: key,
      ...attributesHash[key],
    }));
    const requestedPredicates = Object.keys(predicatesHash).map((key) => ({
      ruleId: key,
      ...predicatesHash[key],
    }));

    const sendAttributes = attributes
      .map((attr) => {
        const reqAttr = requestedAttributes.find(
          (reqAttr) => reqAttr.ruleId === attr.ruleId,
        );
        if (!reqAttr) {
          return null;
        }

        return {
          ruleId: reqAttr.ruleId,
          credId: attr.credId,
        };
      })
      .filter(Boolean) as any[];

    const sendPredicates = attributes
      .map((attr) => {
        const reqPred = requestedPredicates.find(
          (reqAttr) => reqAttr.ruleId === attr.ruleId,
        );
        if (!reqPred) {
          return null;
        }

        return {
          ruleId: reqPred.ruleId,
          credId: attr.credId,
        };
      })
      .filter(Boolean) as any[];

    await this.agentAdmin.presentProof20.recordsSendPresentationCreate(
      proverPresExId,
      {
        indy: {
          requested_attributes: sendAttributes.reduce(
            (memo, curr) => ({
              ...memo,
              [curr.ruleId]: {
                cred_id: curr.credId,
                revealed: true,
              },
            }),
            {} as IndyPresSpec['requested_attributes'],
          ),
          requested_predicates: sendPredicates.reduce(
            (memo, curr) => ({
              ...memo,
              [curr.ruleId]: {
                cred_id: curr.credId,
                revealed: true,
              },
            }),
            {} as IndyPresSpec['requested_predicates'],
          ),
          self_attested_attributes: {},
        },
      },
      {
        headers: {
          ...authorizationHeader(proverToken),
        },
      },
    );

    await this.prisma.proofExchange.update({
      where: {
        id: proofExchangeId,
      },
      data: {
        state: ProofExchangeState.PRESENTATION_SENT,
      },
    });
  }

  async sendProofRequest(proofExchangeId: string) {
    const { verifierWalletId, verifierConnectionId, presRequest } =
      (await this.prisma.proofExchange.findUniqueOrThrow({
        where: {
          id: proofExchangeId,
        },
      })) as unknown as ProofExchangeRecord;
    if (!verifierConnectionId) {
      throw new InternalServerErrorException('Connection not established');
    }

    const verifierToken = await this.accessTokenService.tokenFromWalletId(
      verifierWalletId,
    );

    if (!presRequest) {
      throw new InternalServerErrorException('Proof Request not found');
    }
    const {
      data: { pres_ex_id },
    } = await this.agentAdmin.presentProof20.sendRequestCreate(
      {
        connection_id: verifierConnectionId,
        presentation_request: {
          indy: {
            ...presRequest,
            version: '1.0',
          },
        },
        auto_verify: true,
      },
      {
        headers: {
          ...authorizationHeader(verifierToken),
        },
      },
    );

    if (!pres_ex_id) {
      throw new InternalServerErrorException('Proof Exchange ID not found');
    }

    await this.prisma.proofExchange.update({
      where: {
        id: proofExchangeId,
      },
      data: {
        verifierPresExId: pres_ex_id,
        state: ProofExchangeState.REQUEST_SENT,
      },
    });
  }

  async handleAcceptProofRequest(
    holderAuthorization: string,
    holderWalletId: string,
    proofExchangeId: string,
  ) {
    const { verifierWalletId } =
      (await this.prisma.proofExchange.findUniqueOrThrow({
        where: {
          id: proofExchangeId,
        },
      })) as unknown as ProofExchangeRecord;

    await this.setupConnection(
      verifierWalletId,
      holderAuthorization,
      holderWalletId,
      proofExchangeId,
    );
  }

  async setupConnection(
    verifierWalletId: string,
    holderAuthorization: string,
    holderWalletId: string,
    credentialExchangeId: string,
  ) {
    const activeConnection =
      await this.agentConnectionService.getActiveConnection(
        verifierWalletId,
        holderWalletId,
      );

    if (activeConnection) {
      return await this.setupActiveConnection(
        activeConnection,
        verifierWalletId,
        holderWalletId,
        credentialExchangeId,
      );
    }

    return await this.setupNewConnection(
      verifierWalletId,
      holderAuthorization,
      holderWalletId,
      credentialExchangeId,
    );
  }

  async cleanOldProofExchangeRecords(proverConnectionId: string) {
    await this.prisma.proofExchange.deleteMany({
      where: {
        proverConnectionId: proverConnectionId,
        state: {
          not: 'VERIFIED',
        },
      },
    });
  }

  async setupActiveConnection(
    verifierActiveConnection: ConnRecord,
    verifierWalletId: string,
    holderWalletId: string,
    credentialExchangeId: string,
  ) {
    const proverActiveConnection =
      await this.agentConnectionService.getActiveConnection(
        holderWalletId,
        verifierWalletId,
      );
    if (!proverActiveConnection?.connection_id) {
      throw new InternalServerErrorException(
        'Prover connection not established',
      );
    }
    await this.cleanOldProofExchangeRecords(
      proverActiveConnection.connection_id,
    );

    await this.prisma.proofExchange.update({
      where: {
        id: credentialExchangeId,
      },
      data: {
        verifierConnectionId: verifierActiveConnection.connection_id,
        proverWalletId: holderWalletId,
        proverConnectionId: proverActiveConnection.connection_id,
      },
    });

    await this.sendProofRequest(credentialExchangeId);
  }

  async setupNewConnection(
    verifierWalletId: string,
    holderAuthorization: string,
    holderWalletId: string,
    credentialExchangeId: string,
  ) {
    const verifierToken = await this.accessTokenService.tokenFromWalletId(
      verifierWalletId,
    );

    const {
      data: { invitation, connection_id },
    } = await this.agentAdmin.connections.createInvitationCreate(
      {},
      {},
      {
        headers: {
          ...authorizationHeader(verifierToken),
        },
      },
    );
    if (!invitation) {
      throw new InternalServerErrorException('Failed to create invitation');
    }

    await this.prisma.proofExchange.update({
      where: {
        id: credentialExchangeId,
      },
      data: {
        verifierConnectionId: connection_id,
        proverWalletId: holderWalletId,
      },
    });

    const { data } = await this.agentAdmin.connections.receiveInvitationCreate(
      invitation,
      {},
      {
        headers: {
          authorization: holderAuthorization,
        },
      },
    );
    if (!data?.connection_id) {
      throw new InternalServerErrorException('Failed to receive invitation');
    }

    await this.cleanOldProofExchangeRecords(data.connection_id);

    await this.prisma.proofExchange.update({
      where: {
        id: credentialExchangeId,
      },
      data: {
        proverConnectionId: data.connection_id,
      },
    });

    return data;
  }

  async createProofExchangeWithDeeplink(
    data: ProofExchangeInput,
  ): Promise<ProofExchangeWithDeeplinkObject> {
    const { verifierWalletId, webhookUrl, presRequest, deviceId } = data;
    const { attributes: requestedAttributes, predicates: requestedPredicates } =
      presRequest;
    const name = v4();

    const createData: ProofExchangeRequest = {
      name,
      verifierWalletId: verifierWalletId,
      webhookUrl,
      deviceId,
      presRequest: {
        name,
        requested_attributes: requestedAttributes.reduce<
          Record<string, IndyProofReqAttrSpec>
        >(
          (memo, current) => ({
            ...memo,
            [`${slugify(current.name)}_attr_${v4()}`]: {
              name: current.name,
              restrictions: current?.restrictions?.map(({ credDefId }) => ({
                cred_def_id: credDefId,
              })),
            },
          }),
          {},
        ),
        requested_predicates: requestedPredicates.reduce<
          Record<string, IndyProofReqAttrSpec>
        >(
          (memo, current) => ({
            ...memo,
            [`${current.name}_pred_${v4()}`]: current,
          }),
          {},
        ),
      },
    };
    const proofExchange = await this.prisma.proofExchange.create({
      data: createData as any,
    });

    const deeplinkPayload: DeeplinkProofPayloadObject = {
      id: proofExchange.id,
      type: DeeplinkTypeEnum.PROOF,
    };

    const deeplink = this.deepLinkService.create(deeplinkPayload);

    return {
      ...proofExchange,
      deeplink,
    };
  }

  async allProofs(authorization: string) {
    const {
      data: { results = [] },
    } = await this.agentAdmin.presentProof20.recordsList(
      {},
      {
        headers: {
          authorization,
        },
      },
    );

    return results.map((r) => this.transformProof(r));
  }

  transformProof(raw: V20PresExRecord): ProofObject {
    const { state, by_format: { pres_request = {}, pres = {} } = {} } = raw;

    if (state !== 'done') {
      return {
        raw,
        state,
        attributes: [],
      };
    }

    const { indy: { requested_attributes = {} } = {} } =
      pres_request as V20PresRequestByFormat;

    const requestedAttributesList = Object.entries(requested_attributes).map(
      ([k, v]) => ({
        ...v,
        id: k,
      }),
    );

    const {
      indy: { requested_proof: { revealed_attrs = [] } = {} },
    } = pres as { indy: IndyProof };

    const revealedAttrsList = Object.entries(revealed_attrs).map(([k, v]) => ({
      ...v,
      id: k,
    }));

    const attrs: AttributeValueObject[] = requestedAttributesList
      .map(({ id, name }) => {
        const { raw } = revealedAttrsList.find((rel) => rel.id === id) || {};

        return {
          name: name || '',
          value: raw || '',
        };
      })
      .filter((value) => value.name);

    return {
      raw,
      state,
      attributes: attrs,
    };
  }

  async findOne(id: string) {
    return await this.prisma.proofExchange.findUniqueOrThrow({
      where: {
        id,
      },
    });
  }
}
