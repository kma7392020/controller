import { ProofExchange } from '../../../@generated/type';
import { V20PresRequestByFormat } from '../../../@generated/agent-admin-api/type';

export class ProofExchangeRecord extends ProofExchange {
  presRequest: V20PresRequestByFormat['indy'];
}
