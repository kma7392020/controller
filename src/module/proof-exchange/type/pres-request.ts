import { IndyProofReqPredSpec } from '../../../@generated/agent-admin-api/type';

export type ProofReqAttrRestriction = {
  credDefId: string;
};

export type ProofReqAttr = {
  name: string;
  restrictions: ProofReqAttrRestriction[];
};

export type ProofPresRequest = {
  name: string;
  attributes: ProofReqAttr[];
  predicates: IndyProofReqPredSpec[];
};
