import { Module } from '@nestjs/common';
import { AgentAdminService } from './agent-admin.service';

@Module({
  providers: [AgentAdminService],
  exports: [AgentAdminService],
})
export class AgentAdminModule {}
