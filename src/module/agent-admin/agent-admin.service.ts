import { Injectable } from '@nestjs/common';
import { AppEnvType, InjectAppEnv } from '../app-env/app-env.factory';
import {
  AgentAdminApi,
  ContentType,
} from '../../@generated/agent-admin-api/type';

@Injectable()
export class AgentAdminService extends AgentAdminApi<any> {
  constructor(@InjectAppEnv() appEnv: AppEnvType) {
    const { AGENT_ADMIN_URL } = appEnv;
    super({
      baseUrl: AGENT_ADMIN_URL,
      baseApiParams: {
        type: ContentType.Json,
        headers: {
          'Content-Type': 'application/json',
        },
      },
    });
  }
}
