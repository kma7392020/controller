import { HttpAdapterHost, NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { PrismaClientExceptionFilter, PrismaService } from 'nestjs-prisma';
import { ConfigService } from '@nestjs/config';
import { getAppEnvFrom } from './module/app-env/app-env.factory';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { cors: true });

  // enable shutdown hook
  const prismaService: PrismaService = app.get(PrismaService);
  await prismaService.enableShutdownHooks(app);
  // prismaService.$on('query', (event) => {
  //   console.log(event);
  // });

  // global filters
  const { httpAdapter } = app.get(HttpAdapterHost);
  app.useGlobalFilters(new PrismaClientExceptionFilter(httpAdapter));

  // Listen
  const configService = app.get(ConfigService);
  const { SERVER_PORT } = getAppEnvFrom(configService);
  await app.listen(SERVER_PORT);
}
bootstrap().catch(console.error);
