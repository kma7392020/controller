#!/usr/bin/env sh

pnpm nest g module "$1" --no-spec
pnpm nest g service "$1" --no-spec
